#!/bin/bash

export $(xargs <.env)

. venv/bin/activate

python -m pytest -s $1
