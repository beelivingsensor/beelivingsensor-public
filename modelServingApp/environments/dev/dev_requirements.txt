fastapi[all]==0.68.0
pytest==6.2.4
jsonschema==3.2.0
opencv-python==4.5.1.48
numpy==1.21.1