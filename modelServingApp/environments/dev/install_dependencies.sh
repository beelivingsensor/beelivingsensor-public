#!/bin/bash

sudo apt update
sudo apt upgrade -y
sudo apt install -y libgl1-mesa-glx redis-server

# install python 3.8
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt update
sudo apt install -y python3.8 python3.8-venv python3.8-dev python3-pip

if [ ! -d venv/ ]; then
    python3.8 -m venv venv
fi

. venv/bin/activate

pip install --upgrade pip
pip install wheel
pip install --upgrade setuptools
pip install -r environments/dev/dev_requirements.txt
pip install -r environments/common/requirements.txt
