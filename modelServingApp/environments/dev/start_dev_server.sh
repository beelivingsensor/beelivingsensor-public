#!/bin/bash

export $(xargs <.env)
sh environments/dev/install_dependencies.sh
uvicorn app.main:app --reload --port=8001