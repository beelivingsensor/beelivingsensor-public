# https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html
# https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html

# Pre-installation actions
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#pre-installation-actions
sudo apt-get install -y make gcc pkg-config
sudo apt-get install -y linux-headers-$(uname -r)

lspci | grep -i nvidia
uname -m && cat /etc/*release
gcc --version

# Installation actions
distribution=$(. /etc/os-release;echo $ID$VERSION_ID | sed -e 's/\.//g')
wget https://developer.download.nvidia.com/compute/cuda/repos/$distribution/x86_64/cuda-$distribution.pin
sudo mv cuda-$distribution.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/$distribution/x86_64/7fa2af80.pub
echo "deb http://developer.download.nvidia.com/compute/cuda/repos/$distribution/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda.list
sudo apt-get update
sudo apt-get -y install cuda-drivers

# Post-installation actions
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#post-installation-actions

export PATH=/usr/local/cuda-11.3/bin${PATH:+:${PATH}}


# Verify driver installation
cat /proc/driver/nvidia/version
nvidia-smi