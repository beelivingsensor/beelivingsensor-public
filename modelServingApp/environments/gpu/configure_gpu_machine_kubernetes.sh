# For nodes running on a Kubernetes cluster
# Tested on a Standard_NV6_Promo machine running Ubuntu Server 18.04 LTS deployed in East US
# https://github.com/NVIDIA/k8s-device-plugin


# Install CUDA drivers
sh environments/gpu/install_cuda_drivers.sh

# Install nvidia-docker
sh environments/gpu/install_nvidia_docker.sh

sudo cp environments/gpu/daemon.json /etc/docker/daemon.json

sudo service docker restart

# for debugging: kubectl logs -n kube-system nvidia-device-plugin-daemonset-8bxfb
# kc delete daemonset nvidia-device-plugin-daemonset -n kube-system
# kubectl describe pod nvidia-device-plugin-daemonset-zzjkb -n kube-system