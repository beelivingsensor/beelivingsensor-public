# For nodes NOT running on a Kubernetes cluster
# Tested on a Standard_NV6_Promo machine running Ubuntu Server 18.04 LTS deployed in East US

# Install CUDA drivers
sh environments/gpu/install_cuda_drivers.sh

# Set up Docker
curl https://get.docker.com | sh && sudo systemctl --now enable docker

# Set up Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Install nvidia-docker
sh environments/gpu/install_nvidia_docker.sh

# Setting up NVIDIA Container Toolkit
sudo apt install -y nvidia-cuda-toolkit
