"""Script used by cron to poll for potential next media to be processed."""
import requests

r = requests.post(f'http://0.0.0.0:80/processNextMedia/')

print(f'Status code: {r.status_code}')
