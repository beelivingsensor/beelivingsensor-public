from typing import Dict

from pydantic import BaseModel


class MediaProcessCreate(BaseModel):
    media_pk: int
    media_blob_path: str
    media_type: str
    bee_model_pk: int
    bee_model_blob_path: str
    pollen_model_pk: int
    pollen_model_blob_path: str
    entrance_coordinates: Dict[str, int]
    back_url: str
    skip: int = 1
    profiling: bool = False
