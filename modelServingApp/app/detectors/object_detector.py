from abc import ABC, abstractmethod


class ObjectDetector(ABC):

    @abstractmethod
    def detect(self, image):
        """Get annotations for an image.

        Args:
            image (numpy.ndarray): Raw image.

        Returns:
            app.models.image.Image: An instance of Image.
        """
        pass
