import configparser
import threading
from pathlib import Path
from random import randrange

import cv2
import numpy as np

from app.detectors.object_detector import ObjectDetector
from app.models.annotation import Annotation
from app.models.annotation_set import AnnotationSet
from app.util import get_logger

logger = get_logger(__name__)


class YOLODetector(ObjectDetector):

    def __init__(self, model_pk, weights_path, config_path, annotation_type):
        """Load the model.

        Args:
            model_pk (int): Some identifier for the model.
            weights_path (Path|str): Path to the model weights file.
            config_path (Path|str): Path to the model configuration file.
            annotation_type (str): What object the model detects.
        """
        self.logger = logger
        self.logger.debug(f'Load model {Path(weights_path).stem}...')
        self.net = cv2.dnn.readNetFromDarknet(
            str(config_path), str(weights_path))
        cuda_device_count = cv2.cuda.getCudaEnabledDeviceCount()
        self.logger.info(f'💡 Number of CUDA devices: {cuda_device_count}')
        if cuda_device_count > 0:
            cv2.cuda.setDevice(randrange(cuda_device_count))
            self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
            self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i[0] - 1]
                   for i in self.net.getUnconnectedOutLayers()]
        self.annotation_type = annotation_type
        self.model_pk = model_pk

        config = configparser.ConfigParser(strict=False)
        config.read(config_path)

        self.model_dim = (int(config['net']['width']),
                          int(config['net']['height']))

    def detect(self, image):
        """Get annotations for an image.

        Args:
            image (numpy.ndarray): The raw image.

        Returns:
            app.models.annotation_set.AnnotationSet:
                An instance of AnnotationSet.
        """
        network_output = self.pass_through_network(image)
        detections = self.parse_network_output(network_output, image)
        self.compute_non_max_suppression(detections)
        return self.create_annotation_set(detections)

    def pass_through_network(self, image):
        """Pass image through neural network.

        Args:
            image (numpy.ndarray): The raw image.

        Returns:
            numpy.ndarray: The network's output.
        """
        blob = cv2.dnn.blobFromImage(
            image, 1 / 255.0, self.model_dim, swapRB=True, crop=False)
        self.net.setInput(blob)
        return self.net.forward(self.ln)

    def parse_network_output(self, network_output, image, confidence_threshold=0.5):
        """Parse output of network to get object detections.

        Args:
            network_output (numpy.ndarray): The output of the network.
            image (numpy.ndarray): The raw image.
            confidence_threshold (float): Predictions
                of this confidence to include.
        """
        H, W = image.shape[:2]

        detections = {
            'boxes': [],
            'scores': []
        }
        for output in network_output:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                if confidence > confidence_threshold:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")
                    x_min = int(centerX - (width / 2))
                    x_max = x_min + width
                    y_min = int(centerY - (height / 2))
                    y_max = y_min + height
                    detections['boxes'].append([x_min, y_min, x_max, y_max])
                    detections['scores'].append(float(confidence))
        return detections

    def compute_non_max_suppression(self, detections, overlap_threshold=0.3):
        """Compute non-max suppression over detections.

        Args:
            detections (Dict[str, List]): The detections.
        """
        boxes = np.array(detections['boxes'])
        scores = np.array(detections['scores'])
        pick = []

        if len(boxes) != 0:
            x1 = boxes[:, 0]
            y1 = boxes[:, 1]
            x2 = boxes[:, 2]
            y2 = boxes[:, 3]

            area = (x2 - x1 + 1) * (y2 - y1 + 1)
            idxs = np.argsort(y2)

            while len(idxs) > 0:
                last = len(idxs) - 1
                i = idxs[last]
                pick.append(i)
                suppress = [last]

                for pos in range(0, last):
                    j = idxs[pos]
                    xx1 = max(x1[i], x1[j])
                    yy1 = max(y1[i], y1[j])
                    xx2 = min(x2[i], x2[j])
                    yy2 = min(y2[i], y2[j])

                    w = max(0, xx2 - xx1 + 1)
                    h = max(0, yy2 - yy1 + 1)

                    overlap = float(w * h) / area[j]

                    if overlap > overlap_threshold:
                        suppress.append(pos)

                idxs = np.delete(idxs, suppress)

        detections['boxes'] = boxes[pick].tolist()
        detections['scores'] = scores[pick].tolist()

    def create_annotation_set(self, detections):
        """Create annotation set out of detections.

        Args:
            detections (Dict[str, List]): The detections.
        """
        annotation_set = AnnotationSet(
            model_pk=self.model_pk,
            annotation_type=self.annotation_type
        )
        for box, score in zip(detections['boxes'], detections['scores']):
            x_min, y_min, x_max, y_max = box
            annotation = Annotation(
                x_min=x_min,
                y_min=y_min,
                x_max=x_max,
                y_max=y_max,
                vector={'x1': x_min, 'y1': y_min, 'y2': y_max, 'x2': x_max},
                confidence=score
            )
            annotation_set.annotations.append(annotation)

        return annotation_set
