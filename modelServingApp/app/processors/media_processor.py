import os
import traceback
from abc import abstractmethod, ABC
import concurrent.futures
from pathlib import Path

import cv2
import numpy as np

from app.cloud_storage.azure_helper import AzureHelper
from app.detectors.yolo_detector import YOLODetector
from app.models.annotation import Annotation
from app.models.annotation_set import AnnotationSet
from app.models.annotation_type import AnnotationType
from app.models.image import Image
from app.util import get_timestamp, get_logger, get_annotation_set

logger = get_logger(__name__)


class MediaProcessor(ABC):
    """Analyzer for media to obtain image annotations and bee traffic data.

    Developer note: variables named 'frame' refer to cv2 instances while
    variables named 'image' refer to the model instances.
    Variables prefixed with 'h' and 'b' stand for 'hive' and 'bee' images,
    respectively.
    """
    models_dir = Path('models')
    media_dir = Path(os.environ.get('MODEL_SERVING_MEDIA_PATH'))
    images_dir_name = 'images'
    images_vis_dir = 'visualized'
    annotations_container = os.getenv(
        'MODEL_ANNOTATIONS_CONTAINER', default='model-annotations')

    def __init__(self, delete_data=True, visualized=False):
        """Initialize the media processor.

        Args:
            delete_data (bool): Whether the data (downloaded or created files)
                should be deleted after processing.
            visualized (bool): This also creates images with
                annotations drawn on them. `delete_data` must be enabled.
        """
        self.delete_data = delete_data
        self.visualized = visualized
        self.models_dir.mkdir(exist_ok=True)
        self.media_dir.mkdir(exist_ok=True)
        self.azure_helper = AzureHelper()
        self.bee_detector = None
        self.pollen_detector = None
        self.logger = logger

        self.image_save_executor = concurrent.futures.ThreadPoolExecutor()
        self.image_save_futures = {}

    @abstractmethod
    def process(self, media_pk, media_blob_path, entrance_coordinates):
        pass

    def _create_unique_folder(self, pk):
        """Creates a unique folder locally.

        Name is created by using a timestamp plus some identifier (pk).

        Args:
            pk (str|int): Identifier to be used for the folder's name.

        Returns:
            Path: The path of the folder.
        """
        time_stamp = get_timestamp()
        unique_dir_name = f'{time_stamp}_{pk}'
        unique_dir_path = self.media_dir / unique_dir_name
        unique_dir_path.mkdir()
        return unique_dir_path

    def load_bee_model(self, model_pk, model_blob_path):
        """Load the bee model.

        Initializes a YoloDetector using the bee model weights and
        configuration from Azure.

        Args:
            model_pk (int): Identifier for the bee model.
            model_blob_path (str): The path of the bee model weights on Azure.
        """
        weights_path = self._get_model(model_blob_path)
        cfg_path = self._get_model('public/models/bee/bee.cfg')
        self.bee_detector = YOLODetector(
            model_pk, weights_path, cfg_path, 'bee')

    def load_pollen_model(self, model_pk, model_blob_path):
        """Load the pollen model.

        Initializes a YoloDetector using the pollen model weights
        and configuration from Azure. The configuration files are
        assumed to have the same name as the weights file with '.cfg'
        extension.

        Args:
            model_pk (int): Identifier for the pollen model.
            model_blob_path (str): The path of the pollen
                model weights on Azure.
        """
        weights_path = self._get_model(model_blob_path)
        cfg_path = self._get_model(Path(model_blob_path).with_suffix('.cfg'))
        self.pollen_detector = YOLODetector(
            model_pk, weights_path, cfg_path, 'pollen')

    def _get_model(self, model_path_on_azure):
        """Get model artifact.

        Downloads the model artifact (weights or configuration) from Azure.

        Args:
            model_path_on_azure (Path|str): The path to the model file on Azure

        Example:
            >>> self._get_model('public/models/bee/bee_medium.weights')

        Returns:
            Path: Path to the model artifact.
        """
        model_path = self.models_dir / model_path_on_azure
        self.azure_helper.download(model_path_on_azure, model_path, temp=True)
        return model_path

    def get_annotations_path(self, pk, _type):
        """Get a unique annotations path for cloud storage.

        Args:
            media (app.models.media.Media): The media.
            pk (int): Media identifier.
            _type (str): image or video.
        """
        return f'{self.annotations_container}/{_type}_{pk}_{get_timestamp()}.json'

    def get_image(self, image, pk, path, local_path, type):
        """Create an image instance.

        Args:
            image (numpy.ndarray): The raw image.
            pk (int): The ID of the image.
            path (str): The path on Azure.
            local_path (str): The local path.
            type (str): The type of image (hive|bee).

        Returns:
            app.model.image.Image: The image instance.
        """
        self.logger.debug(f'Create image instance for {local_path}')
        height, width, channels = image.shape
        return Image(
            pk=pk,
            path=path,
            local_path=local_path,
            annotations_path=self.get_annotations_path(pk, 'image'),
            type=type,
            width=width,
            height=height,
            channels=channels,
            raw=image
        )

    def process_bee_images(self, himage):
        """Process bee images within hive images.

        - Crop bees out of hive image
        - Run bee image over pollen detection model
        - Extract color for pollen
        - Upload bee image to Azure

        Args:
            himage (app.models.image.Image): The image instance.
        """
        self.logger.debug(f'Processing bee images of {himage.local_path}')
        images = []
        himage_name = Path(himage.local_path).with_suffix('').name

        bimages_dir_path = Path(himage.local_path).parent / 'bimages'
        bimages_dir_path.mkdir(exist_ok=True)

        if self.visualized:
            bimages_vis_dir_path = bimages_dir_path / 'visualized'
            bimages_vis_dir_path.mkdir(exist_ok=True)

        # crop bees out of hive images
        bee_annotation_set = get_annotation_set(himage, AnnotationType.BEE)
        bee_annotations = bee_annotation_set.annotations

        for bimg_index, annotation in enumerate(bee_annotations, start=1):

            bimage_name = f'{himage_name}_bimage_{bimg_index}_{get_timestamp()}.jpg'
            bimage_path = bimages_dir_path / bimage_name
            bimage_blob_path = f'frames/modelservingapp/{bimage_name}.jpg'

            # Get raw bee image
            bframe = self.crop_image(himage.raw, annotation, margin=20)

            # Get image instance
            bimage = self.get_image(
                bframe,
                bimg_index,
                bimage_blob_path,
                str(bimage_path),
                'bee')

            annotation.image = bimage
            images.append(bimage)

            self.process_bee_image(bimage)
            self.save_image(bimage)

    def process_hive_image(self, himage, entrance_coordinates):
        """Process the hive image.

        Get bee and pollen annotations for image

        Args:
            himage (app.models.image.Image): The hive image.
            entrance_coordinates (Dict[str, int]): The coordinates for the
                hive entrance.
        """
        # Detect objects in hive image
        self.add_annotation_sets(himage, entrance_coordinates)

    def process_bee_image(self, bimage):
        """Process the bee image.

        - Upload the image to cloud storage
        - Detect pollen in the image
        - Extract the color of the pollen

        Args:
            bimage (app.models.image.Image): The bee image.
        """
        self.add_pollen_annotation_set(bimage)
        self.extract_pollen_color(bimage)
        self.logger.debug(f'✅ Bee image {bimage.local_path} processed!')

    def crop_image(self, frame, annotation, margin=0):
        """Crop the image.

        Args:
            frame (numpy.ndarray): The raw image.
            annotation (app.models.annotation.Annotation): The
                annotation instance.
            margin (float): How much margin to add/subtract when cropping.
                If number between 0 and 1 is given, it is interpreted
                as a percentage.
        """
        self.logger.debug(f'✂️ Cropping image by {margin}')
        if 1 > margin > 0:
            m_width = int((abs(annotation.x_min - annotation.x_max)*margin)/2)
            m_height = int((abs(annotation.y_min - annotation.y_max)*margin)/2)
            ymin = annotation.y_min + m_height
            ymax = annotation.y_max - m_height
            xmin = annotation.x_min + m_width
            xmax = annotation.x_max - m_width
        else:
            h, w = frame.shape[:2]
            ymin = max(annotation.y_min - margin, 0)
            ymax = min(annotation.y_max + margin, h)
            xmin = max(annotation.x_min - margin, 0)
            xmax = min(annotation.x_max + margin, w)

        cropped_frame = frame[ymin:ymax, xmin:xmax].copy()
        return cropped_frame

    def extract_pollen_color(self, bimage):
        """Extract pollen colors for pollen annotations in image.

        Generously (40% of the annotation) crops the pollen in the image
        and takes the average color of it.

        Args:
            bimage (app.models.image.Image): The bee image.
        """
        self.logger.debug(
            f'🌈 Extracting pollen color for {bimage.local_path}')
        annotation_set = get_annotation_set(bimage, AnnotationType.POLLEN)
        for annotation in annotation_set.annotations:
            pframe = self.crop_image(bimage.raw, annotation, margin=0.4)
            avg_color_per_row = np.average(pframe, axis=0)
            avg_color = np.average(avg_color_per_row, axis=0)
            bgr = tuple(int(channel) for channel in avg_color)
            annotation.color = bgr[2], bgr[1], bgr[0]

    @classmethod
    def create_visualized_image(cls, image):
        """Write image with annotations drawn on it to disk.

        Args:
            image (app.models.image.Image): The image.
        """
        image_path = Path(image.local_path).parent \
            / cls.images_vis_dir \
            / f'{Path(image.path).name}.jpg'

        frame_visualized = image.raw.copy()
        for annotation_set in image.annotation_sets:
            for a in annotation_set.annotations:
                cv2.rectangle(
                    frame_visualized,
                    (a.x_min, a.y_min),
                    (a.x_max, a.y_max),
                    (0, 255, 0),
                    2)
        cv2.imwrite(str(image_path), frame_visualized)

    def add_entrance_annotation_set(self, himage, coords):
        """Get an annotation set for the entrance.

        Assumed to be manual annotations. Only support for rectangles.

        Args:
            himage (Image): To which image the coordinates should be added to.
            coords (Dict[str, int]): The entrance coordinates.

        Returns:
            AnnotationSet: An instance of AnnotationSet.
        """
        self.logger.debug(
            f'🚪 Add entrance annotations for {himage.local_path}')
        if not coords:
            # compute approximation of entrance
            w = himage.width
            h = himage.height
            fourth_w = int(w / 4)
            fourth_h = int(h / 4)
            coords = {
                'x1': fourth_w,
                'y1': fourth_h,
                'x2': w - fourth_w,
                'y2': h - fourth_h
            }

        annotation_set = AnnotationSet(
            model_pk=-1,
            annotation_type=AnnotationType.ENTRANCE
        )
        annotation = Annotation(
            x_min=coords['x1'],
            y_min=coords['y1'],
            x_max=coords['x2'],
            y_max=coords['y2'],
            vector=coords,
            confidence=1.0
        )
        annotation_set.annotations.append(annotation)
        himage.annotation_sets.append(annotation_set)

    def add_bee_annotation_set(self, himage):
        """Detect bees in hive image and add it as an annotation set.

        Args:
            himage (app.models.image.Image): The hive image.
        """
        self.logger.debug(f'🐝 Running bee model over {himage.local_path}')
        annotation_set = self.bee_detector.detect(himage.raw)
        himage.annotation_sets.append(annotation_set)

    def add_pollen_annotation_set(self, bimage):
        """Detect pollen in bee image and add it as an annotation set.

        Args:
            bimage (app.models.image.Image): The bee image.
        """
        self.logger.debug(f'🟠 Running pollen model over {bimage.local_path}')
        annotation_set = self.pollen_detector.detect(bimage.raw)
        bimage.annotation_sets.append(annotation_set)

    def add_annotation_sets(self, himage, entrance_coordinates):
        """Detect objects in hive image and add them as annotation sets.

        Args:
            himage (app.models.image.Image): The hive image.
            entrance_coordinates (Dict[str, int]): The hive
                entrance coordinates.
        """
        self.add_entrance_annotation_set(himage, entrance_coordinates)
        self.add_bee_annotation_set(himage)
        self.process_bee_images(himage)

    def save_image(self, image):
        """Save the image.

        Saves the image on cloud storage and locally if specified so.

        Args:
            image (app.models.image.Image): The image.
        """
        future = self.image_save_executor.submit(self._save_image, image)
        self.image_save_futures[future] = image

    def _save_image(self, image):
        if self.visualized:
            self.create_visualized_image(image)

        self.azure_helper.upload_blob(image)

        # save memory as it is not needed anymore
        image.raw = None

    def finish_threads(self):
        """Wait for all threads to finish."""
        self.finish_image_saves()

    def finish_image_saves(self):
        """Finish saving all images."""
        for future in concurrent.futures.as_completed(self.image_save_futures):
            image = self.image_save_futures[future]
            try:
                future.result()
            except Exception as exc:
                self.logger.error(traceback.format_exc())
                self.logger.error(
                    f'❌ Image #{image.pk} threw an exception: {exc}')
            else:
                self.logger.debug(f'✅ Image #{image.pk} saved!')
