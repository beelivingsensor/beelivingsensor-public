import dataclasses
import shutil
from pathlib import Path

import cv2

from app.processors.media_processor import MediaProcessor


class ImageProcessor(MediaProcessor):

    def process(self, himage_pk, himage_blob_path, entrance_coordinates):
        """Process the image.

        Args:
            himage_pk (int): The ID of the hive image.
            himage_blob_path (str): The of the hive image on Azure.
            entrance_coordinates (Dict[str, int]): The hive
                entrance coordinates.

        Returns:
            app.models.image.Image: The image object.
        """
        images_dir_path = self._create_unique_folder(f'image_{himage_pk}')
        himage_name = Path(himage_blob_path).name
        himage_path = images_dir_path / himage_name
        self.azure_helper.download(himage_blob_path, himage_path)
        self.logger.info(f'Processing image {himage_name}')
        hframe = cv2.imread(str(himage_path))

        himage = self.get_image(
            hframe,
            himage_pk,
            himage_blob_path,
            str(himage_path),
            'hive')

        self.process_hive_image(himage, entrance_coordinates)

        # save memory as it is not needed anymore
        himage.raw = None

        self.finish_threads()

        if self.delete_data:
            shutil.rmtree(images_dir_path)

        return himage
