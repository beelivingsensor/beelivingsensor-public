import os
import shutil
from pathlib import Path

import cv2

from app.processors.media_processor import MediaProcessor
from app.models.video import Video
from app.trackers.bee_tracker import BeeTracker
from app.trackers.pollen_tracker import PollenTracker
from app.util import get_timestamp


class VideoProcessor(MediaProcessor):
    """Analyzer for videos to obtain image annotations and bee traffic data."""

    def __init__(self, delete_data=True, visualized=False, skip=1):
        """Initialize video processor.

        Args:
            delete_data (bool): Whether the data (downloaded or created files)
                should be deleted after processing.
            visualized (bool): This also creates images with
                annotations drawn on them. `delete_data` must be enabled.
            skip (int): How many images should be skipped
                for each extracted image.
        """
        self.skip = skip
        super().__init__(delete_data=delete_data, visualized=visualized)

    def process(self, video_pk, video_blob_path, entrance_coordinates):
        """Process the video.

        - Downloads the video from Azure
        - Obtains image data
        - Obtains bee traffic data

        Args:
            video_pk (int): An ID for the video.
                            Only used to create image names. May be randomly
                            chosen.
            video_blob_path (str): The blob's path on Azure.
            entrance_coordinates (Dict[str, int]): The coordinates for the
                entrance needed for the bee tracker. For rectangles, needs
                to have the keys 'x1', 'x2', 'y1', 'y2' defined.

        Example:
            >>> self.process(38729,
            >>>              'static/test.mp4',
            >>>              {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585})

        Returns:
            app.models.video.Video: A video object.
        """
        # Download video from cloud
        video_dir_path = self._create_unique_folder(f'video_{video_pk}')
        video_path = video_dir_path / Path(video_blob_path).name
        self.azure_helper.download(video_blob_path, video_path)

        # Create video instance where video data will be stored into
        video = Video(
            pk=video_pk,
            path=video_blob_path,
            local_path=str(video_path),
            annotations_path=self.get_annotations_path(video_pk, 'video'),
            size=Path(video_path).stat().st_size
        )

        self.logger.info(
            f'💡 Worker ID for video #{video.pk} process : {os.getpid()}')

        # Add image and bee traffic data
        self.process_hive_images(video, entrance_coordinates)
        BeeTracker(video, visualized=self.visualized).track()
        PollenTracker(video).track()

        self.finish_threads()
        self.azure_helper.upload_media_annotations(video)

        if self.delete_data:
            shutil.rmtree(video_dir_path)

        return video

    def process_hive_images(self, video, entrance_coordinates):
        """Process the hive images within the video.

        - Gets image metadata of video (FPS, image count, width, height)
        - Extracts the images from the video
        - Obtains the bee annotations & pollen annotations
        - Uploads images to Azure

        Args:
            video (app.models.video.Video): The video instance
                where image data will be stored
            entrance_coordinates (Dict[str, int]): The coordinates for the
                entrance.
        """
        video_path = Path(video.local_path)
        video_dir_path = video_path.parent

        cap = cv2.VideoCapture(str(video_path))

        if not cap.isOpened():
            self.logger.error(f'❌ Cannot read video #{video.pk}!')

        video.fps = int(cap.get(cv2.CAP_PROP_FPS))
        video.width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        video.height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        images_dir_path = video_dir_path / self.images_dir_name
        images_dir_path.mkdir()

        if self.visualized:
            images_vis_dir_path = images_dir_path / self.images_vis_dir
            images_vis_dir_path.mkdir()

        himg_index = 0
        read_failures = 0

        while cap.isOpened():
            himg_index += 1
            status, hframe = cap.read()

            if not status:
                self.logger.error(
                    f'❌ Hive image #{video.pk}/{himg_index} cannot be read')
                read_failures += 1
                if read_failures > 10:
                    break
                else:
                    continue

            read_failures = 0

            if himg_index % self.skip == 0 or himg_index == 1:

                himage_name = f'video_{video.pk}_himage_{himg_index}_{get_timestamp()}'
                himage_path = images_dir_path / f'{himage_name}.jpg'
                himage_blob_path = f'frames/modelservingapp/{himage_name}.jpg'

                # Get Image instance
                himage = self.get_image(
                    hframe,
                    himg_index,
                    himage_blob_path,
                    str(himage_path),
                    'hive')

                self.logger.debug(
                    f'🎞️️  Hive image #{video.pk}/{himage.pk} processing')

                try:
                    self.process_hive_image(himage, entrance_coordinates)
                except Exception as e:
                    self.logger.error(
                        f'❌ Hive image #{video.pk}/{himage.pk} '
                        f'failed to process: {e}')
                else:
                    video.images.append(himage)
                    self.logger.info(
                        f'✅ Hive image #{video.pk}/{himage.pk} processed')
                    self.save_image(himage)

            if self.visualized:
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break

        cap.release()
        cv2.destroyAllWindows()

        video.image_count = himg_index - read_failures

        self.logger.info(f'👍 Image processing for video #{video.pk} done')
