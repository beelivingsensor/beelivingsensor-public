from collections import OrderedDict
from collections import Counter

import pyclipper
from tqdm import tqdm
import numpy as np
import cv2

from app.models.annotation_type import AnnotationType
from app.models.tracker_event import TrackerEvent, Direction
from app.trackers.Tracking.iou_tracker import Tracker
from app.util import get_logger, get_annotation_set


class BeeTracker:

    def __init__(self, video, skip=1, fps=None, visualized=False, offset=20):
        """Initialize the bee tracker.

        Args:
            video (Video): The video.
            fps: The FPS to set. If None, takes the FPS from the video.
                Note: The FPS from the video's metadata might not always be
                correct. In that case, set it manually.
            skip (int): The number of images - 1 to skip for each processed
                image. Note: if you adapt this, you should also adapt the FPS.
            visualized (bool): Whether to create a video for visualization.
            offset (int): Offset to add to entrance coordinates.
        """
        self.video = video
        self.skip = skip
        self.fps = fps if fps else video.fps
        self.traffic_dict = OrderedDict()
        self.ct = self.get_tracker()
        self.contours = self.get_contours(offset=offset)
        self.logger = get_logger(__name__)

        # for visualization
        self.video_writer = None
        self.cap = None

        if visualized:
            self.video_writer = self.get_video_writer()
            self.cap = cv2.VideoCapture(video.local_path)

    def get_video_writer(self):
        out_name = 'bee_output.avi'
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        video_dimensions = (int(self.video.width), int(self.video.height))
        return cv2.VideoWriter(out_name, fourcc, 10.0, video_dimensions)

    def get_tracker(self):
        # dist_threshold, max_frame_skipped, max_trace_length, iou_threshold
        if self.fps >= 150:
            ct = Tracker(50, 20, 50, 0.5)
        elif self.fps >= 75:
            ct = Tracker(100, 15, 50, 0.2)
        elif self.fps >= 37.5:
            ct = Tracker(150, 10, 50, 0.005)
        else:
            ct = Tracker(250, 5, 50, 0.0025)
        return ct

    def get_contours(self, offset=20):
        """Get contours within entrance.

        Args:
            offset (int): Offset to add to entrance coordinates.
        """
        # Create blank image for entrance contour detection
        blank_image = np.zeros((self.video.height, self.video.width, 3), np.uint8)
        blank_image[:, :] = (255, 255, 255)
        # choose some random image to get the entrance coordinate
        image = self.video.images[0]
        annotation_set = get_annotation_set(image, AnnotationType.ENTRANCE)
        annotation = annotation_set.annotations[0]

        # polygon
        if len(annotation.vector) == 8:
            v = annotation.vector
            coords = []
            for i in range(1, int(len(annotation.vector) / 2)+1):
                coords.append([v[f'x{i}'], v[f'y{i}']])

            # add offset
            pco = pyclipper.PyclipperOffset()
            pco.AddPath(coords, pyclipper.JT_ROUND, pyclipper.ET_CLOSEDPOLYGON)
            coords = pco.Execute(offset)

            pts = np.array(coords, np.int32)
            pts = pts.reshape((-1, 1, 2))
            cv2.polylines(blank_image, [pts], True, (0, 0, 0))

        # rectangle
        else:
            # img_center_x = self.video.width // 2 - 55
            # img_center_y = self.video.height // 2 - 20
            # for united queens circle!
            # cv2.circle(blank_image, (img_center_x, img_center_y), 135, (0, 0, 0), 5)

            # first tuple is the start, second tuple the end coordinates
            cv2.rectangle(
                blank_image,
                (annotation.x_min - offset, annotation.y_min - offset),
                (annotation.x_max + offset, annotation.y_max + offset),
                (0, 0, 0),
                5)

        gray = cv2.cvtColor(blank_image, cv2.COLOR_BGR2GRAY)
        gray = cv2.bilateralFilter(gray, 11, 17, 17)
        edged = cv2.Canny(gray, 200, 800, 1)
        contours = cv2.findContours(edged, cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]
        return contours

    def track(self):
        self.logger.info(f'🐝 Tracking bees for video #{self.video.pk}')
        for image_index in tqdm(range(0, len(self.video.images))):
            self.process_image(image_index)

    def process_image(self, image_index):
        image = self.video.images[image_index]

        # for visualization
        image_status = None
        if self.cap:
            image_status, image_np = self.cap.read()
            if image_status:
                self.cap.set(1, image.pk)
                cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
                np.expand_dims(image_np, axis=0)

        if image_index % self.skip != 0:
            return

        rects = []

        annotation_set = get_annotation_set(image, AnnotationType.BEE)

        annotation_mapper = {}
        for a in annotation_set.annotations:
            coords = (a.x_min, a.y_min, a.x_max, a.y_max)
            annotation_mapper[coords] = a
            rects.append(list(coords))

            # for visualization
            if image_status:
                self.draw_annotation_box(a, image_np)

        # TODO: fix this properly
        result = self.ct.update(rects)
        if result is None:
            self.traffic_dict = OrderedDict()
            self.logger.error(
                f'Tracker update did not succeed '
                f'for image #{self.video.pk}/{image_index}')
            return
        else:
            objects, tracks, D, iou_scores, match, no_match = result

        for i, (objectID, coordinates) in enumerate(objects.items()):
            if tuple(coordinates) in annotation_mapper:
                annotation_mapper[tuple(coordinates)].object_id = objectID

            if len(self.traffic_dict) == 0:
                self.traffic_dict[objectID] = []

            for cnt in self.contours:
                centroid = self.get_centroid(coordinates)
                res = cv2.pointPolygonTest(cnt, centroid, False)
                self.traffic_dict[objectID].append(res)

                # for visualization
                if image_status:
                    self.draw_entrance(cnt, image_np)
                    self.draw_bee_point(centroid, image_np, objectID)
                    if res not in [1, 0]:
                        self.draw_tracks(image_np, objectID, tracks)

            try:
                len(self.traffic_dict[objectID + 1])
            except KeyError:
                self.traffic_dict[objectID + 1] = []

        if len(self.traffic_dict) > 0:
            for tb_id, tb_value in self.traffic_dict.items():
                if len(tb_value) == 0:
                    continue
                if tb_id not in objects:
                    last_counter = Counter(tb_value[-20:])
                    total_counter = Counter(tb_value)
                    if tb_value[0] == -1 \
                            and total_counter[-1] >= self.fps // 20 \
                            and last_counter[1] >= self.fps // 20:
                        tracker_event = TrackerEvent(
                            object_id=tb_id,
                            direction=Direction.IN
                        )
                        annotation_set.tracker_events.append(tracker_event)
                        self.traffic_dict[tb_id] = []
                        activity = "Bee {} flew in".format(tb_id)
                        self.logger.debug(activity)
                    if tb_value[0] == 1 \
                            and total_counter[1] >= self.fps // 20 \
                            and last_counter[-1] >= self.fps // 20:
                        tracker_event = TrackerEvent(
                            object_id=tb_id,
                            direction=Direction.OUT
                        )
                        annotation_set.tracker_events.append(tracker_event)
                        self.traffic_dict[tb_id] = []
                        activity = "Bee {} flew out".format(tb_id)
                        self.logger.debug(activity)

        # for visualization
        if image_status:
            self.draw_info_box(image_np, image_index)
            self.write_image_to_video(image_np)

        # Reset the tracker (-> make the tracker faster process)
        if image_index != 0 and image_index % 5000 == 0:
            self.reset()

    def reset(self):
        """Reset the tracker."""
        self.ct.reset()
        self.traffic_dict = OrderedDict()

    @staticmethod
    def get_centroid(coordinates):
        centroid_x = coordinates[0] + (coordinates[2] - coordinates[0]) // 2
        centroid_y = coordinates[1] + (coordinates[3] - coordinates[1]) // 2
        return centroid_x, centroid_y

    @staticmethod
    def draw_entrance(cnt, image_np):
        cv2.drawContours(image_np, [cnt], -1, (36, 255, 12), 2)

    @staticmethod
    def draw_tracks(image_np, objectID, tracks):
        for center in tracks[objectID]:
            x, y = center[0], center[1]
            cv2.circle(image_np, (x, y), 1, (255, 255, 255), -1)

    @staticmethod
    def draw_annotation_box(annotation, image_np, recursive=True):
        cv2.rectangle(
            image_np,
            (annotation.x_min, annotation.y_min),
            (annotation.x_max, annotation.y_max),
            (36, 255, 12),
            2
        )
        if recursive:
            if annotation.image:
                for a_set in annotation.image.annotation_sets:
                    for a in a_set.annotations:
                        cv2.rectangle(
                            image_np,
                            (annotation.x_min-20+a.x_min, annotation.y_min-20+a.y_min),
                            (annotation.x_min-20+a.x_max, annotation.y_min-20+a.y_max),
                            (255, 105, 180),
                            2
                        )

    @staticmethod
    def draw_bee_point(centroid, image_np, object_id):
        # draw circle in the middle of bee
        cv2.circle(image_np, centroid, 4, (0, 255, 0), -1)
        # draw bee ID
        text_coords = (centroid[0] - 10, centroid[1] - 20)
        text_color = (255, 255, 255)
        cv2.putText(
            image_np, str(object_id), text_coords, 0, 0.5, text_color, 2)

    def draw_info_box(self, image_np, image_index):
        out_event = 'None'
        in_event = 'None'
        bee_count = 0
        in_bee_count = 0
        out_bee_count = 0
        for image in self.video.images:
            if image.pk <= image_index + 1:
                annotation_set = get_annotation_set(image, AnnotationType.BEE)
                bee_count += len(annotation_set.annotations)

                out_events = []
                in_events = []
                for event in annotation_set.tracker_events:
                    if event.direction == Direction.IN:
                        in_bee_count += 1
                        in_events.append(str(event.object_id))
                    else:
                        out_bee_count += 1
                        out_events.append(str(event.object_id))

                if out_events:
                    out_event = 'Bee(s) ' + ', '.join(out_events) + ' flew out'

                if in_events:
                    in_event = 'Bee(s) ' + ', '.join(in_events) + ' flew in'

        info = [
            ('Frame', image_index),
            ('Last out event', out_event),
            ('Last in event', in_event),
            ('Nr of bees out', out_bee_count),
            ('Nr of bees in', in_bee_count)
        ]

        # Draw black background rectangle
        cv2.rectangle(image_np,
                      (0, int(self.video.height)-150),
                      (400, int(self.video.height)-10),
                      (0, 0, 0),
                      -1)

        for (i, (k, v)) in enumerate(info):
            text = f'{k}: {v}'
            coords = (10, int(self.video.height) - ((i * 20) + 20))
            font = cv2.FONT_HERSHEY_SIMPLEX
            color = (0, 255, 0)
            cv2.putText(image_np, text, coords, font, 0.7, color, 1)

    def write_image_to_video(self, image_np):
        video_dimensions = (int(self.video.width), int(self.video.height))
        self.video_writer.write(cv2.resize(image_np, video_dimensions))
