from app.models.annotation_type import AnnotationType
from app.models.tracker_event import TrackerEvent, Direction
from app.util import get_logger, get_annotation_set


class PollenTracker:
    """Add pollen information to tracker events.

    Populates a TrackerEvent's `pollen` attribute.
    Pre-condition: Bee Tracker has to be run over the video already.
    """
    def __init__(self, video):
        """Initialize the tracker.

        Args:
            video (app.models.Video): The video instance in which to add the
                pollen information.
        """
        self.video = video
        self.logger = get_logger(__name__)

    def track(self):
        self.logger.info(f'🟠 Tracking pollen for video #{self.video.pk}')
        self.track2()

    def track1(self):
        tracker_events = []
        # object_id -> List[Annotation]
        object_ids = {}

        for image in self.video.images:
            # bee annotation set
            annotation_set = get_annotation_set(image, AnnotationType.BEE)
            for tracker_event in annotation_set.tracker_events:
                tracker_events.append(tracker_event)

            for annotation in annotation_set.annotations:
                if annotation.object_id not in object_ids:
                    object_ids[annotation.object_id] = []
                object_ids[annotation.object_id].append(annotation)

        for tracker_event in tracker_events:
            bee_annotations = object_ids[tracker_event.object_id]

            pollen_sum = 0
            red_sum = 0
            green_sum = 0
            blue_sum = 0
            for bee_annotation in bee_annotations:
                pollen_annotation_set = get_annotation_set(
                    bee_annotation.image, AnnotationType.POLLEN)
                for pollen_annotation in pollen_annotation_set.annotations:
                    pollen_sum += 1
                    red, green, blue = pollen_annotation.color
                    red_sum += red
                    green_sum += green
                    blue_sum += blue

            avg_pollen_count = round(pollen_sum / len(bee_annotations))

            if avg_pollen_count > 0:
                avg_pollen_color = (round(red_sum/pollen_sum),
                                    round(green_sum/pollen_sum),
                                    round(blue_sum/pollen_sum))
                tracker_event.pollen = avg_pollen_count * [avg_pollen_color]

                self.logger.debug(f'{tracker_event.pollen} pollen '
                                  f'flown in by #{tracker_event.object_id}')

    def track2(self):
        """Count number of object IDs with pollen annotations.

        - Assumes that bees with pollen fly inside the hive.
        - There must be at least 4 images on which the object has been
          detected with pollen
        - If the fly-in tracker event has not been registered
          for this object ID, a tracker event is added
          to the last image where it appears.
        """
        # object IDs of tracker events
        tracker_events_ids = {}
        # object IDs of bee annotations with pollen
        annotation_object_ids = {}
        # object IDs with image in which they occur the last
        last_image_object_ids = {}

        for himage in self.video.images:
            bee_annotation_set = get_annotation_set(himage, AnnotationType.BEE)

            # collect all object IDs in the tracker events
            for tracker_event in bee_annotation_set.tracker_events:
                tracker_events_ids[tracker_event.object_id] = tracker_event

            # collect all object IDs with pollen annotations
            for bee_annotation in bee_annotation_set.annotations:
                object_id = bee_annotation.object_id
                bimage = bee_annotation.image
                pollen_annotation_set = get_annotation_set(
                    bimage, AnnotationType.POLLEN)
                last_image_object_ids[object_id] = himage

                obj = None
                for pollen_annotation in pollen_annotation_set.annotations:
                    if object_id not in annotation_object_ids:
                        annotation_object_ids[object_id] = {
                            'image_count': 0,
                            'pollen_count': 0,
                            'red': 0,
                            'green': 0,
                            'blue': 0
                        }
                    obj = annotation_object_ids[object_id]
                    red, green, blue = pollen_annotation.color
                    obj['pollen_count'] += 1
                    obj['red'] += red
                    obj['green'] += green
                    obj['blue'] += blue

                if obj:
                    obj['image_count'] += 1

        # on how many images the object must be detected with pollen at least
        minimum_image_count = 4

        for annotation_object_id, colors in annotation_object_ids.items():
            # TODO: for some reason, some bee annotations have no object ID
            if annotation_object_id:
                image_count = colors['image_count']
                if image_count >= minimum_image_count:
                    if annotation_object_id in tracker_events_ids:
                        tracker_event = tracker_events_ids[annotation_object_id]
                    else:
                        tracker_event = TrackerEvent(
                            object_id=annotation_object_id,
                            direction=Direction.IN
                        )
                        image = last_image_object_ids[annotation_object_id]
                        bee_annotation = get_annotation_set(image, AnnotationType.BEE)
                        bee_annotation.tracker_events.append(tracker_event)

                    pollen_count = colors['pollen_count']
                    red = round(colors['red'] / pollen_count)
                    green = round(colors['green'] / pollen_count)
                    blue = round(colors['blue'] / pollen_count)
                    avg_pollen_color = (red, green, blue)
                    avg_pollen_count = int(pollen_count / image_count)
                    tracker_event.pollen = avg_pollen_count*[avg_pollen_color]
