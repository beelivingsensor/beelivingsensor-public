import dataclasses
import json
import os
from pathlib import Path

import cv2
from azure.storage.blob import BlobServiceClient

from app.util import get_timestamp, get_logger, NumpyJSONEncoder

logger = get_logger(__name__)


class AzureHelper:
    """Helper functions for interacting with Azure cloud storage."""

    def __init__(self):
        """Set up a blob service client with the storage connection string."""
        self.logger = logger
        connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
        if not connect_str:
            self.logger.error('❌ Azure storage connection string not exported!')
        self.blob_service_client = BlobServiceClient.from_connection_string(
            connect_str)

    def download(self, blob_path, local_path, temp=False):
        """Download a blob from Azure cloud storage.

        Args:
            blob_path (Path|str): The path of the blob on Azure.
            local_path (Path|str): Where the blob is downloaded to.
            temp (bool): Whether the file should be downloaded to a

        Example:
            >>> self.download(
            >>>     'public/models/bee/bee_medium.weights',
            >>>     'downloaded_models/public/models/bee/bee_medium.weights')
        """
        local_path = Path(local_path)
        blob_path = Path(blob_path)
        local_path.parent.mkdir(exist_ok=True, parents=True)
        if not local_path.exists():
            container = blob_path.parts[0]
            blob = Path(*blob_path.parts[1:])

            self.logger.info(f'⏬ Downloading {blob_path} to {local_path}')
            blob_client = self.blob_service_client.get_blob_client(
                container=container,
                blob=str(blob))

            if temp:
                download_path = Path(f'{local_path}_{get_timestamp()}')
                self.logger.debug(f'...as temp file {download_path}')
            else:
                download_path = local_path

            with open(download_path, "wb") as f:
                f.write(blob_client.download_blob().readall())

            if temp:
                if local_path.exists():
                    self.logger.debug(f'Delete {download_path} as {local_path} by now exists')
                    download_path.unlink()
                else:
                    self.logger.debug(f'Rename {download_path} to {local_path}')
                    download_path.rename(local_path)
        else:
            self.logger.info(f'✅ Model {local_path} already downloaded')

    def upload_blob_from_file(self, blob_path, file_path):
        """Upload file to Azure cloud storage.

        Args:
            blob_path (str|Path): The path of the blob on Azure.
            file_path (str|Path): The path of the file that is uploaded.
        """
        self.logger.debug(f'⏫️ Uploading {file_path} as {blob_path} to Azure')
        blob_path = Path(blob_path)
        container = blob_path.parts[0]
        blob = Path(*blob_path.parts[1:])
        blob_client = self.blob_service_client.get_blob_client(
            container=container,
            blob=str(blob))

        with open(file_path, 'rb') as data:
            blob_client.upload_blob(data)

    def upload_media_annotations(self, media):
        """Upload the media annotations to cloud storage.

        Args:
            media (app.models.media.Media): The media.
        """
        self.logger.info(f'⏫️ Uploading annotations for video #{media.pk}')
        blob_path = Path(media.annotations_path)
        container = blob_path.parts[0]
        blob = Path(*blob_path.parts[1:])
        blob_client = self.blob_service_client.get_blob_client(
            container=container,
            blob=str(blob))

        json_str = json.dumps(dataclasses.asdict(media), cls=NumpyJSONEncoder)
        blob_client.upload_blob(json_str)

    def upload_blob(self, image):
        """Upload image to Azure cloud storage.

        Args:
            image (app.models.image.Image): The image.
        """
        self.logger.debug(f'⏫️ Uploading image as {image.path} to Azure')
        blob_path = Path(image.path)
        container = blob_path.parts[0]
        blob = Path(*blob_path.parts[1:])
        blob_client = self.blob_service_client.get_blob_client(
            container=container,
            blob=str(blob))

        _, frame = cv2.imencode('.jpg', image.raw)

        blob_client.upload_blob(bytearray(frame))
