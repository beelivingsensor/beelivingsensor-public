import datetime
import os

from celery import Celery
from pyinstrument import Profiler

from app.processors.image_processor import ImageProcessor
from app.processors.video_processor import VideoProcessor
from app.util import send_media, get_logger

celery_app = Celery(__name__)
celery_app.conf.broker_url = os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379")
celery_app.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379")
celery_app.conf.task_track_started = True


logger = get_logger(__name__)


def process_media(proc):
    if not proc['profiling']:
        return _process_media(proc)
    else:
        profiler = Profiler()
        profiler.start()
        media = _process_media(proc)
        profiler.stop()
        print(profiler.output_text(unicode=True, color=True))

        return media


def _process_media(proc):
    media_type = proc['media_type']
    media_pk = proc['media_pk']

    if media_type == 'video':
        processor = VideoProcessor(skip=proc['skip'])
    else:
        processor = ImageProcessor()

    processor.load_bee_model(
        proc['bee_model_pk'],
        proc['bee_model_blob_path'])

    processor.load_pollen_model(
        proc['pollen_model_pk'],
        proc['pollen_model_blob_path'])

    media = processor.process(
        media_pk,
        proc['media_blob_path'],
        proc['entrance_coordinates'])

    send_media(media, proc['back_url'], media_type)

    logger.info(f'💪 Process for {media_type} #{media_pk} completed!')

    return media


@celery_app.task(name="create_media_proc")
def create_media_proc(proc):
    start = datetime.datetime.now()
    media = process_media(proc)
    end = datetime.datetime.now()

    return {
        'annotations_path': media.annotations_path,
        'media_type': proc['media_type'],
        'start': str(start),
        'end': str(end),
        'runtime': str(end - start)
    }
