from dataclasses import dataclass


@dataclass
class Media:
    pk: int
    path: str
    local_path: str
    annotations_path: str
