from dataclasses import dataclass, field
from typing import List

from app.models.annotation import Annotation
from app.models.annotation_type import AnnotationType
from app.models.tracker_event import TrackerEvent


@dataclass
class AnnotationSet:
    model_pk: int
    annotation_type: AnnotationType
    annotations: List[Annotation] = field(default_factory=list)
    tracker_events: List[TrackerEvent] = field(default_factory=list)
