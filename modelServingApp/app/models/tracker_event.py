from dataclasses import dataclass, field
from enum import Enum
from typing import List, Tuple


class Direction(str, Enum):
    IN = 'in'
    OUT = 'out'


@dataclass
class TrackerEvent:
    object_id: int
    direction: Direction
    pollen: List[Tuple[int, int, int]] = field(default_factory=list)
