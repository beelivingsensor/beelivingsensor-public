from dataclasses import dataclass, field
from typing import List, Optional

from app.models.image import Image
from app.models.media import Media


@dataclass
class Video(Media):
    fps: Optional[int] = None
    width: Optional[int] = None
    height: Optional[int] = None
    image_count: Optional[int] = None
    size: Optional[int] = None
    images: List[Image] = field(default_factory=list)
