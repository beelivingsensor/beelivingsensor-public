from enum import Enum


class AnnotationType(str, Enum):
    BEE = 'bee'
    POLLEN = 'pollen'
    ENTRANCE = 'entrance'
