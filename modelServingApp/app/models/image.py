from dataclasses import dataclass, field
from typing import List, Optional
import numpy
from app.models.annotation_set import AnnotationSet
from app.models.annotation_type import AnnotationType
from app.models.media import Media


@dataclass
class Image(Media):
    width: int
    height: int
    channels: int
    type: AnnotationType
    raw: Optional[numpy.ndarray]
    annotation_sets: List[AnnotationSet] = field(default_factory=list)
