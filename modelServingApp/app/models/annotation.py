from __future__ import annotations
from dataclasses import dataclass
from typing import Optional, Tuple, Dict


@dataclass
class Annotation:
    x_min: int
    y_min: int
    x_max: int
    y_max: int

    confidence: float

    # shape independent coordinates
    vector: Dict[str, int]

    # ID assigned by tracker
    object_id: Optional[int] = None

    # image cropped based on annotation
    image: Optional['Image'] = None  # noqa: F821

    # color (only for pollen)
    color: Optional[Tuple[int, int, int]] = None
