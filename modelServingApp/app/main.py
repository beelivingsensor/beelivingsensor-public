from fastapi import FastAPI

from app.database import schemas
from app.util import MediaResponse

from app.worker import create_media_proc, celery_app, process_media

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/media/processes/", status_code=201)
def run_task(proc: schemas.MediaProcessCreate):
    # check if unit test
    if proc.back_url == 'test':
        media = process_media(proc.dict())
        return MediaResponse(media, status_code=201)
    else:
        task = create_media_proc.delay(proc.dict())
        return task.id


@app.get("/media/processes/{proc_id}")
def get_status(proc_id):
    proc_result = celery_app.AsyncResult(proc_id)
    result = {
        'id': proc_id,
        'status': proc_result.status,
        'result': proc_result.result
    }
    return result
