from app.models.annotation_type import AnnotationType
from app.processors.video_processor import VideoProcessor
from app.util import get_annotation_set


def test_video_processor():
    video_processor = VideoProcessor(
        skip=35,
        # delete_data=False,
        # visualized=True
    )
    video_processor.load_bee_model(4, 'public/models/bee/bee_medium.weights')
    video_processor.load_pollen_model(5, 'public/models/pollen/pollen_tiny.weights')
    video = video_processor.process(
        120394803,
        'static/1_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4',
        {'x1': 779, 'y1': 492, 'x2': 1614, 'y2': 597}
    )

    assert video.fps == 23
    assert video.size == 2432455
    assert video.image_count == 47
    assert video.height == 1920
    assert video.width == 2560

    himage = video.images[1]
    hannotation_set = get_annotation_set(himage, AnnotationType.BEE)
    bimage = hannotation_set.annotations[8].image
    bannotation_set = get_annotation_set(bimage, AnnotationType.POLLEN)
    annotation = bannotation_set.annotations[0]

    assert annotation.x_max == 52
    assert annotation.x_min == 24
    assert annotation.y_max == 111
    assert annotation.y_min == 84
    assert annotation.color == (189, 148, 113)
