from pathlib import Path

import cv2

from app.cloud_storage.azure_helper import AzureHelper
from app.detectors.yolo_detector import YOLODetector

resources_dir_path = Path('app/test/tmp_media/')


def test_yolo_detector():
    """Test the YOLO detector using a pollen detection model."""
    model_pk = 5
    model_blob_path = 'public/models/pollen/pollen_tiny.weights'
    azure_helper = AzureHelper()

    weights_path = resources_dir_path / model_blob_path
    azure_helper.download(model_blob_path, weights_path, temp=True)

    cfg_blob_path = Path(model_blob_path).with_suffix('.cfg')
    cfg_path = resources_dir_path / cfg_blob_path
    azure_helper.download(cfg_blob_path, cfg_path, temp=True)

    pollen_detector = YOLODetector(
        model_pk, weights_path, cfg_path, 'pollen')

    image_blob_paths = [
        'static/pollen/pollen1.jpg',
        'static/pollen/pollen2.png',
        'static/pollen/pollen3.jpg',
        'static/pollen/pollen4.jpg',
        'static/pollen/pollen5.jpg',
        'static/pollen/pollen6.jpg',
        'static/pollen/pollen7.jpg',
    ]

    annotation_count = 0

    for image_blob_path in image_blob_paths:
        image_path = Path('app/test/tmp_media/') / image_blob_path
        azure_helper.download(image_blob_path, image_path)
        image = cv2.imread(str(image_path))
        annotation_set = pollen_detector.detect(image)
        annotation_count += len(annotation_set.annotations)

    assert annotation_count == 13
