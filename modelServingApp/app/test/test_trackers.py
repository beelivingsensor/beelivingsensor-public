import pickle
from pathlib import Path

from unittest import skip

from app.cloud_storage.azure_helper import AzureHelper
from app.models.annotation_type import AnnotationType
from app.models.tracker_event import Direction
from app.trackers.bee_tracker import BeeTracker
from app.trackers.pollen_tracker import PollenTracker
from app.util import get_annotation_set

resources_dir_path = Path('app/test/tmp_media/')


def test_bee_tracker_headless():
    """Test bee tracker without visualization with rectangle coordinates."""
    with open('app/test/resources/test_video.pickle', 'rb') as f:
        video = pickle.load(f)
        BeeTracker(video).track()

        # check out-going bees
        for i, bee_id in [(19, 3), (47, 4), (93, 2), (111, 19)]:
            image = video.images[i]
            annotation_set = get_annotation_set(image, AnnotationType.BEE)
            outs = annotation_set.tracker_events
            assert len(outs) == 1
            assert outs[0].object_id == bee_id
            assert outs[0].direction == Direction.OUT

        # check in-going bees
        for i, bee_id in [(54, 11)]:
            image = video.images[i]
            annotation_set = get_annotation_set(image, AnnotationType.BEE)
            ins = annotation_set.tracker_events
            assert len(ins) == 1
            assert ins[0].object_id == bee_id
            assert ins[0].direction == Direction.IN

        sum_in = 0
        sum_out = 0
        for image in video.images:
            annotation_set = get_annotation_set(image, AnnotationType.BEE)
            for tracker_event in annotation_set.tracker_events:
                if tracker_event.direction == Direction.IN:
                    sum_in += 1
                else:
                    sum_out += 1

        assert sum_in == 1
        assert sum_out == 4


def test_bee_tracker_headless_polygon_coordinates():
    """Test bee tracker without visualization with polygon coordinates."""
    with open('app/test/resources/test_video_adv.pickle', 'rb') as f:
        video = pickle.load(f)
        for image in video.images:
            # overwrite the entrance coordinates (they are given as rectangles)
            annotation_set = get_annotation_set(image, AnnotationType.ENTRANCE)
            for annotation in annotation_set.annotations:
                annotation.vector = {
                    'x1': 770, 'y1': 522,
                    'x2': 780, 'y2': 591,
                    'x3': 1598, 'y3': 563,
                    'x4': 1607, 'y4': 497
                }
        BeeTracker(video, offset=0).track()

        in_going = 0

        for image in video.images:
            annotation_set = get_annotation_set(image, AnnotationType.BEE)
            for e in annotation_set.tracker_events:
                if e.direction == Direction.IN:
                    in_going += 1

        assert in_going == 4


def test_pollen_tracker():
    """Test the first pollen tracking algorithm."""
    with open('app/test/resources/test_video_adv.pickle', 'rb') as f:
        video = pickle.load(f)

    BeeTracker(video).track()
    PollenTracker(video).track1()

    # check: video.images[32].annotation_sets[0].tracker_events[0]
    pollen = []
    for image in video.images:
        annotation_set = get_annotation_set(image, AnnotationType.BEE)
        for tracker_event in annotation_set.tracker_events:
            if tracker_event.pollen:
                pollen.append(tracker_event.pollen)

    assert len(pollen) == 1
    assert pollen[0][0] == (168, 129, 79)


def test_pollen_tracker_2():
    """Test the second pollen tracking algorithm."""
    with open('app/test/resources/test_video_adv.pickle', 'rb') as f:
        video = pickle.load(f)

    BeeTracker(video).track()
    PollenTracker(video).track2()

    pollen = []
    for image in video.images:
        annotation_set = get_annotation_set(image, AnnotationType.BEE)
        for tracker_event in annotation_set.tracker_events:
            if tracker_event.pollen:
                pollen.append((tracker_event.object_id, tracker_event.pollen))

    assert len(pollen) == 7
    assert pollen[1][0] == 54
    assert pollen[1][1] == [(168, 129, 79), (168, 129, 79)]


def test_bee_tracker_visualized():
    # static/1_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4
    video_blob_path = 'static/bees_2.mp4'
    video_local_path = resources_dir_path / video_blob_path
    azure_helper = AzureHelper()
    azure_helper.download(video_blob_path, video_local_path)

    # app/test/resources/test_video_adv.pickle
    with open('app/test/resources/test_video.pickle', 'rb') as f:
        video = pickle.load(f)
        video.local_path = str(video_local_path)
        BeeTracker(video, visualized=True).track()


@skip
def test_bee_tracker_visualized_polygon():
    video_blob_path = 'static/1_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4'
    video_local_path = resources_dir_path / video_blob_path
    azure_helper = AzureHelper()
    azure_helper.download(video_blob_path, video_local_path)

    with open('app/test/resources/test_video_adv.pickle', 'rb') as f:
        video = pickle.load(f)

        for image in video.images:
            # overwrite the entrance coordinates (they are given as rectangles)
            annotation_set = get_annotation_set(image, AnnotationType.ENTRANCE)
            for annotation in annotation_set.annotations:
                annotation.vector = {
                    'x1': 770, 'y1': 522,
                    'x2': 780, 'y2': 591,
                    'x3': 1598, 'y3': 563,
                    'x4': 1607, 'y4': 497
                }

        video.local_path = str(video_local_path)
        BeeTracker(video, visualized=True).track()
