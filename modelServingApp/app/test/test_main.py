import json
from types import SimpleNamespace
from unittest import skip

from fastapi.testclient import TestClient

from app.main import app

from app.models.annotation_type import AnnotationType
from app.util import get_annotation_set

client = TestClient(app)


def test_hello_world():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}


def test_video_process_create():
    response = client.post(
        f'/media/processes/',
        json={
            'media_pk': 1,
            'media_blob_path': 'static/test.mp4',
            'media_type': 'video',
            'bee_model_pk': 5,
            'bee_model_blob_path': 'models/single_hive/Doettingen_Hive1',
            'pollen_model_pk': 8,
            'pollen_model_blob_path': 'public/models/pollen/pollen_tiny.weights',
            'entrance_coordinates': {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585},
            'back_url': 'test',
            'skip': 35,
            'profiling': True
        })
    print(response.json())
    assert response.status_code == 201

    # load JSON into an object
    video = json.loads(
        response.content, object_hook=lambda d: SimpleNamespace(**d))

    assert video.pk == 1

    assert len(video.images) > 1
    image = video.images[0]

    assert image.width == 2560
    assert image.height == 1920

    assert len(image.annotation_sets) == 2

    # check the bee annotation
    bee_annotation_set = get_annotation_set(image, AnnotationType.BEE)
    assert bee_annotation_set.model_pk == 5
    assert bee_annotation_set.annotation_type == AnnotationType.BEE

    # check pollen annotation set
    first_bimage = bee_annotation_set.annotations[0].image
    first_bimage_annotation_set = get_annotation_set(
        first_bimage,
        AnnotationType.POLLEN)
    assert first_bimage_annotation_set.model_pk == 8
    assert first_bimage_annotation_set.annotation_type == AnnotationType.POLLEN

    # check the entrance annotation
    entrance_annotation_set = get_annotation_set(image, AnnotationType.ENTRANCE)
    assert entrance_annotation_set.model_pk == -1
    assert entrance_annotation_set.annotation_type == AnnotationType.ENTRANCE
    entrance_annotation = entrance_annotation_set.annotations[0]
    assert entrance_annotation.x_min == 767
    assert entrance_annotation.y_min == 487
    assert entrance_annotation.x_max == 1603
    assert entrance_annotation.y_max == 585


def test_image_process_create():
    response = client.post(
        f'/media/processes/',
        json={
            'media_pk': 24,
            'media_blob_path': 'static/ClemensHiveRed_6421_197.jpg',
            'media_type': 'image',
            'bee_model_pk': 5,
            'bee_model_blob_path': 'models/single_hive/Doettingen_Hive1',
            'pollen_model_pk': 3,
            'pollen_model_blob_path': 'public/models/pollen/pollen_tiny.weights',
            'entrance_coordinates': {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585},
            'back_url': 'test'
        })
    assert response.status_code == 201

    # load JSON into an object
    image = json.loads(
        response.content, object_hook=lambda d: SimpleNamespace(**d))

    assert image.pk == 24
    assert image.width == 1280
    assert image.height == 720

    assert len(image.annotation_sets) == 2

    annotation_set = get_annotation_set(image, AnnotationType.BEE)
    assert annotation_set.model_pk == 5
    assert annotation_set.annotation_type == AnnotationType.BEE

    first_annotation = annotation_set.annotations[0]
    assert first_annotation.x_min == 835
    assert first_annotation.y_min == 564
    assert first_annotation.x_max == 857
    assert first_annotation.y_max == 598

    first_bimage = first_annotation.image
    first_bimage_annotation_set = get_annotation_set(
        first_bimage, AnnotationType.POLLEN)
    assert first_bimage_annotation_set.model_pk == 3
    assert first_bimage_annotation_set.annotation_type == AnnotationType.POLLEN


@skip
def test_performance_cprofiler():
    import cProfile, pstats
    profiler = cProfile.Profile()
    profiler.enable()
    client.post(
        f'/media/processes/',
        json={
            'media_pk': 1,
            'media_blob_path': 'static/5_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4',
            'media_type': 'video',
            'bee_model_pk': 5,
            'bee_model_blob_path': 'public/models/bee/bee_medium.weights',
            'pollen_model_pk': 8,
            'pollen_model_blob_path': 'public/models/pollen/pollen_tiny.weights',
            'entrance_coordinates': {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585},
            'back_url': 'test',
            'skip': 1
        })
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats('cumtime')
    stats.print_stats('/app/processors/')
    stats.print_stats('/app/detectors/')


@skip
def test_performance_pyinstrument():
    from pyinstrument import Profiler
    profiler = Profiler()
    profiler.start()
    client.post(
        f'/media/processes/',
        json={
            'media_pk': 1,
            'media_blob_path': 'static/5_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4',
            'media_type': 'video',
            'bee_model_pk': 5,
            'bee_model_blob_path': 'public/models/bee/bee_medium.weights',
            'pollen_model_pk': 8,
            'pollen_model_blob_path': 'public/models/pollen/pollen_tiny.weights',
            'entrance_coordinates': {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585},
            'back_url': 'test',
            'skip': 1
        })
    profiler.stop()
    print(profiler.output_text(unicode=True, color=True))
