import dataclasses
import json
import logging
import os
import sys
import typing
from datetime import datetime

import numpy as np
import requests
from starlette.responses import Response

from app.models.media import Media

TIMESTAMP_FORMAT = '%Y%m%d-%H-%M-%S-%f'


def get_logger(name, to_file=False):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s|%(levelname)s|%(name)s|%(message)s")

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    if to_file:
        file_handler = logging.FileHandler(
            'media_processing.log', mode='a', encoding='utf-8')
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger


logger = get_logger(__name__)


def get_timestamp():
    """Get the current timestamp."""
    return str(datetime.now().strftime(TIMESTAMP_FORMAT))


def send_media(media: Media, back_url, media_type):
    if back_url != 'test':
        url = os.environ.get('WEBAPP') + back_url
        web_app_api_key = os.environ.get('WEBAPP_API_KEY')
        logger.info(f'💡 Sending request to {url}')

        try:
            # files = {'media': json.dumps(dataclasses.asdict(media))}
            response = requests.post(
                url,
                # files=files,
                data={
                    'api_key': web_app_api_key,
                    'media_type': media_type,
                    'annotations_path': media.annotations_path
                }
            )
        except requests.exceptions.ConnectionError:
            logger.error(
                f'❌ Annotations of #{media.pk} could not be sent to webapp, '
                f'max retries exceeded!')
        except Exception as e:
            logger.error(f'An error with your request occurred: {e}')
        else:
            if response.status_code == 201:
                logger.info(
                    f'🎉 Annotations of #{media.pk} successfully sent to webapp!')
            else:
                logger.info(
                    f'❌ Annotations of #{media.pk} could not be sent to webapp, '
                    f'status code = {response.status_code}!')


def get_annotation_set(image, annotation_type):
    """Get the annotation set for the annotation type.

    Args:
        image (models.image.Image): The image.
        annotation_type (models.annotation_type.AnnotationType): The
            type of annotation.

    Returns:
        Optional[models.annotation_set.AnnotationSet]: The annotation set.
    """
    for annotation_set in image.annotation_sets:
        if annotation_set.annotation_type == annotation_type:
            return annotation_set
    return None


class NumpyJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return None
        return json.JSONEncoder.default(self, obj)


class MediaResponse(Response):
    media_type = "application/json"

    def render(self, content: typing.Any) -> bytes:
        json_str = json.dumps(
            dataclasses.asdict(content),
            cls=NumpyJSONEncoder,
            ensure_ascii=False,
            allow_nan=False,
            indent=None,
            separators=(",", ":"),
        ).encode("utf-8")

        return json_str
