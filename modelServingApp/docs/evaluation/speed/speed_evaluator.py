"""Evaluate speed of processing videos.

Instructions:
Run model-service with docker-compose (cf. README)

Export variables:
export $(xargs <.env)
export PYTHONPATH=$PYTHONPATH:/home/vmadmin/beewatch/modelServingApp

Install dependencies:
sh environments/dev/install_dependencies.sh

Activate environment:
. venv/bin/activate

Run evaluation:
To start the video processes:
python docs/evaluation/speed/speed_evaluator.py start [--videos docs/evaluation/speed/resources/Frohheim14ST8304.txt]

Get the status (and results if finished) of the video processes:
python docs/evaluation/speed/speed_evaluator.py status
"""
import csv
import datetime
import logging
import pprint
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

import requests

from app.cloud_storage.azure_helper import AzureHelper


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SpeedEvaluator:

    process_ids_path = 'process_ids.csv'
    results_path = 'results.csv'

    def __init__(self, videos_file_path):
        """Initialize speed evaluator.

        Args:
            videos_file_path (str): Path to the file listing video paths.
        """
        self.video_paths = self.get_video_paths(videos_file_path)
        self.azure_helper = AzureHelper()

    def get_video_paths(self, videos_file_path):
        video_paths = []
        with open(videos_file_path) as f:
            for row in f:
                row = row.strip()
                video_paths.append(row)
        return video_paths

    def download_video(self, path):
        self.azure_helper.download(path, path)
        logger.info(f'⏬ Video {path} downloaded')

    def download_videos(self):
        logger.info(f'⏬ Download videos')
        with ThreadPoolExecutor() as executor:
            for video_path in self.video_paths:
                if not Path(video_path).exists():
                    executor.submit(self.download_video, video_path)

    def write_process_ids_to_disk(self, process_ids):
        with open(self.process_ids_path, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=['video', 'id'])
            writer.writeheader()

            for video, id_ in zip(self.video_paths, process_ids):
                writer.writerow({'video': video, 'id': id_})

    def start_processes(self):
        logger.info(f'⏬ Start processes')
        process_ids = []
        for i, video_path in enumerate(self.video_paths):
            r = requests.post(
                f'http://0.0.0.0:8001/media/processes/',
                json={
                    'media_pk': i,
                    'media_blob_path': str(video_path),
                    'media_type': 'video',
                    'bee_model_pk': 5,
                    'bee_model_blob_path': 'public/models/bee/bee_medium.weights',
                    'pollen_model_pk': 8,
                    'pollen_model_blob_path': 'public/models/pollen/pollen_tiny.weights',
                    'entrance_coordinates': {'x1': 673, 'x2': 607, 'y1': 1243, 'y2': 1249},
                    'back_url': '/non-existing-endpoint/',
                    'skip': 1,
                    #'profiling': True
                })

            if r.status_code == 201:
                process_id = r.json()
                process_ids.append(process_id)
            else:
                logger.error(f'❌ Request to modelserving app failed!')

        self.write_process_ids_to_disk(process_ids)

    def get_results(self):
        with open(self.process_ids_path) as proc_file:
            reader = csv.DictReader(proc_file)

            with open(self.results_path, 'w') as results_file:
                field_names = ['video', 'id', 'start', 'end', 'runtime']
                writer = csv.DictWriter(results_file, fieldnames=field_names)
                writer.writeheader()

                first_start = None
                last_end = None
                finished = True

                for row in reader:
                    id_ = row['id']
                    video = row['video']

                    r = requests.get(
                        f'http://0.0.0.0:8001/media/processes/{id_}',
                        timeout=10
                    )

                    response = r.json()

                    status = response['status']
                    if status != 'SUCCESS':
                        to_write = {
                            'video': video,
                            'id': id_,
                            'start': status,
                            'end': status,
                            'runtime': status
                        }
                        finished = False
                    else:
                        result = response['result']
                        to_write = {
                            'video': video,
                            'id': id_,
                            'start': result['start'],
                            'end': result['end'],
                            'runtime': result['runtime']
                        }

                        if not first_start:
                            first_start = result['start']
                        elif first_start > result['start']:
                            first_start = result['start']

                        if not last_end:
                            last_end = result['end']
                        elif last_end < result['end']:
                            last_end = result['end']

                    writer.writerow(to_write)
                    pprint.pprint(to_write)

                print()
                if finished:
                    print('First start:', first_start)
                    print('Last end:', last_end)
                    runtime = datetime.datetime.fromisoformat(last_end) - datetime.datetime.fromisoformat(first_start)
                    print('Runtime:', runtime)
                else:
                    print('Processes not finished yet!')


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'action',
        help='Action to be performed',
        type=str,
        choices=['start', 'status', 'download']
    )
    parser.add_argument(
        '--videos',
        help='File listing video paths.',
        type=str
    )

    args = parser.parse_args()

    if args.videos:
        videos_file_path = args.videos
    else:
        videos_file_path = 'docs/evaluation/speed/resources/test.txt'

    speed_evaluator = SpeedEvaluator(videos_file_path)

    if args.action == 'start':
        speed_evaluator.start_processes()
    elif args.action == 'download':
        speed_evaluator.download_videos()
    else:
        speed_evaluator.get_results()


if __name__ == '__main__':
    main()
