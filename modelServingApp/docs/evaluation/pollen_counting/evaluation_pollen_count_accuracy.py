"""
Download video and annotation file from cloud storage and set paths accordingly

Run evaluation:
export PYTHONPATH=$PYTHONPATH:/home/vmadmin/beewatch/modelServingApp
sh environments/dev/install_dependencies.sh
. venv/bin/activate
python docs/evaluation/pollen_counting/evaluation_pollen_count_accuracy.py
"""

import json
from enum import Enum
from pathlib import Path
from types import SimpleNamespace

from app.models.annotation_type import AnnotationType
from app.models.tracker_event import Direction
from app.trackers.bee_tracker import BeeTracker
from app.trackers.pollen_tracker import PollenTracker
from app.util import get_annotation_set

base_path = Path('/home/anna/Schreibtisch/evaluation/accuracy/model/')

# 1330-1335

# sony
video1 = [
    '1330-1335_ILCE_6500_100fps_side_view_C0007.mp4',
    'video_1_20210704-21-39-05-041311.json',
    {'x1': 125, 'y1': 382, 'x2': 1746, 'y2': 910, 'x3': 1755, 'y3': 851, 'x4': 127, 'y4': 350}
]

# canon
video2 = [
    '1330-1335_G7X_50fps_front_view_MVI_1767.mp4',
    'video_1_20210703-17-39-33-792537.json',
    {'x1': 118, 'y1': 464, 'x2': 118, 'y2': 497, 'x3': 1701, 'y3': 505, 'x4': 1703, 'y4': 476}
]

# gopro
video3 = [
    '1330-1335_gopro_200fps_vertical_view_GX015848_1dark_cropped.mp4',
    'video_1_20210704-19-39-18-292257.json',
    {'x1': 1311, 'y1': 444, 'x2': 82, 'y2': 446, 'x3': 78, 'y3': 401, 'x4': 1305, 'y4': 399}
]

videos = [video2]


class OFFSET(int, Enum):
    NO = 0
    YES = 30


def print_counts(video):
    bees_in = 0
    bees_out = 0
    pollen = 0

    for image in video.images:
        bee_annotation_set = get_annotation_set(image, AnnotationType.BEE)
        for tracker_event in bee_annotation_set.tracker_events:
            if tracker_event.direction == Direction.IN:
                bees_in += 1
            elif tracker_event.direction == Direction.OUT:
                bees_out += 1

            if tracker_event.pollen:
                pollen += 1

    print('Ingoing Bees:', bees_in)
    print('Outgoing Bees:', bees_out)
    print('Pollen:', pollen)


for video_name, annotations_name, entrance_coords in videos:
    video_path = base_path / video_name
    annotations_path = base_path / annotations_name

    if not video_path.exists():
        print('Video does not exist!')

    with open(annotations_path) as f:
        video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))

        for image in video.images:
            # set the entrance coordinates
            entrance_aset = get_annotation_set(image, AnnotationType.ENTRANCE)
            for annotation in entrance_aset.annotations:
                annotation.vector = entrance_coords

            # reset tracker data
            bee_aset = get_annotation_set(image, AnnotationType.BEE)
            bee_aset.tracker_events = []
            for annotation in bee_aset.annotations:
                annotation.object_id = None

        video.local_path = str(video_path)

        BeeTracker(
            video,
            #visualized=True,
            offset=OFFSET.NO,
            fps=25,
            skip=2
        ).track()

        PollenTracker(video).track2()

        print_counts(video)
