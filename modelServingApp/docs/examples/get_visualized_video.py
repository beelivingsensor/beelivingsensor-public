import json
from types import SimpleNamespace

from app.trackers.bee_tracker import BeeTracker

model_annotations_path = '/home/anna/Schreibtisch/video_11_20210524-11-52-40-901859.json'
video_path = '/home/anna/Schreibtisch/1min.mp4'

with open(model_annotations_path) as f:
    video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
    video.local_path = video_path
    BeeTracker(video, visualized=True).track()
