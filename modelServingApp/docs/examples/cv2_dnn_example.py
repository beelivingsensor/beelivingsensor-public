import cv2
from datetime import datetime
import subprocess


def benchmark(cfg_path, weights_path, image_path, width, height, iterations):
    net = cv2.dnn.readNetFromDarknet(str(cfg_path), str(weights_path))

    # use GPU
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    frame = cv2.imread(str(image_path))

    start_time = datetime.now()
    for i in range(iterations):
        inpBlob = cv2.dnn.blobFromImage(
            frame,
            1.0 / 255,
            (width, height),
            (0, 0, 0),
            swapRB=True,
            crop=False)
        net.setInput(inpBlob)
        output = net.forward(ln)
    end_time = datetime.now()

    print('FPS:', iterations / (end_time - start_time).seconds)


# bee model test
bee_config_path = 'bee.cfg'
bee_weights_path = 'bee.weights'
hive_image_path = 'beehive.jpg'
subprocess.run(f'wget -q -O {bee_weights_path} https://beelivingsensor.blob.core.windows.net/public/models/bee/bee_medium.weights', shell=True)
subprocess.run(f'wget -q -O {bee_config_path} https://beelivingsensor.blob.core.windows.net/public/models/bee/bee.cfg', shell=True)
subprocess.run(f'wget -q -O {hive_image_path} https://beelivingsensor.blob.core.windows.net/static/Doettingen.jpg', shell=True)
print('Bee model:')
benchmark(bee_config_path, bee_weights_path, hive_image_path, 608, 608, 500)


# pollen model test
pollen_config_path = 'pollen.cfg'
pollen_weights_path = 'pollen.weights'
bee_image_path = 'bee.jpg'
subprocess.run(f'wget -q -O {pollen_weights_path} https://beelivingsensor.blob.core.windows.net/public/models/pollen/pollen_tiny.weights', shell=True)
subprocess.run(f'wget -q -O {pollen_config_path} https://beelivingsensor.blob.core.windows.net/public/models/pollen/pollen_tiny.cfg', shell=True)
subprocess.run(f'wget -q -O {bee_image_path} https://beelivingsensor.blob.core.windows.net/static/pollen/pollen5.jpg', shell=True)
print('Pollen model:')
benchmark(pollen_config_path, pollen_weights_path, bee_image_path, 224, 224, 5000)
