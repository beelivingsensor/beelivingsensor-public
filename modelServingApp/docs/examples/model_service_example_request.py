import sys
import time
from pprint import pprint

import requests


MEDIA_PK = 1
MEDIA_BLOB_PATH = 'static/1_sec_Doettingen_Hive_1_M_Rec_20200427_094708_540_M.mp4'
MEDIA_TYPE = 'video'
ENTRANCE_COORDINATES = {'x1': 767, 'x2': 1603, 'y1': 487, 'y2': 585}

BEE_MODEL_PK = 5
BEE_MODEL_BLOB_PATH = 'public/models/bee/bee_medium.weights'

POLLEN_MODEL_PK = 8
POLLEN_MODEL_BLOB_PATH = 'public/models/pollen/pollen_tiny.weights'

BACK_URL = '/non-existing-endpoint/'
SKIP = 1
PROFILING = False


def run_example():
    proc_id = start_proc()

    status = 'PENDING'
    while status in ['PENDING', 'STARTED']:
        time.sleep(60)
        info = get_proc_info(proc_id)
        pprint(info)
        status = info['status']


def start_proc():
    response = requests.post(
        f'http://0.0.0.0:8001/media/processes/',
        json={
            'media_pk': MEDIA_PK,
            'media_blob_path': MEDIA_BLOB_PATH,
            'media_type': MEDIA_TYPE,
            'bee_model_pk': BEE_MODEL_PK,
            'bee_model_blob_path': BEE_MODEL_BLOB_PATH,
            'pollen_model_pk': POLLEN_MODEL_PK,
            'pollen_model_blob_path': POLLEN_MODEL_BLOB_PATH,
            'entrance_coordinates': ENTRANCE_COORDINATES,
            'back_url': BACK_URL,
            'skip': SKIP,
            'profiling': PROFILING
        })

    if response.status_code == 201:
        print(f'Request to modelserving app successful!')
        return response.json()
    else:
        print(f'Request to modelserving app failed!')
        sys.exit(1)


def get_proc_info(id_):
    response = requests.get(
        f'http://0.0.0.0:8001/media/processes/{id_}',
        timeout=10
    )
    return response.json()


if __name__ == '__main__':
    run_example()
