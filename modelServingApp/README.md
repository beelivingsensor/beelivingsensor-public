# Model-Service
For analyzing videos of beehives using AI.

## Run Application

You can run the application in different environments. We provide
Docker files for [CPU](docker-with-cpu) and [GPU](docker-with-gpu) versions. 
We also provide [convenience scripts](without-docker) for starting 
the application without Docker in your development environment.

Before you run the application, create a file called `.env` in the root
directory of the model-service, containing the following environment variables:

* `AZURE_STORAGE_CONNECTION_STRING`: Azure cloud storage connection string 
  for downloading and uploading blobs.
* `WEBAPP`: The host accepting the media process response.
* `WEBAPP_API_KEY`: The API key for sending the media process response 
  to the host.
* `CELERY_BROKER_URL`: URL for celery broker (redis)
* `CELERY_RESULT_BACKEND`: URL for celery backend (redis)


### Docker with GPU

------------------

Tested on Azure:\
Standard NV6_Promo (6 vcpus, 56 GiB memory)\
Linux (Ubuntu 18.04)\
East US

------------------

#### Without Kubernetes:

Configure the GPU node:
```shell
sh environments/gpu/configure_gpu_machine.sh
```
Run container with docker-compose:
```shell
sudo docker-compose -f environments/gpu/docker-compose.yaml up
```

#### On a Kubernetes cluster:

Configure the GPU node:
```shell
sh environments/gpu/configure_gpu_machine_kubernetes.sh
```
Configure master node:
```shell
kubectl create -f k8sConfigs/nvidia-device-plugin.yml
```

### Docker with CPU

```shell
sudo docker-compose -f environments/cpu/docker-compose.yaml up
```

### Without Docker

```shell
sh environments/dev/start_dev_server.sh
sh environments/dev/start_celery_worker_server.sh 
```

## Testing
Hello World
```shell
curl 0.0.0.0:8001
```
Media processing (adapt blob paths)
```shell
python3 docs/examples/model_service_example_request.py
```

Unit tests:

Docker
```shell
sudo docker-compose -f environments/cpu/docker-compose.yaml exec model-serving python -m pytest -s
```

Development:
```shell
sh environments/dev/run_unittests.sh 
```

## Endpoints
You can find the documentation for the endpoints 
by browsing the `docs` endpoint, e.g.:

```
http://127.0.0.1:8001/docs
```