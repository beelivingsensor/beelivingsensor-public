from django.conf import settings
from django.db import models
from django.apps import apps

from main.models import Apiary
from main.models.machine_learning.model.validation import Validation


class Hive(models.Model):
    """Represents a beehive."""
    # To which apiary it belongs
    apiary = models.ForeignKey(Apiary, on_delete=models.CASCADE)
    # Who added the beehive
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                related_name="origin_owner_hive")
    # A human-readable name for the beehive (e.g. a number or a color)
    name = models.CharField(max_length=200)
    # Whether the data may be used for research purposes
    # TODO: move this up to the apiary level
    private = models.BooleanField(default=True)
    # original name on Google Drive
    google_drive_name = models.CharField(max_length=50, null=True, blank=True)
    # most similar beehive
    similar_beehive = models.ForeignKey('Hive', on_delete=models.SET_NULL,
                                        null=True, blank=True)

    # Whether a new model should be created
    retrain = models.BooleanField(default=False)

    # Shape of entrance
    class Entrance(models.TextChoices):
        CIRCULAR = 'circular'
        RECTANGULAR = 'rectangular'

    entrance = models.CharField(max_length=20, choices=Entrance.choices)

    class Meta:
        unique_together = ('apiary', 'name')

    def isManagedBy(self, user):
        return self.apiary.isManagedBy(user)

    @property
    def bee_model(self):
        """Get best bee model as determined by validation."""
        validations = Validation.objects\
            .filter(hive=self, model__annotation_type__name='bee')\
            .order_by('-f1')
        if validations:
            return validations.first().model

        return None

    @property
    def has_enough_hive_images(self):
        image_count = apps.get_model('main.Image').objects\
            .filter(image_set__video__beehive=self,
                    type=apps.get_model('main.Image').TYPE.HIVE)\
            .count()
        return image_count > 5

    @property
    def has_validation_images(self):
        validation_images = apps.get_model('main.Image').objects.filter(
            beehive=self,
            status=apps.get_model('main.Image').STATUS.VALIDATE)
        return len(validation_images) > 0

    @property
    def tasks(self):
        tasks = apps.get_model('main.HiveTask').objects.filter(hive=self)
        open_tasks = [task for task in tasks if not task.task.finished]
        return open_tasks

    @property
    def entrance_annotation(self):
        """The annotation of the hive entrance."""
        annotation_type = apps.get_model('main.AnnotationType').objects\
            .get(name='entrance')
        annotations = apps.get_model('main.GoldAnnotation').objects\
            .filter(annotation_set__image__beehive=self,
                    annotation_set__annotation_type=annotation_type)\
            .order_by('-pk')

        if annotations:
            return annotations.first()

        return None

    def __str__(self):
        return f'({self.apiary.name}) {self.name} - {self.pk}'
