from django.db import models
from typing import Union


class AnnotationType(models.Model):
    class VECTOR_TYPE():
        BOUNDING_BOX = 1
        POINT = 2
        LINE = 3
        MULTI_LINE = 4
        POLYGON = 5

    name = models.CharField(max_length=20, unique=True)
    active = models.BooleanField(default=True)
    vector_type = models.IntegerField(default=VECTOR_TYPE.BOUNDING_BOX)
    # Number of required nodes (in polygon and multiline) 0->unspecified
    node_count = models.IntegerField(default=0)
    enable_concealed = models.BooleanField(default=True)
    enable_blurred = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @staticmethod
    def get_vector_type_name(vector_type):
        if vector_type is AnnotationType.VECTOR_TYPE.BOUNDING_BOX:
            return 'Bounding Box'
        if vector_type is AnnotationType.VECTOR_TYPE.POINT:
            return 'Point'
        if vector_type is AnnotationType.VECTOR_TYPE.LINE:
            return 'Line'
        if vector_type is AnnotationType.VECTOR_TYPE.POLYGON:
            return 'Polygon'
        if vector_type is AnnotationType.VECTOR_TYPE.MULTI_LINE:
            return 'Multi Line'

    def validate_vector(self, vector: Union[dict, None]) -> bool:
        """
        Validate a vector. Returns whether the vector is valid.
        Necessary type casts are done in-place within the dictionary.
        """
        if vector is None:
            # not in image
            return True
        if not isinstance(vector, dict):
            return False
        # converting vector elements to integers
        for key, value in vector.items():
            try:
                vector[key] = int(value)
            except ValueError:
                return False
        if self.vector_type == AnnotationType.VECTOR_TYPE.BOUNDING_BOX:
            return self._validate_bounding_box(vector)
        if self.vector_type == AnnotationType.VECTOR_TYPE.LINE:
            return self._validate_line(vector)
        if self.vector_type == AnnotationType.VECTOR_TYPE.POINT:
            return self._validate_point(vector)
        if self.vector_type == AnnotationType.VECTOR_TYPE.POLYGON:
            return self._validate_polygon(vector)
        if self.vector_type == AnnotationType.VECTOR_TYPE.MULTI_LINE:
            return self._validate_multi_line(vector)

        # No valid vector type given.
        return False

    def _validate_bounding_box(self, vector: dict) -> bool:
        return (
                       vector.get('x2', float('-inf')) -
                       vector.get('x1', float('inf')) >= 1) and (
                       vector.get('y2', float('-inf')) -
                       vector.get('y1', float('inf')) >= 1) and \
               len(vector.keys()) is 4

    def _validate_line(self, vector: dict) -> bool:
        return (
                vector.get('x2', float('inf')) is not
                vector.get('x1', float('inf')) or
                vector.get('y2', float('inf')) is not
                vector.get('y1', float('inf')) and
                len(vector.keys()) is 4
        )

    def _validate_point(self, vector: dict) -> bool:
        return 'x1' in vector and 'y1' in vector and len(vector.keys()) is 2

    def _validate_polygon(self, vector: dict) -> bool:
        if len(vector) < 6:
            return False  # A polygon vector has to have at least 3 nodes
        if not (self.node_count is 0 or
                self.node_count is int(len(vector) // 2)):
            return False
        for i in range(1, int(len(vector) // 2) + 1):
            for j in range(1, int(len(vector) // 2) + 1):
                if i is not j and \
                        (vector['x' + str(i)] is vector['x' + str(j)] and
                         vector['y' + str(i)] is vector['y' + str(j)]):
                    return False
        return True

    def _validate_multi_line(self, vector: dict) -> bool:
        if len(vector) < 4:
            return False  # A multi line vector has to have at least 2 nodes
        if not (self.node_count is 0 or
                self.node_count is int(len(vector) // 2)):
            return False
        for i in range(1, int(len(vector) // 2) + 1):
            for j in range(1, int(len(vector) // 2) + 1):
                if i is not j and \
                        (vector['x' + str(i)] is vector['x' + str(j)] and
                         vector['y' + str(i)] is vector['y' + str(j)]):
                    return False
        return True
