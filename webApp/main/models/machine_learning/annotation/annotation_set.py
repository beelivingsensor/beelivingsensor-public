from django.db import models


class AnnotationSet(models.Model):
    """Represents the set of annotations on an image."""

    # which image is annotated
    image = models.ForeignKey('main.Image', on_delete=models.CASCADE)

    # what type of annotations are created on the images
    annotation_type = models.ForeignKey(
        'AnnotationType',on_delete=models.PROTECT)

    class Meta:
        abstract = True
