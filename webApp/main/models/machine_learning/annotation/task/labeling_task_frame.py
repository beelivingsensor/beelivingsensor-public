from django.db import models
from django.apps import apps


class LabelingTaskFrame(models.Model):
    """Represents a frame that needs to be labeled as part of a task."""
    frame = models.ForeignKey('main.Image', on_delete=models.CASCADE)
    task = models.ForeignKey('main.Task', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['frame', 'task']

    @property
    def labeled_by(self):
        return apps.get_model('main.User').objects.filter(
            userannotationset__image=self.frame,
            userannotationset__finished=True,
            userannotationset__annotation_type=self.task.annotation_type)
