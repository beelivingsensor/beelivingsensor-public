from django.db import models
from django.utils.translation import gettext_lazy as _


class HiveTask(models.Model):
    """Represents a labeling task for images of a specific hive.

    Typical hive tasks are:
    - labeling validation frames to find the best model for a new beehive
    - labeling training frames for training a new model for a new beehive
    - labeling the hive entry to optimize the bee tracker algorithm

    This model is useful because it allows to determine what kind of task
    it is and take appropriate action (e.g. start the validation over all
    models or start the training of a new model).
    """
    task = models.ForeignKey(
        'main.Task',
        on_delete=models.CASCADE,
        unique=True,
        verbose_name=_('task'),
    )
    hive = models.ForeignKey(
        'main.Hive',
        on_delete=models.CASCADE,
        verbose_name=_('beehive'),
    )

    class Type(models.TextChoices):
        VALIDATION = _('bee labeling for validation')
        TRAINING = _('bee labeling for training')
        ENTRANCE = _('entrance labeling for tracker')

    type = models.CharField(
        max_length=100,
        choices=Type.choices,
        verbose_name=_('type'),
    )
