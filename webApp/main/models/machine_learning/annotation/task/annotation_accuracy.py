from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class AnnotationAccuracy(models.Model):
    """Represents an accuracy that a person assigned for the annotations
    of a given image that a model made.

    This table is useful for knowing the annotation accuracy of an AI model.
    This allows to determine which frames need to be corrected.
    """
    # The person who assigns the accuracy
    checker = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    # The image whose annotations are checked
    image = models.ForeignKey('main.Image', on_delete=models.CASCADE)
    # The AI model that made the annotations
    model = models.ForeignKey('main.AIModel', on_delete=models.CASCADE, null=True,
                              blank=True)

    class AccuracyChoices(models.TextChoices):
        HIGH = 'high', _('All labels correct')
        MEDIUM = 'medium', _('Some labels correct')
        LOW = 'low', _('Most labels incorrect')

    accuracy = models.CharField(choices=AccuracyChoices.choices,
                                max_length=100)