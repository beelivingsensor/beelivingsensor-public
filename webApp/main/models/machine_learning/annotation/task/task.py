from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from main.models import LabelingTaskFrame, HiveTask


class Task(models.Model):
    """Represents a generic task for labeling images.

    Images for this task are added in the model `LabelingTaskFrame`.
    """
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        verbose_name=_('creator')
    )

    annotation_type = models.ForeignKey(
        'main.AnnotationType',
        on_delete=models.PROTECT,
        verbose_name=_('annotation type')
    )

    @property
    def finished(self):
        """Check if all task frames have been labeled by enough people"""
        for task_frame in LabelingTaskFrame.objects.filter(task=self):
            if len(task_frame.labeled_by) < 2:
                return False
        else:
            return True

    @property
    def annotators(self):
        """Users who have finished the task."""
        annotators = {}
        image_count = 0
        for task_image in LabelingTaskFrame.objects.filter(task=self):
            image_count += 1
            for user in task_image.labeled_by:
                if user not in annotators:
                    annotators[user] = 0
                annotators[user] = annotators[user] + 1

        return [user for user in annotators
                if annotators[user] == image_count]

    def is_hive_task(self):
        return HiveTask.objects.filter(task=self).exists()

    @property
    def description(self):
        if self.is_hive_task():
            hive_task = HiveTask.objects.get(task=self)
            return _('{} for hive {}').format(_(hive_task.type), hive_task.hive)

        return _('Label {} in images').format(self.annotation_type)
