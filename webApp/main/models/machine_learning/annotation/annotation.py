from django.db.models import JSONField
from django.db import models


class Annotation(models.Model):
    # {'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2}
    vector = JSONField(null=True)

    time = models.DateTimeField(auto_now_add=True)

    last_edit_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        permissions = [
            ('can_annotate_images', 'Can annotate images')
        ]
