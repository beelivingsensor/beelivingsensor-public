from django.conf import settings
from django.db import models


class VerifiedAnnotations(models.Model):
    """Represents a frame whose annotations a user verified.

    This needed to know which images are finished annotating and which not.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    frame = models.ForeignKey('main.Image', on_delete=models.CASCADE)
    annotation_type = models.ForeignKey('main.AnnotationType',
                                        on_delete=models.PROTECT)
