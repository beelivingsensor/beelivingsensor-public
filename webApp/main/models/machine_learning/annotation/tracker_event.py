from django.contrib.postgres.fields import ArrayField
from django.db import models


class TrackerEvent(models.Model):
    # The ID assigned by the tracker to an object
    object_id = models.IntegerField()

    # Whether the bee flies in the hive or out of the hive
    class DirectionChoices(models.TextChoices):
        IN = 'in'
        OUT = 'out'

    direction = models.CharField(
        max_length=10,
        choices=DirectionChoices.choices
    )

    # In which image's annotation set the event happens
    annotation_set = models.ForeignKey(
        'main.ModelAnnotationSet',
        on_delete=models.CASCADE
    )

    # The pollen colors (encoded in hex) carried into the hive
    pollen = ArrayField(
        models.CharField(max_length=20),
        default=list
    )
