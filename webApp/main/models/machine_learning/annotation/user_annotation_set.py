from django.conf import settings
from django.db import models

from main.models.machine_learning.annotation.annotation_set import \
    AnnotationSet


class UserAnnotationSet(AnnotationSet):
    """Represents the set of annotations on an image that a user creates."""

    # who annotated the image
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    # whether the user has finished annotating the image
    finished = models.BooleanField(default=False)

    class Meta:
        unique_together = ('image', 'annotation_type', 'user')
