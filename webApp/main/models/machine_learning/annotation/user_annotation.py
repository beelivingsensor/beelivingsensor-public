from django.db import models

from main.models import Annotation


class UserAnnotation(Annotation):
    """Represents an annotation created by a user."""

    # To which annotation set the annotation belongs to
    annotation_set = models.ForeignKey(
        'main.UserAnnotationSet',
        on_delete=models.CASCADE,
        related_name='annotations'
    )
