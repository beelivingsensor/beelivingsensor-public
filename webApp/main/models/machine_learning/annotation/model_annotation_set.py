from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from main.models.machine_learning.annotation.annotation_set import \
    AnnotationSet


class ModelAnnotationSet(AnnotationSet):
    """Represents the set of annotations on an image that a model creates."""

    # which model created the annotations
    model = models.ForeignKey('main.AIModel', on_delete=models.CASCADE)

    # how accurate the annotations are
    class AccuracyChoices(models.TextChoices):
        HIGH = 'high', _('All labels correct')
        MEDIUM = 'medium', _('Some labels correct')
        LOW = 'low', _('Most labels incorrect')

    accuracy = models.CharField(
        choices=AccuracyChoices.choices,
        max_length=100,
        null=True,
        blank=True)

    # the person who assigns the accuracy
    verifier = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    class Meta:
        unique_together = ('image', 'annotation_type', 'model')
