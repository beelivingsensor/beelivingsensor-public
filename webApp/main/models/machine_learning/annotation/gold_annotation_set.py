from main.models.machine_learning.annotation.annotation_set import \
    AnnotationSet


class GoldAnnotationSet(AnnotationSet):
    """Represents the final set of annotations on an image."""

    class Meta:
        unique_together = ('image', 'annotation_type')
