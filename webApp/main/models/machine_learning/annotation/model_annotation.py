from django.db import models

from main.models import Annotation


class ModelAnnotation(Annotation):
    """Represents an annotation made by a model."""

    # To which annotation set the annotation belongs to
    annotation_set = models.ForeignKey(
        'main.ModelAnnotationSet',
        on_delete=models.CASCADE,
        related_name='annotations'
    )

    # How sure the model is about its annotation
    confidence = models.FloatField(default=-1)

    # The ID of the object being annotated assigned by the tracker
    object_id = models.IntegerField(null=True, blank=True)

    # The image cropped based on this annotation
    image = models.OneToOneField(
        'main.Image',
        on_delete=models.SET_NULL,
        null=True,
        blank=True)

    # The color (only for pollen)
    color = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
