from django.db import models


class AIModel(models.Model):
    """Represents an AI model that labels frames of videos.

    This table is useful to know what AI models are available to label
    frames of a video.
    """
    description = models.TextField()

    annotation_type = models.ForeignKey('main.AnnotationType',
                                        on_delete=models.PROTECT,
                                        default=1)

    class TrainingMode(models.TextChoices):
        INDIVIDUAL = 'individual_training'
        SINGLE = 'single_hive'
        QUANTITY = 'on_qty_of_train_images'
        STAGED = 'staged_2'

    # the way the model was trained
    training_mode = models.CharField(max_length=50,
                                     choices=TrainingMode.choices, null=True,
                                     blank=True)

    # Where the AI model is stored on the cloud
    path = models.CharField(unique=True, max_length=300)
    # When the model was created
    time = models.DateTimeField(null=True, blank=True)
    # original path on Google Drive
    google_drive_path = models.CharField(max_length=200, null=True, blank=True,
                                         unique=True)

    class Meta:
        permissions = [
            ("can_request_retraining", "Can request AI Model retraining")
        ]

    def __str__(self):
        return f'{self.description}'