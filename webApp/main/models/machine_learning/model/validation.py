from django.db import models


class Validation(models.Model):
    model = models.ForeignKey('main.AIModel', on_delete=models.CASCADE)
    hive = models.ForeignKey('main.Hive', on_delete=models.CASCADE)
    scene = models.CharField(default='default', max_length=50)
    ground_truth = models.IntegerField()
    predicted = models.IntegerField()
    tp = models.IntegerField()
    fp = models.IntegerField()
    fn = models.IntegerField()
    precision = models.FloatField()
    recall = models.FloatField()
    f1 = models.FloatField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('model', 'hive', 'scene')

    @staticmethod
    def compute_f1(precision, recall):
        if precision > 0 or recall > 0:
            f1 = 2 * (precision * recall) / (precision + recall)
        else:
            f1 = 0

        return f1
