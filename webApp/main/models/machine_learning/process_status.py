from django.db import models
from django.utils.translation import gettext_lazy as _


class ProcessStatus(models.TextChoices):
    # celery states
    PROCESS_PENDING = _('PROCESS PENDING')
    PROCESS_STARTED = _('PROCESS STARTED')
    PROCESS_FAILURE = _('FAILURE: PROCESS')
    PROCESS_RETRY = _('PROCESS RETRY')
    PROCESS_REVOKED = _('PROCESS REVOKED')
    PROCESS_SUCCESS = _('PROCESS SUCCESS')

    # custom states
    # model-service is unavailable
    UNAVAILABLE = _('FAILURE: MODEL-SERVICE UNAVAILABLE')
    # the celery worker finished successfully but
    # could not notify the webapp about it
    NOTIFICATION_FAILURE = _('FAILURE: MODEL-SERVICE NOTIFICATION')
    # webapp could not send the request to the model-service
    REQUEST_FAILURE = _('FAILURE: WEBAPP REQUEST')

    @staticmethod
    def map_celery_state(celery_state):
        """Map the celery state to our state."""
        mapping = {
            'PENDING': ProcessStatus.PROCESS_PENDING,
            'STARTED': ProcessStatus.PROCESS_STARTED,
            'FAILURE': ProcessStatus.PROCESS_FAILURE,
            'RETRY': ProcessStatus.PROCESS_RETRY,
            'REVOKED': ProcessStatus.PROCESS_REVOKED,
            'SUCCESS': ProcessStatus.PROCESS_SUCCESS
        }
        return mapping.get(celery_state, None)
