from django.db import models

from main.models import Hive


class HiveSensorDevice(models.Model):
    # at which hive the device is placed
    hive = models.ForeignKey(Hive, on_delete=models.CASCADE)
    # serial number of device
    sn = models.CharField(max_length=100, blank=True, null=True)

    # the brand of the sensor, e.g. CAPAZ
    brand = models.CharField(max_length=50, blank=True)
    COMMON_BRANDS = ['CAPAZ', 'arnia', 'hivewatch', 'beehivemonitoring']

    # the model of the brand, e.g. BEE HIVE SCALE GSM 200
    model = models.CharField(max_length=50)
    # additional information about the device
    description = models.TextField(null=True, blank=True)
    # link to the device page of the device manufacturer
    # Example: https://direct.capaz.de/Webdirekt/PublicViewServlet.html?URL=a38a5461-6851-4d59-9b37-0c3d1a46b0506282f6e0-7d35-49a6-8fcf-8828df594ed4#JuDatenPublicView
    link = models.URLField(null=True, blank=True)

    class Meta:
        unique_together = ('hive', 'brand', 'model')

    def isManagedBy(self, user):
        return self.hive.isManagedBy(user)
