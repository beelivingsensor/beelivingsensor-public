from django.db import models


class SensorType(models.Model):
    """Represents a sensor parameter.

    Typical beehive sensors parameters are weight, temperature, etc.
    """
    # the name of the paramter, e.g. temperature
    name = models.CharField(max_length=500, unique=True)
    # in which unit the parameter values are, e.g. °
    unit = models.CharField(max_length=15)
    # provides more detailed explanation of the parameter
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.name}'
