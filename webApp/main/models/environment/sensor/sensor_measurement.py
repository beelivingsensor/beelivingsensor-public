from django.db import models


class SensorMeasurement(models.Model):
    """Represents a measurement taken by a sensor."""
    # which sensor took the measurement
    sensor = models.ForeignKey('main.HiveSensor', on_delete=models.CASCADE)
    # when the measurement was taken
    time = models.DateTimeField()
    # the actual measurement
    value = models.FloatField()

    def isManagedBy(self, user):
        return self.sensor.isManagedBy(user)
