from django.db import models


class HiveSensor(models.Model):
    """Represents a sensor for a hive.

    The sensor delivers measurements for a given parameter such as
    temperature, weight, etc.
    """
    # in which device the sensor is integrated
    device = models.ForeignKey('main.HiveSensorDevice', on_delete=models.CASCADE)
    # what parameter the sensor measures, e.g. temperature
    sensor_type = models.ForeignKey('main.SensorType', on_delete=models.PROTECT)
    # additional information about the sensor
    description = models.TextField(null=True, blank=True)
    # whether sensor produces valid measurements (the sensor might be broken)
    # expressed as a time range in which the sensor produces valid measurements
    # None means infinity
    valid_from = models.DateTimeField(null=True, blank=True)
    valid_until = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = ('device', 'sensor_type')

    def isManagedBy(self, user):
        return self.device.isManagedBy(user)
