from django.db import models


class WeatherParameter(models.Model):
    # the name of the parameter, e.g. temperature
    name = models.CharField(max_length=100, unique=True)
    # the unit in which the parameter values will be, e.g. degrees
    unit = models.CharField(max_length=15)
    # an explanation of the parameter
    description = models.TextField()

    def __str__(self):
        return self.name
