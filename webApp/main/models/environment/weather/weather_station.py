from django.db import models
from django.contrib.gis.db import models as postgis_models


class WeatherStation(models.Model):
    # ID of OpenWeatherMap
    OWM_id = models.IntegerField(unique=True)
    # GPS coordinates of weather station
    location = postgis_models.PointField(unique=True)
    # the name of the weather station, e.g. Glarus
    name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name} ({self.location.x}, {self.location.y})'
