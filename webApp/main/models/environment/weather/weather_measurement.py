from django.db import models


class WeatherMeasurement(models.Model):
    """Represents weather data for a given apiary at a given time.

    OpenWeatherMap seems to compute the weather data based on one
    weather station's data. It is not documented how it is exactly computed,
    but getting weather data for a specific weather station and getting
    weather data for the apiary's location that is close to that weather
    station yields different weather data:

    api.openweathermap.org/data/2.5/weather?id=6252014&appid={}&mode=xml
    api.openweathermap.org/data/2.5/weather?lat=47.34&lon=8.48&appid={}&mode=xml
    """
    # for which apiary the measurement was taken
    apiary = models.ForeignKey('main.Apiary', on_delete=models.CASCADE)
    # which weather station's data was used
    weather_station = models.ForeignKey('main.WeatherStation',
                                        on_delete=models.PROTECT)
    # last time data was updated
    last_update = models.DateTimeField(null=True)
    # when this measurement was added
    time = models.DateTimeField(auto_now_add=True)
    # what parameter was measured
    parameter = models.ForeignKey('main.WeatherParameter', on_delete=models.PROTECT)
    # the actual measurement
    value = models.FloatField()

    class Meta:
        unique_together = (
        'apiary', 'weather_station', 'last_update', 'parameter')
