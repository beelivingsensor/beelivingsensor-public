from django.conf import settings
from django.db import models


class CommonName(models.Model):
    """Common names for plants in different languages."""

    # Which plant the name is refering to
    plant = models.ForeignKey(
        'main.Plant',
        models.CASCADE
    )

    # In which language the common name is
    language = models.CharField(
        max_length=10,
        choices=settings.LANGUAGES
    )

    # The common name
    name = models.CharField(
        max_length=300
    )

    class Meta:
        unique_together = ('name', 'language', 'plant')
