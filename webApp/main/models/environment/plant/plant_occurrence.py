from django.db import models
from django.contrib.gis.db import models as postgis_models


class PlantOccurrence(models.Model):
    """Table holding the data of Info Flora."""

    plant = models.ForeignKey(
        'main.Plant',
        on_delete=models.CASCADE
    )

    # Info Flora: atlas units (x, y) (region in which plant occurs)
    region = postgis_models.PointField()

    # Info Flora: obs_nb (number of observations in this region)
    obs_nb = models.IntegerField(
        default=0
    )

    class Doubt(models.IntegerChoices):
        """Degree of doubt of whether plant occurs in region."""

        # at least one observation for the specified taxon in the specified
        # atlas unit is considered as being valid
        ZERO = 0
        # observations for the specified taxon in the specified atlas unit
        # have not yet been validated,
        ONE = 1
        # all observations for the specified taxon in the specified atlas unit
        # are considered doubtful
        TWO = 2
        # all observations for the specified taxon in the specified atlas unit
        # are considered false.
        THREE = 3

    # Info Flora: doubt
    doubt = models.IntegerField(
        choices=Doubt.choices
    )

    # Info Flora: year_min (first year of observation of plant in this region)
    year_min = models.IntegerField()

    # Info Flora: year_max (last year of observation of region)
    year_max = models.IntegerField()

    class Meta:
        unique_together = ('plant', 'region')
