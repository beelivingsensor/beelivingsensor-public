from django.db import models
from django.contrib.gis.db import models as postgis_models


class Observation(models.Model):

    # iNaturalist: id
    inaturalist_id = models.IntegerField(
        unique=True
    )

    # iNaturalist: uri
    observation_url = models.URLField(
        unique=True
    )

    # iNaturalist: photos[0].url -> replace 'square' by 'large'
    # https://static.inaturalist.org/photos/112627725/square.jpeg?1613066643
    # https://static.inaturalist.org/photos/112627725/large.jpeg?1613066643
    image_url = models.URLField()

    # iNaturalist: photos[0].attribution
    image_attribution = models.CharField(
        max_length=300
    )

    # iNaturalist: user.login
    user = models.CharField(
        max_length=200
    )

    # iNaturalist: taxon
    plant = models.ForeignKey(
        'main.Plant',
        on_delete=models.PROTECT
    )

    # iNaturalist: created_at
    created_at = models.DateTimeField()

    # iNaturalist: time_observed_at
    observed_on = models.DateTimeField()

    # iNaturalist: location
    location = postgis_models.PointField()

    # iNaturalist: place_guess
    # name of the place (guessed)
    place_guess = models.CharField(
        max_length=400,
        null=True,
        blank=True
    )

    flowering = models.BooleanField(null=True, blank=True)

    # There are legitimate cases where multiple observations
    # with the same {location, observed_on, user} occur
    # e.g. https://www.inaturalist.org/observations/69052421
    # e.g. https://www.inaturalist.org/observations/69052378
    # class Meta:
    #     unique_together = ('location', 'observed_on', 'user')
