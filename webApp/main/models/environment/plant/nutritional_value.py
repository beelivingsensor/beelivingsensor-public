from django.db import models
from django.utils.translation import gettext_lazy as _


class NutritionalValue(models.TextChoices):
    VERY_LOW = 1, _('very low')
    LOW = 2, _('low')
    MEDIUM = 3, _('medium')
    HIGH = 4, _('high')
    VERY_HIGH = 5, _('very high')

    @staticmethod
    def map_grade(grade):
        mapping = {
            '': NutritionalValue.VERY_LOW,
            '*': NutritionalValue.LOW,
            '**': NutritionalValue.MEDIUM,
            '***': NutritionalValue.HIGH,
            '****': NutritionalValue.VERY_HIGH
        }
        return mapping[grade]

    @staticmethod
    def map_value(value):
        value = int(value)
        mapping = {
            0: NutritionalValue.VERY_LOW,
            1: NutritionalValue.LOW,
            2: NutritionalValue.MEDIUM,
            3: NutritionalValue.HIGH,
            4: NutritionalValue.VERY_HIGH
        }

        return mapping[value]
