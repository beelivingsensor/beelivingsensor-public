from django.db import models
from django.db.models import CheckConstraint, Q, F
from django.utils.translation import gettext_lazy as _

# Pritsch data

from main.models import NutritionalValue


class Nutrition(models.Model):

    class Food(models.TextChoices):
        NECTAR = _('nectar')
        POLLEN = _('pollen')

    plant = models.ForeignKey(
        'main.Plant',
        on_delete=models.CASCADE,
        verbose_name=_('plant')
    )

    food = models.CharField(
        choices=Food.choices,
        max_length=25,
        verbose_name=_('food')
    )

    value = models.CharField(
        choices=NutritionalValue.choices,
        max_length=25,
        verbose_name=_('value')
    )

    month = models.IntegerField(
        choices=[(i, i) for i in range(1,13)],
        verbose_name=_('month')
    )

    start_day = models.IntegerField(
        choices=[(i, i) for i in range(1, 31)],
        verbose_name=_('start day')
    )

    end_day = models.IntegerField(
        choices=[(i, i) for i in range(1, 31)],
        verbose_name=_('end day')
    )

    class Meta:
        CheckConstraint(
            check=Q(start_day__lt=F('end_day')),
            name='start_day_less_than_end_day')

        unique_together = (
            ('plant', 'food', 'month', 'start_day'),
            ('plant', 'food', 'month', 'end_day')
        )
