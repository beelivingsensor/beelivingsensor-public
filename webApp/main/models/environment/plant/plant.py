from django.db import models
from django.utils.translation import gettext_lazy as _

from main.models import NutritionalValue


class Plant(models.Model):

    # iNaturalist: id
    inaturalist_id = models.IntegerField(
        unique=True
    )

    # Scientific name
    # iNaturalist: name
    name = models.CharField(
        max_length=300
    )

    # Which taxonomy level
    class RANK(models.TextChoices):
        KINGDOM = 'kingdom', _('kingdom')
        PHYLUM = 'phylum', _('phylum')
        SUBPHYLUM = 'subphylum', _('subphylum')
        SUPERCLASS = 'superclass', _('superclass')
        CLASS = 'class', _('class')
        SUBCLASS = 'subclass', _('subclass')
        SUPERORDER = 'superorder', _('superorder')
        ORDER = 'order', _('order')
        SUBORDER = 'suborder', _('suborder')
        INFRAORDER = 'infraorder', _('infraorder')
        SUPERFAMILY = 'superfamily', _('superfamily')
        EPIFAMILY = 'epifamily', _('epifamily')
        FAMILY = 'family', _('family')
        SUBFAMILY = 'subfamily', _('subfamily')
        SUPERTRIBE = 'supertribe', _('supertribe')
        TRIBE = 'tribe', _('tribe')
        SUBTRIBE = 'subtribe', _('subtribe')
        GENUS = 'genus', _('genus')
        GENUSHYBRID = 'genushybrid', _('genushybrid')
        SPECIES = 'species', _('species')
        HYBRID = 'hybrid', _('hybrid')
        SUBSPECIES = 'subspecies', _('subspecies')
        VARIETY = 'variety', _('variety')
        FORM = 'form', _('form')

    # iNaturalist: rank
    rank = models.CharField(
        max_length=50,
        choices=RANK.choices
    )

    # The next higher taxon
    parent = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    # URL for more information about the plant, e.g. Wikipedia
    # iNaturalist: wikipedia_url
    info_url = models.URLField(
        max_length=500,
        null=True,
        blank=True
    )

    # URL of an example image
    # iNaturalist: default_photo.medium_url
    image_url = models.URLField(
        max_length=500,
        null=True,
        blank=True
    )

    # Attribution of the example image
    # iNaturalist: default_photo.attribution
    image_attribution = models.TextField(
        max_length=400,
        null=True,
        blank=True
    )

    nutrition_grade = models.CharField(
        choices=NutritionalValue.choices,
        max_length=25,
        verbose_name=_('nutritional value'),
        null=True,
        blank=True
    )

    # hex color value
    pollen_color = models.CharField(
        max_length=15,
        null=True,
        blank=True,
        verbose_name=_('pollen color')
    )

    @property
    def taxonomy(self):
        """Get taxonomy tree."""
        taxon = self
        taxa = []
        while taxon.parent:
            taxon = taxon.parent
            taxa.append(taxon)

        return reversed(taxa)

    class Meta:
        unique_together = ('name', 'rank', 'parent')
