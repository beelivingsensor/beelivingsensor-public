import datetime
import logging
import re

import requests
from django.conf import settings
from django.db import models

from main.models import ProcessStatus
from storage import VideoAzureStorage, ModelAnnotationsStorage

logger = logging.getLogger(__name__)


class Video(models.Model):
    """Represents a video uploaded by a user."""
    # Who uploaded the video
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)
    # Which beehive was filmed
    beehive = models.ForeignKey('main.Hive', on_delete=models.CASCADE)
    # Where the video is stored
    path = models.FileField(storage=VideoAzureStorage(), max_length=255)

    # When the video was uploaded
    uploaded = models.DateTimeField(auto_now_add=True)

    # When the video finished processing
    finished = models.DateTimeField(null=True, blank=True)

    # The set of images extracted from the video
    imageset = models.OneToOneField('main.ImageSet', on_delete=models.CASCADE)
    # original name on Google Drive
    google_drive_name = models.CharField(max_length=50, null=True, blank=True)

    # whether the video is still being processed (e.g. checks, extracting frames, predicting annotations, ...)
    processed = models.BooleanField(default=False)

    fileName = models.CharField(max_length=250, null=True, blank=True)

    # images per second
    fps = models.IntegerField(null=True, blank=True)

    # number of images
    image_count = models.IntegerField(null=True, blank=True)

    # video dimensions
    width = models.IntegerField(null=True, blank=True)
    height = models.IntegerField(null=True, blank=True)

    # size of video in bytes
    size = models.IntegerField(null=True, blank=True)

    # process ID of the model-service
    process_id = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )

    # path to model annotations JSON file
    annotations_path = models.FileField(
        storage=ModelAnnotationsStorage(),
        max_length=500,
        null=True,
        blank=True
    )

    def filename(self):
        """The name of video file uploaded by the user."""
        return self.fileName

    def isManagedBy(self, user):
        return self.beehive.isManagedBy(user)

    @property
    def process_status(self):
        """Get the process status.

        Returns:
            ProcessStatus: The process status.
        """
        if self.annotations_path:
            return ProcessStatus.PROCESS_SUCCESS
        elif self.process_id:
            base_url = f'{settings.MODEL_SERVING_APP}/media/processes'
            try:
                r = requests.get(
                    f'{base_url}/{self.process_id}',
                    timeout=10
                )
            except (requests.exceptions.ConnectionError,
                    requests.exceptions.Timeout):
                return ProcessStatus.UNAVAILABLE
            else:
                res = r.json().get('status', None)
                if res == 'SUCCESS':
                    # since annotation path is not available
                    return ProcessStatus.NOTIFICATION_FAILURE
                else:
                    status = ProcessStatus.map_celery_state(res)
                    if not status:
                        logger.warning(
                            f'Process status <{res}> cannot be mapped!')
                    return status
        else:
            return ProcessStatus.REQUEST_FAILURE

    @property
    def duration(self):
        if self.fps and self.image_count:
            return datetime.timedelta(seconds=self.image_count / self.fps)
        else:
            return None

    @property
    def runtime(self):
        if not self.finished:
            return None
        return self.finished - self.uploaded

    @property
    def recorded(self):
        """When the video was recorded.

        Uses the following approaches (in this order):
        - (1) Extract the timestamp from the filename.

        Returns:
            datetime.datetime: The timestamp.
        """
        match = re.search(r'\d{4}\d{2}\d{2}_\d{2}\d{2}\d{2}', self.path.name)
        if match:
            timestamp = match.group()
            return datetime.datetime.strptime(timestamp, '%Y%m%d_%H%M%S')

        return None

    def __str__(self):
        return f'({self.beehive.apiary.name}/{self.beehive.name}) {self.fileName} - {self.pk}'
