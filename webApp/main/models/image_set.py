from django.db import models


class ImageSet(models.Model):
    path = models.CharField(max_length=255, unique=True, null=True)
    name = models.CharField(max_length=255)

    main_annotation_type = models.ForeignKey(
        to='main.AnnotationType',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None
    )

    @property
    def first(self):
        """Get PK of first image in imageset."""
        first_image = self.images.order_by('frame').first().pk
        return first_image

    def isManagedBy(self, user):
        return self.video.isManagedBy(user)
