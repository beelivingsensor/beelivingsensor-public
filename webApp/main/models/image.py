from pathlib import Path

from django.db import models

from django.apps import apps
from storage import ImageAzureStorage


def image_upload_to(image, filename):
    return f'test/{image.hive.apiary.name}/{image.hive.name}/{filename}'


class Image(models.Model):
    class TYPE(models.TextChoices):
        UNCLASSIFIED = 'unclassified'
        HIVE = 'hive'
        BEE = 'bee'
        PLANT = 'plant'

    type = models.CharField(max_length=30, choices=TYPE.choices,
                            default=TYPE.HIVE, blank=True)

    class STATUS(models.TextChoices):
        UNCLASSIFIED = 'unclassified'
        TRAIN = 'train'
        TEST = 'test'
        VALIDATE = 'validate'

    status = models.CharField(max_length=30, choices=STATUS.choices,
                              default=STATUS.UNCLASSIFIED, blank=True)

    # this is only null when the image was not extracted from a video
    image_set = models.ForeignKey(
        'ImageSet',
        on_delete=models.CASCADE,
        related_name='images',
        null=True,
        blank=True
    )

    beehive = models.ForeignKey(
        'main.Hive',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    path = models.ImageField(storage=ImageAzureStorage(),
                             upload_to=image_upload_to,
                             max_length=256)

    name = models.CharField(max_length=300)
    # position of frame, only set when extracted from video
    frame = models.IntegerField(null=True, blank=True)
    filename = models.CharField(max_length=300)
    # image is presented to beekeepers when they create a new hive
    selected = models.BooleanField(default=False)

    # save separately since path.width/height is slow
    width = models.IntegerField()
    height = models.IntegerField()

    process_id = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )

    # When the image finished processing
    finished = models.DateTimeField(null=True, blank=True)

    @property
    def extension(self):
        return Path(self.path.name).suffix

    @property
    def download_name(self):
        if self.hive:
            apiary = self.hive.apiary.name.replace(' ', '_')
            hive = self.hive.name.replace(' ', '_')
        else:
            apiary = 'unknown_apiary'
            hive = 'unknown_hive'

        return f'{apiary}_{hive}_{self.pk}'

    def isManagedBy(self, user):
        if self.image_set:
            return self.image_set.isManagedBy(user)
        elif self.beehive:
            return self.beehive.isManagedBy(user)
        else:
            return user.is_staff

    @property
    def labeled_by(self):
        return apps.get_model('main.User').objects.filter(verifiedannotations__frame=self)

    @property
    def hive(self):
        if self.beehive:
            return self.beehive
        elif self.image_set:
            return self.image_set.video.beehive
        else:
            return None
