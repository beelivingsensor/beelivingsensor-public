from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    # points are updated by database triggers
    points = models.IntegerField(default=0)
    street = models.CharField(max_length=50, null=True, blank=True)
    number = models.CharField(max_length=20, null=True, blank=True)
    zip = models.IntegerField(null=True, blank=True)
    city = models.CharField(max_length=40, null=True, blank=True)
    phone = models.CharField(max_length=30, null=True, blank=True)