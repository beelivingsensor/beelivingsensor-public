from django.conf import settings
from django.db import models
from django.contrib.gis.db import models as postgis_models
from django.utils.translation import gettext_lazy as _


class Apiary(models.Model):
    """Represents an apiary."""
    # Who added the apiary/belongs the apiary
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                related_name="origin_owner_apiary")
    # A human-redable name for the apiary (e.g. the street)
    name = models.CharField(max_length=200, unique=True)
    # Where the apiary is located (GPS)
    location = postgis_models.PointField(null=True, blank=True,
                                         verbose_name=_('location'))
    # At which altitude the apiary stands
    elevation = models.IntegerField(default=0)
    # Where the apiary is located -> computed from the location to
    # save geolocation API requests
    address = models.CharField(max_length=1000, null=True, blank=True)
    # Users able to manage and see the apiary
    managers = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def isManagedBy(self, user):
        return user in self.managers.filter(email=user.email)

    def __str__(self):
        return f'{self.name} - {self.pk}'
