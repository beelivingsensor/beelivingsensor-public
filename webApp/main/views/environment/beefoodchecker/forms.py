from django.contrib.gis.forms import OSMWidget, PointField
from django.utils.translation import ugettext_lazy as _

from main.forms import BootstrapRawForm

from django import forms


class PlantFilterForm(BootstrapRawForm):

    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': _('Name'),
                'list': 'plantOptions'
            }
        ),
        required=False,
        label=_('Name')
    )


class PlantForm(BootstrapRawForm):

    street = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Glärnischstrasse 123'}),
        required=False
    )

    zip = forms.IntegerField(
        widget=forms.TextInput(attrs={'placeholder': '8750'}),
        required=False
    )

    city = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Glarus'}),
        required=False
    )

    radius = forms.IntegerField(
        widget=forms.TextInput(),
        initial=5,
        required=True
    )

    days = forms.IntegerField(
        widget=forms.TextInput(),
        initial=7,
        required=True
    )

    time = forms.DateTimeField(required=False)

    location = PointField(
        label="",
        srid=4326,
        widget=OSMWidget(attrs={
                'map_width': 1000,
                'map_height': 500,
                'default_lat': 46.7985,
                'default_lon': 8.2318,
                'default_zoom': 8
                }),
        required=False
    )

