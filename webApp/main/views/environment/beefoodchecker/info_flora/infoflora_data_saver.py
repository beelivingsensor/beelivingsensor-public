import json
import logging
from pathlib import Path

from django.contrib.gis.geos import Point

from main.models import Plant, PlantOccurrence
from main.util.dump import download_most_recent_dump
from main.views.environment.beefoodchecker.plant_name_mapper import \
    PlantNameMapper


logger = logging.getLogger(__name__)


class InfoFloraDataSaver:
    """Functions for saving Info Flora data in the database."""

    def create_data_from_dump(self, container, blob_prefix):
        """Create the data from the dump on Azure.

        - Download dump on Azure
        - Add the data to the database
        """
        PlantOccurrence.objects.all().delete()
        plant_occurrences = []

        dump_path = download_most_recent_dump(container, blob_prefix)
        taxa_path = Path(dump_path) / 'taxa.json'

        taxon_set = set()
        with open(taxa_path) as taxa_file:
            taxa = json.load(taxa_file)

            for i, taxon in enumerate(taxa['data']):
                latin_name = self.get_latin_name(taxon)
                logger.info(f'🌼 Processing plant <{latin_name}> ({i})')

                plants = PlantNameMapper.map_plant_name(latin_name)

                if plants:
                    if len(plants) > 1:
                        plant_names = [(plant.name, plant.rank) for plant in plants]
                        logger.warning(
                            f'❗ <{latin_name}> has multiple mappings: '
                            f'{plant_names}!')
                    plant = plants[0]
                else:
                    continue

                if plant.pk in taxon_set:
                    logger.error(f'❌ Duplicate plant: <{latin_name}>')
                    continue
                else:
                    taxon_set.add(plant.pk)

                # load corresponding region file
                taxon_id = taxon['id']
                regions_path = Path(dump_path) / f'{taxon_id}_regions.json'

                if not regions_path.exists():
                    logger.error(f'❌ Region file <{regions_path}> does not exist!')
                else:
                    with open(regions_path) as regions_file:

                        try:
                            regions = json.load(regions_file)
                        except json.decoder.JSONDecodeError:
                            logger.error(f'❌ Could not process <{regions_path}>')
                            continue

                        for region in regions['data']:
                            x = region['x']
                            y = region['y']
                            coords = Point(x=float(x), y=float(y))

                            plant_occurrence = PlantOccurrence(
                                plant=plant,
                                region=coords,
                                obs_nb=region['obs_nb'],
                                doubt=region['doubt'],
                                year_min=region['year_min'],
                                year_max=region['year_max'],
                            )

                            plant_occurrences.append(plant_occurrence)

                # perform a bulk creation for every 300th plant
                # prevent memory over-usage
                if i % 300 == 0:
                    PlantOccurrence.objects.bulk_create(plant_occurrences)
                    plant_occurrences = []

        PlantOccurrence.objects.bulk_create(plant_occurrences)

    def get_latin_name(self, taxon):
        """Get the latin name of the taxon.

        - Extracts it from the field 'original_name'
        - Strips away trailing words (only keeps the first two words)
            (e.g. Bellis perennis L. -> Bellis perennis)

        Args:
            taxon (Dict): The taxon data.
        """
        original_name = taxon['original_name']
        latin_name = ' '.join(original_name.split(' ')[:2])
        return latin_name
