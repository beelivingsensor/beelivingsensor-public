import json
import time

import requests


class InfoFloraDataFetcher:
    """Functions for fetching data from InfoFlora via its REST API.

    https://docs.infoflora.ch/
    https://docs.infoflora.ch/api/observation-v4
    """
    base_url = 'https://obs.infoflora.ch/rest'
    
    def send_request(self, path, params):
        """Send a request to Info Flora's API.

        Args:
            path (str): The path of the API.
            params: The parameters to be passed to the API request.

        Returns:
            dict: The response content.
        """
        url = f'{self.base_url}{path}'
        response = requests.get(
            url,
            params=params
        )
        data = response.json()

        if response.status_code != 200:
            print(f'Request with params {params} failed!')
            print(f'Url: {response.request.url}')

        return data

    def get_taxa(self):
        """Get all taxa.

        Returns:
            dict: The response content.
        """
        path = '/public/v4/taxonomy/taxa'
        params = {
            'lang': 'de',
            'references': [3],  # 3 = Flora Helvetica 2018/DB-TAXREFv1
            'ranks': [2263],  # 2263 = Species
            # leave this empty as otherwise some species are not retrieved
            # for some reason
            # A = Accepted Name,
            # C = Name comprehending several taxa (e.g. Taraxacum officinale)
            'status': [],
            'groups': [],
            'limit': 5000,  # right now there are around 4000 species
        }
        return self.send_request(path, params)

    def get_regions(self, taxon_id):
        """Get the regions where the taxon occurs.

        Args:
            taxon_id (int): Info Flora's taxon ID.

        Returns:
            dict: The response content.
        """
        # Community ID for Info Flora: 0
        community_id = 0
        # 5x5km-Raster: Filter auf Atlas_id = 11
        # (Atlas based on 5x5 squares for all CH using TAXREF2017 taxonomy.)
        atlas_id = 11
        path = f'/v4/communities/{community_id}/public/atlases/{atlas_id}/names/{taxon_id}/units'
        params = {
            'srid': 4326,
        }
        return self.send_request(path, params)

    def dump_taxa(self, info_flora_path):
        """Dump taxa data as a JSON file.

        Args:
            info_flora_path (Path): Where to dump the JSON file.

        Returns:
            Path: Path of JSON file.
        """
        print('Dump taxa JSON...')
        file_path = info_flora_path / f'taxa.json'
        content = self.get_taxa()
        with open(file_path, 'w') as f:
            json.dump(content, f, indent=3)

        return file_path

    def dump_regions(self, info_flora_path, taxa_json_path):
        """Dump region data as a collection of JSON files.

        For each taxon, one JSON with regions in which it occurs is generated.

        Args:
            info_flora_path (Path): Where to dump the JSON files.
            taxa_json_path (Path): Path to the taxa JSON file.
        """
        with open(taxa_json_path, 'r') as taxa_file:
            taxa = json.load(taxa_file)
            for taxon in taxa['data']:
                taxon_id = taxon['id']
                file_path = info_flora_path / f'{taxon_id}_regions.json'
                print(f'Dump region JSON {file_path}...')

                content = self.get_regions(taxon_id)
                with open(file_path, 'w') as region_file:
                    json.dump(content, region_file, indent=3)

                # as a pre-caution so that not too many requests are sent
                time.sleep(1)

    def dump(self, info_flora_path):
        """Dump plant occurrence data.

        Args:
            info_flora_path (Path): Where to dump the data.
        """
        taxa_file_path = self.dump_taxa(info_flora_path)
        self.dump_regions(info_flora_path, taxa_file_path)
