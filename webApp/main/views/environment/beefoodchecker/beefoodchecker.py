"""Convenience functions for BeeFoodChecker views."""
import calendar
from datetime import datetime

from django.utils.translation import gettext_lazy as _

from main.models import Observation, Nutrition
from main.views.environment.beefoodchecker.flowering_info import FloweringInfo, \
    ObservedFloweringPhase, FloweringPhase


def get_flowering_info(plant):
    """Get the flowering info of the plant.

    The flowering phases come from iNaturalist and Pritsch. Pritsch
    gives a general flowering phase. In the case of iNaturalist,
    flowering phases are computed from observations of this plant.
    It returns the first and last observation of the plant flowering overall
    and the first and last observation of the plant flowering in this year.

    Args:
        plant (Plant): The plant.

    Returns:
        FloweringInfo: The flowering info.
    """
    flowering_info = FloweringInfo(plant=plant)
    observations = Observation.objects.filter(plant=plant)

    # Overall observed phase
    flowering_observations = observations \
        .filter(flowering=True) \
        .order_by('observed_on__month', 'observed_on__day')
    if flowering_observations:
        flowering_info.overall_observed_phase = ObservedFloweringPhase(
            first_observation=flowering_observations.first(),
            last_observation=flowering_observations.last()
        )

    # Current observed phase
    current_flowering_observations = flowering_observations \
        .filter(observed_on__year=datetime.now().year) \
        .order_by('observed_on')
    if current_flowering_observations:
        flowering_info.current_observed_phase = ObservedFloweringPhase(
            first_observation=current_flowering_observations.first(),
            last_observation=current_flowering_observations.last()
        )

    # General phase
    nutrition_values = Nutrition.objects.filter(
        plant=plant,
        food=Nutrition.Food.POLLEN
    ).order_by('month', 'start_day')

    if nutrition_values:
        first = nutrition_values.first()
        last = nutrition_values.last()
        flowering_info.general_phase = FloweringPhase(
            first_day=first.start_day,
            first_month=first.month,
            last_day=last.end_day,
            last_month=last.month
        )

    return flowering_info


def get_nutrition_values(plant):
    """Get bee nutrition values of plant.

    Args:
        plant (Plant): The plant

    Returns:
        Dict[str, Dict[int, Dict[str, int]]]: Nutrition values
            per month and per month half for pollen and nectar, respectively.
    """
    months = [
        _('January'),
        _('February'),
        _('March'),
        _('April'),
        _('May'),
        _('June'),
        _('July'),
        _('August'),
        _('September'),
        _('October'),
        _('November'),
        _('December')]

    nutrition_values = {
        month: {1: {'P': None, 'N': None}, 16: {'P': None, 'N': None}} for
        month in months}

    for nutrition in Nutrition.objects.filter(plant=plant).order_by('food'):
        month = _(calendar.month_name[nutrition.month])
        start_day = nutrition.start_day
        if start_day not in nutrition_values[month]:
            nutrition_values[month][start_day] = []

        if nutrition.food == Nutrition.Food.NECTAR:
            nutrition_values[month][start_day]['N'] = nutrition.value
        elif nutrition.food == Nutrition.Food.POLLEN:
            nutrition_values[month][start_day]['P'] = nutrition.value

    return nutrition_values
