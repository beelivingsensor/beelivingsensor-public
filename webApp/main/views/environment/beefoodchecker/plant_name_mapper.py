import logging

from main.models import Plant

logger = logging.getLogger(__name__)


class PlantNameMapper:
    """Class for mapping plant names to standardized names."""

    latin_name_mappings = {
        'Acer plantanoides': 'Acer platanoides',
        'Aesculus x carnea': 'Aesculus × carnea',
        'Forsythia ×intermedia': 'Forsythia × intermedia',
        'Narcissus poëticus': 'Narcissus poeticus',
    }

    # plants with no or only a few observations without research grade in CH
    # do not trigger a warning
    no_observations = {
        'Weigelia spec.',
        'Lonicera x heckrottii',
        'Salix caprea x Salix viminalis',
    }

    @classmethod
    def map_plant_name(cls, plant_name):
        """Map plant name to list of plant instances.

        - There can be several latin names for the same plant species. We use
            the latin names used on iNaturalist as a standard.
        - Some floral data sources (e.g. Pritsch and the pollen color table)
            only specify plants on genus taxonomy level (e.g. Salix spec.).
            Those are mapped to the set of species belonging to that genus.
        - Some floral data sources (e.g. Pritsch and the pollen color table)
            contain plant species that do not appear in our database. Those are
            plant not native to Switzerland and/or occurring very rarely.

        Args:
            plant_name (str): The name of the plant.

        Returns:
            List[Plant]: A list of plants.
        """
        # remove trailing whitespaces
        plant_name = plant_name.rstrip()

        # Plants given as genus (rather than species)
        if plant_name.endswith('spec.') or plant_name.endswith('sp.'):
            genus_name = plant_name.split(' ')[0]
            plants = Plant.objects.filter(name__startswith=genus_name)
        else:
            try:
                plant = Plant.objects.get(
                    name=plant_name,
                    rank=Plant.RANK.SPECIES)
            except Plant.DoesNotExist:
                plants = []
            else:
                plants = [plant]

            if not plants:
                plants = Plant.objects.filter(
                    commonname__name=plant_name,
                    commonname__language='sci',
                    rank=Plant.RANK.SPECIES
                ).distinct()

                if not plants:
                    mapped_plant_name = cls.latin_name_mappings.get(
                        plant_name, plant_name)
                    try:
                        plant = Plant.objects.get(name=mapped_plant_name)
                    except Plant.DoesNotExist:
                        plants = []
                    else:
                        plants = [plant]

        if not plants and plant_name not in cls.no_observations:
            logger.error(f'❌ No match: <{plant_name}>')

        return plants
