from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

from main.util.azure_helpers import upload_blob


class DataUploader:

    @staticmethod
    def _upload(container, file_path):
        blob_name = str(file_path)
        upload_blob(file_path, container, blob_name)
        print(f'Blob {blob_name} uploaded ✅')

    @staticmethod
    def upload(container, dump_path):
        """Upload the plant dump to Azure.

        Args:
            container (str): Azure container name.
            dump_path (Path): Path of dump.
        """
        print(f'Uploading dump {dump_path}')
        with ThreadPoolExecutor() as executor:
            for file_path in dump_path.glob('**/*.json'):
                executor.submit(
                    DataUploader._upload,
                    container,
                    file_path)
