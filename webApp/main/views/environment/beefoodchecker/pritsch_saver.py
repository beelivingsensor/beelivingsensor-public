from csv import DictReader

from main.models import Plant, NutritionalValue, Nutrition
from main.util.azure_helpers import download_blob
from main.views.environment.beefoodchecker.plant_name_mapper import \
    PlantNameMapper


class PritschSaver:
    """Add data from Pritsch to database.

    Assumes that plants already exist in the database:
    If not, run python manage.py inaturalist create first!
    """
    container = 'plant'
    blob = 'pritsch.csv'

    def add_pritsch_data(self):
        plant_set = set()
        Nutrition.objects.all().delete()
        download_blob(self.container, self.blob, self.blob)
        nutritions = []

        with open(self.blob) as f:
            reader = DictReader(f)

            for row in reader:
                name = row['name']
                plants = PlantNameMapper.map_plant_name(name)

                for plant in plants:
                    if plant in plant_set:
                        print(f'Plant occurs more than once: {plant.name}')
                        continue
                    else:
                        plant_set.add(plant)

                    # save plant info
                    grade = row['grade']
                    plant.nutrition_grade = NutritionalValue.map_grade(grade)
                    plant.save()

                    for month in range(3, 10):

                        objs = [
                            (Nutrition.Food.POLLEN, row[f'{month}P1'], 1),
                            (Nutrition.Food.POLLEN, row[f'{month}P2'], 2),
                            (Nutrition.Food.NECTAR, row[f'{month}N1'], 1),
                            (Nutrition.Food.NECTAR, row[f'{month}N2'], 2),
                        ]

                        for food, num_value, month_half in objs:
                            if num_value:

                                value = NutritionalValue.map_value(num_value)

                                if month_half == 1:
                                    start_day = 1
                                    end_day = 15
                                else:
                                    start_day = 16
                                    end_day = 31

                                nutrition = Nutrition(
                                    plant=plant,
                                    food=food,
                                    value=value,
                                    month=month,
                                    start_day=start_day,
                                    end_day=end_day
                                )
                                nutritions.append(nutrition)

        Nutrition.objects.bulk_create(nutritions)
