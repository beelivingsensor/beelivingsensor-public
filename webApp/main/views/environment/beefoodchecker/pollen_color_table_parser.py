import csv
import re

from main.models import Plant
from main.util.azure_helpers import download_blob
from main.views.environment.beefoodchecker.plant_name_mapper import \
    PlantNameMapper


class PollenColorTableParser:

    container = 'plant'
    blob = 'pollen_color/schulbiologiezentrum.csv'

    def add_pollen_colors(self):
        """Set the pollen colors for the plants in the databse."""
        plant_names = self.parse_schulbiologiezentrum_csv()
        for plant_name in plant_names:
            plants = PlantNameMapper.map_plant_name(plant_name)
            pollen_color = plant_names[plant_name]
            for plant in plants:
                plant.pollen_color = pollen_color
                plant.save()

    def parse_schulbiologiezentrum_csv(self):
        """Parse the CSV from Schulbiolgiezentrum.

        Returns:
            Dict[str, str]: The scientific names mapped to the pollen colors.
        """
        plants = {}

        download_blob(self.container, self.blob, self.blob)

        with open(self.blob) as f:
            reader = csv.reader(f)

            for row in reader:
                name = row[0]
                color = row[1]

                match = re.fullmatch(r'(.*?)( \(.*\))?', name)
                if not match:
                    print('No match:', name)
                else:
                    name = match.group(1)
                    words = name.split()

                    if words[-2] == 'x':
                        n_words = -3
                    else:
                        n_words = -2

                    scientific_name = ' '.join(words[n_words:])
                    plants[scientific_name] = color

        return plants
