import calendar
from dataclasses import dataclass
from typing import Optional

from django.utils import timezone

from main.models import Plant, Observation
from django.utils.translation import gettext_lazy as _


class FloweringPhase:

    def __init__(self, first_month=None, first_day=None, last_month=None, last_day=None):
        self._first_month = first_month
        self._first_day = first_day
        self._last_month = last_month
        self._last_day = last_day

    @property
    def first_month(self):
        return self._first_month

    @first_month.setter
    def first_month(self, value):
        self._first_month = value

    @property
    def first_day(self):
        return self._first_day

    @first_day.setter
    def first_day(self, value):
        self._first_day = value

    @property
    def last_month(self):
        return self._last_month

    @last_month.setter
    def last_month(self, value):
        self._last_month = value

    @property
    def last_day(self):
        return self._last_day

    @last_day.setter
    def last_day(self, value):
        self._last_day = value

    @property
    def first_date(self):
        month_name = _(calendar.month_name[self.first_month])
        return f'{self.first_day}. {month_name}'

    @property
    def last_date(self):
        month_name = _(calendar.month_name[self.last_month])
        return f'{self.last_day}. {month_name}'

    def get_status(self, time=None) -> Optional[bool]:
        if not time:
            time = timezone.now()

        day = time.day
        month = time.month

        status = self.first_month <= month <= self.last_month
        if month == self.first_month:
            status = day >= self.first_day
        if month == self.last_month:
            status = day <= self.last_day

        return status

    @property
    def status(self):
        return self.get_status()


@dataclass
class ObservedFloweringPhase(FloweringPhase):
    first_observation: Observation
    last_observation: Observation

    @property
    def first_month(self):
        return self.first_observation.observed_on.month

    @property
    def first_day(self):
        return self.first_observation.observed_on.day

    @property
    def last_month(self):
        return self.last_observation.observed_on.month

    @property
    def last_day(self):
        return self.last_observation.observed_on.day

    @property
    def first_detail(self):
        return f'{self.first_observation.place_guess}; ' \
               f'{self.first_observation.observed_on.year}'

    @property
    def last_detail(self):
        return f'{self.last_observation.place_guess}; ' \
               f'{self.last_observation.observed_on.year}'


@dataclass
class FloweringInfo:
    plant: Plant
    general_phase: Optional[FloweringPhase] = None
    overall_observed_phase: Optional[ObservedFloweringPhase] = None
    current_observed_phase: Optional[ObservedFloweringPhase] = None

    def get_status(self, time):
        status = None
        for phase in [self.general_phase, self.overall_observed_phase]:
            if phase:
                phase_status = phase.get_status(time)
                if phase_status:
                    return True
                elif phase_status is False:
                    status = False

        return status
