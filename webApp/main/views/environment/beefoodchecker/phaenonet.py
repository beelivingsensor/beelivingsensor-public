import csv
import json

import requests

from main.util.azure_helpers import download_blob


class PhaenoNet:
    container = 'plant'
    species_blob = 'phaenonet/species_codes.csv'
    phenophases_blob = 'phaenonet/phenophases_codes.csv'

    post_body = {
        "structuredQuery": {
            "select": {
                "fields": [
                    {
                        "fieldPath": "species"
                    },
                    {
                        "fieldPath": "phenophase"
                    },
                    {
                        "fieldPath": "date"
                    },
                    {
                        "fieldPath": "individual"
                    },
                    {
                        "fieldPath": "user"
                    }
                ]
            },
            "from": [
                {
                    "collectionId": "observations"
                }
            ],
            "where": {
                "fieldFilter": {
                    "field": {
                        "fieldPath": "modified"
                    },
                    "op": "GREATER_THAN",
                    "value": {
                        "timestampValue": "2021-01-15T00:00:00.000Z"
                    }
                }
            }
        }
    }

    def __init__(self):
        # map phenophase code to whether it is a flowering phase
        self.code2flowering = {}
        # map species code to the scientific name of the species
        self.code2species = {}
        self.read_species()
        self.read_phenophases()

    def read_species(self):
        """Fetch and read species list."""
        download_blob(PhaenoNet.container, PhaenoNet.species_blob, PhaenoNet.species_blob)
        with open(PhaenoNet.species_blob) as f:
            reader = csv.DictReader(f)
            for row in reader:
                scientific_name = row['scientific name']
                if scientific_name:
                    plant_code = row['species abbreviation']
                    self.code2species[plant_code] = scientific_name

    def read_phenophases(self):
        """Fetch and read phenophase list."""
        download_blob(PhaenoNet.container, PhaenoNet.phenophases_blob, PhaenoNet.phenophases_blob)
        with open(PhaenoNet.phenophases_blob) as f:
            reader = csv.DictReader(f)
            for row in reader:
                pheno_code = row['phenophase']
                self.code2flowering[pheno_code] = pheno_code in ['BLA', 'BLB']

    def fetch(self):
        # curl: curl -H "Content-Type: application/json" --data @phaenonet_post_body.json "https://firestore.googleapis.com/v1/projects/phaenonet-test/databases/(default)/documents:runQuery"
        url = 'https://firestore.googleapis.com/v1/projects/phaenonet-test/databases/(default)/documents:runQuery'
        headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
        response = requests.post(
            url,
            data=json.dumps(self.post_body),
            headers=headers)

        content = response.json()
        # document.name
        phaenonet_id = ''
        # document.fields.species
        plant = ''
        # document.fields.phenophase
        phenophase = ''
        # document.createTime
        created_at = ''
        # document.fields.date
        observed_on = ''
        # document.fields.individual
        station_id = ''
        # document.fields.user
        user_id = ''

        print(content)
