import re
import urllib

from django.core.paginator import Paginator
from django.db.models import Count
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone

from main.models import Observation, Plant
from main.util.grouping import grouped
from main.views.environment.beefoodchecker.beefoodchecker import \
    get_flowering_info, get_nutrition_values
from main.views.environment.beefoodchecker.forms import PlantForm, \
    PlantFilterForm
from main.views.environment.beefoodchecker.inaturalist.inaturalist_data_saver import \
    INaturalistDataSaver


def beefoodchecker_overview_view(request):

    return render(
        request,
        'main/environment/beefoodchecker/beefoodchecker-overview.html')


def beefoodchecker_observation_create_view(request):
    saver = INaturalistDataSaver()
    saver.update_observations()
    return redirect('beefoodchecker-observation-list')


def beefoodchecker_observation_list_view(request):
    observations = Observation.objects.all().order_by('-observed_on')

    paginator = Paginator(observations, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'observation_count': observations.count(),
        'page_obj': page_obj
    }
    return render(
        request,
        'main/environment/beefoodchecker/beefoodchecker-observation-list.html',
        context
    )


def beefoodchecker_plant_list_view(request):
    plants = Plant.objects.filter(rank=Plant.RANK.SPECIES).annotate(
        count=Count('observation__id')).order_by('-count')

    form = PlantFilterForm(request.GET or None)
    querystring = ''
    if form.is_valid():
        name = form.cleaned_data.get('name')
        if name:
            match = re.search(r'\((.*)\)', name)
            if match:
                latin_name = match.group(1)
            else:
                latin_name = name

            plants = Plant.objects.filter(name=latin_name,
                                          rank=Plant.RANK.SPECIES)
            if plants:
                return redirect(
                    'beefoodchecker-plant-detail',
                    plant_pk=plants.first().pk)

        querystring = urllib.parse.urlencode(form.cleaned_data)

    paginator = Paginator(plants, 18)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    plant_groups = grouped(page_obj, 6)
    context = {
        'querystring': querystring,
        'form': form,
        'plant_count': plants.count(),
        'page_obj': page_obj,
        'plant_groups': plant_groups
    }
    return render(
        request,
        'main/environment/beefoodchecker/beefoodchecker-plant-list.html',
        context
    )


def beefoodchecker_plant_detail_view(request, plant_pk):
    plant = get_object_or_404(Plant, pk=plant_pk)
    observations = Observation.objects.filter(plant=plant)
    flowering_info = get_flowering_info(plant)

    context = {
        'plant': plant,
        'nutrition_values': get_nutrition_values(plant),
        'observation_count': observations.count(),
        'flowering_info': flowering_info
    }

    return render(
        request,
        'main/environment/beefoodchecker/beefoodchecker-plant-detail.html',
        context)


def beefoodchecker_search_view(request):
    searched = False
    location = None
    radius = None
    time = timezone.now()
    days = None
    plant_form = PlantForm(request.GET or None)

    if plant_form.is_valid():
        searched = True
        location = plant_form.cleaned_data.get('location')
        radius = plant_form.cleaned_data.get('radius')
        time = plant_form.cleaned_data.get('time', time)
        days = plant_form.cleaned_data.get('days')

    context = {
        'searched': searched,
        'location': location,
        'radius': radius,
        'time': time,
        'days': days,
        'plant_form': plant_form,
    }

    return render(request,
                  'main/environment/beefoodchecker/beefoodchecker-search.html',
                  context)
