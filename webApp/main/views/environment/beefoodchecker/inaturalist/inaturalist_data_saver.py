import json
import logging
from datetime import datetime, timedelta
from pathlib import Path

from django.contrib.gis.geos import Point

from main.models import Observation, Plant, CommonName
from main.util.dump import download_most_recent_dump
from main.views.environment.beefoodchecker.inaturalist.inaturalist_data_fetcher import \
    INaturalistDataFetcher


logger = logging.getLogger(__name__)


class INaturalistDataSaver:
    """Functions for saving INaturalist data in the database."""

    date = str((datetime.now() - timedelta(days=7)).date())

    def __init__(self):
        self.fetcher = INaturalistDataFetcher()

    def save_data_from_dump(self, container, blob_prefix):
        """Create data from the dump on Azure in the database.

        - Download dump on Azure (if not already)
        - Add the data to the database
        """
        dump_path = Path(download_most_recent_dump(container, blob_prefix))
        Observation.objects.all().delete()
        CommonName.objects.all().delete()
        Plant.objects.all().delete()
        self.save_taxa(dump_path)
        self.save_observations(dump_path)

    def save_taxa(self, dump_path):
        logger.warning('⚠️ Deleting old taxa data')
        parentage = {}

        for taxa_file_path in dump_path.glob('taxa/*.json'):
            logger.info(f'Add taxa of {taxa_file_path}')

            with open(taxa_file_path) as f:
                taxa = json.load(f)
                parentage.update(self._add_taxa_to_db(taxa))

        self.set_parentage(parentage)

    def set_parentage(self, parentage):
        """Set parent field for all plants."""
        logger.info('Set parentage of taxa')
        for i, (id_, parent_id) in enumerate(parentage.items()):
            plant = Plant.objects.get(inaturalist_id=id_)
            try:
                parent_plant = Plant.objects.get(inaturalist_id=parent_id)
            except Plant.DoesNotExist:
                logger.error(f'❌ ({i}) Parent with ID {parent_id} does not exist')
            else:
                logger.info(f'✅ ({i}) Parent with ID {parent_id} exists')
                plant.parent = parent_plant
                plant.save()

    def _add_taxa_to_db(self, data):
        parentage = {}
        common_names = []
        plants = []
        bulk_step = 1000

        for i, taxon in enumerate(data['results'], start=1):
            taxon_id = taxon['id']
            taxon_name = taxon['name']

            if self.ignore_taxon(taxon):
                logger.warning(f'⚠️ Ignoring taxon: {taxon_name}')
                continue

            logger.info(f'({i}) 🌼 Creating taxon: {taxon_name}')

            if taxon['default_photo']:
                image_url = taxon['default_photo']['medium_url']
                image_attribution = taxon['default_photo']['attribution']
            else:
                image_url = None
                image_attribution = None

            plant = Plant(
                inaturalist_id=taxon_id,
                name=taxon_name,
                rank=taxon['rank'],
                info_url=taxon['wikipedia_url'],
                image_url=image_url,
                image_attribution=image_attribution
            )

            plants.append(plant)
            parentage[taxon_id] = taxon['parent_id']

            for name in taxon['names']:
                if name['locale'] in ['sci', 'en', 'de', 'fr', 'it']:
                    common_name = CommonName(
                        plant=plant,
                        language=name['locale'],
                        name=name['name']
                    )
                    common_names.append(common_name)

            if i % bulk_step == 0:
                logger.info(f'Bulk creation of taxa: {i}')
                self.bulk_create_taxa(plants, common_names)
                plants = []
                common_names = []

        self.bulk_create_taxa(plants, common_names)

        return parentage

    def ignore_taxon(self, taxon):
        """Decide to ignore the taxon.

        Ignores taxa with only a few observations for species and taxa below
        the species rank.
        """
        below_species = taxon['rank'] in [
            Plant.RANK.FORM, Plant.RANK.VARIETY, Plant.RANK.SUBSPECIES]

        if taxon['rank'] == Plant.RANK.SPECIES:
            low_obsv_count = taxon['observations_count'] < 50
        else:
            low_obsv_count = False

        return below_species or low_obsv_count

    def bulk_create_taxa(self, plants, common_names):
        Plant.objects.bulk_create(plants)
        CommonName.objects.bulk_create(common_names, ignore_conflicts=True)

    def save_observations(self, dump_path):
        for observation_file_path in dump_path.glob('observations/*.json'):
            logger.info(f'Add observations of {observation_file_path}')

            with open(observation_file_path) as f:
                observations = json.load(f)
                self._add_observations_to_db(observations)

    def update_observations(self):
        for observation_set in self.fetcher.iter_observations(date=self.date):
            self._add_observations_to_db(observation_set)

    def _add_observations_to_db(self, data):
        observations = []
        plants = {}
        common_names = []

        for i, obsv in enumerate(data['results'], start=1):
            obsv_id = obsv['id']
            logger.info(f'({i}) 🔭 Creating observation: {obsv_id}')

            taxon = obsv['taxon']

            try:
                plant = Plant.objects.get(inaturalist_id=taxon['id'])
            except Plant.DoesNotExist:
                if taxon['id'] in plants:
                    plant = plants[taxon['id']]
                else:
                    taxon_name = taxon['name']
                    logger.info(f'🌼 Creating plant: {taxon_name}')

                    # ignore rare species (usually sub-species)
                    if not taxon['default_photo']:
                        logger.warning(f'⚠️ Ignoring plant: {taxon_name}')
                        continue

                    try:
                        parent = \
                            Plant.objects.get(inaturalist_id=taxon['parent_id'])
                    except Plant.DoesNotExist:
                        parent = None

                    plant = Plant(
                        inaturalist_id=taxon['id'],
                        name=taxon_name,
                        parent=parent,
                        rank=taxon['rank'],
                        info_url=taxon['wikipedia_url'],
                        image_url=taxon['default_photo']['medium_url'],
                        image_attribution=taxon['default_photo']['attribution']
                    )
                    plants[taxon['id']] = plant

                    logger.info(f'Creating common names')

                    names = []
                    if 'preferred_common_name' in taxon:
                        names.append(
                            ('de', taxon['preferred_common_name']))
                    if 'english_common_name' in taxon:
                        names.append(
                            ('en', taxon['english_common_name']))

                    for language, name in names:
                        common_name = CommonName(
                            plant=plant,
                            language=language,
                            name=name
                        )
                        common_names.append(common_name)

            # take the first photo
            photo = obsv['photos'][0]

            y, x = obsv['location'].split(',')
            location = Point(x=float(x), y=float(y))

            try:
                observation = Observation.objects.get(inaturalist_id=obsv['id'])
            except Observation.DoesNotExist:
                if obsv['time_observed_at']:
                    observed_on = obsv['time_observed_at']
                else:
                    observed_on = obsv['observed_on']

                observation = Observation(
                    inaturalist_id=obsv['id'],
                    observation_url=obsv['uri'],
                    image_url=photo['url'],
                    image_attribution=photo['attribution'],
                    user=obsv['user']['login'],
                    plant=plant,
                    created_at=obsv['created_at'],
                    observed_on=observed_on,
                    location=location,
                    place_guess=obsv['place_guess'],
                    flowering=self._get_flowering_status(obsv)
                )
                observations.append(observation)
            else:
                # update flowering status
                # in iNaturalist annotations can only be specified after having
                # added the observation. Therefore, the annotation might
                # be added at later point.
                if observation.flowering is None:
                    observation.flowering = self._get_flowering_status(obsv)
                    observation.save()

        self.bulk_create_taxa(plants.values(), common_names)
        Observation.objects.bulk_create(observations)

    @staticmethod
    def _get_flowering_status(obsv):
        """Get the flowering status from the annotations.

        Returns:
            Optional[bool]: Whether plant is flowering (or it is unknown).
        """
        flowering = None
        for annotation in obsv['annotations']:
            controlled_attr = annotation['controlled_attribute_id']
            controlled_value = annotation['controlled_value_id']
            if controlled_attr == 12:
                flowering = controlled_value == 13

        return flowering
