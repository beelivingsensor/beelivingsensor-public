import json
import time
from pathlib import Path

import requests


class INaturalistDataFetcher:
    """Functions for fetching data from INaturalist via its REST API."""
    
    base_url = 'https://api.inaturalist.org/v1'
    page_size = 200
    switzerland_id = 7236
    
    def send_request(self, path, params):
        """Send a request to iNaturalist's API.

        Args:
            path (str): The path of the API.
            params: The parameters to be passed to the API request.

        Returns:
            dict: The response content.
        """
        url = f'{self.base_url}{path}'
        response = requests.get(
            url,
            params=params
        )
        data = response.json()

        if response.status_code != 200:
            print(f'Request with params {params} failed!')
            print(f'Url: {response.request.url}')

        return data

    def get_observations(
            self, date=None, lat=None, lng=None, page=1, id_above=None):
        """Send a request to iNaturalist's observation endpoint.

        Observations may be filtered by id, date, coordinates, place and
        quality grade 'research'.

        Args:
            date (str): Date from which on observations were made.
            lat (float): Latitude of observation.
            lng (float): Longitude of observation.
            page (int): Which page of the results to fetch.
            id_above (int): Observations that have an ID above `id_above`.

        Returns:
            dict: The response content.
        """
        path = '/observations'
        params = {
            'taxon_id': '47125,136329',
            'quality_grade': 'research',
            'locale': 'de',
            'per_page': self.page_size,
            'page': page,
            'order_by': 'id',
            'order': 'asc'
        }
        if date:
            params['d1'] = date

        if id_above:
            params['id_above'] = id_above

        if lat and lng:
            params['lat'] = lat
            params['lng'] = lng
            params['radius'] = 10
        else:
            # all observations from Switzerland
            params['place_id'] = self.switzerland_id

        return self.send_request(path, params)

    def iter_observations(self, date=None):
        """Iter response contents of iNaturalist's observation endpoint.

        Args:
            date (str): Observations that were made after this date.

        iNaturalist's API only yields 200 observations per request.
        Therefore, when there are more than 200 observations,
        repeated requests have to be made.

        Yields:
            dict: The response content.
        """
        id_above = 0
        results = 0

        while True:
            observations = self.get_observations(
                page=1,
                id_above=id_above,
                date=date
            )

            if not observations['results']:
                break

            yield observations

            results += len(observations['results'])
            id_above = observations['results'][-1]['id']
            print(f'Observation results processed so far: {results}')
            print(f'ID above: {id_above}')
            print()

            # as a pre-caution so that not too many requests are sent
            time.sleep(1)

    def get_taxa(self, id_above=None):
        path = '/taxa'
        params = {
            'is_active': True,
            'all_names': True,
            'taxon_id': '47125,136329',
            'per_page': self.page_size,
            'locale': 'de',
            'order_by': 'id',
            'order': 'asc',
        }

        if id_above:
            params['id_above'] = id_above

        return self.send_request(path, params)

    def iter_taxa(self):
        id_above = 0
        results = 0

        while True:
            taxa = self.get_taxa(id_above=id_above)

            if not taxa['results']:
                break

            yield taxa

            results += len(taxa['results'])
            id_above = taxa['results'][-1]['id']
            print(f'Taxa results processed so far: {results}')
            print(f'ID above: {id_above}')
            print()

            # as a pre-caution so that not too many requests are sent
            time.sleep(1)

    def dump_response_data_as_json(self, dir_path, endpoint):
        """Get all response data as a collection of JSON files.

        For each request to a iNaturalist endpoint, a JSON file is created.

        Args:
            dir_path (Path): Path of directory where the files are written to
            endpoint: observations|taxa
        """
        if endpoint == 'observations':
            func = self.iter_observations
        else:
            func = self.iter_taxa

        dir_path.mkdir(parents=True)

        for i, result_set in enumerate(func(), start=1):
            file_path = dir_path / f'{endpoint}_{i}.json'
            with open(file_path, 'w') as f:
                json.dump(result_set, f, indent=3)
