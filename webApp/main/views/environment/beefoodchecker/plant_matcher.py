from django.contrib.gis.measure import Distance
from main.models import Observation, NutritionalValue, Plant
from main.views.environment.beefoodchecker.beefoodchecker import \
    get_flowering_info


class PlantMatcher:

    def __init__(self, plant):
        self.plant = plant

    def match(self, coords, earliest_date, latest_date, radius, time):
        """Match the plant based on different criteria."""
        self.match_plant(
            self.plant, coords, earliest_date, latest_date, radius, time)

    def get_score(self):
        """Score how likely the pollen comes from this pollen.

        Returns:
            int: The score.
        """
        return self.get_plant_matching_score(self.plant)

    def get_dict(self):
        """Get a dictionary representation of the plant."""
        return self.get_plant_matching_dict(self.plant)

    @staticmethod
    def match_plant(plant, coords, earliest_date, latest_date, radius, time):
        plant.occurring = True
        plant.flowering = get_flowering_info(plant).get_status(time)
        plant.observed = False
        plant.observed_around_this_time = False
        plant.observed_this_year = False
        plant.observed_this_year_around_this_time = False
        plant.observed_this_year_around_this_time_flowering = False
        plant.observations = []
        plant.observations_around_this_time = []
        plant.observations_this_year = []
        plant.observations_this_year_around_this_time = []
        plant.observations_this_year_around_this_time_flowering = []
        observations = Observation.objects.filter(
            plant=plant,
            location__distance_lt=(coords, Distance(km=radius)),
        )

        for obsv in observations:
            plant.observed = True
            plant.observations.append(obsv)

            if obsv.observed_on.month == earliest_date.month:
                if obsv.observed_on.day >= earliest_date.day:
                    plant.observed_around_this_time = True
            elif obsv.observed_on.month == latest_date.month:
                if obsv.observed_on.day <= latest_date.day:
                    plant.observed_around_this_time = True
            else:
                if earliest_date.month < obsv.observed_on.month < latest_date.month:
                    plant.observed_around_this_time = True

            if plant.observed_around_this_time:
                plant.observations_around_this_time.append(obsv)

            if obsv.observed_on.year == latest_date.year:
                plant.observed_this_year = True
                plant.observations_this_year.append(obsv)

            if earliest_date <= obsv.observed_on <= latest_date:
                plant.observed_this_year_around_this_time = True
                plant.observations_this_year_around_this_time.append(obsv)

                if obsv.flowering:
                    plant.observed_this_year_around_this_time_flowering = True
                    plant.observations_this_year_around_this_time_flowering.append(
                        obsv)

    @staticmethod
    def get_plant_matching_score(plant):
        score = 0
        if plant.occurring:
            score += 10
        if plant.flowering:
            score += 10
        if plant.observed:
            score += 1
        if plant.observed_around_this_time:
            score += 1
        if plant.observed_this_year:
            score += 1
        if plant.observed_this_year_around_this_time:
            score += 1
        if plant.observed_this_year_around_this_time_flowering:
            score += 1
        if plant.pollen_color:
            score += 2
        if plant.nutrition_grade:
            score += 2

        return score

    @staticmethod
    def get_observations_dict(observations):
        return [
            {
                'id': o.inaturalist_id,
                'url': o.observation_url
            }
            for o in observations
        ]

    @staticmethod
    def get_plant_matching_dict(plant):
        genus = None
        family = None
        for taxon in plant.taxonomy:
            if taxon.rank == Plant.RANK.GENUS:
                genus = taxon.name
            elif taxon.rank == Plant.RANK.FAMILY:
                family = taxon.name

        dic = {
            'pk': plant.pk,
            'species': plant.name,
            'genus': genus,
            'family': family,
            'occurring': plant.occurring,
            'flowering': plant.flowering,
            'observed': plant.observed,
            'observed_around_this_time': plant.observed_around_this_time,
            'observed_this_year': plant.observed_this_year,
            'observed_this_year_around_this_time': plant.observed_this_year_around_this_time,
            'observed_this_year_around_this_time_flowering': plant.observed_this_year_around_this_time_flowering,
            'observations': PlantMatcher.get_observations_dict(plant.observations),
            'observations_around_this_time': PlantMatcher.get_observations_dict(plant.observations_around_this_time),
            'observations_this_year': PlantMatcher.get_observations_dict(plant.observations_this_year),
            'observations_this_year_around_this_time': PlantMatcher.get_observations_dict(plant.observations_this_year_around_this_time),
            'observations_this_year_around_this_time_flowering': PlantMatcher.get_observations_dict(plant.observations_this_year_around_this_time_flowering),
            'nutrition_grade': plant.nutrition_grade,
            'pollen_color': plant.pollen_color
        }

        return dic
