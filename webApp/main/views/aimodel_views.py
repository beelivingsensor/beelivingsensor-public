import json
import random
from urllib.parse import quote_plus

import requests
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.urls import reverse

from main.views.management_views import open_labeling_task_for_training
from main.forms import AIModelForm, MLDataFilterForm
from main.models import AIModel, Hive, Validation, VerifiedAnnotations, \
    Annotation, AnnotationType, Image, GoldAnnotationSet, Video
from main.util.model_performance import get_avg_precision_at_iou, \
    aggregate_results


@permission_required("main.view_aimodel")
def aimodel_list_view(request):
    aimodels = AIModel.objects.all().order_by('pk')
    context = {
        'aimodels': aimodels
    }
    return render(request, 'main/aimodel/aimodel-list.html', context)


@permission_required("main.add_aimodel")
def aimodel_upload_view(request):
    aimodel_form = AIModelForm(request.POST or None)
    if aimodel_form.is_valid():
        aimodel_form.save()
        return redirect('aimodel-list')

    context = {
        'aimodel_form': aimodel_form
    }
    return render(request, 'main/aimodel/aimodel-upload.html', context)


@staff_member_required
def ml_validations_view(request):
    """Hive overview for model validations."""
    hives = Hive.objects.all()
    context = {
        'hives': hives
    }
    return render(request, 'main/aimodel/ml_validations.html', context)


# TODO: review permission
@staff_member_required
def aimodel_detail_view(request, aimodel_pk):
    aimodel = get_object_or_404(AIModel, pk=aimodel_pk)
    hives = Hive.objects.filter(validation__model=aimodel).distinct()
    context = {
        'aimodel': aimodel,
        'hives': hives
    }
    return render(request, 'main/aimodel/aimodel-detail.html', context)


# TODO: review permission
@staff_member_required
def hive_validations_view(request, hive_pk):
    """Show all model validations of a hive."""
    hive = get_object_or_404(Hive, pk=hive_pk)
    validations = Validation.objects.filter(hive=hive).order_by('-f1')
    context = {
        'hive': hive,
        'validations': validations
    }
    return render(request, 'main/aimodel/hive-validation-list.html', context)


# TODO: review permission
@staff_member_required
def validation_detail_view(request, aimodel_pk, hive_pk):
    """Show validations of AI model on the hive."""
    aimodel = get_object_or_404(AIModel, pk=aimodel_pk)
    hive = get_object_or_404(Hive, pk=hive_pk)
    validations = Validation.objects.filter(model=aimodel, hive=hive)
    referer = request.META.get('HTTP_REFERER')

    if 'aimodels/' in referer:
        back_url = reverse('aimodel-detail', args=(validations.first().model.pk,))
    else:
        back_url = reverse('hive-validation-list', args=(hive.pk,))

    context = {
        'back_url': back_url,
        'validations': validations
    }
    return render(request, 'main/aimodel/validation-detail.html', context)


@staff_member_required
def ml_overview_view(request):
    return render(request, 'main/aimodel/ml_overview.html')


@staff_member_required
def ml_annotation_view(request):
    return render(request, 'main/aimodel/ml_annotation.html')


@staff_member_required
def ml_model_serving_app_view(request):
    videos = Video.objects.all().order_by('-pk')

    paginator = Paginator(videos, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'video_count': videos.count(),
        'page_obj': page_obj
    }

    return render(request, 'main/aimodel/ml_model_serving_app.html', context)


@staff_member_required
def ml_data_view(request):
    """View that shows all data used for the models."""
    # get train and dev images whose annotations were verified
    images = Image.objects.all()
    total_image_count = images.count()

    images = Image.objects.filter(
        goldannotationset__isnull=False
    ).distinct()

    # filter criteria
    form = MLDataFilterForm(request.GET or None)

    if form.is_valid():
        status = form.cleaned_data.get('status')

        model = form.cleaned_data.get('model')
        if model:
            annotation_type = AnnotationType.objects.get(name=model)

            if annotation_type.name == 'bee':
                images = images.filter(type=Image.TYPE.HIVE)
            else:
                images = images.filter(type=Image.TYPE.BEE)

        querystring = f'model={model}&'
    else:
        status = form.fields.get('status').initial
        querystring = ''

    querystring += '&'.join([f'status={s}' for s in status])

    images = images.filter(status__in=status)

    # paging
    paginator = Paginator(images, 30)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'querystring': querystring,
        'total_image_count': total_image_count,
        'image_count': images.count(),
        'form': form,
        'page_obj': page_obj,
    }

    return render(request, 'main/aimodel/ml_data.html', context)


def validateModel(model, hive):
    validation_frames = Image.objects.filter(beehive=hive, status=Image.STATUS.VALIDATE)
    print('Validating model: ', model.description)
    model_path = quote_plus(model.path)


    results = {}

    for frame in validation_frames:
        frame_url = frame.path.url
        print('  request:', frame.pk, end='', flush=True)
        r = requests.get(
            f'{settings.MODEL_SERVING_APP}/frame/annotations/?model={model_path}',
            params={
                "frameUrl": frame_url,
                "fileName": frame.name
            })
        if r.status_code == 200:
            print('->response OK', end='', flush=True)
            pred_bboxes = json.loads(r.content)

            # TODO: choose merged annotations
            print('->ground truth', end='', flush=True)
            verified_annotations = VerifiedAnnotations.objects.filter(frame=frame)
            verified_annotation = random.choice(verified_annotations)
            ground_truth = Annotation.objects.filter(image__verifiedannotations=verified_annotation)
            gt_bboxes = {}
            gt_bboxes[frame.name] = [[a.vector['x1'], a.vector['y1'], a.vector['x2'], a.vector['y2']]
                                        for a in ground_truth]

            print('->compute metrics', end='', flush=True)
            result = get_avg_precision_at_iou(frame.name, gt_bboxes, pred_bboxes)
            results.update(result)
            print('->done', end='', flush=True)

    print(flush=True)
    print(f'Aggregate results for model {model.description}...', flush=True)
    aggregated_results = aggregate_results(results)
    print(aggregated_results, flush=True)

    Validation.objects.update_or_create(
        model=model,
        hive=hive,
        defaults={
            'ground_truth': aggregated_results['qty_ground_truth'],
            'predicted': aggregated_results['qty_predicted'],
            'tp': aggregated_results['true_positive'],
            'fp': aggregated_results['false_positive'],
            'fn': aggregated_results['false_negative'],
            'precision': aggregated_results['precision'],
            'recall': aggregated_results['recall'],
            'f1': Validation.compute_f1(aggregated_results['precision'],
                                        aggregated_results['recall'])
        }
    )


def validate_models(hive):
    """Validate all models on the hive's validation frames.

    This will determine which model performs the best for a hive.
    """
    # for testing
    # export STORAGE_KEY="here comes the key"
    # python manage.py shell
    # from annotations.views import validate_models
    # from main.models import Hive
    # hive = Hive.objects.get(pk=3)
    # validate_models(hive)
    models = AIModel.objects.filter(validation__f1__gte=0.90).distinct()
    print(f'{models.count()} models to be validated')

    for model in models:
        validateModel(model, hive)

    # get validation with highest f1
    best_validation = Validation.objects.filter(hive=hive).order_by('-f1').first()
    print(f'Validation done for hive {hive} - Best model: {best_validation.model}, {best_validation.f1}!',
          flush=True)

    # check if training is needed
    if best_validation.f1 < 0.9:
        # check if beehive has labeled train and test frames
        train_imgs = Image.objects.filter(beehive=hive, status=Image.STATUS.TRAIN)
        if not train_imgs:
            print(f'Opening labeling task of training frames for {hive}', flush=True)
            open_labeling_task_for_training(hive)
        else:
            print(f'Setting retrain parameter to True for {hive}')
            hive.retrain = True
            hive.save()