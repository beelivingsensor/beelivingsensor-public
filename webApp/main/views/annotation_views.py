import json
import random
import threading

from django.contrib.admin.views.decorators import staff_member_required
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import permission_required, login_required

from django.shortcuts import render
from django.utils.timezone import now
from rest_framework.decorators import api_view
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, \
    HTTP_201_CREATED

from main.views.aimodel_views import validate_models
from main.forms import AnnotationAccuracyForm, AnnotationTypeCreationForm, \
    AnnotationTypeEditForm
from main.models import Video, AnnotationAccuracy, ModelAnnotation, AIModel, \
    User, Task, VerifiedAnnotations, LabelingTaskFrame, HiveTask, \
    UserAnnotation, Annotation, AnnotationType, Image, GoldAnnotationSet, \
    GoldAnnotation, UserAnnotationSet, ModelAnnotationSet
from main.decorators import user_is_manager
from main.serializers import AnnotationSerializer, AnnotationTypeSerializer, \
    serialize_annotations
from main.views.annotation.annotation_merge import merge_task_annotations


@permission_required("main.view_modelannotation")
@user_is_manager(Video, "video_pk")
def video_model_annotations_check(request, video_pk):
    """Get annnotations of the first image in the video made by the model."""
    video = get_object_or_404(Video, pk=video_pk)
    image_pk = video.imageset.images.order_by('frame').first().pk
    return redirect('image-model-annotations', image_pk=image_pk)


@permission_required("main.view_modelannotation")
@user_is_manager(Video, "video_pk")
def video_model_annotations_check_done(request, video_pk):
    """Show this page once the user has given accuracy to each frame."""
    video = get_object_or_404(Video, pk=video_pk)
    images = video.imageset.images
    image_count = images.count()
    high = images.filter(annotationaccuracy__accuracy=AnnotationAccuracy.AccuracyChoices.HIGH).count()
    medium = images.filter(annotationaccuracy__accuracy=AnnotationAccuracy.AccuracyChoices.MEDIUM).count()
    low = images.filter(annotationaccuracy__accuracy=AnnotationAccuracy.AccuracyChoices.LOW).count()

    if low:
        low_message = """
        Since you rated some of your images with low accuracy, we will ask people to help correct your frames.
        We will use the corrected frames to retrain the model to improve its performance.
        """
    else:
        low_message = ''

    context = {
        'low_message':  low_message,
        'image_count': image_count,
        'high': high,
        'medium': medium,
        'low': low,
        'video': video
    }
    return render(request, 'main/annotation/annotations_check_done.html', context)


@permission_required("main.view_modelannotation")
@user_is_manager(Image, "image_pk")
def image_model_annotations_check(request, image_pk):
    """Check the annotations of an image made by the model."""
    image = get_object_or_404(Image, pk=image_pk)
    # take the model with the most recent annotations on the image
    model = AIModel.objects.filter(modelannotation__annotation__image=image).order_by('-modelannotation__annotation__time').first()
    annotation_accuracies = AnnotationAccuracy.objects.filter(image=image)

    if annotation_accuracies:
        form = AnnotationAccuracyForm(request.POST or None, instance=annotation_accuracies[0])
    else:
        form = AnnotationAccuracyForm(request.POST or None)

    if form.is_valid():
        annotation_accuracy = form.save(commit=False)
        annotation_accuracy.checker = request.user
        annotation_accuracy.image = image
        annotation_accuracy.save()

        next_images = image.image_set.images.filter(frame__gt=image.frame).order_by('frame')

        # check if last image is reached
        if next_images:
            next_image = next_images[0]
            return redirect('image-model-annotations', image_pk=next_image.pk)
        else:
            return redirect('video-model-annotations-done', video_pk=image.image_set.video.pk)

    context = {
        'form': form,
        'image': image,
        'model': model
    }

    return render(
        request,'main/annotation/annotations_check.html', context)


@login_required
@api_view(['GET'])
def model_annotation_list_api_view(request, model_pk, frame_pk):
    model = get_object_or_404(AIModel, pk=model_pk)
    image = get_object_or_404(Image, pk=frame_pk)
    annotations = ModelAnnotation.objects.filter(
        annotation_set__model=model,
        annotation_set__image=image)
    annotations = [a.vector for a in annotations]
    return JsonResponse(annotations, safe=False)


# TODO: review permission
@staff_member_required
@api_view(['GET'])
def user_annotation_list_api_view(request, user_pk, frame_pk):
    user = get_object_or_404(User, pk=user_pk)
    image = get_object_or_404(Image, pk=frame_pk)
    annotations = UserAnnotation.objects.filter(
        annotation_set__user=user,
        annotation_set__image=frame_pk)
    annotations = [a.vector for a in annotations]
    return JsonResponse(annotations, safe=False)


@login_required
def gold_annotation_list_api_view(request, frame_pk):
    image = get_object_or_404(Image, pk=frame_pk)
    annotations = GoldAnnotation.objects.filter(annotation_set__image=image)
    annotations = [a.vector for a in annotations]
    return JsonResponse(annotations, safe=False)


@login_required
def annotate(request, task_pk):
    task = get_object_or_404(Task, pk=task_pk)

    if 'finished' in request.GET:
        frame_pk = request.GET['finished']
        finished_frame = get_object_or_404(Image, pk=frame_pk)

        user_annotation_set = UserAnnotationSet.objects.get(
            image=finished_frame,
            user=request.user,
            annotation_type=task.annotation_type
        )
        user_annotation_set.finished = True
        user_annotation_set.save()

    # get frames that user has finished annotating
    finished_images = Image.objects.filter(
        userannotationset__user=request.user,
        userannotationset__annotation_type=task.annotation_type,
        userannotationset__finished=True)

    task_frames = LabelingTaskFrame.objects\
        .filter(task=task)\
        .exclude(frame__in=finished_images)\
        .order_by('frame__frame')

    if task_frames:
        selected_frame = random.choice(task_frames).frame
        annotation_types = AnnotationType.objects.filter(active=True)
        response = render(request, 'main/annotation/annotate.html', {
            'task': task,
            'selected_image': selected_frame,
            'annotation_types': annotation_types,
        })
        response['Cache-Control'] = 'no-store'
        return response
    else:
        print(f'Task {task.pk} completed by {request.user}!')
        if task.finished:
            print(f'Task {task.pk} finished!')
            merge_task_annotations(task)
            if task.is_hive_task():
                hive_task = HiveTask.objects.get(task=task)
                if hive_task.type == HiveTask.Type.VALIDATION:
                    print('Start validation...')
                    # validate over all models
                    x = threading.Thread(
                        target=validate_models,
                        args=(hive_task.hive,))
                    x.start()
                elif hive_task.type == HiveTask.Type.TRAINING:
                    print(f'Set retrain parameter to True for {hive_task.hive}...')
                    hive_task.hive.retrain = True
                    hive_task.hive.save()

        return redirect('user-task-list')


@login_required
@api_view(['DELETE'])
def api_delete_annotation(request) -> Response:
    try:
        annotation_id = int(request.query_params['annotation_id'])
    except (KeyError, TypeError, ValueError):
        raise ParseError

    annotation = get_object_or_404(UserAnnotation, pk=annotation_id)

    image = annotation.annotation_set.image
    annotation.delete()

    user_annotations = UserAnnotation.objects \
        .filter(annotation_set__image=image,
                annotation_set__user=request.user) \
        .order_by('annotation_set__annotation_type__name')

    data = serialize_annotations(user_annotations)

    return Response(data, status=HTTP_200_OK)


@login_required
@api_view(['POST'])
def create_annotation(request) -> Response:
    try:
        image_id = int(request.data['image_id'])
        annotation_type_id = int(request.data['annotation_type_id'])
        vector = request.data['vector']
    except (KeyError, TypeError, ValueError):
        raise ParseError

    image = get_object_or_404(Image, pk=image_id)
    annotation_type = get_object_or_404(AnnotationType, pk=annotation_type_id)

    if not annotation_type.validate_vector(vector):
        user_annotations = UserAnnotation.objects\
            .filter(annotation_set__image=image,
                    annotation_set__user=request.user)\
            .order_by('annotation_set__annotation_type__name')

        data = serialize_annotations(user_annotations)
        data['detail'] = 'the vector is invalid.'
        return Response(data, status=HTTP_400_BAD_REQUEST)

    with transaction.atomic():
        user_annotation_set, _ = UserAnnotationSet.objects.get_or_create(
            image=image,
            annotation_type=annotation_type,
            user=request.user
        )

        UserAnnotation.objects.create(
            annotation_set=user_annotation_set,
            vector=vector
        )

    user_annotations = UserAnnotation.objects \
        .filter(annotation_set__image=image,
                annotation_set__user=request.user) \
        .order_by('annotation_set__annotation_type__name')

    data = serialize_annotations(user_annotations)

    return Response(data, status=HTTP_201_CREATED)


@login_required
@api_view(['GET'])
def load_annotations(request) -> Response:
    """Get the annotations of an image.

    First checks whether the user has already made annotations to it. If
    not, the annotations from the model are loaded, otherwise the user's
    annotations are loaded.

    Used by annotations.js:
    - loadAnnotationsToCache()
    """
    try:
        image_id = int(request.query_params['image_id'])
    except (KeyError, TypeError, ValueError):
        raise ParseError

    image = get_object_or_404(Image, pk=image_id)

    user_annotations = UserAnnotation.objects.filter(
        annotation_set__image=image,
        annotation_set__user=request.user)

    # check if user has already made annotations to it
    # (=has loaded annotation page for given image)
    if not user_annotations and not image.hive.bee_model:

        bee_type = AnnotationType.objects.get(name='bee')

        model_annotation_sets = ModelAnnotationSet.objects.filter(
            image=image,
            annotation_type=bee_type
        )

        if model_annotation_sets:
            # choose random annotation set
            model_annotation_set = model_annotation_sets[0]

            # create clones of the model annotations for the user
            user_annotation_set = UserAnnotationSet.objects.create(
                image=image,
                user=request.user,
                annotation_type=AnnotationType.objects.get(name='bee')
            )
            for model_annotation in model_annotation_set.annotations.all():
                UserAnnotation.objects.create(
                    annotation_set=user_annotation_set,
                    vector=model_annotation.vector
                )

            user_annotations = user_annotation_set.annotations.all()

    user_annotations = user_annotations.order_by(
        'annotation_set__annotation_type__name')

    data = serialize_annotations(user_annotations)

    return Response(data, status=HTTP_200_OK)


@login_required
@api_view(['GET'])
def load_annotation_types(request) -> Response:

    annotation_types = AnnotationType.objects.filter(active=True)
    serializer = AnnotationTypeSerializer(
        annotation_types,
        many=True,
        context={'request': request},
    )
    return Response({
        'annotation_types': serializer.data,
    }, status=HTTP_200_OK)


@login_required
@api_view(['POST'])
def update_annotation(request) -> Response:
    try:
        annotation_id = int(request.data['annotation_id'])
        image_id = int(request.data['image_id'])
        annotation_type_id = int(request.data['annotation_type_id'])
        vector = request.data['vector']
    except (KeyError, TypeError, ValueError):
        raise ParseError

    annotation = get_object_or_404(UserAnnotation, pk=annotation_id)
    annotation_type = get_object_or_404(AnnotationType, pk=annotation_type_id)

    if annotation.annotation_set.image.pk != image_id:
        raise ParseError('the image id does not match the annotation id.')

    if not annotation_type.validate_vector(vector):
        user_annotations = UserAnnotation.objects \
            .filter(annotation_set__image__pk=image_id,
                    annotation_set__user=request.user) \
            .order_by('annotation_set__annotation_type__name')

        data = serialize_annotations(user_annotations)
        data['detail'] = 'the vector is invalid.'

        return Response(data, status=HTTP_400_BAD_REQUEST)

    with transaction.atomic():
        annotation.vector = vector
        annotation.save()

    user_annotations = UserAnnotation.objects \
        .filter(annotation_set__image__pk=image_id,
                annotation_set__user=request.user) \
        .order_by('annotation_set__annotation_type__name')

    data = serialize_annotations(user_annotations)

    return Response(data, status=HTTP_200_OK)


@staff_member_required
def annotation_type_list_view(request):
    annotation_types = AnnotationType.objects.all()
    context = {'annotation_types': annotation_types}
    return render(request, 'main/annotation/annotation_type_list.html', context)


@staff_member_required
def annotation_type_create_view(request):
    annotation_form = AnnotationTypeCreationForm(request.POST or None)
    if annotation_form.is_valid():
        annotation_form.save()
        return redirect('annotation-type-list')
    context = {'annotation_type_form': annotation_form}
    return render(request, 'main/annotation/annotation_type_create.html', context)


@staff_member_required
def annotation_type_delete_view(request, pk):
    get_object_or_404(AnnotationType, pk=pk).delete()
    return redirect('annotation-type-list')


@staff_member_required
def annotation_type_update_view(request, pk):
    annotation_type = get_object_or_404(AnnotationType, pk=pk)
    annotation_form = AnnotationTypeEditForm(request.POST or None,
                                             instance=annotation_type)
    if annotation_form.is_valid():
        annotation_form.save()
        return redirect('annotation-type-list')
    context = {'annotation_type_form': annotation_form}
    return render(request, 'main/annotation/annotation_type_update.html', context)
