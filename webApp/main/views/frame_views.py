import os

from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.http import JsonResponse, FileResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse
from rest_framework.decorators import api_view

from main.models import Video, AnnotationAccuracy, Image, GoldAnnotationSet
from main.decorators import user_is_manager


def grouped(objs, n):
    return [objs[i:i + n] for i in range(0, len(objs), n)]

@permission_required("main.view_image")
def frame_list_view(request):
    frames = Image.objects.all()
    video = None
    if 'video' in request.GET:
        video = get_object_or_404(Video, pk=request.GET['video'])
        frames = frames.filter(image_set__video=video).order_by('frame')

    images = []

    accuracy_color = {
        AnnotationAccuracy.AccuracyChoices.HIGH: 'text-success',
        AnnotationAccuracy.AccuracyChoices.MEDIUM: 'text-warning',
        AnnotationAccuracy.AccuracyChoices.LOW: 'text-danger'
    }

    for frame in frames:
        annotation_accuracies = AnnotationAccuracy.objects.filter(image=frame)
        if annotation_accuracies:
            accuracy = annotation_accuracies[0].accuracy
            color = accuracy_color[accuracy]
        else:
            accuracy = "Not checked yet"
            color = 'text-secondary'
        images.append([frame, accuracy, color])

    paginator = Paginator(images, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {'page_obj': page_obj,
               'frame_groups': grouped(page_obj, 12),
               'video': video}
    return render(request, 'main/frame/frame_list.html', context)


@permission_required("main.view_image")
@user_is_manager(Image)
def frame_detail_view(request, pk):
    frame = get_object_or_404(Image, pk=pk)

    if frame.image_set:
        back_url = reverse('video-image-list', args=(frame.image_set.video.pk,))
    elif frame.beehive:
        back_url = reverse('beehive-image-list', args=(frame.beehive.pk,))
    else:
        back_url = reverse('ml-data')

    context = {
        'back_url': back_url,
        'frame': frame,
    }
    return render(request, 'main/frame/frame_detail.html', context)


@permission_required("main.view_image")
@api_view(['GET'])
def api_frame_detail_view(_, frame_pk):
    """Frame properties view.

    This is view is used for frame retrieval optimization.
    """
    frame = get_object_or_404(Image, pk=frame_pk)
    detail = {
        'path': frame.path.url,
        'width': frame.width,
        'height': frame.height
    }
    return JsonResponse(detail, safe=False)


@permission_required("main.view_image")
@api_view(['GET'])
def frame_path_view(request, frame_pk):
    """Get the path for the frame."""
    frame = get_object_or_404(Image, pk=frame_pk)
    return JsonResponse(frame.path.url, safe=False)


# TODO: review permission
@staff_member_required
@api_view(['POST'])
def api_frame_list_view(request):
    """Get list of frames."""
    images = Image.objects.all()

    # filtering
    if request.data:
        apiaries = request.data.get('apiaries')
        beehives = request.data.get('beehives')
        videos = request.data.get('videos')
        annotation_type = request.data.get('annotation_type')
        image_id = request.data.get('image_id')

        if apiaries:
            images = images.filter(beehive__apiary__in=apiaries)
        if beehives:
            images = images.filter(beehive__in=beehives)
        if videos:
            images = images.filter(image_set__video__in=videos)
        if image_id:
            images = images.filter(pk=image_id)

        # only consider images that have no been labeled yet with this
        # annotation type
        # TODO: also ignore images that are already labeled as part of the task
        if annotation_type:
            annotation_type = int(annotation_type)
            annotated_img_pks = GoldAnnotationSet.objects\
                .filter(annotation_type=annotation_type)\
                .values_list('image__pk', flat=True)\
                .distinct()
            images = images.exclude(pk__in=annotated_img_pks)
            print(images.query)

    images = list(images.values_list('pk', 'name'))

    return JsonResponse(images, safe=False)


@login_required
def view_image(request, image_pk):
    """
    This view is to authenticate direct access to the images via nginx auth_request directive

    it will return forbidden on if the user is not authenticated
    """
    image = get_object_or_404(Image, id=image_pk)

    response = FileResponse(open(image.path.url, 'rb'), content_type="image")

    response["Content-Length"] = os.path.getsize(image.path.path)
    return response