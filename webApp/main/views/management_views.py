import json
from itertools import groupby

from django.db.models import Count, Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse

from main.forms import TaskForm, TaskImageListFilterForm
from main.models import Task, Video, AnnotationAccuracy, LabelingTaskFrame, \
    Apiary, Hive, User, HiveTask, AnnotationType, Image
from django.contrib.admin.views.decorators import staff_member_required

from main.views.frame_views import grouped


@login_required
@staff_member_required
def management_overview_view(request):
    context = {}
    return render(request, 'main/management/management_overview.html', context)

@permission_required("main.view_task")
@staff_member_required
def management_task_list_view(request):
    """The list of tasks as seen by the admin in the management section."""
    tasks = sorted(Task.objects.all(), key=lambda x: x.finished)
    context = {
        'tasks': tasks
    }
    return render(request, 'main/management/tasks/management_task_list.html', context)


@login_required
def user_task_list_view(request):
    """The list of tasks as seen by a user on the side navbar.

    The user can select a given video in the viedo to start labeling its
    frames.

    Any user that can annotate images can see this view, and all of the availabe tasks.
    """
    tasks = sorted(Task.objects.all(), key= lambda x: x.finished)
    context = {
        'tasks': tasks
    }
    return render(request, 'main/management/tasks/user_task_list.html', context)


@permission_required("main.add_task")
@staff_member_required
def task_create_view(request):
    if request.body:
        data = json.loads(request.body.decode('utf-8'))
        annotation_type = data.get('annotation_type')
        selected_images = data.get('selected_images')
        if annotation_type and selected_images:
            task = Task.objects.create(
                creator=request.user,
                annotation_type=AnnotationType.objects.get(pk=annotation_type),
            )

            for image in Image.objects.filter(pk__in=selected_images):
                LabelingTaskFrame.objects.create(
                    frame=image,
                    task=task,
                )

            return HttpResponse(reverse('management-task-list'))

        return HttpResponse('error')

    task_form = TaskForm()
    filter_form = TaskImageListFilterForm(request.GET or None)

    context = {
        'task_form': task_form,
        'filter_form': filter_form
    }
    return render(request, 'main/management/tasks/task_create.html', context)


# TODO: review permission
@staff_member_required
def task_detail_view(request, task_pk):
    task = get_object_or_404(Task, pk=task_pk)
    task_frames = task.labelingtaskframe_set.order_by('frame__name')
    context = {
        'task': task,
        'task_frames': task_frames
    }

    return render(request, 'main/management/tasks/task_detail.html', context)


# TODO: review permission
@staff_member_required
def task_delete_view(request, task_pk):
    task = get_object_or_404(Task, pk=task_pk)
    task.delete()
    return redirect('management-task-list')


# TODO: review permission
@staff_member_required
def admin_hive_images_list_view(request):
    """Shows admin the list of selected beehive images.

    Those are the images shown when the beekeepers add a new beehive and
    selects the beehive that's most similar to it.
    """
    images = Image.objects.filter(selected=True)
    images = grouped(images, 4)
    if not images:
        return redirect('admin-hive-images-select')

    context = {
        'images': images
    }
    return render(request, 'main/management/admin-hive-images-list.html', context)


# TODO: review permission
@staff_member_required
def admin_hive_images_select_view(request):
    """Allows admin to select beehive images.

    Those are the images shown when the beekeeper adds a new beehive and
    selects the beehive that's most similar to it.
    """
    if request.method == 'POST':
        Image.objects.all().update(selected=False)
        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)
        for img_pk in body_data:
            img = Image.objects.get(pk=img_pk)
            img.selected = True
            img.save()

        return HttpResponse(reverse('admin-hive-images-list'))

    hive_images = groupby(Image.objects.all().order_by('beehive', '-selected'), key=lambda d: d.hive)
    hive_images = [(hive, [image for image in images]) for hive, images in hive_images]
    context = {
        'hive_images': grouped(hive_images, 4)
    }
    return render(request, 'main/management/admin-hive-images-select.html', context)


@staff_member_required
def environment_overview_view(request):
    return render(request, 'main/management/environment_overview.html')


@staff_member_required
def apiary_overview_view(request):
    apiaries = Apiary.objects.all()
    hives = Hive.objects.all()
    videos = Video.objects.all().order_by('-id')[:10]

    context = {
        'apiaries': apiaries,
        'hives': hives,
        'videos': videos
    }
    return render(request, 'main/management/apiary_overview.html', context)


def open_labeling_task_for_training(hive):
    """Open labeling task for hive for training."""
    # Save 100 randomly selected images as training/test frames
    train_frames = Image.objects \
       .filter(beehive=hive, status=Image.STATUS.UNCLASSIFIED) \
       .order_by('?')[:100]

    # create a labeling task
    task = Task.objects.create(
        creator=User.objects.get(username='admin'),
        annotation_type=AnnotationType.objects.get(name='bee')
    )
    HiveTask.objects.create(
        task=task,
        hive=hive,
        type=HiveTask.Type.TRAINING
    )
    for frame in train_frames:
        frame.status = Image.STATUS.TRAIN
        frame.save()

        LabelingTaskFrame.objects.update_or_create(
            task=task,
            frame=frame
        )