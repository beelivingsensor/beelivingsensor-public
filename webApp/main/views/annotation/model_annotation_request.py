import requests
from django.conf import settings

from main.models import AIModel


def process_video(video):
    if video.beehive.bee_model:
        bee_model = video.beehive.bee_model
    else:
        bee_model = AIModel.objects.get(description='ETH medium bee model')

    request_annotations(bee_model, video, media_type='video')


def process_image(image):
    if image.hive.bee_model:
        bee_model = image.hive.bee_model
    else:
        bee_model = AIModel.objects.get(description='ETH medium bee model')

    request_annotations(bee_model, image, media_type='image')


def request_annotations(bee_model, media, media_type):
    """Request the model-service to process the media.

    Args:
        bee_model (AIModel): The bee model to use.
        media (Video|Image): The media to process.
        media_type (str): The type of media ('video' or 'image')
    """
    # get tiny pollen model
    pollen_model = AIModel.objects.get(description='ETH tiny pollen model')

    if media.beehive.entrance_annotation:
        entrance_annotation = media.beehive.entrance_annotation.vector
    else:
        entrance_annotation = {}

    media_blob_path = f'{media.path.storage.azure_container}/{media.path.name}'
    r = requests.post(
        f'{settings.MODEL_SERVING_APP}/media/processes/',
        json={
            'media_pk': media.pk,
            'media_blob_path': media_blob_path,
            'media_type': media_type,
            'bee_model_pk': bee_model.pk,
            'bee_model_blob_path': bee_model.path,
            'pollen_model_pk': pollen_model.pk,
            'pollen_model_blob_path': pollen_model.path,
            'entrance_coordinates': entrance_annotation,
            'back_url': '/en/api/modelAnnotations/create/',
            'skip': 1
        })

    if r.status_code == 201:
        print(f'Request to modelserving app for {media.path.name} successful!')
        media.process_id = r.json()
        media.save()
    else:
        print(f'Request to modelserving app for {media.path.name} failed!')
