import json
import logging
import threading
from pathlib import Path
from types import SimpleNamespace

from django.conf import settings
from django.http import HttpResponse
from rest_framework.decorators import api_view

from main.util.azure_helpers import download_blob
from main.views.annotation.model_serving_app_data_saver import ModelServingAppDataSaver


logger = logging.getLogger(__name__)


@api_view(['POST'])
def model_annotations_create_view(request):
    """Add the media annotations made by the modelserving app."""
    if request.method != 'POST':
        return HttpResponse(status=405)

    sent_api_key = request.POST.get('api_key', None)
    webapp_api_key = settings.WEBAPP_API_KEY

    if not webapp_api_key or sent_api_key != webapp_api_key:
        return HttpResponse(status=403)

    annotations_path = request.POST.get('annotations_path')
    media_type = request.POST.get('media_type')

    if not annotations_path or not media_type:
        return HttpResponse(
            'Annotations path and media type cannot be empty',
            status=400)

    # check if for unit testing
    if request.POST.get('test'):
        _process_annotations(annotations_path, media_type)
    else:
        x = threading.Thread(
            target=_process_annotations, args=(annotations_path, media_type))
        x.start()

    return HttpResponse(status=201)


def _process_annotations(annotations_path, media_type):
    try:
        annotations_path = Path(annotations_path)
        container = annotations_path.parts[0]
        blob = Path(*annotations_path.parts[1:])
        logger.info('⏬ Downloading model annotations')
        download_blob(container, blob, blob)
        with open(blob) as f:
            media = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
        blob.unlink()
        logger.info('💾 Saving model annotations in DB')
        model_serving_app_data_saver = ModelServingAppDataSaver()
        status = model_serving_app_data_saver.add(media, media_type)
    except Exception as e:
        logger.info(f'❌ Processing model annotations failed: {e}')
    else:
        if status:
            logger.info('✅ Processing of model annotations done!')
        else:
            logger.info(f'❌ Processing model annotations failed!')
