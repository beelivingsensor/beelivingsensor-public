import logging
from datetime import datetime
from pathlib import Path

from django.shortcuts import get_object_or_404

from main.models import Image, ModelAnnotationSet, AnnotationType, AIModel, \
    ModelAnnotation, Video
from main.models.machine_learning.annotation.tracker_event import TrackerEvent

logger = logging.getLogger(__name__)


class ModelServingAppDataSaver:
    """Save the data of the model-serving app in the DB.

    This includes:
    - hive and bee images
    - bee and pollen annotations
    - tracker events

    Technical note: variables prefixed with 'p_' stand for posted data,
    i.e. data delivered by the model-serving app,
    whereas those with this prefix are database data.
    """

    # after how many images, they should be added to the DB
    bulk_create_step = 300

    def __init__(self):
        self.images = []
        self.annotation_sets = []
        self.annotations = []
        self.tracker_events = []
        self.media_type = None
        self.media = None

    def add(self, p_media, media_type):
        """Add the data of the model-serving app to the DB.

        Args:
            p_media (Media): The media data posted by the model-serving app.
            media_type (str): The type of media, either 'image' or 'video'.

        Returns:
            status (bool): Whether adding the annotations succeeded.
        """
        self.media_type = media_type

        try:
            if media_type == 'video':
                try:
                    self.media = Video.objects.get(pk=p_media.pk)
                except Video.DoesNotExist:
                    logger.error(
                        f'❌ Video {p_media.pk} does not exist in database. '
                        f'Model annotations are not added!')
                    return False
                status = self._add_video_annotations_to_db(p_media)
            else:
                self.media = Image.objects.get(pk=p_media.pk)
                status = self._add_image_annotations_to_db(p_media)
        except Exception as e:
            logger.error(f'❌ Creating model annotation objects failed: {e}')
            return False

        if status:
            status = self.bulk_create()

        self.media.finished = datetime.now()
        self.media.save()

        return status

    def bulk_create(self):
        logger.debug(f'Bulk creation of model annotations')
        try:
            Image.objects.bulk_create(self.images)
            ModelAnnotationSet.objects.bulk_create(self.annotation_sets)
            ModelAnnotation.objects.bulk_create(self.annotations)
            TrackerEvent.objects.bulk_create(self.tracker_events)
        except Exception as e:
            logger.error(f'❌ Bulk creation of model annotations failed: {e}')
            return False

        self.images = []
        self.annotation_sets = []
        self.annotations = []
        self.tracker_events = []

        return True

    def _add_video_annotations_to_db(self, p_video):
        if self.media.imageset.images.count() > 0:
            logger.error(f'❌ Video {p_video.pk} already has images. '
                         f'Model annotations are not added!')
            return False

        self._add_video_metadata(p_video)
        for p_img in p_video.images:
            img = self._create_image(p_img)
            self._create_annotation_sets(p_img, img)
            logger.debug(f'✅ Processing of model annotations of image #{p_img.pk} done')

            if p_img.pk % self.bulk_create_step == 0:
                status = self.bulk_create()
                if not status:
                    return status

        return True

    def _add_video_metadata(self, p_video):

        annotations_path = Path(p_video.annotations_path)
        annotations_blob = Path(*annotations_path.parts[1:])
        self.media.annotations_path.name = str(annotations_blob)

        self.media.fps = p_video.fps
        self.media.width = p_video.width
        self.media.height = p_video.height
        self.media.image_count = p_video.image_count
        self.media.size = p_video.size
        self.media.save()

    def _add_image_annotations_to_db(self, p_img):
        self._create_annotation_sets(p_img, self.media)
        return True

    def _create_image(self, p_img):
        if self.media_type == 'video':
            image_set = self.media.imageset
            beehive = self.media.beehive
        else:
            image_set = None
            beehive = self.media.hive

        fname = Path(p_img.path).name
        # removes the container name (= get blob name)
        path = str(Path(*Path(p_img.path).parts[1:]))
        img = Image(
            image_set=image_set,
            beehive=beehive,
            name=fname,
            filename=fname,
            width=p_img.width,
            height=p_img.height,
            type=p_img.type,
            frame=p_img.pk
        )
        img.path.name = path
        self.images.append(img)

        return img

    def _create_annotation_sets(self, p_img, img):
        for p_annotation_set in p_img.annotation_sets:
            # skip entrance annotation set
            if p_annotation_set.annotation_type != 'entrance':
                annotation_set = self._create_annotation_set(
                    img, p_annotation_set)
                self._create_annotations(annotation_set, p_annotation_set)

    def _create_annotation_set(self, img, p_annotation_set):
        model = get_object_or_404(AIModel, pk=p_annotation_set.model_pk)
        annotation_type = get_object_or_404(
            AnnotationType, name=p_annotation_set.annotation_type)
        annotation_set = ModelAnnotationSet(
            model=model,
            annotation_type=annotation_type,
            image=img
        )

        for p_tracker_event in p_annotation_set.tracker_events:
            tracker_event = TrackerEvent(
                annotation_set=annotation_set,
                object_id=p_tracker_event.object_id,
                direction=p_tracker_event.direction,
                pollen=[self.to_hex_color(c) for c in p_tracker_event.pollen]
            )
            self.tracker_events.append(tracker_event)

        self.annotation_sets.append(annotation_set)
        return annotation_set

    def _create_annotations(self, annotation_set, p_annotation_set):
        for p_annotation in p_annotation_set.annotations:
            self._create_annotation(annotation_set, p_annotation)

    def _create_annotation(self, annotation_set, p_annotation):
        annotation = ModelAnnotation(
            annotation_set=annotation_set,
            confidence=p_annotation.confidence,
            vector={
                'x1': p_annotation.x_min,
                'y1': p_annotation.y_min,
                'x2': p_annotation.x_max,
                'y2': p_annotation.y_max},
            object_id=p_annotation.object_id,
            color=self.to_hex_color(p_annotation.color)
        )

        if p_annotation.image:
            crop_img = self._create_image(p_annotation.image)
            annotation.image = crop_img
            self._create_annotation_sets(p_annotation.image, crop_img)

        self.annotations.append(annotation)

    @staticmethod
    def to_hex_color(rgb):
        if rgb:
            r, g, b = rgb
            return '#{0:02x}{1:02x}{2:02x}'.format(r, g, b)

        return None
