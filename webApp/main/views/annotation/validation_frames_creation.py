from main.models import Image, AnnotationType, User, \
    Task, HiveTask, LabelingTaskFrame


def create_validation_images(beehive):
    if not beehive.has_validation_images:
        select_validation_images(beehive)
        open_labeling_task(beehive)


def select_validation_images(beehive):
    """Select 40 random images from the beehive as validation frames."""
    validation_frames = Image.objects \
        .filter(beehive=beehive,
                status=Image.STATUS.UNCLASSIFIED,
                type=Image.TYPE.HIVE
                ) \
        .order_by('?')[:40]

    for frame in validation_frames:
        frame.status = Image.STATUS.VALIDATE
        frame.save()


def open_labeling_task(beehive):
    """Open a labeling task for the validation images of the beehive."""
    validation_frames = Image.objects.filter(
        beehive=beehive,
        status=Image.STATUS.VALIDATE,
        type=Image.TYPE.HIVE
    )

    # create a labeling task for those validation frames
    task = Task.objects.create(
        creator=User.objects.get(username='admin'),
        annotation_type=AnnotationType.objects.get(name='bee')
    )

    HiveTask.objects.create(
        task=task,
        hive=beehive,
        type=HiveTask.Type.VALIDATION
    )

    for frame in validation_frames:
        LabelingTaskFrame.objects.update_or_create(
            task=task,
            frame=frame
        )
