from main.models import LabelingTaskFrame, UserAnnotation, GoldAnnotationSet, \
    GoldAnnotation
from main.util.model_performance import calc_iou


def merge_task_annotations(task):
    """Merge all annotations of all images of a task.

    Args:
        task (main.Task): The labeling task.
    """
    images = LabelingTaskFrame.objects.filter(task=task)
    for image in images:
        merge_image_annotations(image.frame, task.annotation_type)


def merge_image_annotations(image, annotation_type):
    """Merge annotations in an image.

    Creates GoldAnnotations.

    Args:
        image (Image): The image for which the annotations should be merged.
        annotation_type (AnnotationType): What kind of annotations
        should be merged.
    """
    user_annotations = UserAnnotation.objects.filter(
        annotation_set__image=image,
        annotation_set__annotation_type=annotation_type,
        annotation_set__finished=True
    )

    annotation_groups = group_annotations(user_annotations)
    merged_groups = merge_annotation_groups(annotation_groups)
    cleaned_groups = clean_groups(merged_groups)

    gold_annotation_set = GoldAnnotationSet.objects.create(
        image=image,
        annotation_type=annotation_type
    )

    gold_annotations = []

    for merged_group in cleaned_groups:
        avg_vector = compute_avg_annotation(merged_group)
        gold_annotation = GoldAnnotation(
            annotation_set=gold_annotation_set,
            vector=avg_vector
        )
        gold_annotations.append(gold_annotation)

    GoldAnnotation.objects.bulk_create(gold_annotations)


def compute_avg_annotation(annotations):
    """Compute the average of several annotations.

    Args:
        annotations (Iterable[UserAnnotation]): The user annotations.

    Returns:
        Dict[str, int]: The vector of the averaged annotation.
    """
    a_avg = {}
    for p in ('x1', 'x2', 'y1', 'y2'):
        avg_p = sum(a.vector[p] for a in annotations) / len(annotations)
        a_avg[p] = int(avg_p)
    return a_avg


def group_annotations(annotations):
    """Put pairs of annotations in group if their annotations are similar.

    Args:
        annotations (List[UserAnnotation]): The annotations of the users.

    Returns:
        Set[FrozenSet[UserAnnotation]]: The groups of user annotations.
    """
    iou_threshold = 0.7
    groups = set()
    for a1 in annotations:
        for a2 in annotations:
            if a1 == a2:
                continue

            iou = calc_iou(conv(a1.vector), conv(a2.vector))
            if iou >= iou_threshold:
                group = sorted([a1, a2], key=lambda x: x.pk)
                groups.add(frozenset(group))

    return groups


def merge_annotation_groups(annotation_groups):
    """Merge groups where the same annotation occurs.

    Args:
        annotation_groups (Set[FrozenSet[UserAnnotation]]) The groups of
            user annotations.

    Returns:
        Set[FrozenSet[UserAnnotation]]: The merged groups of user annotations.
    """
    merged_groups = set()
    overlapping_groups = set()

    # Merge overlapping groups
    for g1 in annotation_groups:
        for g2 in annotation_groups:
            if g1 == g2:
                continue

            # if they intersect, union them
            if g1 & g2:
                merged_groups.add(g1 | g2)
                overlapping_groups.add(g1)
                overlapping_groups.add(g2)

    # Add the rest of the groups that do not overlap with other groups
    for g in annotation_groups:
        if g not in overlapping_groups:
            merged_groups.add(g)

    return merged_groups


def clean_groups(merged_groups):
    """Remove annotations of the same user if they are in the same group.

    Args:
        merged_groups (Set[FrozenSet[UserAnnotation]]) The merged groups of
            user annotations.

    Returns:
        Set[FrozenSet[UserAnnotation]]: The cleaned merged groups
            of user annotations.
    """
    cleaned_groups = set()
    for g in merged_groups:
        users = set()
        cleaned_group = []
        for a in sorted(list(g), key=lambda x: x.pk):
            if a.annotation_set.user not in users:
                users.add(a.annotation_set.user)
                cleaned_group.append(a)
        if len(cleaned_group) > 1:
            cleaned_groups.add(frozenset(cleaned_group))

    return cleaned_groups


def conv(a):
    return a['x1'], a['y1'], a['x2'], a['y2']
