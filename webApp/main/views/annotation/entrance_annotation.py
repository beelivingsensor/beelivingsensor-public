import random

from main.models import Image, User, AnnotationType, Task, HiveTask, \
    LabelingTaskFrame


def open_entrance_labeling_task(beehive):
    """Open a labeling task for the entrance of the beehive."""
    images = Image.objects.filter(
        beehive=beehive,
        type=Image.TYPE.HIVE
    )

    if images:
        image = random.choice(images)

        task = Task.objects.create(
            creator=User.objects.get(username='admin'),
            annotation_type=AnnotationType.objects.get(name='entrance')
        )

        HiveTask.objects.create(
            task=task,
            hive=beehive,
            type=HiveTask.Type.ENTRANCE
        )

        LabelingTaskFrame.objects.create(
            task=task,
            frame=image
        )
