from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render

from main.forms import UserForm
from main.models import User


@login_required
def user_update_view(request, user_pk):
    user = get_object_or_404(User, pk=user_pk)
    if request.user != user:
        raise PermissionDenied()

    user_form = UserForm(request.POST or None, instance=user)

    if user_form.is_valid():
        user_form.save()
        return redirect('index')

    context = {'user_form': user_form}

    return render(request, 'main/user/user_update.html', context)
