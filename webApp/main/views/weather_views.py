import gzip
from pathlib import Path

from django.contrib.gis.geos import Point
from django.shortcuts import get_object_or_404, render, redirect
from xml.etree import ElementTree
from django.conf import settings
from django.contrib.auth.decorators import permission_required

from main.models import Apiary, WeatherStation, WeatherMeasurement, \
    WeatherParameter

from main.decorators import user_is_manager

import requests
import urllib
import json

OPENWEATHERMAP_MAPPINGS = {
    'temperature': 'temperature',
    'humidity': 'humidity',
    'pressure': 'pressure',
    'feels_like': 'feels-like'
}

def create_all_weather_parameters():
    """Create WeatherParameter instances.

    This is required when adding a weather measurement as each measurement
    needs to specify what kind of parameter was measured (such as temperature,
    humidity, etc.).
    """
    WeatherParameter.objects.update_or_create(
        name='temperature',
        unit='°',
        description='Canonical temperature'
    )

    WeatherParameter.objects.update_or_create(
        name='humidity',
        unit='%',
        description='Humidity'
    )

    WeatherParameter.objects.update_or_create(
        name='pressure',
        unit='hPa',
        description='Pressure'
    )

    WeatherParameter.objects.update_or_create(
        name='feels-like',
        unit='°',
        description='The heat index that combines air temperature and relative humidity.'
    )


def fetch_weather_station_json(weather_stations_json_path):
    """Fetch the JSON file with the weather stations from OpenWeatherMap."""
    URL = 'http://bulk.openweathermap.org/sample/city.list.json.gz'
    with urllib.request.urlopen(URL) as response:
        content = gzip.decompress(response.read())
        with open(weather_stations_json_path, 'wb') as f:
            f.write(content)


def get_weather_station(weather_stations_json_path, weather_station_id):
    """Create WeatherStation instance.

    Searches the weather station in the JSON file and uses the coordinates
    stored there to create a WeatherStation instance.

    Args:
        weather_stations_json_path: Path to the json file where all
            weather stations of OpenWeatherMap are stored.
        weather_station_id: The ID of the weather station that is searched
            for.
    """
    with open(weather_stations_json_path, 'r') as f:
        data = json.load(f)
        for record in data:
            city_id = str(record['id'])
            if city_id == weather_station_id:
                return WeatherStation.objects.create(
                    OWM_id=city_id,
                    name=record['name'],
                    location=Point(
                        record['coord']['lon'],
                        record['coord']['lat']
                    )
                )
    return None


def create_weather_station(weather_station_id):
    """Create weather station in database.

    OpenWeatherMap keeps a json file with all weather stations and their
    coordinates (the coordinates returned in the API calls are the
    coordinates searched by the user not the weather station's coordinates).

    First, the JSON file is downloaded if it not exists yet. Then, the weather
    station with the given ID is created. If the weather station does not
    occur in the JSON file, this JSON is re-downloaded.

    Args:
        weather_station_id: The ID given by OpenWeatherMap to the weather
                            station.
    """
    json_dir = Path(settings.BASE_DIR) / 'static' / 'json'
    json_dir.mkdir(parents=True, exist_ok=True)
    weather_stations_json_path = json_dir / 'weather_stations.json'

    if not weather_stations_json_path.exists():
        fetch_weather_station_json(weather_stations_json_path)

    weather_station = get_weather_station(weather_stations_json_path,
                                          weather_station_id)

    if weather_station is None:
        fetch_weather_station_json(weather_stations_json_path)
        weather_station = get_weather_station(weather_stations_json_path,
                                              weather_station_id)

    return weather_station


def create_weather_measurements(apiary):
    """Create current weather measurements for an apiary.

    Uses the OpenWeatherMap API to get weather data for the location of
    the apiary. A request to this API returns an XML with a set of
    weather parameter values (temperature, humidity, etc.) and
    the ID of the weather station (called `city` in OpenWeatherMap).
    To retrieve the GPS location of the weather station, a separate JSON file
    has to be downloaded and parsed from OpenWeatherMap.
    NB: the coordinates returned in the API calls are the
    coordinates searched by the user not the weather station's coordinates.
    """
    # TODO: call this somewhere else
    create_all_weather_parameters()

    OWM_API_KEY = settings.OPENWEATHERMAP_API_KEY
    longitude = apiary.location.x
    latitude = apiary.location.y
    OWM_API_URL = f'http://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OWM_API_KEY}&mode=xml&units=metric'

    response = requests.get(OWM_API_URL)
    response.raise_for_status() # in case of 404 or any error, throw an exception
    root = ElementTree.fromstring(response.content)
    city_elem = root[0]
    city_id = city_elem.get('id')
    weather_stations = WeatherStation.objects.filter(OWM_id=city_id)
    if not weather_stations.exists():
        weather_station = create_weather_station(city_id)
    else:
        weather_station = weather_stations.first()

    last_update = root.find('lastupdate').get('value')

    for child_elem in root:
        if child_elem.tag in OPENWEATHERMAP_MAPPINGS:
            name = OPENWEATHERMAP_MAPPINGS[child_elem.tag]
            param = WeatherParameter.objects.get(name=name)
            value = child_elem.get('value')

            WeatherMeasurement.objects.get_or_create(
                apiary=apiary,
                weather_station=weather_station,
                last_update=last_update,
                parameter=param,
                defaults={
                    'value': value
                }
            )


@permission_required("main.add_weathermeasurement")
@user_is_manager(Apiary)
def apiary_weather_measurement_create_view(_, pk):
    apiary = get_object_or_404(Apiary, pk=pk)
    create_weather_measurements(apiary)

    return redirect('apiary-weather-measurement-list', pk=pk)


@permission_required("main.view_weathermeasurement")
@user_is_manager(Apiary)
def apiary_weather_measurement_list_view(request, pk):
    """Shows a list of weather measurements for the given apiary."""
    apiary = get_object_or_404(Apiary, pk=pk)

    weather_measurements = WeatherMeasurement.objects.filter(apiary=apiary)

    context = {
        'apiary': apiary,
        'weather_measurements': weather_measurements
    }

    return render(request, 'main/weather/apiary_weather_measurement_list.html', context)


@permission_required("main.delete_weathermeasurement")
@user_is_manager(Apiary, "apiary_pk")
def weather_measurement_delete_view(request, apiary_pk, measurement_pk):
    """Deletes a weather measurement of a given apiary."""
    get_object_or_404(WeatherMeasurement, pk=measurement_pk).delete()
    return redirect('apiary-weather-measurement-list', pk=apiary_pk)
