import random
import urllib

import requests
from django.conf import settings
from django.core.paginator import Paginator
from django.db import models
from django.db.models import Value, F
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.urls import reverse
from PIL import Image as PILImage

from main.decorators import user_is_manager
from django.contrib.admin.views.decorators import staff_member_required

from rest_framework.decorators import api_view

from main.forms import BeehiveForm, BeehiveImageForm, ImageListFilterForm
from main.models import Hive, Video, Apiary, SensorMeasurement, \
    WeatherMeasurement, Image
from main.serializers import MeasurementSerializer
from main.views.annotation.entrance_annotation import \
    open_entrance_labeling_task
from main.views.annotation.validation_frames_creation import \
    select_validation_images, open_labeling_task, create_validation_images
from main.views.frame_views import grouped


@permission_required("main.add_hive")
def beehive_create_view(request, pk):
    apiary = get_object_or_404(Apiary, pk=pk)
    beehive_form = BeehiveForm(request.POST or None)

    if beehive_form.is_valid():
        beehive = beehive_form.save(commit=False)
        beehive.apiary = apiary
        beehive.creator = request.user
        beehive.save()
        return redirect('beehive-detail', beehive.pk)

    context = {
        'apiary': apiary,
        'beehive_form': beehive_form
    }

    return render(request, 'main/beehive/beehive_create.html', context)


# TODO: review permission
@staff_member_required
def select_similar_beehive_view(request, beehive_pk):
    """Let user select most similar beehive when adding a new beehive.

    Based on the selected beehive, the three best models of that beehive
    are used for the new beehive.
    """
    hive = get_object_or_404(Hive, pk=beehive_pk)
    if request.method == 'POST':
        similar_beehive_pk = int(request.body.decode('utf-8'))
        similar_beehive = get_object_or_404(Hive, pk=similar_beehive_pk)
        hive.similar_beehive = similar_beehive
        hive.save()
        return HttpResponse(reverse('beehive-detail', args=(beehive_pk,)))

    images = Image.objects.filter(selected=True)
    images = grouped(images, 4)
    context = {
        'beehive': hive,
        'images': images
    }
    return render(request, 'main/beehive/similar_beehive-select.html', context)


@permission_required("main.view_hive")
@user_is_manager(Hive)
def beehive_detail_view(request, pk):
    beehive = Hive.objects.get(pk=pk)
    videos = Video.objects.filter(beehive=beehive).order_by('-pk')

    if 'representative-videos-uploaded' in request.GET:
        create_validation_images(beehive)
        open_entrance_labeling_task(beehive)

    context = {
        'beehive': beehive,
        'video_count': videos.count,
        'videos': videos[:5]
    }
    return render(request, 'main/beehive/beehive_detail.html', context)


@permission_required("main.change_hive")
@user_is_manager(Hive)
def beehive_update_view(request, pk):
    beehive = get_object_or_404(Hive, pk=pk)
    beehive_form = BeehiveForm(request.POST or None, instance=beehive)

    if beehive_form.is_valid():
        beehive_form.save()
        return redirect('beehive-detail', pk=beehive.pk)

    context = {
        'beehive': beehive,
        'beehive_form': beehive_form
    }

    return render(request, 'main/beehive/beehive_update.html', context)


@permission_required("main.view_hive")
@staff_member_required
def beehive_list_view(request):
    hives = Hive.objects.all()
    return render(request, 'main/beehive/beehive_list.html', {"hives": hives})


@permission_required("main.delete_hive")
@user_is_manager(Hive, "pk")
def beehive_delete_view(_, pk):
    hive = get_object_or_404(Hive, pk=pk)
    Hive.objects.filter(pk=hive.id).delete()
    return redirect('apiary-detail', pk=hive.apiary.pk)


@permission_required("main.view_hive")
@user_is_manager(Hive, "beehive_pk")
def beehive_video_list_view(request, beehive_pk):
    """List videos for a given beehive.

    Args:
        beehive_pk (int): The primary key of the beehive.
    """
    beehive = get_object_or_404(Hive, pk=beehive_pk)
    videos = Video.objects.filter(beehive=beehive).order_by('-pk')

    paginator = Paginator(videos, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'beehive': beehive,
        'video_count': videos.count(),
        'page_obj': page_obj
    }
    return render(request, 'main/video/beehive_video_list.html', context)


@permission_required("main.view_hive")
@user_is_manager(Hive, "hive_pk")
@api_view(['GET'])
def beehive_graph_data_api_view(_, hive_pk):
    """Get all data used in the graph in the beehive detail view.

    It fetches all the data from the sensors (SensorMeasurement) and
    of the weather stations (WeatherMeasurement).
    """
    hive = get_object_or_404(Hive, pk=hive_pk)
    # get all measurements of all sensors of the beehive
    sensor_measurements = SensorMeasurement.objects.filter(sensor__device__hive=hive)
    sensor_measurements = sensor_measurements.values(
        'value',
        'time',
        source=Value(value='hive-sensor', output_field=models.CharField()),
        param=F('sensor__sensor_type__name'),
        unit=F('sensor__sensor_type__unit')
    )
    # TODO: add pressure
    weather_measurements = WeatherMeasurement.objects\
        .filter(apiary=hive.apiary)\
        .exclude(parameter__name='pressure')
    weather_measurements = weather_measurements.values(
        'value',
        'time',
        source=Value(value='weather', output_field=models.CharField()),
        param=F('parameter__name'),
        unit=F('parameter__unit')
    )

    measurements = sensor_measurements.union(weather_measurements).order_by('time')

    serializer = MeasurementSerializer(measurements, many=True)
    return JsonResponse(serializer.data, safe=False)


@permission_required("main.view_hive")
@user_is_manager(Hive, "beehive_pk")
def beehive_image_list_view(request, beehive_pk):
    """The list of images for a given beehive."""
    beehive = get_object_or_404(Hive, pk=beehive_pk)
    images = Image.objects.filter(beehive=beehive)

    # filter criteria
    form = ImageListFilterForm(request.GET or None)
    querystring = ''
    if form.is_valid():
        uploaded_modus = form.cleaned_data.get('uploaded_modus')
        if uploaded_modus == 'video':
            images = images.filter(image_set__isnull=False)
        elif uploaded_modus == 'image':
            images = images.filter(image_set__isnull=True)

        status = form.cleaned_data.get('status')
        for stat in Image.STATUS:
            if status == stat:
                images = images.filter(status=stat)

        type_ = form.cleaned_data.get('type')
        for typ in Image.TYPE:
            if typ == type_:
                images = images.filter(type=typ)

        querystring = urllib.parse.urlencode(form.cleaned_data)

    # paging
    paginator = Paginator(images, 8)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'querystring': querystring,
        'image_count': images.count(),
        'form': form,
        'beehive': beehive,
        'page_obj': page_obj,
        'image_groups': grouped(page_obj, 12)
    }
    return render(request, 'main/beehive/beehive_images_list.html', context)


# TODO: review permission
@staff_member_required
def beehive_image_create_view(request, beehive_pk):
    beehive = get_object_or_404(Hive, pk=beehive_pk)

    if request.method == 'POST':
        upload_form = BeehiveImageForm(request.POST, request.FILES)
        if upload_form.is_valid():

            with PILImage.open(request.FILES['file']) as im:
                width, height = im.size

            Image.objects.create(
                path=request.FILES['file'],
                beehive=beehive,
                name='blabla',
                filename='blabla',
                width=width,
                height=height
            )
            return redirect('beehive-image-list', beehive_pk=beehive.pk)
    else:
        upload_form = BeehiveImageForm()

    context = {
        'upload_form': upload_form
    }

    return render(request, 'main/beehive/beehive_image_create.html', context)
