from pathlib import Path

from django.shortcuts import render

from main.models import Image, GoldAnnotation
from main.util.azure_helpers import get_latest_snapshot_url


def download_list_view(request):
    return render(request, 'main/download/download_list.html', {})


def download_ml_data_view(request):
    total_image_count = Image.objects.filter(goldannotationset__isnull=False)\
        .distinct()\
        .count()

    bee_image_count = Image.objects\
        .filter(goldannotationset__annotation_type__name='bee')\
        .count()

    pollen_image_count = Image.objects\
        .filter(goldannotationset__annotation_type__name='pollen')\
        .count()

    bee_annotation_count = GoldAnnotation.objects\
        .filter(annotation_set__annotation_type__name='bee')\
        .count()

    pollen_annotation_count = GoldAnnotation.objects\
        .filter(annotation_set__annotation_type__name='pollen')\
        .count()

    base_url = 'https://beelivingsensor.blob.core.windows.net/public'

    latest_snapshot_url = get_latest_snapshot_url()
    if latest_snapshot_url:
        archive_name = Path(latest_snapshot_url).name
    else:
        archive_name = None

    context = {
        'tiny_pollen_model_url': f'{base_url}/models/pollen/pollen_tiny.weights',
        'large_pollen_model_url': f'{base_url}/models/pollen/pollen_large.weights',
        'medium_bee_model_url': f'{base_url}/models/bee/bee_medium.weights',
        'large_bee_model_url': f'{base_url}/models/bee/bee_large.weights',
        'latest_snapshot_url': latest_snapshot_url,
        'archive_name': archive_name,
        'total_image_count': total_image_count,
        'bee_image_count': bee_image_count,
        'pollen_image_count': pollen_image_count,
        'bee_annotation_count': bee_annotation_count,
        'pollen_annotation_count': pollen_annotation_count,
    }
    return render(request, 'main/download/download_ml_data.html', context)
