import datetime
import logging
import random
from azure.storage.blob import generate_blob_sas, BlobSasPermissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.urls import reverse

from main.decorators import user_is_manager

from django.conf import settings
from django.db import transaction
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import permission_required
from django.contrib.admin.views.decorators import staff_member_required

from main.forms import VideoUpdateForm
from main.models import Apiary, Video, Hive, AIModel, ModelAnnotation, \
    Validation, \
    Image, ImageSet, User, ModelAnnotationSet, AnnotationType
from main.models.machine_learning.annotation.tracker_event import TrackerEvent

from main.views.annotation.model_annotation_request import process_video


logger = logging.getLogger(__name__)


def generate_video_blob_url(fileName):
    return "{}/{}/{}?{}".format(
        settings.AZURE_BLOB_HOST,
        settings.AZURE_VIDEOS_CONTAINER,
        fileName,
        generateVideoSAS(fileName)
    )

def generateVideoSAS(fileName):
    sasExpiry = datetime.datetime.now() + datetime.timedelta(days=settings.AZURE_SAS_DURATION_DAYS) # When is the SAS link no longer usable
    perm = BlobSasPermissions(create=True, add=True, read=True, write=True)

    return generate_blob_sas(
        settings.AZURE_STORAGE_ACCOUNT,
        settings.AZURE_VIDEOS_CONTAINER,
        fileName,
        account_key=settings.STORAGE_KEY,
        permission=perm,
        expiry=sasExpiry
    )


@api_view(['POST'])
@permission_required("main.add_video")
@user_is_manager(Hive)
def video_upload_sas_key(request,pk):

    fileName = request.data["file-name"]
    beehive = get_object_or_404(Hive, pk=pk)
    apiary = beehive.apiary
    fileName = "{}_{}_{}".format(apiary.name, beehive.name, fileName)
    path = "/{}/{}".format(settings.AZURE_VIDEOS_CONTAINER, fileName)

    

    return JsonResponse({"host": settings.AZURE_BLOB_HOST,
    "path" :path, # Path where the file is saved on azure
    "fileName": fileName, # Name of the file, but different! It is using underscores, and contains also the apiary and beehive names
    "sasToken" : "?" + generateVideoSAS(fileName)})


def addVideo(user, hive, filename, process=True):
    video = Video()
    video.path.name = filename
    video.fileName = filename
    video.creator = user
    video.beehive = hive
    video.processed = False

    with transaction.atomic():
        imageset = ImageSet()
        imageset.name = video.filename()
        imageset.save()
        # PK not available until imageset creation
        imageset.path = f'imageset_{imageset.pk}'
        imageset.save()
        video.imageset = imageset

    video.save()

    if process:
        process_video(video)


@api_view(['POST'])
def confirmUploadFTP(request):
    sent_api_key = request.data.get('apikey', None)
    webapp_api_key = settings.WEBAPP_API_KEY
    if not webapp_api_key or sent_api_key != webapp_api_key:
        logger.error("Incorrect API key")
        return HttpResponse(status=403)

    # The hive has to exist! before we can start sending FTP confirm uploads to it.
    logger.debug(request.data)
    filename = request.data["filename"]

    if filename.split(".")[-1] != "mp4":
        logger.error("Not a video file!")
        return HttpResponseNotFound("Not a video file!")

    user = User.objects.get(username=request.data["username"])
    logger.debug(f'{filename} {user}')

    apiary = Apiary.objects.get(name=filename.split("/")[0])
    hive = apiary.hive_set.get(name=filename.split("/")[1])
    if hive.isManagedBy(user):
        addVideo(user, hive, filename, process=False)
        return Response(status=status.HTTP_200_OK)
    logger.error("User is not the manager of the Hive.")
    return HttpResponseNotFound("User is not the manager of the Hive.")



@api_view(['POST'])
@permission_required("main.add_video")
@user_is_manager(Hive)
def confirmUpload(request, pk):
    fname = request.data["fileName"]
    logger.debug(f"confirm upload {fname}")
    hive = get_object_or_404(Hive, pk=pk)
    addVideo(request.user, hive, request.data["fileName"])

    return Response(status=status.HTTP_200_OK)


@permission_required("main.add_video")
def video_create_view(request, pk):
    hive = get_object_or_404(Hive, pk=pk)

    context = {
        'hive': hive
    }

    return render(request, 'main/video/video_create.html', context)


@permission_required("main.view_video")
@user_is_manager(Video)
def video_detail_view(request, pk):
    video = get_object_or_404(Video, pk=pk)
    check_accuracy = False
    images_url = reverse('video-image-list', args=(pk,))
    in_bees = 0
    out_bees = 0
    pollen_count = 0
    pollen_colors = set()
    model = None

    bee_type = AnnotationType.objects.get(name='bee')

    models = AIModel.objects.filter(
        modelannotationset__image__image_set__video=video,
        modelannotationset__annotation_type=bee_type
    ).order_by('-modelannotationset__pk')
    logger.debug(models)

    if models:
        model = models.first()

        tracker_events = TrackerEvent.objects.filter(
            annotation_set__annotation_type=bee_type,
            annotation_set__model=model,
            annotation_set__image__image_set__video=video
        )
        in_events = tracker_events.filter(
            direction=TrackerEvent.DirectionChoices.IN)
        in_bees = in_events.count()
        out_bees = tracker_events.filter(
            direction=TrackerEvent.DirectionChoices.OUT).count()

        for event in in_events:
            for pollen in event.pollen:
                pollen_count += 1
                pollen_colors.add(pollen)

    context = {
        'images_url': images_url,
        'check_accuracy': check_accuracy,
        'video': video,
        'in_bees': in_bees,
        'out_bees': out_bees,
        'pollen_count': pollen_count,
        'pollen_colors': pollen_colors,
        'model': model
    }

    return render(request, 'main/video/video_detail.html', context)


@permission_required("main.change_video")
@user_is_manager(Video)
def video_update_view(request, pk):
    video = get_object_or_404(Video, pk=pk)
    videoForm = VideoUpdateForm(request.POST or None, instance=video)

    if videoForm.is_valid():
        videoForm.save()
        return redirect('video-detail', pk=video.pk)

    context = {'video_form': videoForm,}

    return render(request, 'main/video/video_update.html', context)


@permission_required("main.view_video")
@staff_member_required
def video_list_view(request):
    videos = Video.objects.all()
    context = {'videos': videos}
    return render(request, 'main/video/video_list.html', context)


@permission_required("main.delete_video")
@user_is_manager(Video)
def video_delete_view(_, pk):
    video = get_object_or_404(Video, pk=pk)
    Video.objects.filter(pk=video.id).delete()
    return redirect('beehive-detail', pk=video.beehive.pk)


@permission_required("main.view_video")
def video_image_list_view(request, pk):
    """Show list of images of the video."""
    video = get_object_or_404(Video, pk=pk)
    context = {
        'video': video
    }
    return render(request, 'main/video/video_image_list.html', context)

# TODO: review permission
@staff_member_required
def select_best_model_for_video_frame(request, video_pk):
    """Let user select the best model for a random frame of the video.

    The annotations of three models are shown on the image and the
    user has to decide which annotations are the most accurate. The three
    best models are chosen based on the most similar beehive.
    """
    video = get_object_or_404(Video, pk=video_pk)
    if request.method == 'POST':
        model_pk = int(request.body.decode('utf-8'))
        model = get_object_or_404(AIModel, pk=model_pk)
        video.best_model = model
        video.save()
        return HttpResponse(reverse('video-detail', args=(video_pk,)))

    most_similar_beehive = video.beehive.similar_beehive

    # TODO: use f1-measure here as a cominbation of recall and precision
    models = AIModel.objects\
        .filter(validation__hive=most_similar_beehive)\
        .order_by('-validation__recall')[:3]

    frame = random.choice(Image.objects.filter(image_set__video=video))

    context = {
        'models': models,
        'frame': frame
    }

    return render(request, 'main/video/select_best_model.html', context)
