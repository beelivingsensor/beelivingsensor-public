import threading
import requests

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from rest_framework import status
from rest_framework.response import Response

from main import forms
from main.models import Apiary, Hive, AIModel, Image, UserAnnotation
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from main.views.aimodel_views import validateModel

from main.util.ftpAPI import getAuthHeader, utilAddFTPUser


@login_required
def home_detail_view(request):
    apiaries = Apiary.objects.filter(creator=request.user)
    has_apiary_permissions = request.user.has_perm('main.add_apiary')

    annotation_count = UserAnnotation.objects\
        .filter(annotation_set__user=request.user).count()
    image_annotation_count = Image.objects.filter(
        userannotationset__user=request.user).count()

    hive_count = Hive.objects.filter(creator=request.user).count()
    image_count = Image.objects.filter(beehive__creator=request.user).count()

    stats = {
        'annotation_count': annotation_count,
        'image_annotation_count': image_annotation_count,
        'hive_count': hive_count,
        'image_count': image_count
    }
    context = {
        'has_apiary_permissions': has_apiary_permissions,
        'apiaries': apiaries,
        'stats': stats
    }
    return render(request, 'main/home-detail.html', context)


def register(request):
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("index")

    else:
        form = forms.SignUpForm()

    return render(request, 'main/register.html', {'form': form})



def ssl(request):
    return HttpResponse("qr44ju3Dl6A14u84DJoD7U3HSnp0Pvc6Hww3phDLX00.3lgIlJw85aYm7zT9Rm_xLqUCuevKsSc-gP5AAXoAqJ0", content_type='text/plain')


# [
#     {
#         "hiveName": "erlen",
#         "testImages": [
#             "one",
#             "two"
#         ]
#     },
#     {
#         "hiveName": "mehlen",
#         "testImages": [
#             "one",
#             "two"
#         ]
#     }
# ]


@login_required
@api_view(['GET'])
def getRetrainBeehives(request):
    "returns a list of hives and images that should be used to create new models"
    beehives = Hive.objects.filter(retrain=True)
    retrain = []
    for hive in beehives:

        images = hive.image_set.all()
        trainImages = []
        testImages = []
        

        for i in images:
            annotations = []
            for annot in i.annotations.all():
                vector = annot.vector
                annotations.append(
                    {
                        "min_x": vector["x1"],
                        "min_y": vector["y1"],
                        "max_x": vector["x2"],
                        "max_y": vector["y2"]
                    }
                )
            if i.status == Image.STATUS.TEST:
                testImages.append(
                    {
                        "name": i.path.name,
                        "annotations": annotations,
                        "width": i.width,
                        "height": i.height
                    }
                )
            elif i.status == Image.STATUS.TRAIN:
                trainImages.append(
                    {
                        "name": i.path.name,
                        "annotations": annotations,
                        "width": i.width,
                        "height": i.height
                    }
                )

        retrain.append({
            "beehiveName": hive.name,
            "beehiveID": hive.id,
            "apiary": hive.apiary.name,
            "trainImages": trainImages,
            "testImages": testImages
        })

    return JsonResponse(retrain, safe=False)


@staff_member_required
@api_view(['POST'])
def registerNewModel(request):
    # test: requests.post('http://127.0.0.1:8000/en/registermodel/', {'beehiveID': 3, 'modelPath': 'models/single_hive/Doettingen_Hive1'})

    if request.data:
        beehive = get_object_or_404(Hive, pk=int(request.data.get("beehiveID")))
        model, created = AIModel.objects.get_or_create(
            description=f'{beehive.name} model',
            path=request.data.get('modelPath')
        )
        x = threading.Thread(
            target=validateModel,
            args=(model, beehive))
        x.start()
        return Response(status=status.HTTP_200_OK)

    return Response(status=status.HTTP_400_BAD_REQUEST)


@staff_member_required
def addFTPUser(request):
    heading = "Add new FTP user"
    if request.method == 'POST':
        form = forms.NewFTPUserForm(request.POST)
        if form.is_valid():
            confirm = form.cleaned_data['confirmPassword']
            new = form.cleaned_data['userPassword']
            if new == confirm:
                r = utilAddFTPUser(
                    form.cleaned_data['username'],
                    new,
                    form.cleaned_data['adminPassword']
                )
                print("add user output=",r.json())
                heading = "New user successfully added!"
            else:
                form = forms.NewFTPUserForm()
                heading = "User password and confirmation does not match"


    else:
        form = forms.NewFTPUserForm()

    context = {
        'form': form,
        "heading": heading
    }

    return render(request, 'main/ftp/generic-form.html', context=context)

@staff_member_required
def changeFTPAdminPassword(request):
    heading = "Change FTP admin password"
    if request.method == 'POST':
        form = forms.ChangeAdminPasswordForm(request.POST)
        if form.is_valid():
            old = form.cleaned_data['oldPassword']
            new = form.cleaned_data['newPassword']
            confirm = form.cleaned_data['confirmPassword']
            if confirm == new:
                header = getAuthHeader("admin",old)
                data = {
                    "current_password": old,
                    "new_password": new
                }
                r = requests.put("http://ftp-sftpgo/api/v2/changepwd/admin", json=data, verify=False, headers=header)
                print (r.json())
                heading = "Password successfully changed!"
            else:
                heading = "New password and confirmation does not match"
                form = forms.ChangeAdminPasswordForm()
    else:
        form = forms.ChangeAdminPasswordForm()

    context = {
        'form': form,
        "heading": heading
    }

    return render(request, 'main/ftp/generic-form.html', context=context)