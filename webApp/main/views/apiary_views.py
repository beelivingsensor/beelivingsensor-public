from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import permission_required, login_required

from main.forms import ApiaryForm, AddManagerApiaryForm
from main.models import Apiary, User, WeatherMeasurement
from main.decorators import user_is_manager

from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="beewatch")


@permission_required('main.add_apiary')
def apiary_create_view(request):

    apiary_form = ApiaryForm(request.POST or None)

    if apiary_form.is_valid():
        apiary = apiary_form.save(commit=False)
        apiary.creator = request.user
        # prevent create tests from executing this
        if apiary.location:
            geo = geolocator.reverse(f"{apiary.location.y}, {apiary.location.x}")
            apiary.address = geo.address
        apiary.save()
        apiary.managers.add(request.user)
        return redirect('apiary-detail', pk=apiary.pk)

    context = {
        'apiary_form': apiary_form,
    }

    return render(request, 'main/apiary/apiary_create.html', context)

@permission_required('main.view_apiary')
@user_is_manager(Apiary)
def apiary_detail_view(request, pk):
    apiary = get_object_or_404(Apiary, pk=pk)
    managers = apiary.managers.all()

    weather_measurements = WeatherMeasurement.objects.filter(apiary=apiary).order_by('-time')[:4]
    weather_station = weather_measurements.first().weather_station if weather_measurements else ''

    context = {
        'weather_station': weather_station,
        'weather_measurements': weather_measurements,
        'apiary': apiary,
        "managers": managers
    }
    return render(request, 'main/apiary/apiary_detail.html', context)


@login_required
def apiary_list_view(request):
    apiaries = Apiary.objects.all()
    if not request.user.is_staff:
        apiaries = apiaries.filter(managers__pk=request.user.pk)

    context = {'apiaries': apiaries}
    return render(request, 'main/apiary/apiary_list.html', context)


@permission_required('main.change_apiary')
@user_is_manager(Apiary)
def apiary_update_view(request, pk):
    apiary = get_object_or_404(Apiary, pk=pk)
    apiary_form = ApiaryForm(request.POST or None, instance=apiary)
    addManagerForm = AddManagerApiaryForm()
    managers = apiary.managers.all()

    if apiary_form.is_valid():
        apiary = apiary_form.save(commit=False)
        if 'location' in apiary_form.changed_data:
            geo = geolocator.reverse(
                f"{apiary.location.y}, {apiary.location.x}")
            apiary.address = geo.address

        apiary.save()
        return redirect('apiary-detail', pk=pk)

    context = {
        'apiary_form': apiary_form,
        "add_manager_form": addManagerForm,
        "managers": managers,
        "apiary": apiary
    }
    return render(request, 'main/apiary/apiary_update.html', context)


@permission_required('main.delete_apiary')
@user_is_manager(Apiary)
def apiary_delete_view(_, pk):
    get_object_or_404(Apiary, pk=pk).delete()
    return redirect('apiary-list')


@permission_required('main.change_apiary')
@user_is_manager(Apiary)
def addManager(request, pk):
    managersForm = AddManagerApiaryForm(request.POST or None)
    if not managersForm.is_valid():
        return redirect('apiary-update', pk)
    email = managersForm.cleaned_data["email"]

    managers = User.objects.get(email=email)
    apiary = Apiary.objects.get(pk=pk)
    apiary.managers.add(managers)
    apiary.save()
    return redirect('apiary-update', pk)


@permission_required('main.change_apiary')
@user_is_manager(Apiary, "apiary_pk")
def deleteManager(request, apiary_pk, user_pk):
    manager = User.objects.get(pk=user_pk)
    apiary = Apiary.objects.get(pk=apiary_pk)
    if apiary.creator == manager:
        redirect('apiary-update', apiary_pk)
        print("Cannot remove creator from the managers!") # Allowing the removal of the creator could create "memory" leaks in the DB, i.e. there would be objects that nobody has access to.
    apiary.managers.remove(manager)
    apiary.save()
    return redirect('apiary-update', apiary_pk)
