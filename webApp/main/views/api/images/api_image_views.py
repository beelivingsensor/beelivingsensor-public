from django.contrib.auth.decorators import permission_required
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view

from main.models import Image, User, AIModel
from main.views.api.images.image_navigation import get_previous_next_image, \
    get_previous_next_hive_image, get_previous_next_video_image


@permission_required("main.view_image")
@api_view(['GET'])
def api_image_list_view(request):
    """Get list of images.

    Query Params:
        type (str): hive/bee
        video (int): pk of video
        image (int): pk of parent image (e.g. get all bee images of hive image)
        page (int): Which page to fetch.
        per_page (int): Number of images per page.
    """
    images = Image.objects.all()

    type_ = request.GET.get('type', None)
    video = request.GET.get('video', None)
    image = request.GET.get('image', None)
    page_number = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 25)

    if type_:
        images = images.filter(type=type_)
    if video:
        images = images.filter(image_set__video__pk=video)
    if image:
        images = images\
            .filter(modelannotation__annotation_set__image__pk=image)

    images = images.order_by('pk')

    paginator = Paginator(images, per_page)
    page_obj = paginator.get_page(page_number)

    data = {
        'items': images.count(),
        'page': page_number,
        'pages': paginator.num_pages,
        'per_page': per_page,
        'images': [
            {
                'pk': image.pk,
                'width': image.width,
                'height': image.height,
                'path': image.path.url
            }
            for image in page_obj
        ]
    }

    return JsonResponse(data, safe=False)


@permission_required("main.view_image")
@api_view(['GET'])
def api_image_detail_view(_, pk):
    """Get the image.

    Args:
        pk: The primary key of the image.
    """
    image = get_object_or_404(Image, pk=pk)
    data = {
        'pk': image.pk,
        'type': image.type,
        'status': image.status,
        'video': image.image_set.video.pk if image.image_set else None,
        'beehive': image.beehive.pk,
        'url': image.path.url,
        'path': f'{image.path.storage.azure_container}/{image.path.name}',
        'frame': image.frame,
        'width': image.width,
        'height': image.height
    }
    return JsonResponse(data, safe=False)


@permission_required("main.view_image")
@api_view(['GET'])
def api_image_annotator_list_view(_, pk):
    """Get users that finished annotating the image.

    Args:
        pk (int): The primary key of the image.
    """
    image = get_object_or_404(Image, pk=pk)
    users = User.objects.filter(
        userannotationset__image=image,
        userannotationset__finished=True
    )
    data = {'users': [{'pk': a.pk, 'name': a.username} for a in users]}
    return JsonResponse(data, safe=False)


@permission_required("main.view_image")
@api_view(['GET'])
def api_image_model_list_view(_, pk):
    """Get models that created annotations on the image.

    Args:
        pk (int): The primary key of the image.
    """
    image = get_object_or_404(Image, pk=pk)
    models = AIModel.objects.filter(modelannotationset__image=image)
    data = {'models': [{'pk': m.pk, 'name': m.description} for m in models]}
    return JsonResponse(data, safe=False)


@permission_required("main.view_image")
@api_view(['GET'])
def api_image_navigation_view(_, pk):
    """Get previous and next image.

    Args:
        pk (int): The primary key of the image.
    """
    image = get_object_or_404(Image, pk=pk)

    # image is part of a video
    if image.image_set:
        next_image, previous_image = get_previous_next_video_image(image)
    else:
        # image belongs to a hive
        if image.beehive:
            next_image, previous_image = get_previous_next_hive_image(image)
        else:
            next_image, previous_image = get_previous_next_image(image)

    data = {'previous_image': previous_image, 'next_image': next_image}

    return JsonResponse(data, safe=False)
