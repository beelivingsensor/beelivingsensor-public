import logging

from main.models import Image


logger = logging.getLogger(__name__)


def get_previous_next_image(image):
    """Get the previous and next image.

    Args:
        image (Image): The image as a starting position.

    Returns:
        Tuple[int,int]: The primary keys of the previous and next image.
    """
    try:
        previous_image = Image.objects.get(pk=image.pk - 1).pk
    except Exception as e:
        logger.info(f'❌ Could not fetch previous image: {e}')
        previous_image = None
    try:
        next_image = Image.objects.get(pk=image.pk + 1).pk
    except Exception as e:
        logger.info(f'❌ Could not fetch next image: {e}')
        next_image = None
    return next_image, previous_image


def get_previous_next_hive_image(image):
    """Get the previous and next image of the hive.

    Args:
        image (Image): The image as a starting position.

    Returns:
        Tuple[int,int]: The primary keys of the previous and next image.
    """
    previous_images = Image.objects.filter(
        beehive=image.beehive, pk__lt=image.pk)
    previous_image = previous_images.last().pk if previous_images else None
    next_images = Image.objects.filter(
        beehive=image.beehive, pk__gt=image.pk)
    next_image = next_images.first().pk if next_images else None
    return next_image, previous_image


def get_previous_next_video_image(image):
    """Get the previous and next image of the video.

    Args:
        image (Image): The image as a starting position.

    Returns:
        Tuple[int,int]: The primary keys of the previous and next image.
    """
    if image.frame and image.type == 'hive':
        try:
            previous_image = Image.objects.get(
                image_set__video=image.image_set.video,
                frame=image.frame - 1,
                type='hive').pk
        except Exception as e:
            logger.info(f'❌ Could not fetch previous image: {e}')
            previous_image = None
        try:
            next_image = Image.objects.get(
                image_set__video=image.image_set.video,
                frame=image.frame + 1,
                type='hive').pk
        except Exception as e:
            logger.info(f'❌ Could not fetch next image: {e}')
            next_image = None

        # case when video frames are skipped
        if not next_image and not previous_image:
            next_image, previous_image = get_previous_next_hive_image(image)
    else:
        next_image, previous_image = get_previous_next_hive_image(image)

    return next_image, previous_image
