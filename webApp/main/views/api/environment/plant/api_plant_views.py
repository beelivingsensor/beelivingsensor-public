import datetime

from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view

from main.models import Plant, PlantOccurrence, CommonName

from django.contrib.gis.measure import Distance
from django.contrib.gis.geos import Point

from main.views.environment.beefoodchecker.plant_matcher import PlantMatcher


def get_plant_dict(plants):
    """Convert Plant QuerySet into dictionary.

    Args:
        plants (QuerySet[Plant]): The plants

    Returns:
        Dict: The plants.
    """
    dic = [
        {
            'pk': plant.pk,
            'name': plant.name,
            'common_names': {
                common_name.language: common_name.name
                for common_name in CommonName.objects.filter(plant=plant)
            },
            'nutrition_grade': plant.nutrition_grade,
            'pollen_color': plant.pollen_color
        }
        for plant in plants
    ]
    return dic


@api_view(['GET'])
def api_plant_list_view(_):
    """Get list of plants."""
    plants = Plant.objects.filter(rank=Plant.RANK.SPECIES)
    data = get_plant_dict(plants)
    return JsonResponse(data, safe=False)


@api_view(['GET'])
def api_plant_occurrence_list_view(_, pk):
    """Get list of occurrences for a given plant.

    Args:
        pk: The primary key of the plant.
    """
    plant = get_object_or_404(Plant, pk=pk)
    occurrences = PlantOccurrence.objects.filter(plant=plant)

    data = {
        'items': occurrences.count(),
        'data': [
            {
                'pk': occ.pk,
                'x': occ.region.x,
                'y': occ.region.y,
                'obs_nb': occ.obs_nb,
                'doubt': occ.doubt,
                'year_min': occ.year_min,
                'year_max': occ.year_max,
            }
            for occ in occurrences
        ]
    }

    return JsonResponse(data, safe=False)


@api_view(['GET'])
def api_plant_match_view(request):
    """Match plants according to time, location and color.

    Query Params:
        time (datetime.datetime): To match a plant's flowering status.
        x (float): To match a plant's occurrence (latitude).
        y (float): To match a plant's occurrence (longitude)
        colors (List[str]): Pollen colors to match.
        radius (int): The radius within which plants should be matched.
        days (int): The number of days for observations to consider
            around the specified time.
    """
    x = request.GET.get('x', None)
    y = request.GET.get('y', None)
    time = request.GET.get('time', None)
    time = datetime.datetime.fromisoformat(time)
    if not x or not y or not time:
        return HttpResponseBadRequest('time, x and y params must be specified!')
    coords = Point(x=float(x), y=float(y))
    radius = int(request.GET.get('radius', 5))
    days = int(request.GET.get('days', 7))

    # occurring plants
    # at least 5 km distance (info flora has 5 km precision)
    distance = Distance(km=max(radius, 5))
    plants = Plant.objects\
        .filter(
            rank=Plant.RANK.SPECIES,
            plantoccurrence__region__distance_lt=(coords, distance))\
        .distinct()\
        .order_by('name')

    earliest_date = time - datetime.timedelta(days=days)
    latest_date = time + datetime.timedelta(days=days)

    scores = {}
    plant_count = 0
    for plant in plants:
        plant_count += 1
        matcher = PlantMatcher(plant)
        matcher.match(coords, earliest_date, latest_date, radius, time)
        score = matcher.get_score()
        dic = matcher.get_dict()

        if score not in scores:
            scores[score] = []

        scores[score].append(dic)

    ranking = []
    for rank, score in enumerate(sorted(scores, reverse=True), start=1):
        plants = scores[score]
        ranking.append({
            'rank': rank,
            'score': score,
            'plants': plants,
            'count': len(plants)
        })

    data = {
        'data': {
            'ranking': ranking,
            'count': plant_count
        }
    }

    return JsonResponse(data, safe=False)
