import datetime
import io

from django.db import models
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from rest_framework.decorators import api_view

from main.forms import SensorTypeForm, HiveSensorForm, SensorMeasurementForm, \
    HiveSensorDeviceForm, SensorBulkUploadForm
from main.models import SensorType, HiveSensor, Hive, SensorMeasurement, \
    HiveSensorDevice
from django.db.models import Q, Value, F
import pandas
from django.contrib.admin.views.decorators import staff_member_required
import csv
from django.contrib.auth.decorators import permission_required
from main.decorators import user_is_manager

from main.serializers import MeasurementSerializer

@permission_required("main.add_sensortype")
@staff_member_required
def sensor_type_create_view(request):
    sensor_type_form = SensorTypeForm(request.POST or None)
    if sensor_type_form.is_valid():
        sensor_type_form.save()
        return redirect('sensor-type-list')

    context = {
        'sensor_type_form': sensor_type_form
    }
    return render(request, 'main/sensor/sensor_type_create.html', context)


@permission_required("main.view_sensortype")
@staff_member_required
def sensor_type_list_view(request):
    sensor_types = SensorType.objects.all()
    context = {
        'sensor_types': sensor_types
    }
    return render(request, 'main/sensor/sensor_type_list.html', context)


@permission_required("main.delete_sensortype")
@staff_member_required
def sensor_type_delete_view(_, pk):
    get_object_or_404(SensorType, pk=pk).delete()
    return redirect('sensor-type-list')


@permission_required("main.change_sensortype")
@staff_member_required
def sensor_type_update_view(request, pk):
    sensor_type = get_object_or_404(SensorType, pk=pk)
    sensor_type_form = SensorTypeForm(request.POST or None, instance=sensor_type)
    if sensor_type_form.is_valid():
        sensor_type_form.save()
        return redirect('sensor-type-list')

    context = {
        'pk': pk,
        'sensor_type_form': sensor_type_form
    }
    return render(request, 'main/sensor/sensor_type_update.html', context)


@permission_required("main.add_hivesensordevice")
@user_is_manager(Hive, "hive_pk")
def hive_sensor_device_create_view(request, hive_pk):
    hive_sensor_device_form = HiveSensorDeviceForm(request.POST or None)
    if hive_sensor_device_form.is_valid():
        hive_sensor_device = hive_sensor_device_form.save(commit=False)
        hive = get_object_or_404(Hive, pk=hive_pk)
        hive_sensor_device.hive = hive

        brand_choices = hive_sensor_device_form.cleaned_data['brand_choices']
        if brand_choices != 'custom':
            hive_sensor_device.brand = brand_choices

        hive_sensor_device.save()
        return redirect(reverse('hive-sensor-device-list')+f'?hive={hive.pk}')

    context = {
        'hive_sensor_device_form': hive_sensor_device_form
    }
    return render(request, 'main/sensor/sensor_device_create.html', context)


@permission_required("main.change_hivesensordevice")
@user_is_manager(HiveSensorDevice, "device_pk")
def hive_sensor_device_update_view(request, device_pk):
    hive_sensor_device = get_object_or_404(HiveSensorDevice, pk=device_pk)
    hive_sensor_device_form = HiveSensorDeviceForm(
        request.POST or None, instance=hive_sensor_device)

    if hive_sensor_device.brand in HiveSensorDevice.COMMON_BRANDS:
        hive_sensor_device_form.fields['brand_choices'].initial = hive_sensor_device.brand
    else:
        hive_sensor_device_form.fields['brand_choices'].initial = 'custom'

    if hive_sensor_device_form.is_valid():
        hive_sensor_device = hive_sensor_device_form.save(commit=False)
        brand_choices = hive_sensor_device_form.cleaned_data['brand_choices']
        if brand_choices != 'custom':
            hive_sensor_device.brand = brand_choices

        hive_sensor_device.save()
        return redirect('hive-sensor-device-detail', device_pk=hive_sensor_device.pk)

    context = {
        'hive_sensor_device_form': hive_sensor_device_form
    }
    return render(request, 'main/sensor/sensor_device_update.html', context)


@permission_required("main.view_hivesensordevice")
def hive_sensor_device_list_view(request):
    hive_sensor_devices = HiveSensorDevice.objects.all()
    hive = None
    if 'hive' in request.GET:
        hive_pk = request.GET['hive']
        hive = get_object_or_404(Hive, pk=hive_pk)
        hive_sensor_devices = hive_sensor_devices.filter(hive=hive)

    context = {
        'hive': hive,
        'hive_sensor_devices': hive_sensor_devices,
    }
    return render(request, 'main/sensor/sensor_device_list.html', context)


@permission_required("main.add_hivesensor")
@user_is_manager(HiveSensorDevice, "device_pk")
def hive_sensor_create_view(request, device_pk):
    hive_sensor_form = HiveSensorForm(request.POST or None)
    if hive_sensor_form.is_valid():
        hive_sensor = hive_sensor_form.save(commit=False)
        device = get_object_or_404(HiveSensorDevice, pk=device_pk)
        hive_sensor.device = device

        hive_sensor.save()
        return redirect('hive-sensor-device-detail', device_pk=device_pk)

    context = {
        'hive_sensor_form': hive_sensor_form
    }
    return render(request, 'main/sensor/hive_sensor_create.html', context)


@permission_required("main.view_hivesensordevice")
@user_is_manager(HiveSensorDevice, "device_pk")
def hive_sensor_device_detail(request, device_pk):
    device = get_object_or_404(HiveSensorDevice, pk=device_pk)
    hive_sensors = HiveSensor.objects.filter(device=device)

    now = datetime.datetime.now()

    active_sensors = hive_sensors.filter(
        Q(valid_until__isnull=True)
        | Q(valid_until__gte=now))
    disabled_sensors = hive_sensors.filter(valid_until__lt=now)

    context = {
        'device_list_url': reverse('hive-sensor-device-list')+f'?hive={device.hive.pk}',
        'device': device,
        'hive_sensors': active_sensors,
        'disabled_sensors': disabled_sensors
    }
    return render(request, 'main/sensor/sensor_device_detail.html', context)


@api_view(['GET'])
@permission_required("main.view_hivesensordevice")
@user_is_manager(HiveSensorDevice, "device_pk")
def hive_sensor_device_measurements_api_view(request, device_pk):
    device = get_object_or_404(HiveSensorDevice, pk=device_pk)
    sensor_measurements = SensorMeasurement.objects.filter(sensor__device=device).order_by('time')
    sensor_measurements = sensor_measurements.values(
        'value',
        'time',
        source=Value(value='hive-sensor', output_field=models.CharField()),
        param=F('sensor__sensor_type__name'),
        unit=F('sensor__sensor_type__unit')
    )

    serializer = MeasurementSerializer(sensor_measurements, many=True)
    return JsonResponse(serializer.data, safe=False)


CAPAZ_MAPPINGS = {
        'Regen (L/qm)': 'rain',
        'Außentemp. Min °C': 'outside minimum temperature',
        'Außentemp. Max °C': 'outside maximum temperature',
        'Luftfeuchte min%': 'minimum humidity',
        'Luftfeuchte max%': 'maximum humidity',
        'Gewicht kg': 'weight',
    }


def create_capaz_sensors(device):
    sensors = {}
    for capaz_name in CAPAZ_MAPPINGS:
        sensor_type_name = CAPAZ_MAPPINGS[capaz_name]
        sensor_type = SensorType.objects.get(name=sensor_type_name)
        sensor, _ = HiveSensor.objects.update_or_create(
            device=device,
            sensor_type=sensor_type
        )
        sensors[capaz_name] = sensor

    return sensors


@permission_required("main.add_sensormeasurement")
@user_is_manager(HiveSensorDevice, "device_pk")
def hive_sensor_device_bulk_upload(request, device_pk):
    device = get_object_or_404(HiveSensorDevice, pk=device_pk)

    sensors = create_capaz_sensors(device)

    if request.method == 'POST':
        upload_form = SensorBulkUploadForm(request.POST, request.FILES)
        if upload_form.is_valid():

            data_xls = pandas.read_excel(request.FILES['file'], index_col=None)
            csv_str = data_xls.to_csv(encoding='utf-8')

            reader = csv.DictReader(io.StringIO(csv_str))
            for row in reader:
                for field in CAPAZ_MAPPINGS:
                    if field in row:
                        value = float(row[field])
                        time = datetime.datetime.strptime(row['Datum']+' 00:00', '%d.%m.%y %H:%M')
                        sensor = sensors[field]
                        SensorMeasurement.objects.get_or_create(
                            time=time,
                            sensor=sensor,
                            value=value
                        )

            return redirect('hive-sensor-device-detail', device_pk=device_pk)
    else:
        upload_form = SensorBulkUploadForm()

    context = {
        'upload_form': upload_form
    }

    return render(request, 'main/sensor/hive_sensor_bulk_upload.html',context)


@permission_required("main.view_hivesensor")
@user_is_manager(HiveSensor)
def hive_sensor_detail_view(request, pk):
    hive_sensor = get_object_or_404(HiveSensor, pk=pk)
    sensor_measurements = SensorMeasurement.objects.filter(sensor=hive_sensor).order_by('-time')
    context = {
        'sensor_measurements': sensor_measurements,
        'hive_sensor': hive_sensor
    }
    return render(request, 'main/sensor/hive_sensor_detail.html', context)


@api_view(['GET'])
@permission_required("main.view_hivesensor")
@user_is_manager(HiveSensor)
def hive_sensor_measurements_api_view(request, pk):
    hive_sensor = get_object_or_404(HiveSensor, pk=pk)
    sensor_measurements = SensorMeasurement.objects.filter(sensor=hive_sensor).order_by('time')
    sensor_measurements = sensor_measurements.values(
        'value',
        'time',
        source=Value(value='hive-sensor', output_field=models.CharField()),
        param=F('sensor__sensor_type__name'),
        unit=F('sensor__sensor_type__unit')
    )

    serializer = MeasurementSerializer(sensor_measurements, many=True)
    return JsonResponse(serializer.data, safe=False)


@permission_required("main.change_hivesensor")
@user_is_manager(HiveSensor)
def hive_sensor_update_view(request, pk):
    hive_sensor = get_object_or_404(HiveSensor, pk=pk)
    hive_sensor_form = HiveSensorForm(request.POST or None,
                                      instance=hive_sensor)
    if hive_sensor_form.is_valid():
        hive_sensor_form.save()
        return redirect('hive-sensor-detail', pk=hive_sensor.pk)

    context = {
        'hive_sensor_form': hive_sensor_form
    }
    return render(request, 'main/sensor/hive_sensor_update.html', context)


@permission_required("main.delete_hivesensor")
@user_is_manager(HiveSensor)
def hive_sensor_delete_view(_, pk):
    hive_sensor = get_object_or_404(HiveSensor, pk=pk)
    device_pk = hive_sensor.device.pk
    hive_sensor.delete()
    return redirect('hive-sensor-device-detail', device_pk=device_pk)


@permission_required("main.add_sensormeasurement")
@user_is_manager(HiveSensor, "sensor_pk")
def sensor_measurement_create_view(request, sensor_pk):
    sensor = get_object_or_404(HiveSensor, pk=sensor_pk)
    sensor_measurement_form = SensorMeasurementForm(request.POST or None)
    if sensor_measurement_form.is_valid():
        sensor_measurement = sensor_measurement_form.save(commit=False)
        sensor_measurement.sensor = sensor
        sensor_measurement.save()
        return redirect('hive-sensor-detail', pk=sensor_pk)

    context = {
        'sensor': sensor,
        'sensor_measurement_form': sensor_measurement_form
    }
    return render(request, 'main/sensor/sensor_measurement_create.html', context)
