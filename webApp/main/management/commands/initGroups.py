from django.core.management import BaseCommand

from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType


from main.models import Apiary, Hive, Video, SensorType, AIModel, HiveSensor, \
    WeatherMeasurement, Image, ModelAnnotation, SensorMeasurement, \
    UserAnnotation, HiveSensorDevice

# Groups settings
# Permissions were done
# according to https://www.mindmeister.com/1563264995?t=OFzc7t3alI
DEFAULT_GROUP = "Citizen Scientist"
GROUP_LIST = [
    {
        "name": "Government",
        "rules": [
            # Results
        ] 
    },
    {
        "name": "Environmentalist",
        "rules": [
            # Results
        ] 
    }, 
    {
        "name": "Researcher",
        "rules": [
            {
                "model": UserAnnotation,
                "permissions": [
                    "can_annotate_images",
                ]
            },
            {
                "model": AIModel,
                "permissions": [
                    "can_request_retraining",
                ]
            }
        ] 
    }, 
    {
        "name": "Beekeeper",
        "rules": [
            {
                "model": Apiary,
                "permissions": [
                    "add_apiary",
                    "change_apiary",
                    "delete_apiary",
                    "view_apiary",
                ]
            },
            {
                "model": Hive,
                "permissions": [
                    "add_hive",
                    "change_hive",
                    "delete_hive",
                    "view_hive",
                ]
            },
            {
                "model": Video,
                "permissions": [
                    "add_video",
                    "change_video",
                    "delete_video",
                    "view_video",
                ]
            },
            {
                "model": HiveSensorDevice,
                "permissions": [
                    "add_hivesensordevice",
                    "change_hivesensordevice",
                    "delete_hivesensordevice",
                    "view_hivesensordevice",
                ]
            },
            {
                "model": HiveSensor,
                "permissions": [
                    "add_hivesensor",
                    "change_hivesensor",
                    "delete_hivesensor",
                    "view_hivesensor",
                ]
            },
            {
                "model": SensorMeasurement,
                "permissions": [
                    "add_sensormeasurement",
                    "change_sensormeasurement",
                    "delete_sensormeasurement",
                    "view_sensormeasurement",
                ]
            },
            {
                "model": SensorType,
                "permissions": [
                    "view_sensortype",
                ]
            },
            {
                "model": AIModel,
                "permissions": [
                    "view_aimodel",
                    "add_aimodel",
                    "can_request_retraining",
                ]
            },
            {
                "model": WeatherMeasurement,
                "permissions": [
                    "add_weathermeasurement",
                    "view_weathermeasurement",
                ]
            },
            {
                "model": Image,
                "permissions": [
                    "view_image",
                ]
            },
            {
                "model": ModelAnnotation,
                "permissions": [
                    "view_modelannotation",
                ]
            },
            {
                "model": UserAnnotation,
                "permissions": [
                    "can_annotate_images",
                ]
            }
        ]
    },
    {
        "name": "Sensor",
        "rules": [
            {
                "model": SensorMeasurement,
                "permissions": [
                    "add_sensormeasurement",
                ]
            }
        ] 
    }, 
    {
        "name": "Labeler",
        "rules": [
            {
                "model": UserAnnotation,
                "permissions": [
                    "can_annotate_images",        
                ] 
            }
        ]
    }, 
    {
        "name": "Visitor",
        "rules": [
            {
                "model": Apiary,
                "permissions": [
                    "view_apiary"
                ] 
            }
        ]
    },
    {
        "name": "Citizen Scientist",
        "rules": [
            {
                "model": UserAnnotation,
                "permissions": [
                    # Can CRUDL env objects
                    "can_annotate_images",        
                ] 
            }
        ]
    } 
]


class Command(BaseCommand):
    help = 'Create groups defined in settings.py, ' \
           'and assign their permissions ' \
           'according to GROUP_LIST in settings.py .'

    def handle(self, *args, **options):
        for dictGroup in GROUP_LIST:
            group_obj, _ = Group.objects.get_or_create(
                name=dictGroup["name"])

            # Rules are all permission per group
            for rule in dictGroup["rules"]:

                content_type = ContentType.objects.get_for_model(rule["model"])

                # apply all permissions associated with a model
                for perm in rule["permissions"]:

                    perm_obj = Permission.objects.get(
                        codename=perm,
                        content_type=content_type)

                    if perm_obj not in group_obj.permissions.all():
                        print(f"Adding permission: {perm} "
                              f"for model {content_type.name}")
                        group_obj.permissions.add(perm_obj)
