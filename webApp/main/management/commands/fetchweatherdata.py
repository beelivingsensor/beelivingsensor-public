from django.core.management.base import BaseCommand

from main.models import Apiary
from main.views.weather_views import create_weather_measurements


class Command(BaseCommand):
    help = 'Fetch weather data for all apiaries.'

    def handle(self, *args, **options):
        print('Fetching weather data...')
        apiaries = Apiary.objects.all()
        for apiary in apiaries:
            create_weather_measurements(apiary)
