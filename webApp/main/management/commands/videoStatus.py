from main.models import Video, Apiary, Hive, User
import os
from azure.storage.blob import BlobServiceClient
from main.views.video_views import addVideo
from django.core.management.base import BaseCommand
from main.views.environment.beefoodchecker.pritsch_saver import PritschSaver



def getVideosFromAzure():
    # Container which you would like to view
    CONTAINER = os.environ.get("AZURE_CONTAINER")
    print("Container name: {}".format(CONTAINER))
    # Go into the Azure UI >> Storage account >> Shared Access Signature >> Generate one >> There is a "Connection String" section
    CONNECTION_STRING = os.environ.get("AZURE_CONNECTION_STRING")
    if CONNECTION_STRING == "" or CONTAINER == "":
        print("You did not define the Connection string or Container name")
        exit(1)

    blob_service_client = BlobServiceClient.from_connection_string(CONNECTION_STRING)
    container_client = blob_service_client.get_container_client(CONTAINER)
    blobs_list = container_client.list_blobs()
    videoNames = set()
    otherFiles = 0
    print("Extracting data...")
    for blob in blobs_list: # Very long process of downloading all the metadata about the 300k vides we have
        if blob.name.endswith(".mp4"):
            videoNames.add(blob.name)
            continue
        otherFiles += 1
    print("Videos in Azure: {}".format(len(videoNames)))
    print("Other files in Azure: {}".format(otherFiles))
    return videoNames



def getVideosFromDB():
    videosInDB = Video.objects.all()
    videoNames = set()
    for vid in videosInDB:
        videoNames.add(vid.fileName)
    print("Videos in DB: {}".format(len(videoNames)))
    return videoNames

class Command(BaseCommand):
    help = 'Add missing videos from Azure to our DB'

    def handle(self, *args, **options):
        print("DO NOT FORGET to set the connectiong string, container, and how many videos you want to process.")

        # It is actually just a list of names / filepaths
        azureVideos = getVideosFromAzure()
        dbVideos = getVideosFromDB()

        missingVideos = set()
        missingVideos = azureVideos - dbVideos # Videos that are on Azure, but not in our DB
        print("Videos not in the DB: {}".format(len(missingVideos)))
        something = dbVideos - azureVideos # Stuff that is on DB, but not in the Azure
        print("Stuff not in the DB: {}".format(len(something)))

        vids = 0

        for filename in missingVideos:
            # Only add the first 100 videos. No real reason, just so that the DB is not overwhelmed or something
            vids += 1
            if vids > 100:
                break
            
            apiaryName = filename.split("/")[0]
            hiveName = filename.split("/")[1]

            # Get or create apiary and creator
            creator = None        
            apiary = None        
            try:
                apiary = Apiary.objects.get(name=apiaryName)
                creator = apiary.creator
            except Apiary.DoesNotExist: # Django forced me to use exceptions as IF logic, bleh
                creator = User.objects.get(username="vladimir_masarik")
                apiary = Apiary()
                apiary.name = apiaryName
                apiary.creator = creator
                apiary.save()
                
            # Get or create hive
            hive = None
            try:
                hive = Hive.objects.get(name=hiveName)
            except Hive.DoesNotExist: # Django forced me to use exceptions as IF logic, bleh
                hive = Hive()
                hive.creator = creator
                hive.name = hiveName
                hive.apiary = apiary
                hive.save()


            addVideo(creator, hive, filename, process=False)
            print("Added:", filename)
            




