import os
import zipfile
from datetime import datetime
from io import BytesIO
from itertools import groupby
from pathlib import Path

import requests
from django.core.management.base import BaseCommand

from main.models import GoldAnnotationSet, Image
from main.util.azure_helpers import delete_blobs, upload_blob
from main.util.pascal_voc_writer import PascalVOCWriter


class Command(BaseCommand):
    help = 'Upload a snapshot of all annotations to Azure.'

    def handle(self, *args, **options):
        # first check whether credentials are exported
        connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
        if not connect_str:
            print('Azure storage connection string not exported!')
            return
        storage_key = os.getenv('STORAGE_KEY')
        if not storage_key:
            print('Storage key not exported!')
            return

        folder = 'ML-Data'
        timestamp = str(datetime.now().strftime('%Y%m%d-%H-%M-%S-%f'))
        zip_name = f'{folder}_{timestamp}.zip'
        archive = zipfile.ZipFile(zip_name, 'w')

        # group gold annotation sets by image
        annotation_sets_list = groupby(
            GoldAnnotationSet.objects
                .exclude(image__status=Image.STATUS.VALIDATE)
                .order_by('image'),
            key=lambda item: item.image)

        for d, (image, annotation_sets) in enumerate(annotation_sets_list):
            image_path = f'{folder}/{image.download_name}{image.extension}'
            print(f'Writing {folder}/{image.download_name} to archive')

            # get XML writer
            writer = PascalVOCWriter(folder, image)

            # get the image from Azure and write to archive
            response = requests.get(image.path.url)
            image_content = BytesIO(response.content).getvalue()
            archive.writestr(image_path, image_content)

            # write the annotations to the XML
            for annotation_set in annotation_sets:
                writer.add_annotation_set(annotation_set)

            # write XML to archive
            xml_name = f'{image.download_name}.xml'
            xml_path = f'{folder}/{xml_name}'
            xml_content = writer.generate_xml()
            archive.writestr(xml_path, xml_content)

        archive.close()
        container_name = 'public'
        prefix = 'annotations'

        # delete old snapshots
        delete_blobs(container_name, prefix)

        # upload snapshot to Azure
        print(f'Uploading archive: {zip_name}')
        blob_name = f'{prefix}/{zip_name}'
        upload_blob(zip_name, container_name, blob_name)

        # delete local archive
        zip_path = Path(zip_name)
        print(f'Deleting local archive {zip_path.resolve()}')
        zip_path.unlink()

        print('Snapshot done!')
