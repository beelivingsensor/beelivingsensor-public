from pathlib import Path

from django.core.management.base import BaseCommand

from main.util.dump import get_most_recent_local_dump
from main.util.timestamping import get_timestamp
from main.views.environment.beefoodchecker.data_uploader import DataUploader
from main.views.environment.beefoodchecker.inaturalist.inaturalist_data_fetcher import \
    INaturalistDataFetcher
from main.views.environment.beefoodchecker.inaturalist.inaturalist_data_saver import \
    INaturalistDataSaver


class Command(BaseCommand):
    help = 'Fetch data from iNaturalist.'

    def add_arguments(self, parser):
        parser.add_argument(
            'action',
            help='Action to be performed',
            type=str,
            choices=['fetch', 'upload', 'create', 'update']
        )

    def fetch(self, base_path):
        dump = base_path / get_timestamp()
        fetcher = INaturalistDataFetcher()
        for endpoint in ['taxa', 'observations']:
            fetcher.dump_response_data_as_json(dump / endpoint, endpoint)
        return dump

    def handle(self, *args, **options):
        base_path = Path('inaturalist/')
        container = 'plant'

        action = options['action']

        if action == 'fetch':
            self.fetch(base_path)
        elif action == 'upload':
            dump_path = get_most_recent_local_dump(base_path)
            if not dump_path:
                dump_path = self.fetch(base_path)
            DataUploader().upload(container, dump_path)
        elif action == 'update':
            saver = INaturalistDataSaver()
            saver.update_observations()
        else:
            saver = INaturalistDataSaver()
            saver.save_data_from_dump(container, base_path)
