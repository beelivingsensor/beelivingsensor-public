from django.core.management.base import BaseCommand

from main.models import Annotation


class Command(BaseCommand):
    help = 'Create a snapshot of the ML data.'

    def handle(self, *args, **options):
        pollen_annotations = Annotation.objects.filter(
            annotation_type__name='pollen',
            gold=True
        )

        bee_annotations = Annotation.objects.filter(
            annotation_type__name='bee',
            gold=True
        )
