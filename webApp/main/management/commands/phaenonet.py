import csv

from django.core.management.base import BaseCommand

from main.models import CommonName
from main.util.azure_helpers import download_blob
from main.views.environment.beefoodchecker.phaenonet import PhaenoNet


class Command(BaseCommand):
    help = 'Add PhaenoNet data to database.'

    def handle(self, *args, **options):
        PhaenoNet()
        #self.match_scientific_names()

    def match_scientific_names(self):
        """Just to get the scientific names for the species sheet."""
        download_blob(PhaenoNet.container, PhaenoNet.species_blob, PhaenoNet.species_blob)
        with open(PhaenoNet.species_blob) as f:
            reader = csv.DictReader(f)
            for row in reader:
                german_name = row['german name']
                if german_name:
                    species_name = row['species abbreviation']

                    try:
                        scientific_name = CommonName.objects.filter(name=german_name).first().plant.name
                        print(f'{german_name}, {scientific_name}')
                    except:
                        pass