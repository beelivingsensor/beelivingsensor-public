from django.core.management.base import BaseCommand

from main.views.environment.beefoodchecker.pollen_color_table_parser import \
    PollenColorTableParser


class Command(BaseCommand):
    help = 'Add Pritsch data to database.'

    def handle(self, *args, **options):
        PollenColorTableParser().add_pollen_colors()
