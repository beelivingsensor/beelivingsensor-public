import csv
import os
import pickle

from django.contrib.gis.geos import Point
from django.core.management import BaseCommand

from main.models import User, Apiary, Hive, AIModel, Validation, \
    UserAnnotation, VerifiedAnnotations, SensorType, Annotation, \
    AnnotationType, Image, UserAnnotationSet, GoldAnnotationSet, GoldAnnotation
from geopy.geocoders import Nominatim

from main.util.grouping import grouped
from main.views.annotation.entrance_annotation import \
    open_entrance_labeling_task

geolocator = Nominatim(user_agent="beewatch")


apiaries = {
    'Chueried': {
        'hives': [
            ('HempBox', 'Chueried_Hempbox', Hive.Entrance.CIRCULAR),
            ('Churied_01_ST', 'Chueried_Hive01', Hive.Entrance.CIRCULAR),
        ],
        'geolocation': (47.4213889, 8.5780556),
        'owner': 'daniel_boschung'
    },
    'Doettingen': {
        'hives': [
            ('Doettingen_Hive_1', 'Doettingen_Hive1', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.5602778, 8.2547222),
        'owner': 'christoph_villiger'
    },
    'Echolinde': {
        'hives': [
            ('Echolinde_Hive_11', 'Echolinde', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.3830556, 8.0383333),
        'owner': 'markus_frankhauser'
    },
    'Erlen': {
        'hives': [
            ('Erlen_Hive_04', 'Erlen_Hive04', Hive.Entrance.RECTANGULAR),
            ('Erlen_Hive_07', '', Hive.Entrance.RECTANGULAR),
            ('Erlen_Hive_11', 'Erlen_Hive11', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.4244444, 8.6002778),
        'owner': 'daniel_boschung'
    },
    'Frohheimstrasse_23': {
        'hives': [
            ('Froh23_TC', 'Froh23_TreeCavity', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.4188889, 8.5894444),
        'owner': 'daniel_boschung'
    },
    'Frohheimstrasse_14': {
        'hives': [
            ('Froh_14_ST', 'Froh14', Hive.Entrance.CIRCULAR),
            ('united_queens', 'UnitedQueens', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.4183333, 8.5911111),
        'owner': 'daniel_boschung'
    },
    'OldSchoolHoney': {
        'hives': [
            ('1-Old', 'ClemensRed', Hive.Entrance.RECTANGULAR),
            ('2-School', 'ClemensYellow', Hive.Entrance.RECTANGULAR)
        ],
        'geolocation': (47.13819528580358, 9.290952086448671),
        'owner': 'clemens_mader'
    }
}

mode_mapping = {
    'on_qty_of_train_images': AIModel.TrainingMode.QUANTITY,
    'single_hive': AIModel.TrainingMode.SINGLE,
    'individual_training': AIModel.TrainingMode.INDIVIDUAL,
    'staged_2': AIModel.TrainingMode.STAGED
}


class Command(BaseCommand):
    help = 'Pre-populate database with apiaries and their data.'

    def handle(self, *args, **options):
        if not self.data_is_already_inserted():
            self.insert_annotation_types()
            self.insert_users()
            self.insert_apiaries_hives()
            self.insert_models()
            self.insert_images_annotations()
            self.insert_sensor_types()
            self.insert_tasks()

    def data_is_already_inserted(self):
        if Apiary.objects.all():
            print('Apiary data already inserted!!')
            return True
        else:
            return False

    def insert_sensor_types(self):
        SensorType.objects.update_or_create(
            name='rain',
            unit='L/qm'
        )
        SensorType.objects.update_or_create(
            name='outside minimum temperature',
            unit='°'
        )
        SensorType.objects.update_or_create(
            name='outside maximum temperature',
            unit='°'
        )
        SensorType.objects.update_or_create(
            name='minimum humidity',
            unit='%'
        )
        SensorType.objects.update_or_create(
            name='maximum humidity',
            unit='%'
        )
        SensorType.objects.update_or_create(
            name='weight',
            unit='kg'
        )

    def insert_models(self):
        print('Insert models...')
        # add models from UZH group
        bee_annotation_type = AnnotationType.objects.get(name='bee')
        with open('main/resources/hive_top_models.csv') as f:
            reader = csv.DictReader(f)
            for row in reader:
                mode = row['trained_model'].split('/')[0]
                mode = mode_mapping[mode]

                model, created = AIModel.objects.update_or_create(
                    description=row['trained_model'],
                    path='models/' + row['trained_model'],
                    training_mode=mode,
                    google_drive_path=row['trained_model'],
                    annotation_type=bee_annotation_type
                )

                hive_name, scene = self.get_hive_scene(row['hive_name'])

                hive = Hive.objects.get(google_drive_name=hive_name)

                precision = float(row['precision'])
                recall = float(row['recall'])

                Validation.objects.update_or_create(
                    model=model,
                    hive=hive,
                    scene=scene,
                    ground_truth=row['qty_ground_truth'],
                    predicted=row['qty_predicted'],
                    tp=row['true_positive'],
                    fp=row['false_positive'],
                    fn=row['false_negative'],
                    precision=precision,
                    recall=recall,
                    f1=Validation.compute_f1(precision, recall)
                )

        # Add models from ETH group
        pollen_annotation_type = AnnotationType.objects.get(name='pollen')
        eth_models = [
            ('ETH large bee model', 'public/models/bee/bee_large.weights', bee_annotation_type),
            ('ETH medium bee model', 'public/models/bee/bee_medium.weights', bee_annotation_type),
            ('ETH large pollen model', 'public/models/pollen/pollen_large.weights', pollen_annotation_type),
            ('ETH tiny pollen model', 'public/models/pollen/pollen_tiny.weights', pollen_annotation_type),
        ]
        for description, path, annotation_type in eth_models:
            AIModel.objects.create(
                description=description,
                path=path,
                annotation_type=annotation_type
            )

    def insert_apiaries_hives(self):
        """Insert apiaries and their hives into the DB."""
        print('Insert apiaries and hives...')
        for apiary_name in apiaries:
            owner = apiaries[apiary_name]['owner']
            creator = User.objects.get(username=owner)
            long, lat = apiaries[apiary_name]['geolocation']
            apiary, _ = Apiary.objects.update_or_create(
                name=apiary_name,
                creator=creator,
                location=Point(lat, long)
            )
            geo = geolocator.reverse(f"{apiary.location.y}, {apiary.location.x}")
            apiary.address = geo.address
            apiary.managers.add(creator)
            apiary.save()

            for hive_name, google_drive_name, entrance in apiaries[apiary_name]['hives']:
                Hive.objects.update_or_create(
                    name=hive_name,
                    apiary=apiary,
                    creator=creator,
                    google_drive_name=google_drive_name,
                    entrance=entrance
                )

    def insert_images_annotations(self):
        """Insert all images and their annotations into the DB."""
        self.insert_bee_annotations()
        self.insert_pollen_annotations()

        for beehive in Hive.objects.all():
            images = Image.objects.filter(beehive=beehive)
            if images:
                image = images.first()
                image.selected = True
                image.save()

    def hive_img_blob_name(self, hive_name, status, filename):
        return f'{hive_name}/{status}/{filename}.jpg'

    def bee_img_blob_name(self, hive_name, _, filename):
        if hive_name is None:
            blob_name = f'unknown_hive/pollen/{filename}'
        else:
            blob_name = f'{hive_name}/pollen/{filename}'

        return blob_name

    def insert_images(self, frames, blob_func, type_):
        """Insert all images into the DB."""
        img_objs = []
        for frame in frames:
            frame_info = frames[frame]

            hive_name = frame_info['hive']
            status = frame_info['status']
            filename = frame_info['frame']
            width = frame_info['width']
            height = frame_info['height']

            hive_name, _ = self.get_hive_scene(hive_name)
            if hive_name is None:
                hive = None
            else:
                hive = Hive.objects.get(google_drive_name=hive_name)
            image = Image(
                beehive=hive,
                name=filename,
                filename=filename,
                status=status,
                type=type_,
                width=width,
                height=height
            )
            image.path.name = blob_func(hive_name, status, filename)
            img_objs.append(image)

        return Image.objects.bulk_create(img_objs)

    def insert_bee_annotations(self):
        with open('main/resources/bee_annotations.pickle', 'rb') as f:
            pickle_data = pickle.load(f)
            print('Insert hive images')
            img_objs = self.insert_images(
                pickle_data, self.hive_img_blob_name, Image.TYPE.HIVE)
            print('Insert bee annotations')
            bee_type = AnnotationType.objects.get(name='bee')

            uzh_annotation_sets = []
            eth_annotation_sets = []
            img_objs_eth = []
            for i, image in enumerate(pickle_data.values()):
                annotation_set = image['annotations']
                uzh_annotation_sets.append(annotation_set['GP'])
                if 'AKS' in annotation_set:
                    img_objs_eth.append(img_objs[i])
                    eth_annotation_sets.append(annotation_set['AKS'])

            # the groups of people who made the annotations
            uzh_annotators = [
                User.objects.get(username='pascal_engeli'),
                User.objects.get(username='gabriela_lopez_magana')
            ]
            eth_annotators = [
                User.objects.get(username='korbinian_abstreiter'),
                User.objects.get(username='andrius_buinovskij'),
                User.objects.get(username='simon_boehm'),
            ]

            print('Insert annotations of UZH group')
            self.create_annotations(
                img_objs, uzh_annotation_sets, uzh_annotators, bee_type)
            print('Insert annotations of ETH group')
            self.create_annotations(
                img_objs_eth,
                eth_annotation_sets,
                eth_annotators,
                bee_type,
                gold=True)
            
    def insert_pollen_annotations(self):
        with open('main/resources/pollen_annotations.pickle', 'rb') as f:
            pickle_data = pickle.load(f)

        print('Insert bee images')
        img_objs = self.insert_images(
            pickle_data, self.bee_img_blob_name, Image.TYPE.BEE)
        print('Insert pollen annotations')
        pollen_type = AnnotationType.objects.get(name='pollen')

        groups = {
            'AKS': {
                'user_objs': [
                    User.objects.get(username='korbinian_abstreiter'),
                    User.objects.get(username='andrius_buinovskij'),
                    User.objects.get(username='simon_boehm'),
                ],
                'annotation_sets': [],
                'images': []
            },
            'DN': {
                'user_objs': [
                    User.objects.get(username='nicholas_dykemann'),
                    User.objects.get(username='dominique_heynd'),
                ],
                'annotation_sets': [],
                'images': []
            },
            'piperod': {
                'user_objs': [
                    User.objects.get(username='piperod'),
                ],
                'annotation_sets': [],
                'images': []
            }
        }

        for i, image in enumerate(pickle_data.values()):
            annotators = image['annotations']
            for group in annotators:
                groups[group]['annotation_sets'].append(annotators[group])
                groups[group]['images'].append(img_objs[i])

        for group in groups:
            self.create_annotations(
                groups[group]['images'],
                groups[group]['annotation_sets'],
                groups[group]['user_objs'],
                pollen_type,
                gold=True
            )

    def create_annotations(
            self, images, annotation_sets, annotators, annotation_type, gold=False):
        """Create annotations in database.

        Args:
            images (List[Image]): List of images.
            annotation_sets (List[List[Tuple(int, int, int, int)]]): List of
                annotation sets. Order has to match that of images.
            annotation_type (main.models.AnnotationType): The type of annotation.
            annotators (List[main.models.User]): Who made the annotations.
        """
        user_annotation_set_objs = []
        gold_annotation_set_objs = []

        for image in images:
            for annotator in annotators:
                user_annotation_set = UserAnnotationSet(
                    image=image,
                    annotation_type=annotation_type,
                    user=annotator,
                    finished=True
                )
                user_annotation_set_objs.append(user_annotation_set)

            if gold:
                gold_annotation_set = GoldAnnotationSet(
                    image=image,
                    annotation_type=annotation_type
                )
                gold_annotation_set_objs.append(gold_annotation_set)

        user_annotation_sets = UserAnnotationSet.objects.bulk_create(
            user_annotation_set_objs)
        user_annotation_sets_grouped = grouped(
            user_annotation_sets, len(annotators))

        gold_annotation_sets = GoldAnnotationSet.objects.bulk_create(
            gold_annotation_set_objs)

        zipped = zip(user_annotation_sets_grouped, annotation_sets)

        user_annotation_objs = []
        gold_annotation_objs = []

        for i, (group, annotations) in enumerate(zipped):
            for annotator_annotation_set in group:
                for x1, y1, x2, y2 in annotations:
                    user_annotation = UserAnnotation(
                        annotation_set=annotator_annotation_set,
                        vector={'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2},
                    )
                    user_annotation_objs.append(user_annotation)

            if gold:
                gold_annotation_set = gold_annotation_sets[i]
                for x1, y1, x2, y2 in annotations:
                    gold_annotation = GoldAnnotation(
                        annotation_set=gold_annotation_set,
                        vector={'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2},
                    )
                    gold_annotation_objs.append(gold_annotation)

        UserAnnotation.objects.bulk_create(user_annotation_objs)
        GoldAnnotation.objects.bulk_create(gold_annotation_objs)

    def get_hive_scene(self, hive_name):
        """Extract the scene and the Google Drive's hive name from the filename.

        Example: 'Chueried_Hive01_green80' => (Chueried_Hive01, green80)
        """
        if hive_name is None:
            return None, None

        scenes = [
            'Night',
            'diagonalview',
            'frontview',
            'smartphone',
            'grayscale',
            'red70',
            'green80'
        ]

        scene_name = 'default'
        for scene in scenes:
            if hive_name.endswith(scene):
                hive_name = hive_name.rstrip(f'_{scene}')
                scene_name = scene
        return hive_name, scene_name

    def insert_annotation_types(self):
        """Create all annotations types."""
        print('Insert annotation types...')
        names = [
            'bee',
            'pollen',
            'varroa',
            'entrance'
        ]
        for name in names:
            AnnotationType.objects.update_or_create(name=name)

    def insert_users(self):
        """Create all users."""
        print('Insert users...')
        password = os.getenv('DJANGO_PASSWORD')
        if not password:
            password = None
            print('DJANGO_PASSWORD not exported!')

        with open('main/resources/users.csv') as f:
            reader = csv.DictReader(f)
            for row in reader:
                admin = True if row['admin'] == 'True' else False

                for field in row:
                    if row[field] == 'None':
                        row[field] = None

                User.objects.create_user(
                    username=row['username'],
                    first_name=row['firstname'],
                    is_staff=admin,
                    is_active=admin,
                    is_superuser=admin,
                    last_name=row['lastname'],
                    password=password,
                    email=row['email'],
                    phone=row['phone'],
                    street=row['street'],
                    number=row['nr'],
                    zip=row['plz'],
                    city=row['city']
                )

        # create dummy user (e.g. when creating tasks automatically, etc)
        User.objects.create(
            username='admin',
            is_staff=True,
            is_superuser=True,
            is_active=False
        )

    @staticmethod
    def insert_tasks():
        """Add entrance labeling tasks."""
        for hive in Hive.objects.all():
            open_entrance_labeling_task(hive)
