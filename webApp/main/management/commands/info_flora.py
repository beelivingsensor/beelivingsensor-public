from pathlib import Path

from django.core.management.base import BaseCommand

from main.util.dump import get_most_recent_local_dump
from main.util.timestamping import get_timestamp
from main.views.environment.beefoodchecker.data_uploader import DataUploader
from main.views.environment.beefoodchecker.info_flora.infoflora_data_fetcher import \
    InfoFloraDataFetcher
from main.views.environment.beefoodchecker.info_flora.infoflora_data_saver import \
    InfoFloraDataSaver


class Command(BaseCommand):
    help = 'Get data from Info Flora.'

    def add_arguments(self, parser):
        parser.add_argument(
            'action',
            help='Action to be performed',
            type=str,
            choices=['fetch', 'upload', 'create']
        )

    def fetch(self, base_path):
        dump_path = base_path / get_timestamp()
        dump_path.mkdir(parents=True)
        fetcher = InfoFloraDataFetcher()
        fetcher.dump(dump_path)
        return dump_path

    def handle(self, *args, **options):
        base_path = Path('info_flora/')
        container = 'plant'
        action = options['action']

        if action == 'fetch':
            self.fetch(base_path)
        elif action == 'upload':
            dump_path = get_most_recent_local_dump(base_path)
            if not dump_path:
                dump_path = self.fetch(base_path)
            DataUploader().upload(container, dump_path)
        else:
            saver = InfoFloraDataSaver()
            saver.create_data_from_dump(container, base_path)
