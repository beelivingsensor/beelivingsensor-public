from django.core.management.base import BaseCommand
from main.views.environment.beefoodchecker.pritsch_saver import PritschSaver


class Command(BaseCommand):
    help = 'Add Pritsch data to database.'

    def handle(self, *args, **options):
        PritschSaver().add_pritsch_data()
