from django.urls import path

from main.views import annotation_views

urlpatterns = [
    path('api/annotation/create/',
         annotation_views.create_annotation, name='create_annotation'),
    path('api/annotation/delete/',
         annotation_views.api_delete_annotation, name='delete_annotation'),
    path('api/annotation/load/',
         annotation_views.load_annotations, name='load_annotations'),
    # loads annotations of an image
    path('api/annotation/loadannotationtypes/',
         annotation_views.load_annotation_types, name='load_annotation_types'),
    # loads all active annotation types
    path('api/annotation/update/',
         annotation_views.update_annotation, name='update_annotations'),
]