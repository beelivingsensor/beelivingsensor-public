from django.urls import path

from main.views.environment.beefoodchecker import beefoodchecker_views

urlpatterns = [
    path('',
         beefoodchecker_views.beefoodchecker_overview_view,
         name='beefoodchecker-overview'),

    path('observations/',
         beefoodchecker_views.beefoodchecker_observation_list_view,
         name='beefoodchecker-observation-list'),

    path('observations/create/',
         beefoodchecker_views.beefoodchecker_observation_create_view,
         name='beefoodchecker-observation-create'),

    path('plants/',
         beefoodchecker_views.beefoodchecker_plant_list_view,
         name='beefoodchecker-plant-list'),

    path('plants/<int:plant_pk>/',
         beefoodchecker_views.beefoodchecker_plant_detail_view,
         name='beefoodchecker-plant-detail'),

    path('search/',
         beefoodchecker_views.beefoodchecker_search_view,
         name='beefoodchecker-search'),
]
