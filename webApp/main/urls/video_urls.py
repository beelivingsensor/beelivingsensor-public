from django.urls import path

from main.views import video_views

urlpatterns = [
    path('<int:pk>/update/',
         video_views.video_update_view,
         name='video-update'),

    path('<int:pk>/',
         video_views.video_detail_view,
         name='video-detail'),

    path('',
         video_views.video_list_view,
         name='video-list'),

    path('<int:pk>/delete',
         video_views.video_delete_view,
         name='video-delete'),

    path('<int:pk>/images/',
         video_views.video_image_list_view,
         name='video-image-list'),

    path('<int:video_pk>/bestModel/select/',
         video_views.select_best_model_for_video_frame,
         name='best-model-select'),
]