from django.urls import path

from main.views.api.images import api_image_views

urlpatterns = [
    path('',
         api_image_views.api_image_list_view,
         name='api-image-list'
    ),

    path('<int:pk>/',
         api_image_views.api_image_detail_view,
         name='api-image-detail'
    ),

    path('<int:pk>/navigation/',
         api_image_views.api_image_navigation_view,
         name='api-image-navigation'
    ),

    path('<int:pk>/annotators/',
         api_image_views.api_image_annotator_list_view,
         name='api-image-annotator-list'
    ),

    path('<int:pk>/models/',
         api_image_views.api_image_model_list_view,
         name='api-image-model-list'
    ),
]
