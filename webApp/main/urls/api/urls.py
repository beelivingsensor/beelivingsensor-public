from django.urls import path, include

urlpatterns = [
    path('images/', include('main.urls.api.image_urls')),
    path('plants/', include('main.urls.api.plant_urls'))
]