from django.urls import path

from main.views.api.environment.plant import api_plant_views


urlpatterns = [
    path('',
         api_plant_views.api_plant_list_view,
         name='api-plant-list'
    ),

    path('<int:pk>/occurrences/',
         api_plant_views.api_plant_occurrence_list_view,
         name='api-plant-occurrence-list'
    ),

    path('match/',
         api_plant_views.api_plant_match_view,
         name='api-plant-match'
    ),
]
