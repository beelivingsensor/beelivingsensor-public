from django.test import TestCase
from django.test import Client
from django.urls import reverse

from main.models import Hive, Apiary, User


class HiveTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create(username='test', is_superuser=True, is_staff=True)
        cls.user.set_password('test')
        cls.user.save()
        cls.c = Client()
        cls.c.login(username='test', password='test')
        cls.apiary = Apiary.objects.create(name='The GREAT', creator=cls.user)

    def test_beehive_detail(self):
        beehive = Hive.objects.create(
            creator=self.user,
            apiary=self.apiary,
            name='Blau',
            private=False
        )
        response = self.c.get(reverse('beehive-detail', args=(beehive.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_beehive_list(self):
        response = self.c.get(reverse('beehive-list'))
        self.assertEqual(response.status_code, 200)

    def test_beehive_update_get(self):
        beehive = Hive.objects.create(
            creator=self.user,
            apiary=self.apiary,
            name='Blau',
            private=False
        )
        response = self.c.get(reverse('beehive-update', args=(beehive.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_beehive_update_post(self):
        beehive = Hive.objects.create(
            creator=self.user,
            apiary=self.apiary,
            name='Blau',
            private=False
        )
        response = self.c.post(
            reverse('beehive-update', args=(beehive.pk,)),
            {'private': True,
             'name': 'Rot',
             'entrance': Hive.Entrance.RECTANGULAR
             })
        self.assertEqual(response.status_code, 302)
        beehive.refresh_from_db()
        self.assertEqual(beehive.private, True)
        self.assertEqual(beehive.name, 'Rot')
        self.assertEqual(beehive.entrance, Hive.Entrance.RECTANGULAR)

    def test_beehive_create_get(self):
        response = self.c.get(reverse('beehive-create',
                                      args=(self.apiary.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_beehive_create_post(self):
        self.c.post(reverse('beehive-create', args=(self.apiary.pk,)),
                    {'name': 'Blau',
                     'private': False,
                     'entrance': Hive.Entrance.RECTANGULAR})
        beehive = Hive.objects.last()
        self.assertEqual(beehive.name, 'Blau')
        self.assertEqual(beehive.creator.username, 'test')
        self.assertEqual(beehive.entrance, Hive.Entrance.RECTANGULAR)

    def test_beehive_delete(self):
        beehive = Hive.objects.create(
            creator=self.user,
            apiary=self.apiary,
            name='Blau',
            private=False
        )
        response = self.c.get(
            reverse('beehive-delete', args=(beehive.pk,)))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Hive.objects.filter(pk=beehive.pk).all())
