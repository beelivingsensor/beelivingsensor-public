from django.test import TestCase

from main.models import User, AnnotationType, UserAnnotation, \
    UserAnnotationSet, Image, GoldAnnotation, GoldAnnotationSet
from main.views.annotation.annotation_merge import merge_image_annotations, \
    conv


class AnnotationMergeTest(TestCase):

    def setUp(self):
        user1 = User.objects.create(
            username='user1', is_superuser=True, is_staff=True)
        user2 = User.objects.create(
            username='user2', is_superuser=True, is_staff=True)
        user3 = User.objects.create(
            username='user3', is_superuser=True, is_staff=True)
        user4 = User.objects.create(
            username='user4', is_superuser=True, is_staff=True)

        bee_annotation_type = AnnotationType.objects.create(
            name='bee'
        )
        self.bee_annotation_type = bee_annotation_type

        entrance_annotation_type = AnnotationType.objects.create(
            name='entrance'
        )

        image = Image.objects.create(
            path='bla/bla',
            name='bla',
            filename='bla',
            width=111,
            height=111
        )
        self.image = image

        annotation_set_1 = UserAnnotationSet.objects.create(
            user=user1,
            annotation_type=bee_annotation_type,
            finished=True,
            image=image
        )

        annotation_set_2 = UserAnnotationSet.objects.create(
            user=user2,
            annotation_type=bee_annotation_type,
            finished=True,
            image=image
        )

        annotation_set_3 = UserAnnotationSet.objects.create(
            user=user3,
            annotation_type=bee_annotation_type,
            finished=True,
            image=image
        )

        annotation_set_4 = UserAnnotationSet.objects.create(
            user=user4,
            annotation_type=bee_annotation_type,
            finished=False,
            image=image
        )

        annotation_set_5 = UserAnnotationSet.objects.create(
            user=user2,
            annotation_type=entrance_annotation_type,
            finished=True,
            image=image
        )

        # matching coordinates
        # first group
        UserAnnotation.objects.create(
            annotation_set=annotation_set_1,
            vector={'x1': 298, 'y1': 213, 'x2': 543, 'y2': 476},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 324, 'y1': 208, 'x2': 530, 'y2': 469},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_3,
            vector={'x1': 256, 'y1': 202, 'x2': 525, 'y2': 458},
        )

        # second group
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 93, 'y1': 532, 'x2': 192, 'y2': 641},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_3,
            vector={'x1': 102, 'y1': 541, 'x2': 194, 'y2': 643},
        )

        # non-matching coordinates, non-overlapping
        UserAnnotation.objects.create(
            annotation_set=annotation_set_1,
            vector={'x1': 63, 'y1': 104, 'x2': 161, 'y2': 232},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 795, 'y1': 605, 'x2': 863, 'y2': 669},
        )

        # non-matching coordinates, overlapping
        UserAnnotation.objects.create(
            annotation_set=annotation_set_3,
            vector={'x1': 439, 'y1': 441, 'x2': 616, 'y2': 519},
        )

        # has to be ignored as not finished
        UserAnnotation.objects.create(
            annotation_set=annotation_set_4,
            vector={'x1': 299, 'y1': 214, 'x2': 544, 'y2': 477},
        )
        # has to be ignored because different annotation type
        UserAnnotation.objects.create(
            annotation_set=annotation_set_5,
            vector={'x1': 300, 'y1': 215, 'x2': 545, 'y2': 478},
        )
        # has to be ignored because same user made similar annotations
        UserAnnotation.objects.create(
            annotation_set=annotation_set_3,
            vector={'x1': 110, 'y1': 541, 'x2': 194, 'y2': 643},
        )
        # has to be ignored because same user makes same annotations
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 705, 'y1': 77, 'x2': 809, 'y2': 155},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 705, 'y1': 77, 'x2': 809, 'y2': 155},
        )
        UserAnnotation.objects.create(
            annotation_set=annotation_set_2,
            vector={'x1': 705, 'y1': 77, 'x2': 809, 'y2': 155},
        )

    def test(self):
        merge_image_annotations(self.image, self.bee_annotation_type)
        gold_annotation_sets = GoldAnnotationSet.objects.filter(
            image=self.image,
            annotation_type=self.bee_annotation_type
        )
        assert len(gold_annotation_sets) == 1

        gold_annotations = GoldAnnotation.objects.filter(
            annotation_set=gold_annotation_sets[0]
        )
        assert len(gold_annotations) == 2

        vectors = {conv(a.vector) for a in gold_annotations}

        assert (292, 207, 532, 467) in vectors
        assert (97, 536, 193, 642) in vectors
