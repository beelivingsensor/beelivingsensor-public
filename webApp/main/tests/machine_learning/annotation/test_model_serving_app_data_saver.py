import json
from pathlib import Path
from types import SimpleNamespace

from django.test import TestCase

from main.models import Video, Image, Hive, ModelAnnotation
from main.models.machine_learning.annotation.tracker_event import TrackerEvent
from main.tests.machine_learning.annotation.utils import model_service_setup
from main.views.annotation.model_serving_app_data_saver import \
    ModelServingAppDataSaver


class ModelServingAppDataSaverTest(TestCase):

    def test_default(self):
        model_service_setup()
        json_path = Path('main/tests/machine_learning/annotation/test_video.json')
        with open(json_path) as f:
            p_video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
            saver = ModelServingAppDataSaver()
            saver.add(p_video, 'video')
            tracker_events = TrackerEvent.objects.all()
            self.assertGreater(tracker_events.count(), 3)
            with_pollen = tracker_events.filter(pollen__len__gt=0)
            self.assertEqual(with_pollen.count(), 1)

            video = Video.objects.get(pk=p_video.pk)
            images = Image.objects.filter(image_set__video=video)

            # check if hive images were added
            hive_images = images.filter(type=Image.TYPE.HIVE)
            self.assertEqual(hive_images.count(), 47)
            # check if bee images were added
            bee_images = images.filter(type=Image.TYPE.BEE)
            self.assertGreater(bee_images.count(), 0)

            # check if first annotation is correct
            annotation = ModelAnnotation.objects.first()
            self.assertEqual(annotation.vector['x1'], 1833)
            self.assertEqual(annotation.vector['y1'], 1856)
            self.assertEqual(annotation.vector['x2'], 2047)
            self.assertEqual(annotation.vector['y2'], 1920)

    def test_video_does_not_exist(self):
        json_path = Path(
            'main/tests/machine_learning/annotation/test_video.json')
        with open(json_path) as f:
            p_video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
            saver = ModelServingAppDataSaver()
            status = saver.add(p_video, 'video')

        self.assertFalse(status)

    def test_video_annotations_already_exist(self):
        model_service_setup()
        video = Video.objects.get(pk=120394803)
        hive = Hive.objects.all().first()
        fname = 'video_120394803_himage_1_20210314-10-53-41-230156.jpg'
        Image.objects.create(
            image_set=video.imageset,
            beehive=hive,
            name=fname,
            filename=fname,
            width=2560,
            height=1920,
            type='hive',
        )

        json_path = Path(
            'main/tests/machine_learning/annotation/test_video.json')

        with open(json_path) as f:
            p_video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
            saver = ModelServingAppDataSaver()
            status = saver.add(p_video, 'video')

        self.assertFalse(status)
        self.assertEqual(video.imageset.images.count(), 1)

    def test_bulk_create(self):
        model_service_setup()
        json_path = Path(
            'main/tests/machine_learning/annotation/test_video.json')
        with open(json_path) as f:
            p_video = json.load(f, object_hook=lambda d: SimpleNamespace(**d))
            saver = ModelServingAppDataSaver()
            saver.bulk_create_step = 20
            status = saver.add(p_video, 'video')

        self.assertTrue(status)
