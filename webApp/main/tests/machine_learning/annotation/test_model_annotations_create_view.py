from unittest import TestCase

from django.urls import reverse

from django.conf import settings
from django.test import Client

from main.tests.machine_learning.annotation.utils import model_service_setup


class TestModelAnnotationsCreateView(TestCase):

    def setUp(self):
        model_service_setup()
        self.c = Client()
        self.c.login(username='test', password='test')

    def test_post(self):
        annotations_path = 'model-annotations/test_video_model_annotations.json'

        response = self.c.post(
            reverse('model-annotations-create-api'),
            data={'api_key': settings.WEBAPP_API_KEY,
                  'media_type': 'video',
                  'annotations_path': annotations_path,
                  'test': True,
            }
        )
        self.assertEqual(response.status_code, 201)
