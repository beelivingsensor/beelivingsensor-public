from main.models import User, Apiary, Hive, Video, ImageSet, AnnotationType, \
    AIModel


def model_service_setup():
    user = User.objects.create(
        username='test', is_superuser=True, is_staff=True)
    user.set_password('test')
    user.save()

    apiary = Apiary.objects.create(
        creator=user,
        name='blabla'
    )

    beehive = Hive.objects.create(
        apiary=apiary,
        creator=user,
        name='blabla'
    )

    video = Video()
    video.path.name = 'bla/bla'
    video.fileName = 'bla.mp4'
    video.creator = user
    video.beehive = beehive
    video.processed = False

    imageset = ImageSet()
    imageset.name = video.filename()
    imageset.save()
    imageset.path = f'imageset_{imageset.pk}'
    imageset.save()

    video.imageset = imageset
    video.pk = 120394803
    video.save()

    bee_annotation_type = AnnotationType.objects.create(
        name='bee'
    )

    pollen_annotation_type = AnnotationType.objects.create(
        name='pollen'
    )

    AIModel.objects.create(
        pk=4,
        description='blabla',
        path='bee_model_path',
        annotation_type=bee_annotation_type
    )

    AIModel.objects.create(
        pk=5,
        description='blabla',
        path='pollen_model_path',
        annotation_type=pollen_annotation_type
    )