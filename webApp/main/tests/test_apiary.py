from django.test import TestCase
from django.test import Client
from django.urls import reverse

from main.models import Apiary, User


class ApiaryTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create(username='test', is_superuser=True, is_staff=True)
        cls.user.set_password('test')
        cls.user.save()
        cls.c = Client()
        cls.c.login(username='test', password='test')

    def test_apiary_detail(self):
        apiary = Apiary.objects.create(name='The GREAT', creator=self.user)
        response = self.c.get(reverse('apiary-detail', args=(apiary.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_apiary_list(self):
        response = self.c.get(reverse('apiary-list'))
        self.assertEqual(response.status_code, 200)

    def test_apiary_update_get(self):
        apiary = Apiary.objects.create(name='The GREAT', creator=self.user)
        response = self.c.get(reverse('apiary-update', args=(apiary.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_apiary_update_post(self):
        apiary = Apiary.objects.create(name='The GREAT', creator=self.user)
        response = self.c.post(
            reverse('apiary-update', args=(apiary.pk,)), {'name': 'SUNNY'})
        self.assertEqual(response.status_code, 302)
        apiary.refresh_from_db()
        self.assertEqual(apiary.name, 'SUNNY')

    def test_apiary_create_get(self):
        response = self.c.get(reverse('apiary-create'))
        self.assertEqual(response.status_code, 200)

    def test_apiary_create_post(self):
        self.c.post(reverse('apiary-create'), {'name': 'The GREAT'})
        apiary = Apiary.objects.last()
        self.assertEqual(apiary.name, 'The GREAT')
        self.assertEqual(apiary.creator.username, 'test')

    def test_apiary_delete(self):
        apiary = Apiary.objects.create(name='The GREAT', creator=self.user)
        response = self.c.get(
            reverse('apiary-delete', args=(apiary.pk,)))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Apiary.objects.filter(pk=apiary.pk))
