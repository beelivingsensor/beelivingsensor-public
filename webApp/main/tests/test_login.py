import re

from django.test import TestCase
from django.test import Client
from django.urls import reverse, URLResolver
from main.urls.urls import urlpatterns


class LoginTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()

    def test_login(self):
        for url in urlpatterns:
            if isinstance(url, URLResolver):
                for sub_url in url.url_patterns:
                    self.check(sub_url)
            else:
                self.check(url)

    def check(self, url):
        exceptions = []
        full_url_name = url.name

        if full_url_name in exceptions or full_url_name.startswith('download-'):
            return

        kwargs = {}
        for match in re.finditer(r'<int:(.*?)>', str(url.pattern)):
            kwargs[match.group(1)] = 1
        try:
            response = self.client.get(reverse(full_url_name, kwargs=kwargs))
        except:
            raise Exception(f'{full_url_name} view accessible without login!')

        # Method not allowed
        if response.status_code == 405:
            response = self.client.post(reverse(full_url_name, kwargs=kwargs))

        self.assertIn(response.status_code,
                      [302, 403],
                      msg=f'{full_url_name} view accessible without login!')

        if response.status_code == 302:
            self.assertTrue(
                '/user/login/' in response.url or '/admin/login/' in response.url,
                msg=f'{full_url_name} view accessible without login!')

