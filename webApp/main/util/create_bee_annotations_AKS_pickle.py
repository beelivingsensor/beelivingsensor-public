# Creates a pickle of all bee annotations of the ETH guys from the previous hive data

# Required dependencies:
# - Yolo2Pascal-annotation-conversion

# Required resources
# - data folder with labeled images of ETH guys

# Run:
# python create_bee_annotations_existing_data_pickle_andy_korbinian_simon.py [path to data folder]


from pathlib import Path
import pickle
import xml.etree.ElementTree as ET
import zipfile
import subprocess
import sys

andy_korbi_simon_data_dir_path = Path(sys.argv[1])


bee_zips = [
    'Chueried_Hempbox.zip',
    'Chueried_Hive01.zip',
    'ClemensRed_blurred_labeled.zip',
    'Doettingen_Hive1_blurred_labeled.zip',
    'Echolinde.zip',
    'Erlen_Hive04_diagonalview_blurred_labeled.zip',
    'Erlen_Hive04_frontview_blurred_labeled.zip',
    'Erlen_Hive04_smartphone_blurred_labeled.zip',
    'Erlen_Hive11_blurred_labeled.zip',
    'Froh14_blurred_labeled.zip',
    'Froh23_TreeCavity.zip',
    'UnitedQueens_blurred_labeled.zip',
]


frames = {}


def get_to_right_dir_depth(current_path: Path):
    """Go to right directory depth.

    Because the labeled images are at different directory depths 🤦‍.
    """
    dir_names = [fn.name for fn in current_path.iterdir()]
    if 'train' in dir_names:
        return current_path

    return get_to_right_dir_depth(current_path / dir_names[0])


for hive_zip in andy_korbi_simon_data_dir_path.iterdir():

    # check if the zip contains labeled bees
    if hive_zip.name in bee_zips:
        print(hive_zip)

        hive_dir_name = hive_zip.stem

        hive_dir_path = andy_korbi_simon_data_dir_path / hive_dir_name
        # unzip folder
        with zipfile.ZipFile(hive_zip, 'r') as zip_ref:
            zip_ref.extractall(hive_dir_path)

        hive_dir = get_to_right_dir_depth(hive_dir_path)

        for status_dir in hive_dir.iterdir():
            if status_dir.name in ['validate', 'test', 'train']:

                # Add classes.txt, since missing
                if (hive_dir_name == 'Chueried_Hive01' and status_dir.name == 'validate')\
                        or (hive_dir_name == 'ClemensRed_blurred_labeled' and status_dir.name == 'test')\
                        or hive_dir_name == 'Erlen_Hive04_diagonalview_blurred_labeled':
                    with open(status_dir / 'classes.txt', 'w') as f:
                        f.write('bee\n')

                # convert Yolo to Pascal VOC
                subprocess.run(
                    f'python Yolo2Pascal-annotation-conversion/yolo2pascal/yolo2voc.py {status_dir.resolve()}',
                    shell=True,
                    stdout=subprocess.DEVNULL
                )

                for file_path in status_dir.iterdir():
                    # only keep last and third last component of path
                    # (hive_name/file_name)
                    relevant_path = Path('./').joinpath(file_path.parts[-3], file_path.parts[-1])
                    extension = relevant_path.suffix
                    key = relevant_path.with_suffix('')
                    if key not in frames:
                        frames[key] = {
                            'annotations': [],
                            'image_exists': False,
                            'width': 0,
                            'height': 0
                        }
                    if extension == '.jpg':
                        frames[key]['image_exists'] = True
                    elif extension == '.xml':
                        etree = ET.parse(file_path)
                        root = etree.getroot()

                        objects = root.findall('object')
                        for object in objects:
                            box = object.find('bndbox')
                            xmin = int(box.find('xmin').text)
                            ymin = int(box.find('ymin').text)
                            xmax = int(box.find('xmax').text)
                            ymax = int(box.find('ymax').text)

                            frames[key]['annotations'].append((xmin, ymin, xmax, ymax))

                        width = int(root.find('size/width').text)
                        height = int(root.find('size/height').text)
                        depth = int(root.find('size/depth').text)
                        frames[key]['width'] = width
                        frames[key]['height'] = height
                        frames[key]['depth'] = depth

print('Original frame count:', len(frames))
# ignore frames that have no jpg
new_annotations = {k: v for k, v in frames.items() if v['image_exists']}
print('Cleaned frame count:', len(new_annotations))

print(list(new_annotations.items())[:3])

with open('../resources/bee_annotations_AKS.pickle', 'wb') as f:
    pickle.dump(new_annotations, f)
