from lxml.etree import Element, SubElement, tostring


class PascalVOCWriter:

    depth = '3'
    database = 'Unknown'
    segmented = '0'
    pose = 'Unspecified'
    truncated = '0'
    difficult = '0'

    def __init__(self, folder, image):
        self.folder = folder
        self.filename = f'{image.download_name}{image.extension}'
        self.width = image.width
        self.height = image.height
        self.objects = []

    def add_annotation_set(self, annotation_set):
        annotation_type = annotation_set.annotation_type.name
        for annotation in annotation_set.annotations.all():
            object_e = Element('object')
            SubElement(object_e, 'name').text = annotation_type
            SubElement(object_e, 'pose').text = self.pose
            SubElement(object_e, 'truncated').text = self.truncated
            SubElement(object_e, 'difficult').text = self.difficult

            bndbox_e = SubElement(object_e, 'bndbox')
            SubElement(bndbox_e, 'xmin').text = str(annotation.vector['x1'])
            SubElement(bndbox_e, 'ymin').text = str(annotation.vector['y1'])
            SubElement(bndbox_e, 'xmax').text = str(annotation.vector['x2'])
            SubElement(bndbox_e, 'ymax').text = str(annotation.vector['y2'])

            self.objects.append(object_e)

    def generate_xml(self):
        annotation_e = Element('annotation')
        SubElement(annotation_e, 'folder').text = self.folder
        SubElement(annotation_e, 'filename').text = self.filename
        SubElement(annotation_e, 'path').text = f'{self.folder}/{self.filename}'

        source_e = SubElement(annotation_e, 'source')
        SubElement(source_e, 'database').text = self.database

        size_e = SubElement(annotation_e, 'size')
        SubElement(size_e, 'width').text = str(self.width)
        SubElement(size_e, 'height').text = str(self.height)
        SubElement(size_e, 'depth').text = str(self.depth)

        SubElement(annotation_e, 'segmented').text = self.segmented

        for object_e in self.objects:
            annotation_e.append(object_e)

        return tostring(annotation_e, encoding='utf-8', pretty_print=True)
