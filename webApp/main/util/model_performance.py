# Gabriela López Magaña and Pascal Engeli
import numpy as np
from copy import deepcopy


def calc_iou(gt_bbox, pred_bbox):
    '''
    This function takes the predicted bounding box and ground truth bounding box and
    return the IoU ratio
    '''
    x_topleft_gt, y_topleft_gt, x_bottomright_gt, y_bottomright_gt = gt_bbox
    x_topleft_p, y_topleft_p, x_bottomright_p, y_bottomright_p = pred_bbox

    if (x_topleft_gt > x_bottomright_gt) or (y_topleft_gt > y_bottomright_gt):
        raise AssertionError("Ground Truth Bounding Box is not correct")
    if (x_topleft_p > x_bottomright_p) or (y_topleft_p > y_bottomright_p):
        raise AssertionError("Predicted Bounding Box is not correct",
                             x_topleft_p, x_bottomright_p, y_topleft_p,
                             y_bottomright_p)

    # if the GT bbox and predcited BBox do not overlap then iou=0
    if (x_bottomright_gt < x_topleft_p):
        # If bottom right of x-coordinate  GT  bbox is less than or above the top left of x coordinate of  the predicted BBox

        return 0.0
    if (
            y_bottomright_gt < y_topleft_p):  # If bottom right of y-coordinate  GT  bbox is less than or above the top left of y coordinate of  the predicted BBox

        return 0.0
    if (
            x_topleft_gt > x_bottomright_p):  # If bottom right of x-coordinate  GT  bbox is greater than or below the bottom right  of x coordinate of  the predcited BBox

        return 0.0
    if (
            y_topleft_gt > y_bottomright_p):  # If bottom right of y-coordinate  GT  bbox is greater than or below the bottom right  of y coordinate of  the predcited BBox

        return 0.0

    GT_bbox_area = (x_bottomright_gt - x_topleft_gt + 1) * (
                y_bottomright_gt - y_topleft_gt + 1)
    Pred_bbox_area = (x_bottomright_p - x_topleft_p + 1) * (
                y_bottomright_p - y_topleft_p + 1)

    x_top_left = np.max([x_topleft_gt, x_topleft_p])
    y_top_left = np.max([y_topleft_gt, y_topleft_p])
    x_bottom_right = np.min([x_bottomright_gt, x_bottomright_p])
    y_bottom_right = np.min([y_bottomright_gt, y_bottomright_p])

    intersection_area = (x_bottom_right - x_top_left + 1) * (
                y_bottom_right - y_top_left + 1)

    union_area = (GT_bbox_area + Pred_bbox_area - intersection_area)

    return intersection_area / union_area


def get_model_scores(pred_boxes):
    """Creates a dictionary of from model_scores to image ids.
    Args:
        pred_boxes (dict): dict of dicts of 'boxes' and 'scores'
    Returns:
        dict: keys are model_scores and values are image ids (usually filenames)
    """
    model_score = {}
    for img_id, val in pred_boxes.items():
        for score in val['scores']:
            if score not in model_score.keys():
                model_score[score] = [img_id]
            else:
                model_score[score].append(img_id)
    return model_score


def calc_precision_recall(image_results):
    """Calculates precision and recall from the set of images
    Args:
        img_results (dict): dictionary formatted like:
            {
                'img_id1': {'true_pos': int, 'false_pos': int, 'false_neg': int},
                'img_id2': ...
                ...
            }
    Returns:
        tuple: of floats of (precision, recall)
    """
    true_positive = 0
    false_positive = 0
    false_negative = 0
    precision = 0
    recall = 0
    for img_id, res in image_results.items():
        true_positive += res['true_positive']
        false_positive += res['false_positive']
        false_negative += res['false_negative']
        try:
            precision = true_positive / (true_positive + false_positive)
        except ZeroDivisionError:
            precision = 0.0
        try:
            recall = true_positive / (true_positive + false_negative)
        except ZeroDivisionError:
            recall = 0.0
    return (precision, recall)


def get_single_image_results(gt_boxes, pred_boxes, iou_thr):
    """Calculates number of true_pos, false_pos, false_neg from single batch of boxes.
    Args:
        gt_boxes (list of list of floats): list of locations of ground truth
            objects as [xmin, ymin, xmax, ymax]
        pred_boxes (dict): dict of dicts of 'boxes' (formatted like `gt_boxes`)
            and 'scores'
        iou_thr (float): value of IoU to consider as threshold for a
            true prediction.
    Returns:
        dict: qty_ground_truth (int), qty_predicted (int), true positives (int), false positives (int), false negatives (int)
    """
    all_pred_indices = range(len(pred_boxes))
    all_gt_indices = range(len(gt_boxes))
    qty_predicted = len(pred_boxes)
    qty_ground_truth = len(gt_boxes)
    tp = 0
    fp = 0
    fn = 0
    output = "output"

    if len(all_pred_indices) == 0:
        return {'qty_ground_truth': qty_ground_truth,
                'qty_predicted': qty_predicted, 'true_positive': tp,
                'false_positive': fp, 'false_negative': fn}
    if len(all_gt_indices) == 0:
        return {'qty_ground_truth': qty_ground_truth,
                'qty_predicted': qty_predicted, 'true_positive': tp,
                'false_positive': fp, 'false_negative': fn}

    gt_idx_thr = []
    pred_idx_thr = []
    ious = []

    for ipb, pred_box in enumerate(pred_boxes):
        # print(ipb, pred_box)

        for igb, gt_box in enumerate(gt_boxes):
            # print(igb, gt_box)
            iou = calc_iou(gt_box, pred_box)

            if iou > iou_thr:
                gt_idx_thr.append(igb)
                pred_idx_thr.append(ipb)
                ious.append(iou)
    iou_sort = np.argsort(ious)[::1]
    if len(iou_sort) == 0:
        return {'qty_ground_truth': qty_ground_truth,
                'qty_predicted': qty_predicted, 'true_positive': tp,
                'false_positive': len(pred_boxes), 'false_negative': fn}
    else:
        gt_match_idx = []
        pred_match_idx = []
        for idx in iou_sort:
            gt_idx = gt_idx_thr[idx]
            pr_idx = pred_idx_thr[idx]
            # If the boxes are unmatched, add them to matches
            if (gt_idx not in gt_match_idx) and (pr_idx not in pred_match_idx):
                gt_match_idx.append(gt_idx)
                pred_match_idx.append(pr_idx)
        tp = len(gt_match_idx)
        fp = len(pred_boxes) - len(pred_match_idx)
        fn = len(gt_boxes) - len(gt_match_idx)
        output = {'qty_ground_truth': qty_ground_truth,
                  'qty_predicted': qty_predicted, 'true_positive': tp,
                  'false_positive': fp, 'false_negative': fn}
    return output


def get_avg_precision_at_iou(image, gt_boxes, pred_bb, no_predictions=False,
                             iou_thr=0.5):
    if (no_predictions):
        return {image: {'qty_ground_truth': len(gt_boxes[image]),
                        'qty_predicted': 0, 'true_positive': 0,
                        'false_positive': 0,
                        'false_negative': len(gt_boxes[image])}}

    model_scores = get_model_scores(pred_bb)
    # TODO: does not work with scores that are all equal
    # print("model_scores: ", model_scores)
    sorted_model_scores = sorted(model_scores.keys())
    # print("ggg sorted model scores: ", sorted_model_scores)
    # Sort the predicted boxes in descending order (lowest scoring boxes first):
    for img_id in pred_bb.keys():
        # print('img_id: ',img_id)
        arg_sort = np.argsort(pred_bb[img_id]['scores'])
        pred_bb[img_id]['scores'] = np.array(pred_bb[img_id]['scores'])[
            arg_sort].tolist()
        pred_bb[img_id]['boxes'] = np.array(pred_bb[img_id]['boxes'])[
            arg_sort].tolist()

    pred_boxes_pruned = deepcopy(pred_bb)

    precisions = []
    recalls = []
    model_thrs = []
    img_results = {}
    img_ids = []
    model_score_thr = 0
    # Loop over model score thresholds and calculate precision, recall
    for ithr, model_score_thr in enumerate(sorted_model_scores[:-1]):
        # On first iteration, define img_results for the first time:
        # print("Model score thr : ", model_score_thr)
        img_ids = gt_boxes.keys() if ithr == 0 else model_scores[
            model_score_thr]

    if len(img_ids) == 0:
        print("pruned img_ids from after score", image)
        return {image: {'qty_ground_truth': len(gt_boxes[image]),
                        'qty_predicted': 0, 'true_positive': 0,
                        'false_positive': 0,
                        'false_negative': len(gt_boxes[image])}}

    for img_id in img_ids:
        # return if the size of prediction is 0
        if len(pred_bb[img_id]['boxes']) == 0:
            return {img_id: {'qty_ground_truth': len(gt_boxes[img_id]),
                             'qty_predicted': 0, 'true_positive': 0,
                             'false_positive': 0,
                             'false_negative': len(gt_boxes[img_id])}}

        gt_boxes_img = gt_boxes[img_id]
        box_scores = pred_boxes_pruned[img_id]['scores']
        start_idx = 0

        for score in box_scores:
            if score <= 0:
                start_idx += 1
            else:
                break
                # Remove boxes, scores of lower than threshold scores:
        pred_boxes_pruned[img_id]['scores'] = pred_boxes_pruned[img_id][
                                                  'scores'][start_idx:]
        pred_boxes_pruned[img_id]['boxes'] = pred_boxes_pruned[img_id][
                                                 'boxes'][start_idx:]
        # Recalculate image results for this image
        img_results[img_id] = get_single_image_results(gt_boxes_img,
                                                       pred_boxes_pruned[
                                                           img_id]['boxes'],
                                                       iou_thr=0.25)
        # calculate precision and recall

        # if len(img_results) == 0 and len(pred_bb[img_id]['boxes']) == 0 and len(gt_boxes[img_id]) != 0:
        if len(img_results[img_id]) == 0 and len(gt_boxes[img_id]) != 0:
            print("get_avg_precision_at_iou all predictions were pruned")
            return {img_id: {'qty_ground_truth': len(gt_boxes[img_id]),
                             'qty_predicted': 0, 'true_positive': 0,
                             'false_positive': 0,
                             'false_negative': len(gt_boxes[img_id])}}

    prec, rec = calc_precision_recall(img_results)
    precisions.append(prec)
    recalls.append(rec)
    model_thrs.append(model_score_thr)
    precisions = np.array(precisions)
    recalls = np.array(recalls)
    prec_at_rec = []
    for recall_level in np.linspace(0.0, 1.0, 11):
        try:
            args = np.argwhere(recalls > recall_level).flatten()
            prec = max(precisions[args])
            # print(recalls, "Recall")
            # print(recall_level, "Recall Level")
            # print(args, "Args")
            # print(prec, "precision")
        except ValueError:
            prec = 0.0
        prec_at_rec.append(prec)
    avg_prec = np.mean(prec_at_rec)
    return img_results


def aggregate_results(results):
    """Aggregate performance results across images.

    Args:
        results (Dict[Dict[str->int]]): The results of the images.
    """
    aggregated_results = {
        'qty_ground_truth': 0,
        'qty_predicted': 0,
        'true_positive': 0,
        'false_positive': 0,
        'false_negative': 0
    }

    for img, stats in results.items():
        for stat, val in stats.items():
            aggregated_results[stat] += val

    tp = aggregated_results['true_positive']
    fp = aggregated_results['false_positive']
    fn = aggregated_results['false_negative']

    predicted = tp + fp
    ground_truth = tp + fn
    aggregated_results['precision'] = tp/predicted if predicted else 0
    aggregated_results['recall'] = tp/ground_truth if ground_truth else 0

    return aggregated_results
