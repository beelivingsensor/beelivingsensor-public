
def grouped(objs, n):
    return [objs[i:i + n] for i in range(0, len(objs), n)]
