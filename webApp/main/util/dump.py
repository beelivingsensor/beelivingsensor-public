from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

from main.util.azure_helpers import list_blobs, download_blob


def get_most_recent_local_dump(base_path):
    """Get the most recent dump locally.

    Returns:
        Path: Path to most recent dump folder.
    """
    base_path = Path(base_path)

    if not base_path.exists():
        return None

    # if the base path exists but no dumps are there
    dumps = sorted(base_path.iterdir())
    if not dumps:
        return None

    return dumps[-1]


def get_azure_dumps(container, blob_prefix):
    """Get all dumps on Azure.

    Args:
        container (str): The container name on Azure.
        blob_prefix (str): Blob names starting with this prefix.

    Returns:
        Dict[Path, List[str]]: Dumps with their blob names.
    """
    blobs = list_blobs(container, blob_prefix)
    dumps = {}

    for blob in blobs:
        dump = '/'.join(Path(blob.name).parts[:2])
        if dump not in dumps:
            dumps[dump] = []
        dumps[dump].append(blob.name)

    return dumps


def _download_blob(container, from_path, to_path):
    download_blob(container, from_path, to_path)
    print(f'Blob {to_path} downloaded ✅')


def download_most_recent_dump(container, blob_prefix):
    """Download the blobs of the dump.

    Args:
        container (str): The container name on Azure.
        blob_prefix (str): Blob names starting with this prefix.

    Returns:
        str: Path to dump.
    """
    dumps = get_azure_dumps(container, blob_prefix)
    most_recent_dump = sorted(dumps)[-1]
    blobs = dumps[most_recent_dump]

    if Path(most_recent_dump).exists():
        print('Dump already downloaded!')
    else:
        with ThreadPoolExecutor() as executor:
            for blob in blobs:
                executor.submit(
                    _download_blob,
                    container,
                    blob,
                    blob
                )

    return most_recent_dump
