# Creates a pickle of all pollen annotations and uploads the images to azure
# cloud storage
#
# Required dependencies:
# - Yolo2Pascal-annotation-conversion
# - lxml
# - PyQt5
#
# Required resources
# - data folder with labeled images of ETH group (backup/xxx/)
#
# Run:
# python create_pollen_annotations_AKS_pickle.py [path to data folder]
#
# Output:
# ../resources/pollen_annotations_AKS.pickle


import os
from pathlib import Path
import pickle
import xml.etree.ElementTree as ET
import zipfile
import subprocess
import sys
import cv2

from azure.storage.blob import BlobClient


andy_korbi_simon_data_dir_path = Path(sys.argv[1])

pollen_zip_path = andy_korbi_simon_data_dir_path / 'pollen_data_complete.zip'

with zipfile.ZipFile(pollen_zip_path, 'r') as zip_ref:
    zip_ref.extractall(andy_korbi_simon_data_dir_path)

pollen_dir_path = andy_korbi_simon_data_dir_path / 'all_Pollen_data'


hive_zips = {
    'Doettingen_hybrid.zip': 'Doettingen_Hive1',
    'Doettingen_more_hybrid.zip': 'Doettingen_Hive1',
    'DSL_labeled.zip': 'UnitedQueens',
    'PollenDataset.zip': None,
}


annotations = {}


def get_to_right_dir_depth(current_path: Path):
    """Go to right directory depth.

    Because the labeled images are at different directory depths 🤦‍.
    """
    fnames = list(current_path.iterdir())
    for fn in fnames:
        if fn.suffix in ['.jpg', '.png']:
            return current_path

    for fn in fnames:
        if fn.is_dir():
            return get_to_right_dir_depth(current_path / fn.name)


def upload_to_azure(image_path, blob_name):
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')

    container_name = 'frames'

    blob_client = BlobClient.from_connection_string(
        conn_str=connect_str,
        container_name=container_name,
        blob_name=blob_name)

    with open(image_path, 'rb') as data:
        blob_client.upload_blob(data, overwrite=True)


for hive_zip in pollen_dir_path.iterdir():

    # Check if the zip contains labeled bees
    if hive_zip.name in hive_zips:
        print(hive_zip)

        hive_dir_name = hive_zip.stem

        hive_dir_path = pollen_dir_path / hive_dir_name
        # Unzip folder
        with zipfile.ZipFile(hive_zip, 'r') as zip_ref:
            zip_ref.extractall(hive_dir_path)

        # Get path of directory where images & annotations are
        hive_dir = get_to_right_dir_depth(hive_dir_path)

        # Create classes.txt if missing
        with open(hive_dir / 'classes.txt', 'w') as f:
            f.write('Pollen\n')

        # Move files out of directory because otherwise conversion to
        # Pascal VOC fails
        train_test_dir_path = hive_dir.parent
        if (hive_dir / 'train.txt').exists():
            (hive_dir / 'train.txt').rename(train_test_dir_path / 'train.txt')
            (hive_dir / 'test.txt').rename(train_test_dir_path / 'test.txt')
        if (hive_dir / 'all.txt').exists():
            (hive_dir / 'all.txt').rename(train_test_dir_path / 'all.txt')

        # Convert Yolo to Pascal VOC
        subprocess.run(
            f'python Yolo2Pascal-annotation-conversion/yolo2pascal/yolo2voc.py {hive_dir.resolve()}',
            shell=True,
            stdout=subprocess.DEVNULL
        )

        # Read train.txt and test.txt to determine which images are used
        # for training and testing
        with open(train_test_dir_path / 'train.txt') as f:
            train_files = {Path(row.strip()).name for row in f}
        with open(train_test_dir_path / 'test.txt') as f:
            test_files = {Path(row.strip()).name for row in f}

        # Read and save image names & annotations
        for file_path in hive_dir.iterdir():
            hive_name = hive_zips[hive_zip.name]
            file_name_wo_sfx = file_path.with_suffix('').name

            # For external datasets, hive is unknown
            if hive_name is None:
                key = f'unknown/{file_name_wo_sfx}'
            else:
                key = f'{hive_name}/{file_name_wo_sfx}'

            if hive_name == 'Doettingen_Hive1':
                annotators = 'AKS'
            elif hive_name == 'UnitedQueens':
                annotators = 'DN'
            else:
                annotators = 'piperod'

            extension = file_path.suffix
            if key not in annotations:
                annotations[key] = {
                    'annotations': {annotators: []},
                    'has_image': False
                }
            if extension in ['.jpg', '.png']:
                if file_path.name in train_files:
                    status = 'train'
                else:
                    status = 'test'

                # remove whitespaces (e.g. in united queens images)
                frame_name = file_path.name.replace(' ', '_')

                annotations[key]['has_image'] = True
                annotations[key]['hive'] = hive_name
                annotations[key]['status'] = status
                annotations[key]['frame'] = frame_name

                if hive_name is None:
                    blob_name = f'unknown_hive/pollen/{frame_name}'
                else:
                    blob_name = f'{hive_name}/pollen/{frame_name}'

                im = cv2.imread(str(file_path))
                height, width, channels = im.shape

                annotations[key]['width'] = width
                annotations[key]['height'] = height
                annotations[key]['channels'] = channels

                #print(f'Uploading {blob_name}')
                #upload_to_azure(file_path, blob_name)

            elif extension == '.xml':
                etree = ET.parse(file_path)
                root = etree.getroot()

                objects = root.findall('object')
                for object in objects:
                    box = object.find('bndbox')
                    xmin = int(box.find('xmin').text)
                    ymin = int(box.find('ymin').text)
                    xmax = int(box.find('xmax').text)
                    ymax = int(box.find('ymax').text)

                    annotations[key]['annotations'][annotators].append((xmin, ymin, xmax, ymax))

print('Original frame count:', len(annotations))
print('Missing images:', {k: v for k, v in annotations.items() if not v['has_image']})

# Ignore annotations where image is missing (e.g. folders, classes.txt, ...)
pickle_data = {k: v for k, v in annotations.items() if v['has_image']}
print('Cleaned frame count:', len(pickle_data))

print('Preview:', list(pickle_data.items())[:3])

with open('../resources/pollen_annotations.pickle', 'wb') as f:
    pickle.dump(pickle_data, f)
