import requests
from django.conf import settings

def buildFilters(allowed_ip, denied_ip, denied_login_methods, denied_patterns, allowed_patterns,
                max_upload_file_size, denied_protocols):
    filters = {"max_upload_file_size":max_upload_file_size}
    if allowed_ip:
        if len(allowed_ip) == 1 and not allowed_ip[0]:
            filters.update({'allowed_ip':[]})
        else:
            filters.update({'allowed_ip':allowed_ip})
    if denied_ip:
        if len(denied_ip) == 1 and not denied_ip[0]:
            filters.update({'denied_ip':[]})
        else:
            filters.update({'denied_ip':denied_ip})
    if denied_login_methods:
        if len(denied_login_methods) == 1 and not denied_login_methods[0]:
            filters.update({'denied_login_methods':[]})
        else:
            filters.update({'denied_login_methods':denied_login_methods})
    if denied_protocols:
        if len(denied_protocols) == 1 and not denied_protocols[0]:
            filters.update({'denied_protocols':[]})
        else:
            filters.update({'denied_protocols':denied_protocols})
    patterns_filter = []
    patterns_denied = []
    patterns_allowed = []
    if denied_patterns:
        for e in denied_patterns:
            if '::' in e:
                directory = None
                values = []
                for value in e.split('::'):
                    if directory is None:
                        directory = value
                    else:
                        values = [v.strip() for v in value.split(',') if v.strip()]
                if directory:
                    patterns_denied.append({'path':directory, 'denied_patterns':values,
                                            'allowed_patterns':[]})
    if allowed_patterns:
        for e in allowed_patterns:
            if '::' in e:
                directory = None
                values = []
                for value in e.split('::'):
                    if directory is None:
                        directory = value
                    else:
                        values = [v.strip() for v in value.split(',') if v.strip()]
                if directory:
                    patterns_allowed.append({'path':directory, 'allowed_patterns':values,
                                            'denied_patterns':[]})
    if patterns_allowed and patterns_denied:
        for allowed in patterns_allowed:
            for denied in patterns_denied:
                if allowed.get('path') == denied.get('path'):
                    allowed.update({'denied_patterns':denied.get('denied_patterns')})
            patterns_filter.append(allowed)
        for denied in patterns_denied:
            found = False
            for allowed in patterns_allowed:
                if allowed.get('path') == denied.get('path'):
                    found = True
            if not found:
                patterns_filter.append(denied)
    elif patterns_allowed:
        patterns_filter = patterns_allowed
    elif patterns_denied:
        patterns_filter = patterns_denied
    if allowed_patterns or denied_patterns:
        filters.update({'file_patterns':patterns_filter})
    return filters


def buildFsConfig(fs_provider, s3_bucket, s3_region, s3_access_key, s3_access_secret, s3_endpoint,
                s3_storage_class, s3_key_prefix, gcs_bucket, gcs_key_prefix, gcs_storage_class,
                gcs_credentials_file, gcs_automatic_credentials, s3_upload_part_size, s3_upload_concurrency,
                az_container, az_account_name, az_account_key, az_sas_url, az_endpoint, az_upload_part_size,
                az_upload_concurrency, az_key_prefix, az_use_emulator, az_access_tier, crypto_passphrase,
                sftp_endpoint, sftp_username, sftp_password, sftp_private_key_path, sftp_fingerprints, sftp_prefix):
    fs_config = {'provider':0}
    if fs_provider == 'S3':
        secret = {}
        if s3_access_secret:
            secret.update({"status":"Plain", "payload":s3_access_secret})
        s3config = {'bucket':s3_bucket, 'region':s3_region, 'access_key':s3_access_key, 'access_secret':
                secret, 'endpoint':s3_endpoint, 'storage_class':s3_storage_class, 'key_prefix':
                s3_key_prefix, 'upload_part_size':s3_upload_part_size, 'upload_concurrency':s3_upload_concurrency}
        fs_config.update({'provider':1, 's3config':s3config})
    elif fs_provider == 'GCS':
        gcsconfig = {'bucket':gcs_bucket, 'key_prefix':gcs_key_prefix, 'storage_class':gcs_storage_class,
                    'credentials':{}}
        if gcs_automatic_credentials == "automatic":
            gcsconfig.update({'automatic_credentials':1})
        else:
            gcsconfig.update({'automatic_credentials':0})
        if gcs_credentials_file:
            with open(gcs_credentials_file) as creds:
                secret = {"status":"Plain", "payload":creds.read()}
                gcsconfig.update({'credentials':secret, 'automatic_credentials':0})
        fs_config.update({'provider':2, 'gcsconfig':gcsconfig})
    elif fs_provider == "AzureBlob":
        secret = {}
        if az_account_key:
            secret.update({"status":"Plain", "payload":az_account_key})
        azureconfig = {'container':az_container, 'account_name':az_account_name, 'account_key':secret,
                    'sas_url':az_sas_url, 'endpoint':az_endpoint, 'upload_part_size':az_upload_part_size,
                    'upload_concurrency':az_upload_concurrency, 'key_prefix':az_key_prefix, 'use_emulator':
                    az_use_emulator, 'access_tier':az_access_tier}
        fs_config.update({'provider':3, 'azblobconfig':azureconfig})
    elif fs_provider == 'Crypto':
        cryptoconfig = {'passphrase':{'status':'Plain', 'payload':crypto_passphrase}}
        fs_config.update({'provider':4, 'cryptconfig':cryptoconfig})
    elif fs_provider == 'SFTP':
        sftpconfig = {'endpoint':sftp_endpoint, 'username':sftp_username, 'fingerprints':sftp_fingerprints,
                    'prefix':sftp_prefix}
        if sftp_password:
            pwd = {'status':'Plain', 'payload':sftp_password}
            sftpconfig.update({'password':pwd})
        if sftp_private_key_path:
            with open(sftp_private_key_path) as pkey:
                key = {'status':'Plain', 'payload':pkey.read()}
                sftpconfig.update({'private_key':key})
        fs_config.update({'provider':5, 'sftpconfig':sftpconfig})
    return fs_config


def buildUserObject(user_id=0, username='', password='', public_keys=[], home_dir='', uid=0, gid=0,
                max_sessions=0, quota_size=0, quota_files=0, permissions={}, upload_bandwidth=0, download_bandwidth=0,
                status=1, expiration_date=0, allowed_ip=[], denied_ip=[], fs_provider='local', s3_bucket='',
                s3_region='', s3_access_key='', s3_access_secret='', s3_endpoint='', s3_storage_class='',
                s3_key_prefix='', gcs_bucket='', gcs_key_prefix='', gcs_storage_class='', gcs_credentials_file='',
                gcs_automatic_credentials='automatic', denied_login_methods=[], virtual_folders=[],
                denied_patterns=[], allowed_patterns=[], s3_upload_part_size=0, s3_upload_concurrency=0,
                max_upload_file_size=0, denied_protocols=[], az_container='', az_account_name='', az_account_key='',
                az_sas_url='', az_endpoint='', az_upload_part_size=0, az_upload_concurrency=0, az_key_prefix='',
                az_use_emulator=False, az_access_tier='', additional_info='', crypto_passphrase='', sftp_endpoint='',
                sftp_username='', sftp_password='', sftp_private_key_path='', sftp_fingerprints=[], sftp_prefix=''):
    user = {'id':user_id, 'username':username, 'uid':uid, 'gid':gid,
        'max_sessions':max_sessions, 'quota_size':quota_size, 'quota_files':quota_files,
        'upload_bandwidth':upload_bandwidth, 'download_bandwidth':download_bandwidth,
        'status':status, 'expiration_date':expiration_date, 'additional_info':additional_info}
    if password is not None:
        user.update({'password':password})
    if public_keys:
        if len(public_keys) == 1 and not public_keys[0]:
            user.update({'public_keys':[]})
        else:
            user.update({'public_keys':public_keys})
    if home_dir:
        user.update({'home_dir':home_dir})
    if permissions:
        user.update({'permissions':permissions})
    # if virtual_folders:
    #     user.update({'virtual_folders':self.buildVirtualFolders(virtual_folders)})

    user.update({'filters':buildFilters(allowed_ip, denied_ip, denied_login_methods, denied_patterns,
                                                allowed_patterns, max_upload_file_size, denied_protocols)})
    user.update({'filesystem':buildFsConfig(fs_provider, s3_bucket, s3_region, s3_access_key, s3_access_secret,
                                                s3_endpoint, s3_storage_class, s3_key_prefix, gcs_bucket,
                                                gcs_key_prefix, gcs_storage_class, gcs_credentials_file,
                                                gcs_automatic_credentials, s3_upload_part_size, s3_upload_concurrency,
                                                az_container, az_account_name, az_account_key, az_sas_url,
                                                az_endpoint, az_upload_part_size, az_upload_concurrency, az_key_prefix,
                                                az_use_emulator, az_access_tier, crypto_passphrase, sftp_endpoint,
                                                sftp_username, sftp_password, sftp_private_key_path,
                                                sftp_fingerprints, sftp_prefix)})
    return user




def utilAddFTPUser(username, userPassword, adminPassword):
    header = getAuthHeader("admin", adminPassword)
    data = buildUserObject(
        username=username,
        password=userPassword,
        home_dir="/srv/sftpgo/data/admin/",
        fs_provider="AzureBlob",
        az_account_key=settings.STORAGE_KEY,
        az_container="videos",
        az_account_name="beelivingsensor",
        az_access_tier="Hot",                 
        permissions={"/": ["*"],}
    )
    return requests.post("http://ftp-sftpgo/api/v2/users", json=data, verify=False, headers=header)



def getAuthHeader(username, password):
    auth = requests.auth.HTTPBasicAuth(username, password)
    r = requests.get("http://ftp-sftpgo/api/v2/token", auth=auth, verify=False)

    return {"Authorization": "Bearer " + r.json()["access_token"]}

