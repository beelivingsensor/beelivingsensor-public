from datetime import datetime

TIMESTAMP_FORMAT = '%Y%m%d-%H-%M-%S-%f'


def get_timestamp():
    """Get the current timestamp."""
    return str(datetime.now().strftime(TIMESTAMP_FORMAT))
