# Merges the following two pickles:
# - bee_annotations_AKS.pickle
# - bee_annotations_GP.pickle

import pickle

with open('../resources/bee_annotations_GP.pickle', 'rb') as f:
    GP_annotations = pickle.load(f)

with open('../resources/bee_annotations_AKS.pickle', 'rb') as f:
    AKS_annotations = pickle.load(f)

duplicate_frames = {}
annotations = {}

# Add annotations of GP (and remove duplicate images)
# sort to get a reproducible result when removing images on Azure
for frame_path in sorted(GP_annotations.keys()):
    hive_name, status, frame_name = frame_path.parts

    if hive_name == 'Chueried_Hempbox_rotated':
        hive_name = 'Chueried_Hempbox'

    name = f'{hive_name}/{frame_name}'
    if name in annotations:
        old_status = annotations[name]['status']

        # prioritizing validate images
        if status == 'validate':
            duplicate_frames[name] = old_status
            annotations[name]['status'] = 'validate'
        else:
            duplicate_frames[name] = status
    else:
        annotations[name] = {
            'hive': hive_name,
            'status': status,
            'frame': frame_name,
            'annotations': {
                'GP': GP_annotations[frame_path]['annotations']
            },
            'width': GP_annotations[frame_path]['width'],
            'height': GP_annotations[frame_path]['height'],
            'depth': GP_annotations[frame_path]['depth'],
        }

# Add annotations of AKS
for frame_path in AKS_annotations:
    name = str(frame_path)
    if name in annotations:
        annotations[name]['annotations']['AKS'] = AKS_annotations[frame_path]['annotations']
    else:
        print('Missing: ', name)

# Create pickle of merged annotations
with open('../resources/bee_annotations.pickle', 'wb') as f:
    pickle.dump(annotations, f)


# just for printing information
for frame in annotations:
    if len(annotations[frame]['annotations']) == 2:
        if len(annotations[frame]['annotations']['AKS']) != len(annotations[frame]['annotations']['GP']):
            print('Number of annotations differs:', annotations[frame])
    else:
        print('Missing image:', annotations[frame])
