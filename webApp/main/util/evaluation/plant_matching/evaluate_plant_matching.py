import json
from csv import DictReader

import requests
from matplotlib import pyplot as plt

import logging


logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger(__name__)


class PlantMatchingEvaluator:

    samples = [
        ('gold1.csv', 'pred1.json', '2021-05-04T12:00:00+00:00'),
        ('gold2.csv', 'pred2.json', '2021-06-01T12:00:00+00:00')
    ]

    min_score = 27

    @classmethod
    def save_plant_matching_results(cls, fname, time):
        print(f'Run plant matching algorithm for {fname}...')

        params = {
            'x': '8.254988014632545',
            'y': '47.560816770598684',
            'time': time,
            'radius': 3,
            'days': 7
        }

        response = requests.get(
            'https://beelivingsensor.eu/api/plants/match/',
            params=params
        )

        with open(fname, 'w') as f:
            json.dump(response.json(), f, indent=3)

    @classmethod
    def save_pred_results(cls):
        for _, pred, time in cls.samples:
            cls.save_plant_matching_results(pred, time)

    @classmethod
    def evaluate_pred(cls, gold_file_path, pred_file_path):
        gold_data = cls.get_gold_plant_types(gold_file_path)
        pred_data = cls.get_pred_plant_types(pred_file_path)

        recall = cls.compute_recall(gold_data, pred_data)
        precision = cls.compute_precision(gold_data, pred_data)
        f1 = 2*((precision*recall)/(precision + recall))

        return recall, precision, f1

    @classmethod
    def compute_recall(cls, gold_data, pred_data):
        gold_families, gold_genera, gold_species, gold_plant_types = gold_data
        pred_families, pred_genera, pred_species, pred_plant_types = pred_data

        tp = 0
        fn = 0
        for g_family in gold_families:
            if g_family in pred_families:
                tp += 1
            else:
                logger.debug(f'Missed by plant-matching algo: Family {g_family}')
                fn += 1

        for g_genus in gold_genera:
            if g_genus in pred_genera:
                tp += 1
            else:
                logger.debug(f'Missed by plant-matching algo: Genus {g_genus}')
                fn += 1

        for g_spec in gold_species:
            if g_spec in pred_species:
                tp += 1
            else:
                logger.debug(f'Missed by plant-matching algo: Species {g_spec}')
                fn += 1

        found_by_pollen_analysis = tp + fn
        logger.info(f'Found by pollen analysis: {found_by_pollen_analysis}')
        logger.info(f'TP: {tp}, FN: {fn}')

        return tp / found_by_pollen_analysis

    @classmethod
    def compute_precision(cls, gold_data, pred_data):
        gold_families, gold_genera, gold_species, gold_plant_types = gold_data
        pred_families, pred_genera, pred_species, pred_plant_types = pred_data
        tp = 0
        fp = 0
        for p_plant_type in pred_plant_types:
            p_family, p_genus, p_spec = p_plant_type

            if (p_family in gold_families) or (p_genus in gold_genera) or (p_spec in gold_species):
                tp += 1
                logger.debug(f'Found in pollen analysis: {p_plant_type}')
            else:
                logger.debug(f'Not found in pollen analysis: {p_plant_type}')
                fp += 1

        found_by_algo = tp + fp
        logger.info(f'Found by algo: {found_by_algo}')
        logger.info(f'TP: {tp}, FP: {fp}')

        return tp / found_by_algo

    @classmethod
    def get_gold_plant_types(cls, gold_file_path):
        families = set()
        genera = set()
        species = set()
        plant_types = set()

        with open(gold_file_path) as f:
            reader = DictReader(f)
            for row in reader:
                family = row['Familie']
                genus = row['Gattung']
                spec = row['Art']

                if family == 'Unknown':
                    family = None
                if genus == 'Unknown':
                    genus = None
                if spec == 'Unknown':
                    spec = None

                if spec:
                    species.add(spec)
                elif genus:
                    genera.add(genus)
                elif family:
                    families.add(family)

                if spec or genus or family:
                    plant_types.add((family, genus, spec))

        logger.info(f'Plant types: {len(plant_types)}')
        logger.info(f'Species: {len(species)}')
        logger.info(f'Genera: {len(genera)}')
        logger.info(f'Families: {len(families)}')

        return families, genera, species, plant_types

    @classmethod
    def get_pred_plant_types(cls, pred_file_path):
        families = set()
        genera = set()
        species = set()
        plant_types = set()
        with open(pred_file_path) as f:
            results = json.load(f)
            for rank in results['data']['ranking']:
                if rank['score'] >= cls.min_score:
                    for plant in rank['plants']:
                        family = plant['family']
                        genus = plant['genus']
                        spec = plant['species']

                        families.add(family)
                        genera.add(genus)
                        species.add(spec)

                        plant_types.add((family, genus, spec))

        return families, genera, species, plant_types

    @classmethod
    def evaluate_all(cls):
        for i, (gold, pred, _) in enumerate(cls.samples, start=1):
            min_scores = []
            recalls = []
            precisions = []
            f1_scores = []

            best_min_score = 0
            best_f1_score = 0
            for min_score in range(29, 9, -1):
                logger.info('------------------------------------------------')
                logger.info(f'Files used for evaluation: {gold}, {pred}')
                logger.info(f'Min. score: {min_score}')

                cls.min_score = min_score
                recall, precision, f1 = cls.evaluate_pred(gold, pred)
                min_scores.append(min_score)
                recalls.append(recall)
                precisions.append(precision)
                f1_scores.append(f1)

                logger.info(f'Recall: {recall}')
                logger.info(f'Precision: {precision}')
                logger.info(f'F1 score: {f1}')

                if f1 > best_f1_score:
                    best_f1_score = f1
                    best_min_score = min_score

            logger.info('****************************************************')
            logger.info(f'Best min. score: {best_min_score} ({best_f1_score})')
            logger.info('****************************************************')

            plt.figure(figsize=(10, 5), dpi=200)
            plt.plot(min_scores, recalls, label='Recall')
            plt.plot(min_scores, precisions, label='Precision')
            plt.plot(min_scores, f1_scores, label='F1-Score')
            plt.xlabel('Min. score')
            plt.ylabel('Metric Value')
            plt.xticks(min_scores)
            plt.title(f'Pollen sample {i}')
            plt.vlines(best_min_score,
                       0,
                       1,
                       colors='r',
                       label='Best min. score',
                       linestyles='dotted')
            plt.legend()
            plt.show()


def main():
    evaluator = PlantMatchingEvaluator()
    # evaluator.save_pred_results()
    evaluator.evaluate_all()


if __name__ == '__main__':
    main()
