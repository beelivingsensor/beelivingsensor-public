# Helper functions for azure blob storage

import os
from pathlib import Path

from azure.storage.blob import ContainerClient, BlobClient, BlobServiceClient


def delete_blobs(container_name, prefix):
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')
        return

    container_client = ContainerClient.from_connection_string(
        conn_str=connect_str,
        container_name=container_name,
    )

    blobs = container_client.list_blobs(name_starts_with=prefix)
    for blob in blobs:
        print(f'Delete blob {blob.name}')
        container_client.delete_blob(blob.name)


def example_delete_blobs():
    """Example that shows how to use function delete_blobs."""
    container_name = 'frames'
    prefix = 'UnitedQueens/pollen'
    delete_blobs(container_name, prefix)


def upload_blob(file_path, container_name, blob_name):
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')
        return

    blob_client = BlobClient.from_connection_string(
        conn_str=connect_str,
        container_name=container_name,
        blob_name=blob_name)

    with open(file_path, 'rb') as data:
        blob_client.upload_blob(data)


def list_blobs(container, prefix):
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')
        return

    container_client = ContainerClient.from_connection_string(
        conn_str=connect_str,
        container_name=container,
    )
    return list(container_client.list_blobs(name_starts_with=prefix))


def download_blob(container, blob, local_path):
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')
        return
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)

    blob_client = blob_service_client.get_blob_client(
        container=container,
        blob=blob)

    Path(local_path).parent.mkdir(parents=True, exist_ok=True)

    with open(local_path, 'wb') as f:
        f.write(blob_client.download_blob().readall())


def get_latest_snapshot_url():
    connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
    if not connect_str:
        print('Azure storage connection string not exported!')
        return

    container_client = ContainerClient.from_connection_string(
        conn_str=connect_str,
        container_name='public',
    )
    blobs = list(container_client.list_blobs(name_starts_with='annotations'))
    if blobs:
        blob_name = blobs[0].name
        return f'https://beelivingsensor.blob.core.windows.net/public/{blob_name}'

    return None
