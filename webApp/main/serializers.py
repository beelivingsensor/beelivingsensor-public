from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from main.models import Annotation, AnnotationType, Image, ImageSet


class MeasurementSerializer(serializers.Serializer):
    source = serializers.CharField()
    param = serializers.CharField()
    time = serializers.DateTimeField()
    value = serializers.FloatField()
    unit = serializers.CharField()


def serialize_annotations(user_annotations):
    serialized = {'annotations': []}

    for user_annotation in user_annotations:
        annotation_type = user_annotation.annotation_set.annotation_type
        image = user_annotation.annotation_set.image
        data = {
            'annotation_type': {
                'id': annotation_type.pk,
                'name': annotation_type.name,
                'vector_type': annotation_type.vector_type,
                'node_count': annotation_type.node_count,
                'enable_concealed': True,
                'enable_blurred': True,
            },
            'id': user_annotation.pk,
            'vector': user_annotation.vector,
            'image': {
                'id': image.pk,
                'name': image.name,
            },
        }
        serialized['annotations'].append(data)

    return serialized


class AnnotationTypeSerializer(ModelSerializer):
    class Meta:
        model = AnnotationType
        fields = (
            'id',
            'name',
            'vector_type',
            'node_count',
            'enable_concealed',
            'enable_blurred',
        )


class ImageSerializer(ModelSerializer):
    class Meta:
        model = Image
        fields = (
            'id',
            'name',
        )


class AnnotationSerializer(ModelSerializer):

    class Meta:
        model = Annotation
        fields = (
            'annotation_type',
            'id',
            'vector',
            'image',
        )

    annotation_type = AnnotationTypeSerializer(read_only=True)
    image = ImageSerializer(read_only=True)


class ImageSetSerializer(ModelSerializer):
    class Meta:
        model = ImageSet
        fields = (
            'id',
            'name',
            'location',
            'description',
            'images',
        )

    images = ImageSerializer(many=True)