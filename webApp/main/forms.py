from django import forms
from django.contrib.auth.models import Group
from django.contrib.gis.forms import OSMWidget
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django_registration.forms import RegistrationForm

from main.management.commands import initGroups
from main.models import Hive, Video, Apiary, AnnotationAccuracy, Task, \
    AIModel, SensorType, HiveSensor, SensorMeasurement, HiveSensorDevice, User, \
    AnnotationType, Image


class BootstrapForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if hasattr(visible.field.widget, 'input_type'):
                widget_type = visible.field.widget.input_type

                if widget_type == 'text':
                    visible.field.widget.attrs['class'] = 'form-control'
                elif widget_type == 'checkbox':
                    visible.field.widget.attrs['class'] = 'form-check-input'
                elif widget_type == 'select':
                    visible.field.widget.attrs['class'] = 'form-select'
                else:
                    visible.field.widget.attrs['class'] = 'form-control'
            else:
                visible.field.widget.attrs['class'] = 'form-control'


class BootstrapRawForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if hasattr(visible.field.widget, 'input_type'):
                widget_type = visible.field.widget.input_type

                if widget_type == 'text':
                    visible.field.widget.attrs['class'] = 'form-control'
                elif widget_type == 'checkbox':
                    visible.field.widget.attrs['class'] = 'form-check-input'
                elif widget_type == 'select':
                    visible.field.widget.attrs['class'] = 'form-select'
                else:
                    visible.field.widget.attrs['class'] = 'form-control'
            else:
                visible.field.widget.attrs['class'] = 'form-control'



class ApiaryForm(BootstrapForm):
    class Meta:
        model = Apiary
        exclude = ['creator', 'address', 'elevation', "managers"]
        widgets = {
            'location': OSMWidget(attrs={
                'map_width': 1000,
                'map_height': 600,
                'default_lat': 46.7985,
                'default_lon': 8.2318,
                'default_zoom': 8
                },
            )
        }

class AddManagerApiaryForm(forms.Form):
    email = forms.EmailField()

class BeehiveForm(BootstrapForm):
    class Meta:
        model = Hive
        exclude = ['location', 'creator', "managers", 'apiary', 'similar_beehive', 'google_drive_name', 'retrain']


class BeehiveImageForm(forms.Form):
    file = forms.ImageField()


class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['fileName']


class VideoUpdateForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = []


class SignUpForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ('username', 'password1', 'password2')


class AnnotationAccuracyForm(BootstrapForm):
    class Meta:
        model = AnnotationAccuracy
        fields = ['accuracy']


class TaskForm(BootstrapForm):

    class Meta:
        model = Task
        fields = ['annotation_type']


class AIModelForm(BootstrapForm):
    class Meta:
        model = AIModel
        fields = ['description', 'path']


class SensorTypeForm(BootstrapForm):
    class Meta:
        model = SensorType
        fields = '__all__'


class HiveSensorDeviceForm(BootstrapForm):

    BRAND_CHOICES = [
        ('', '----------'),
        ('custom', 'custom'),
    ] + [(brand, brand) for brand in HiveSensorDevice.COMMON_BRANDS]

    brand_choices = forms.ChoiceField(required=True, choices=BRAND_CHOICES)

    field_order = [
        'hive', 'sn', 'brand_choices', 'brand', 'model', 'description']

    class Meta:
        model = HiveSensorDevice
        exclude = ['hive']

    def clean(self):
        cleaned_data = super().clean()
        brand = cleaned_data.get('brand')
        brand_choices = cleaned_data.get('brand_choices')

        if brand_choices == 'custom' and not brand:
            raise ValidationError('Specify custom brand')


class HiveSensorForm(BootstrapForm):

    class Meta:
        model = HiveSensor
        exclude = ['device']


class SensorMeasurementForm(BootstrapForm):
    class Meta:
        model = SensorMeasurement
        exclude = ['sensor']


class SensorBulkUploadForm(forms.Form):
    file = forms.FileField()


class NewFTPUserForm(forms.Form):
    username = forms.CharField()
    userPassword = forms.CharField(widget=forms.PasswordInput())
    confirmPassword = forms.CharField(widget=forms.PasswordInput())
    adminPassword = forms.CharField(widget=forms.PasswordInput())


class ChangeAdminPasswordForm(forms.Form):
    oldPassword = forms.CharField(widget=forms.PasswordInput())
    newPassword = forms.CharField(widget=forms.PasswordInput())
    confirmPassword = forms.CharField(widget=forms.PasswordInput())


class UserRegistrationForm(RegistrationForm):
    """
    Subclasses should feel free to add any additional validation they
    need, but should take care when overriding ``save()`` to respect
    the ``commit=False`` argument, as several registration workflows
    will make use of it to create inactive user accounts.
    """
    class Meta(RegistrationForm.Meta):
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
            "first_name",
            "last_name"
        ]

    def save(self, commit=True):
        user = super().save(commit=False)
        group, _ = Group.objects.get_or_create(name=initGroups.DEFAULT_GROUP)
        user.save()
        user.groups.add(group)
        user.save()
        return user


class UserForm(BootstrapForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class ImageListFilterForm(BootstrapRawForm):
    upload_modus_choices = [
        ('', '-------------'),
        ('image','image'),
        ('video', 'video')
    ]
    uploaded_modus = forms.ChoiceField(choices=upload_modus_choices, initial='', required=False)

    status_choices = [
        ('', '-------------'),
        ('unclassified', 'unclassified'),
        ('train', 'train'),
        ('test', 'test'),
        ('validate', 'validate')
    ]

    status = forms.ChoiceField(choices=status_choices, initial='', required=False)

    type_choices = [
        ('', '-------------'),
        ('hive', 'hive'),
        ('bee', 'bee'),
        ('unclassified', 'unclassified'),
    ]

    type = forms.ChoiceField(choices=type_choices, initial='', required=False)


class MLDataFilterForm(BootstrapRawForm):
    model_choices = [
        ('', '-------------'),
        ('bee','bee'),
        ('pollen', 'pollen')
    ]
    model = forms.ChoiceField(choices=model_choices, initial='', required=False)

    status_choices = [
        ('unclassified', 'unclassified'),
        ('train', 'train'),
        ('test', 'test'),
        ('validate', 'validate'),
    ]
    status = forms.MultipleChoiceField(
        choices=status_choices,
        initial=['unclassified', 'train', 'test'], help_text='(hold ctrl to select multiple)',
        required=False
    )


class TaskImageListFilterForm(BootstrapRawForm):
    apiaries = forms.ModelMultipleChoiceField(queryset=Apiary.objects.all().order_by('name'), required=False)
    beehives = forms.ModelMultipleChoiceField(queryset=Hive.objects.all().order_by('apiary__name', 'name'), required=False)
    videos = forms.ModelMultipleChoiceField(queryset=Video.objects.all().order_by('beehive__apiary__name', 'beehive__name', 'fileName'), required=False)
    image_id = forms.IntegerField(required=False)


class AnnotationTypeCreationForm(forms.ModelForm):
    class Meta:
        model = AnnotationType
        fields = [
            'name',
            'active',
            'node_count',
            'vector_type',
            'enable_concealed',
            'enable_blurred',
        ]


class AnnotationTypeEditForm(forms.ModelForm):
    class Meta:
        model = AnnotationType
        fields = [
            'name',
            'active',
            'enable_concealed',
            'enable_blurred',
        ]