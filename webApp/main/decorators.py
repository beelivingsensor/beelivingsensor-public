from django.shortcuts import resolve_url
from django.contrib.auth.views import redirect_to_login


from functools import wraps

def user_is_manager(model, pkArg="pk"):
    """
    Decorator that checks whether the model instace, a function is operating on, is under the management of the user used in the function. 
    
    model = reference whose primary key(pk) is being handed over to the function.
    pkArg = argument name that holds the primary key value of the model mentioned in "model" argument

    Example, User John is calling Apiary #42 detail view. This decorator checks whether the Apiary #42 is managed by the User John.
    """

    def decorator(viewFunc):

        @wraps(viewFunc)
        def wrap(request, *args, **kwargs):
            modelInstance = model.objects.get(pk=kwargs[pkArg])
            if modelInstance.isManagedBy(request.user) or request.user.is_staff:
                return viewFunc(request, *args, **kwargs)
            else:
                resolved_login_url = resolve_url('/admin/login')
                return redirect_to_login(
                    request.get_full_path(), resolved_login_url)

        return wrap

    return decorator