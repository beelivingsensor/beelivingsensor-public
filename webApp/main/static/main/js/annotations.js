/*jshint esversion: 6 */

globals = {
  // Element: image
  image: {},
  imageScaleWidth: 1,
  imageScaleHeight: 1,
  editedAnnotationsId: undefined,
  // Element: 'You are currenly editing an annotation'-warning element when editing
  editActiveContainer: {},
  restoreSelection: undefined,
  restoreSelectionVectorType: 1,
  restoreSelectionNodeCount: 0,
  moveSelectionStepSize: 2,
  // Boolean: Whether annotations should be shown or not
  drawAnnotations: true,
  mouseUpX: undefined,
  mouseUpY: undefined,
  mouseClickX: undefined,
  mouseClickY: undefined,
  mouseDownX: undefined,
  mouseDownY: undefined,
  currentAnnotations: undefined,
  allAnnotations: undefined,
  stdColor: '#6beb34',
  mutColor: '#6beb34'
};

/**
 * Calculate the correct imageScale value.
 */
function calculateImageScale() {
  globals.imageScaleWidth = globals.image.get(0).naturalWidth / globals.image.width();
  globals.imageScaleHeight = globals.image.get(0).naturalHeight / globals.image.height();
}

function initialize_annotate_view() {
  const API_ANNOTATIONS_BASE_URL = '/annotations/api/';
  const FEEDBACK_DISPLAY_TIME = 3000;
  const STATIC_ROOT = '/static/';

  let gCsrfToken;
  let gHeaders;
  let gHideFeedbackTimeout;
  let gImageId;
  let gImageSetId;
  let gMousepos;
  let gAnnotationType = -1;
  let gAnnotationCache = {};
  let gHighlightedAnnotation;

  // save the current annotations of the image, so we can draw and hide the
  let tool;

  function initTool() {
    setTool();
    tool.initSelection();
    loadAnnotateView(gImageId);
  }

  function setTool() {
    let selected_annotation = $('#annotation_type_id').children(':selected').data();
    let vector_type = selected_annotation.vectorType;
    let node_count = selected_annotation.nodeCount;
    let annotationTypeId = parseInt($('#annotation_type_id').children(':selected').val());
    if (tool && tool.annotationTypeId === annotationTypeId) {
      // Tool does not have to change
      return;
    }
    if (tool) {
      tool.resetSelection();
      tool.cancelSelection();
      tool.initSelection();
      tool.reset();
      delete tool;
    }
    $('#feedback_multiline_information').hide();
    switch (vector_type) {
      case 1: // Bounding Box
        // Remove unnecessary number fields
        for (let i = 3; $('#x' + i + 'Field').length; i++) {
          $('#x' + i + 'Box').remove();
          $('#y' + i + 'Box').remove();
        }
        tool = new BoundingBoxes(annotationTypeId);
        $('#image_canvas').hide();
        $('.hair').show();
        $('#image').css('cursor', 'none');
        $('.imgareaselect-outer').css('cursor', 'none');
        break;
      case 4: // Multiline, fallthrough
        $('#feedback_multiline_information').show();
      case 2: // Point, fallthrough
      case 3: // Line, fallthrough
      case 5: // Polygon
        $('#image_canvas').show().attr('width', Math.ceil($('#image').width())).attr('height', Math.ceil($('#image').height()));
        tool = new Canvas($('#image_canvas'), vector_type, node_count, annotationTypeId);
        $('.hair').hide();
        $('#image_canvas').css('cursor', 'crosshair');
        break;
      default:
        // Dummytool
        tool = {
          initSelection: function() {},
          resetSelection: function() {},
          restoreSelection: function() {},
          cancelSelection: function() {},
          reset: function() {},
          drawExistingAnnotations: function() {},
          handleEscape: function() {},
          handleMousemove: function() {},
          handleMouseDown: function() {},
          handleMouseUp: function() {},
          handleMouseClick: function() {},
          moveSelectionLeft: function() {},
          moveSelectionRight: function() {},
          moveSelectionUp: function() {},
          moveSelectionDown: function() {},
          decreaseSelectionSizeFromRight: function() {},
          decreaseSelectionSizeFromTop: function() {},
          increaseSelectionSizeRight: function() {},
          increaseSelectionSizeUp: function() {},
          setHighlightColor: function() {},
          unsetHighlightColor: function() {}
        };
    }
    if (globals.currentAnnotations) {
      tool.drawExistingAnnotations(globals.currentAnnotations);
    }
    console.log("Using tool " + tool.constructor.name);
  }

  /**
   * Create an annotation using the form data from the current page.
   * If an annotation is currently edited, an update is triggered instead.
   *
   * @param event
   * @param successCallback a function to be executed on success
   * @param markForRestore
   */
  function createAnnotation(event, successCallback, markForRestore, reload_list) {
    if (event !== undefined) {
      // triggered using an event handler
      event.preventDefault();
    }

    var annotationTypeId = parseInt($('#annotation_type_id').val());
    var vector = null;

    if (isNaN(annotationTypeId)) {
      displayFeedback($('#feedback_annotation_type_missing'));
      return;
    }


    vector = {};
    for (let i = 1; $('#x' + i + 'Field').length; i++) {
      vector["x" + i] = parseInt($('#x' + i + 'Field').val());
      vector["y" + i] = parseInt($('#y' + i + 'Field').val());
    }

    // Swap points if the second one is left of the first one
    // (e.g. user pulls left side of rectangle over right side of rectangle)
    if (Object.keys(vector).length == 4) {
      if (vector.x1 > vector.x2 || vector.x1 === vector.x2 && vector.y1 > vector.y2) {
        let tmp_x1 = vector.x2;
        let tmp_y1 = vector.y2;
        vector.x2 = vector.x1;
        vector.y2 = vector.y1;
        vector.x1 = tmp_x1;
        vector.y1 = tmp_y1;
      }
    }

    let selected_annotation = $('#annotation_type_id').children(':selected').data();
    let vector_type = selected_annotation.vectorType;
    let node_count = selected_annotation.nodeCount;
    if (!validate_vector(vector, vector_type, node_count)) {
      displayFeedback($('#feedback_annotation_invalid'));
      return;
    }

    if (markForRestore === true) {
      globals.restoreSelection = vector;
      globals.restoreSelectionVectorType = vector_type;
      globals.restoreSelectionNodeCount = node_count;
    }

    var action = 'create';
    var data = {
      annotation_type_id: annotationTypeId,
      image_id: gImageId,
      vector: vector
    };
    var editing = false;
    if (globals.editedAnnotationsId !== undefined) {
      // edit instead of create
      action = 'update';
      data.annotation_id = globals.editedAnnotationsId;
      editing = true;
    }

    $('.js_feedback').stop().hide();
    $('.annotate_button').prop('disabled', true);
    $.ajax(API_ANNOTATIONS_BASE_URL + 'annotation/' + action + '/', {
      type: 'POST',
      headers: gHeaders,
      dataType: 'json',
      data: JSON.stringify(data),
      success: function(data, textStatus, jqXHR) {
        if (jqXHR.status === 200) {
          if (editing) {
            if (data.detail === 'similar annotation exists.') {
              displayFeedback($('#feedback_annotation_exists_deleted'));
              $('#annotation_edit_button_' + globals.editedAnnotationsId).parent().parent(
                ).fadeOut().remove();
            } else {
              displayFeedback($('#feedback_annotation_updated'));
              displayExistingAnnotations(data.annotations);
              tool.drawExistingAnnotations(globals.currentAnnotations);
            }
          } else {
            displayFeedback($('#feedback_annotation_exists'));
          }
        } else if (jqXHR.status === 201) {
          displayFeedback($('#feedback_annotation_created'));
          displayExistingAnnotations(data.annotations);
        }
        // update current annotations
        globals.allAnnotations = data.annotations;
        globals.currentAnnotations = globals.allAnnotations.filter(function(e) {
          return e.annotation_type.id === gAnnotationType;
        });
        gAnnotationCache[gImageId] = globals.allAnnotations;

        tool.drawExistingAnnotations(globals.currentAnnotations);

        globals.editedAnnotationsId = undefined;
        $('.annotation').removeClass('alert-info');
        globals.editActiveContainer.hide();
        tool.resetSelection(true);

        if (typeof(successCallback) === "function") {
          successCallback();
        }
        $('.annotate_button').prop('disabled', false);
      },
      error: function() {
        $('.annotate_button').prop('disabled', false);
        displayFeedback($('#feedback_connection_error'));
      }
    });
  }

  /**
   * Delete an annotation.
   *
   * @param event
   * @param annotationId
   */
  function deleteAnnotation(event, annotationId) {
    if (globals.editedAnnotationsId === annotationId) {
      // stop editing
      tool.resetSelection(true);
    }

    if (event !== undefined) {
      event.preventDefault();
    }

    $('.js_feedback').stop().hide();
    var params = {
      annotation_id: annotationId
    };
    $.ajax(API_ANNOTATIONS_BASE_URL + 'annotation/delete/?' + $.param(params), {
      type: 'DELETE',
      headers: gHeaders,
      dataType: 'json',
      success: function(data) {
        globals.allAnnotations = data.annotations;
        globals.currentAnnotations = globals.allAnnotations.filter(function(e) {
          return e.annotation_type.id === gAnnotationType;
        });
        gAnnotationCache[gImageId] = globals.allAnnotations;
        // redraw the annotations
        tool.drawExistingAnnotations(globals.currentAnnotations);
        displayExistingAnnotations(globals.allAnnotations);
        displayFeedback($('#feedback_annotation_deleted'));
        globals.editedAnnotationsId = undefined;
      },
      error: function() {
        $('.annotate_button').prop('disabled', false);
        displayFeedback($('#feedback_connection_error'));
      }
    });
  }

  /**
   * Display  existing annotations on current page.
   *
   * @param annotations
   */
  function displayExistingAnnotations(annotations) {
    var existingAnnotations = $('#existing_annotations');
    var noAnnotations = $('#no_annotations');

    existingAnnotations.hide();

    if (annotations.length === 0) {
      noAnnotations.show();
      return;
    }

    noAnnotations.hide();
    existingAnnotations.html('');

    // display new annotations
    for (var i = 0; i < annotations.length; i++) {
      var annotation = annotations[i];

      var alertClass = '';
      if (globals.editedAnnotationsId === annotation.id) {
        alertClass = ' alert-info';
      }
      var newAnnotation = $(
        '<div id="annotation_' + annotation.id +
        '" class="annotation' + alertClass + '">');

      if (annotation.vector !== null) {
        annotation.content = '';
        for (let i = 1; annotation.vector.hasOwnProperty("x" + i); i++) {
          if (annotation.content !== '') {
            annotation.content += ' &bull; ';
          }
          if (i === 4) {
            // Show no more than three points
            annotation.content += '...';
            break;
          }
          annotation.content += 'x' + i + ': ' + annotation.vector["x" + i];
          annotation.content += ' &bull; ';
          annotation.content += 'y' + i + ': ' + annotation.vector["y" + i];
        }
      } else {
        annotation.content = 'not in image';
      }

      newAnnotation.append(annotation.annotation_type.name + ':');

      var annotationLinks = $('<div style="float: right;">');
      var editButton = $('<a href="#">' +
      '<img src="' + STATIC_ROOT + 'symbols/pencil.png" alt="edit" class="annotation_edit_button">' +
      '</a>');
      var deleteButton = $('<a href="/annotations/' + annotation.id + '/delete/">' +
      '<img src="' + STATIC_ROOT + 'symbols/bin.png" alt="delete" class="annotation_delete_button">' +
      '</a>');
      const annotationId = annotation.id;
      editButton.attr('id', 'annotation_edit_button_' + annotationId);
      editButton.click(function(event) {
        editAnnotation(event, this, annotationId);
      });
      editButton.data('annotationtypeid', annotation.annotation_type.id);
      editButton.data('annotationid', annotation.id);
      editButton.data('vector', annotation.vector);
      deleteButton.click(function(event) {
        deleteAnnotation(event, annotationId);
      });
      annotationLinks.append(' ');
      annotationLinks.append(editButton);
      annotationLinks.append(' ');
      annotationLinks.append(deleteButton);

      newAnnotation.append(annotationLinks);
      newAnnotation.append('<br>');
      newAnnotation.append(annotation.content);

      existingAnnotations.append(newAnnotation);
    }

    existingAnnotations.show();
  }

  /**
   * Highlight one annotation in a different color
   * @param annotationTypeId
   * @param annotationId
   */

  function handleMouseClick(e) {
    if (e && (e.target.id === 'image' || e.target.id === 'image_canvas')) {
      var position = globals.image.offset();
      globals.mouseClickX = Math.round((e.pageX - position.left));
      globals.mouseClickY = Math.round((e.pageY - position.top));
      tool.handleMouseClick(e);
    }

    // remove any existing highlight
    if (e.target.className !== 'annotation_edit_button') {
      $('.annotation').removeClass('alert-info');
      if (gHighlightedAnnotation) {
        tool.unsetHighlightColor(gHighlightedAnnotation, globals.currentAnnotations.filter(function(element) {
          return element.id === gHighlightedAnnotation;
        }));
        gHighlightedAnnotation = undefined;
      }
    } else {
      // when the click was on the annotation edit button corresponding to the
      // currently highlighted annotation, do not remove the blue highlight
      if (gHighlightedAnnotation && gHighlightedAnnotation !== $(e.target).parent().data('annotationid')) {
        tool.unsetHighlightColor(gHighlightedAnnotation, globals.currentAnnotations.filter(function(element) {
          return element.id === gHighlightedAnnotation;
        }));
        gHighlightedAnnotation = undefined;
      }
    }

    // create a new highlight if the click was on an annotation
    if (e.target.className === 'annotation') {
      let editButton = $(e.target).find('.annotation_edit_button').parent();
      $('#annotation_type_id').val(editButton.data('annotationtypeid'));
      handleAnnotationTypeChange();
      tool.setHighlightColor(editButton.data('annotationid'));
      gHighlightedAnnotation = editButton.data('annotationid');
      $(e.target).addClass('alert-info');
    }
  }

  /**
   * Display an image.
   *
   * @param imageId
   */
  function displayImage(imageId) {
    imageId = parseInt(imageId);
    let imageUrl = document.getElementById('image_url').innerHTML.replace(/&amp;/g, "&");

    var currentImage = globals.image;
    var newImage = $('<img>').data('imageid', imageId).attr('src', imageUrl);

    currentImage.attr('id', '');
    newImage.attr('id', 'image');
    gImageId = imageId;

    currentImage.replaceWith(newImage);
    globals.image = newImage;
    calculateImageScale();
    tool.initSelection();
    tool.resetSelection();
  }

  /**
   * Display a feedback element for a few seconds.
   *
   * @param elem
   */
  function displayFeedback(elem) {
    if (gHideFeedbackTimeout !== undefined) {
      clearTimeout(gHideFeedbackTimeout);
    }

    elem.show();

    gHideFeedbackTimeout = setTimeout(function() {
      elem.hide();
    }, FEEDBACK_DISPLAY_TIME);
  }

  /**
   * Edit an annotation.
   *
   * @param event
   * @param annotationElem the element which stores the edit button of the annotation
   * @param annotationId
   */
  function editAnnotation(event, annotationElem, annotationId) {
    annotationElem = $(annotationElem);
    let annotationTypeId = annotationElem.data('annotationtypeid');
    $('#annotation_type_id').val(annotationTypeId);
    handleAnnotationTypeChange();
    globals.editedAnnotationsId = annotationId;
    globals.editActiveContainer.show();

    if (event !== undefined) {
      // triggered using an event handler
      event.preventDefault();
    }
    $('.js_feedback').stop().hide();
    var params = {
      annotation_id: annotationId
    };

    var annotationData = annotationElem.data('vector');
    if (annotationData === undefined) {
      annotationData = annotationElem.data('escapedvector');
    }

    // highlight currently edited annotation
    $('.annotation').removeClass('alert-info');
    annotationElem.parent().parent().addClass('alert-info');

    if (annotationData === null) {
      return;
    }

    $('#annotation_type_id').val(annotationTypeId);
    tool.reloadSelection(annotationId, annotationData);
  }

  /**
   * Validate a vector.
   *
   * @param vector
   * @param vector_type
   * @param node_count
   */
  function validate_vector(vector, vector_type, node_count) {
    if (vector === null) {
      // not in image
      return true;
    }
    let len = Object.keys(vector).length;
    switch (vector_type) {
      case 1: // Ball (Boundingbox)
        return vector.x2 - vector.x1 >= 1 && vector.y2 - vector.y1 >= 1 && len === 4;
      case 2: // Point
        return vector.hasOwnProperty('x1') && vector.hasOwnProperty('y1') && len === 2 && !(vector.x1 === 0 && vector.y1 === 0);
      case 3: // Line
        return vector.x1 !== vector.x2 || vector.y1 !== vector.y2 && len === 4;
      case 4: // Multiline
        // a multiline should have at least two points
        if (len < 4) {
          return false;
        }
        for (let i = 1; i < len / 2 + 1; i++) {
          for (let j = 1; j < len / 2 + 1; j++) {
            if (i !== j && vector['x' + i] === vector['x' + j] && vector['y' + i] === vector['y' + j]) {
              return false;
            }
          }
        }
        return true;
      case 5: // Polygon
        if (len < 6) {
          // A polygon should have at least three points
          return false;
        }
        if (node_count !== 0 && node_count !== (len / 2)) {
          return false;
        }
        for (let i = 1; i <= len / 2; i++) {
          for (let j = 1; j <= len / 2; j++) {
            if (i !== j && vector["x" + i] === vector["x" + j] && vector["y" + i] === vector["y" + j]) {
              return false;
            }
          }
        }
        return true;
    }
    return false;
  }

  /**
   * Handle toggle of the draw annotations checkbox.
   *
   * @param event
   */
  function handleShowAnnotationsToggle(event) {
    globals.drawAnnotations = $('#draw_annotations').is(':checked');
    if (globals.drawAnnotations) {
      tool.drawExistingAnnotations(globals.currentAnnotations);
    } else {
      tool.clear();
    }
  }

  /**
   * Handle a selection using the mouse.
   *
   * @param event
   */
  function handleSelection(event) {
    calculateImageScale();
    var cH = $('#crosshair-h'), cV = $('#crosshair-v');
    var position = globals.image.offset();
    if (event.pageX > position.left &&
          event.pageX < position.left + globals.image.width() &&
          event.pageY > position.top &&
          event.pageY < position.top + globals.image.height()) {
      cH.show();
      cV.show();
      gMousepos.show();

      cH.css('top', event.pageY + 1);
      cV.css('left', event.pageX + 1);
      cV.css('height', globals.image.height() - 1);
      cV.css('margin-top', position.top);
      cH.css('width', globals.image.width() - 1);
      cH.css('margin-left', position.left);

      gMousepos.css({
        top: (event.pageY) + 'px',
        left: (event.pageX)  + 'px'
      }, 800);
      gMousepos.text(
        '(' + Math.round((event.pageX - position.left) * globals.imageScaleWidth) + ', ' +
        Math.round((event.pageY - position.top) * globals.imageScaleHeight) + ')');
      event.stopPropagation();
    }
    else{
      cH.hide();
      cV.hide();
      gMousepos.hide();
    }
    tool.handleMousemove(event);
  }

  /**
   * Handle a resize event of the window.
   *
   * @param event
   */
  function handleResize() {
    tool.cancelSelection();
    calculateImageScale();
    tool.drawExistingAnnotations(globals.currentAnnotations);
  }

  /**
   * Load the annotation view for another image.
   *
   * @param imageId
   * @param fromHistory
   */
  function loadAnnotateView(imageId, fromHistory) {
    globals.editedAnnotationsId = undefined;

    imageId = parseInt(imageId);

    var noAnnotations = $('#no_annotations');
    var existingAnnotations = $('#existing_annotations');
    var loading = $('#annotations_loading');
    existingAnnotations.hide();
    noAnnotations.hide();
    loading.show();
    $('#annotation_type_id').val(gAnnotationType);

    displayImage(imageId);

    tool.restoreSelection(false);

    let handleNewAnnotations = function() {
      // image is in cache.
      globals.allAnnotations = gAnnotationCache[imageId];
      globals.currentAnnotations = globals.allAnnotations.filter(function(e) {
        return e.annotation_type.id === gAnnotationType;
      });
      loading.hide();
      displayExistingAnnotations(globals.allAnnotations);
      tool.drawExistingAnnotations(globals.currentAnnotations);

      if (globals.restoreSelection !== undefined) {
        tool.restoreSelection();
      } else {
        tool.resetSelection();
      }
    };

    // load existing annotations for this image
    if (gAnnotationCache[imageId] === undefined) {
      // image is not available in cache. Load it.
      loadAnnotationsToCache(imageId);
      $(document).one("ajaxStop", handleNewAnnotations);
    } else if ($.isEmptyObject(gAnnotationCache[imageId])) {
      // we are already loading the annotation, wait for ajax
      $(document).one("ajaxStop", handleNewAnnotations);
    } else {
      handleNewAnnotations();
    }
  }

  /**
   * Load the annotations of an image to the cache if they are not in it already.
   *
   * @param imageId
   */
  function loadAnnotationsToCache(imageId) {
    imageId = parseInt(imageId);

    if (gAnnotationCache[imageId] !== undefined) {
      // already cached
      return;
    }
    // prevent multiple ajax requests for the same image
    gAnnotationCache[imageId] = {};

    var params = {
      image_id: imageId
    };
    $.ajax(API_ANNOTATIONS_BASE_URL + 'annotation/load/?' + $.param(params), {
      type: 'GET',
      headers: gHeaders,
      dataType: 'json',
      success: function(data) {
        // save the current annotations to the cache
        gAnnotationCache[imageId] = data.annotations;
        console.log("Saving annotations for", imageId);
      },
      error: function() {
        console.log("Unable to load annotations for image" + imageId);
      }
    });
  }


  /**
   * Handle the selection change of the annotation type.
   */

  function handleAnnotationTypeChange() {
    gAnnotationType = parseInt($('#annotation_type_id').val());
    globals.currentAnnotations = globals.allAnnotations.filter(function(e) {
      return e.annotation_type.id === gAnnotationType;
    });
    setTool();
  }

  function handleMouseDown(event) {
    if (!$('#draw_annotations').is(':checked'))
      return;

    var position = globals.image.offset();
    if (event.pageX > position.left && event.pageX < position.left + globals.image.width() &&
            event.pageY > position.top && event.pageY < position.top + globals.image.height())
    {
      if (parseInt($('#annotation_type_id').val()) === -1) {
        displayFeedback($('#feedback_annotation_type_missing'));
        return;
      }
      globals.mouseDownX = Math.round((event.pageX - position.left) * globals.imageScaleWidth);
      globals.mouseDownY = Math.round((event.pageY - position.top) * globals.imageScaleHeight);
      tool.handleMouseDown(event);
    }
  }

  function handleMouseUp(event) {
    if (!$('#draw_annotations').is(':checked'))
      return;

    var position = globals.image.offset();
    globals.mouseUpX = Math.round((event.pageX - position.left)/* * globals.imageScaleWidth*/);
    globals.mouseUpY = Math.round((event.pageY - position.top)/* * globals.imageScaleHeight*/);

    if (event.pageX > position.left && event.pageX < position.left + globals.image.width() &&
      event.pageY > position.top && event.pageY < position.top + globals.image.height()) {
      tool.handleMouseUp(event);
    }
  }

  // handle DEL key press
  function handleDelete(event) {
    if (globals.editedAnnotationsId === undefined)
      return;

    deleteAnnotation(event, globals.editedAnnotationsId);
  }

  $(function() {
    globals.editActiveContainer = $('#edit_active');
    globals.image = $('#image');
    globals.drawAnnotations = document.getElementById('draw_annotations').checked;
    gMousepos = $('#mousepos');
    gMousepos.hide();

    // get current environment
    gCsrfToken = $('[name="csrfmiddlewaretoken"]').first().val();
    gImageId = parseInt($('#image_id').html());
    gImageSetId = parseInt($('#image_set_id').html());
    gHeaders = {
      "Content-Type": 'application/json',
      "X-CSRFTOKEN": gCsrfToken
    };

    // W3C standards do not define the load event on images, we therefore need to use
    // it from window (this should wait for all external sources including images)
    $(window).on('load', function() {
      initTool();
    }());

    $('.annotation_value').on('input', function() {
      tool.reloadSelection();
    });
    $('select').on('change', function() {
      document.activeElement.blur();
    });
    $('#draw_annotations').on('change', handleShowAnnotationsToggle);
    $('select#annotation_type_id').on('change', handleAnnotationTypeChange);

    // register click events
    $(window).click(function(e) {
      handleMouseClick(e);
    });
    $('#cancel_edit_button').click(function() {
      tool.resetSelection(true);
    });
    $('#save_button').click(createAnnotation);
    $('#reset_button').click(function() {
      tool.resetSelection(true);
    });
    $('.js_feedback').mouseover(function() {
      $(this).hide();
    });

    // annotation buttons
    $('.annotation_edit_button').each(function(key, elem) {
      elem = $(elem);
      elem.click(function(event) {
        editAnnotation(event, this, parseInt(elem.data('annotationid')));
      });
    });
    $('.annotation_delete_button').each(function(key, elem) {
      elem = $(elem);
      elem.click(function(event) {
        deleteAnnotation(event, parseInt(elem.data('annotationid')));
      });
    });

    $(document).on('mousemove touchmove', handleSelection);
    $(window).on('resize', handleResize);
    window.onpopstate = function(event) {
      if (event.state !== undefined && event.state !== null && event.state.imageId !== undefined) {
        loadAnnotateView(event.state.imageId, true);
      }
    };

    // attach listeners for mouse events
    $(document).on('mousedown.annotation_edit', handleMouseDown);
    // we have to bind the mouse up event globally to also catch mouseup on small selections
    $(document).on('mouseup.annotation_edit', handleMouseUp);

    $(document).keydown(function(event) {
      switch (event.keyCode){
        case 27: // Escape
          tool.handleEscape();
          break;
      }
    });

    $(document).keyup(function(event) {
      switch (event.keyCode){
        case 86: //'v'
          $('#save_button').click();
          break;
        case 46: //'DEL'
          handleDelete(event);
          break;
      }
    });
  });
}

initialize_annotate_view()
