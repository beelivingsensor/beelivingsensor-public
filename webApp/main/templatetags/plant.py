from django import template

from main.models import NutritionalValue

register = template.Library()


@register.simple_tag
def translate_common_name(request, common_names):
    for common_name in common_names:
        if common_name.language == request.LANGUAGE_CODE:
            return common_name.name

    return ''


@register.simple_tag
def colorize_nutrition_value(nutrition_value: NutritionalValue):
    if not nutrition_value:
        return '#000000'
    elif nutrition_value == NutritionalValue.VERY_HIGH:
        return '#ff4800'
    elif nutrition_value == NutritionalValue.HIGH:
        return '#ff6224'
    elif nutrition_value == NutritionalValue.MEDIUM:
        return '#ff7f4d'
    elif nutrition_value == NutritionalValue.LOW:
        return '#ff9a73'
    elif nutrition_value == NutritionalValue.VERY_LOW:
        return '#ffcbb8'
