import calendar
import datetime

import humanize
from django import template
from django.utils.translation import gettext_lazy as _

register = template.Library()


@register.filter
def humanize_size(value):
    """Return human-readable file size."""
    if isinstance(value, int):
        return humanize.naturalsize(value)

    return None


@register.filter
def humanize_month(value):
    return _(calendar.month_name[value])


@register.filter()
def humanize_datetime(value):
    return datetime.datetime.fromisoformat(value)
