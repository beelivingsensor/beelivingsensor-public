from django import template

register = template.Library()


@register.filter
def bool_emoji(value):
    if value:
        return '✅'
    else:
        return '❌'
