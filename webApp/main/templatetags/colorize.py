from django import template

from main.models import ProcessStatus

register = template.Library()


@register.filter
def colorize(value):
    """Return the CSS class name for colorizing the text based on the value."""
    if value < 0.8:
        return 'text-danger'
    elif value < 0.9:
        return 'text-warning'
    else:
        return 'text-success'


@register.simple_tag
def colorize_count(count, min_count):
    if count < min_count:
        return 'text-danger'
    else:
        return 'text-success'


@register.filter
def colorize_process_status(process_status):
    """Bootstrap color coding for model-service process status of a media."""
    mapping = {
        ProcessStatus.PROCESS_PENDING: 'info',
        ProcessStatus.PROCESS_STARTED: 'primary',
        ProcessStatus.PROCESS_FAILURE: 'danger',
        ProcessStatus.PROCESS_RETRY: 'info',
        ProcessStatus.PROCESS_REVOKED: 'info',
        ProcessStatus.PROCESS_SUCCESS: 'success',
        ProcessStatus.UNAVAILABLE: 'warning',
        ProcessStatus.NOTIFICATION_FAILURE: 'danger',
        ProcessStatus.REQUEST_FAILURE: 'danger',
    }

    return mapping.get(process_status, 'secondary')
