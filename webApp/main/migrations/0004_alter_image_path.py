# Generated by Django 3.2a1 on 2021-02-06 18:27

from django.db import migrations, models
import main.models.image
import storage


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_alter_user_first_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='path',
            field=models.ImageField(max_length=256, storage=storage.ImageAzureStorage(), upload_to=main.models.image.image_upload_to),
        ),
    ]
