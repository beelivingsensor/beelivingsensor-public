from beewatch.settings import *

DEBUG = False

ALLOWED_HOSTS = ['.beelivingsensor.org',
                 ".beelivingsensor.eu",
                 "web-app", # Requests from inside the cluster
                 "web-app.default.svc.cluster.local", # Requests from inside the cluster
                 ]

MIDDLEWARE += ['whitenoise.middleware.WhiteNoiseMiddleware']

MODEL_SERVING_APP = 'http://model-serving:80'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = 'https://static.beelivingsensor.eu/static/'
MEDIA_URL = 'https://static.beelivingsensor.eu/media/'
