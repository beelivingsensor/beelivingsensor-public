from beewatch.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'beewatch',
        'USER': 'beewatch',
        'PASSWORD': 'beewatch',
        'PORT': 5432,
        'HOST': 'postgis-postgis'
    }
}
