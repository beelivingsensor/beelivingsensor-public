"""beewatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from django.conf.urls.i18n import i18n_patterns
from django_registration.backends.one_step.views import RegistrationView
from main.forms import UserRegistrationForm

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('annotations/', include('main.urls.annotation_urls')),
    path('api/', include('main.urls.api.urls'))
]

urlpatterns += i18n_patterns(
    path('', include('main.urls.urls')),
    path('user/', include('django.contrib.auth.urls')),
    path('accounts/register/',
         RegistrationView.as_view(form_class=UserRegistrationForm),
         name='django_registration_register'),
    path('accounts/',
         include('django_registration.backends.activation.urls')),
    path('admin/', admin.site.urls),

    path('beefoodchecker/', include(
        'main.urls.environment.beefoodchecker_urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
