import os

from storages.backends.azure_storage import AzureStorage


class BeeLivingSensorStorage(AzureStorage):
    account_name = 'beelivingsensor'
    account_key = os.getenv('STORAGE_KEY')
    expiration_secs = 60*5


class ImageAzureStorage(AzureStorage):
    account_name = 'beelivingsensor'
    account_key = os.getenv('STORAGE_KEY')
    azure_container = 'frames'
    expiration_secs = 60*5


class VideoAzureStorage(AzureStorage):
    account_name = 'beelivingsensor'
    account_key = os.getenv('STORAGE_KEY')
    azure_container = 'videos'
    expiration_secs = 60*5


class ModelAnnotationsStorage(BeeLivingSensorStorage):
    azure_container = 'model-annotations'
