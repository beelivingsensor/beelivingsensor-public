# Create database diagram:
* everything: python manage.py graph_models -a -o docs/database_schema.png
* annotations: 

python manage.py graph_models -a \
    -I ModelAnnotation,UserAnnotation,GoldAnnotation,ModelAnnotationSet,UserAnnotationSet,GoldAnnotationSet \
    -o docs/database_schema_annotation.png

# BeeFoodChecker schema
python manage.py graph_models -a -I Nutrition,Plant,Observation,CommonName -o docs/nutrition.png
