#!/bin/bash
set -xe # leave -e so that if something, like DB connection, fails, the container is rerun and we can connect to the DB successfully

cron
env >> /etc/environment

pushd nodejsSrc
npm install
webpack --mode=development -o ../static/js/upload.js
popd


python manage.py makemigrations
python manage.py migrate
python manage.py compilemessages > compileMessagesLog.txt
# python manage.py initGroups
# python manage.py initApiaries

daphne -v 3 -b 0.0.0.0 -p 443 beewatch.asgi:application # with verbosity set to Debug. There is no documentation on daphne, I had to check the source code.