# This is just for testing purposes
# A cron job for the weather data fetch will be created scheduled to run every minute.
# After a minute the job will be deleted again to prevent it from executing and consuming API calls.

current_dir=$(pwd)

interval="* * * * *"

secrets_env="${current_dir}/../../secrets.env"
secrets_export="export $(grep 'OPENWEATHERMAP_API_KEY' "${secrets_env}")"

python_path="${current_dir}/venv/bin/python"
python_script="${current_dir}/manage.py"
python_cmd="${python_path} ${python_script} fetchweatherdata"

log_redirect="> ${current_dir}/cron.log 2>&1"

cron_command="${interval} ${secrets_export} && ${python_cmd} ${log_redirect}"

(crontab -l ; echo "${cron_command}") | sort - | uniq - | crontab -

echo "Waiting one minute...Please do not interrupt, otherwise cron job will not be deleted at the end"
sleep 60

# delete again
crontab -l | grep -v "${python_cmd}"  | crontab -