# before running make sure nothing is accessing the database

export $(xargs <.env)

# install dependencies first
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
sudo apt update
# install general dependencies
sudo apt install -y software-properties-common libssl-dev libffi-dev
# install python 3.8
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt update
sudo apt dist-upgrade -y
sudo apt install -y python3.8 python3.8-venv python3.8-dev python3-pip python3-psycopg2
# install postgis stuff
sudo apt install -y postgresql-12 postgresql-12-postgis-3 postgresql-client-12 postgresql-server-dev-12 postgis libgdal-dev libpq-dev binutils libproj-dev gdal-bin
sudo apt install -y graphviz graphviz-dev gettext

# re-create beewatch database
sudo -u postgres dropdb --if-exists beewatch
sudo -u postgres createdb beewatch
sudo -u postgres psql -U postgres -d beewatch -c "create extension postgis;"

# re-create beewatch user
sudo -u postgres dropuser --if-exists beewatch
sudo -u postgres createuser -s beewatch
sudo -u postgres psql -U postgres -c "alter user ${POSTGIS_PASSWORD} with password '${POSTGRES_DB}';"

if [ ! -d venv/ ]; then
    python3.8 -m venv venv
fi

. venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py compilemessages > compilemessages.log
python manage.py initGroups
python manage.py initApiaries

echo '(NB: If database is not re-created, check no processes are accessing the database)'
