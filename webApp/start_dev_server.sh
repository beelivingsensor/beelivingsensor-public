sh setup_postgis_dev.sh

export $(xargs <.env)

sudo apt-get install -y nodejs-dev node-gyp libssl1.0-dev
sudo apt-get install -y npm webpack

cd nodejsSrc
npm install
webpack --mode=development -o ../static/js/upload.js
cd ..

. venv/bin/activate
# for testing weather data fetch
#sh setup_cron_dev.sh
python manage.py runserver