const {
    BlockBlobClient,
    AnonymousCredential
} = require("@azure/storage-blob");


const crypto = require('crypto');
const cookie = require('js-cookie');
var https

if (window.location.protocol == "http") {
    https = require('http');
} else {
    https = require('https');
}

const querystring = require('querystring');
const sha256 = crypto.createHash('sha256');

const UPLOAD_TYPES = {
    SINGLE: "single",
    PARALLEL: "parallel",
    PROGRESS: "progress"
}

var numberOfFiles = 0;
var numberOfUploadedFiles = 0;
var sizeAllFiles = 0;
var sizeAllUploadedData = 0;
var uploader = {};
const parallelUploadsNum = 4;

/**
 * The process of updating progress is quite imprefect.
 * The initial uploads of a file are exact, but at the end, it is hard to compute
 * the amount that was uploaded at the end. I.e. the upload sizes are 32MB, 32MB, ... 400kB
 * or something like that. So at the end we report that we have progressing
 * more than we actually are. To avoid then that we report we are done, yet we are not,
 * we set the bar to 99%, and only when really all files have been uploaded, eg. when you 
 * call @finishedUpload and all files are confirmed, we set the progress bar to 100%.
 * @param {number} uploadedSize How much did the upload progress increase
 */
function updateProgress(uploadedSize) {
    sizeAllUploadedData += uploadedSize;
    let bar = document.getElementById("progress-bar");
    if (sizeAllUploadedData > (sizeAllFiles * 0.98)){ // Oof, there is a case where the progress bar shows 100%, and then jumps to 99%, so I am setting it so that the bar shows 99% before it can get a chance to hit 100%. This code is getting worse everyday. I wish I knew JS.
        let sizeAllMB = (sizeAllFiles/(1000*1000)).toFixed(0) // 1000*1000 is to represent MBs
        let sizeUploadedMB = ((sizeAllFiles * 0.99)/(1000*1000)).toFixed(0) // Set to 99% so that we don't get over the 100% limit
        
        bar.innerHTML = `${sizeUploadedMB}MB / ${sizeAllMB}MB`
        bar.setAttribute("style", "width:99%") // left at 99% because the progress update process is imperfect.
    } else {
        const percent = ((sizeAllUploadedData / sizeAllFiles) * 100).toFixed(0)
        let sizeAllMB = (sizeAllFiles/(1000*1000)).toFixed(0)
        let sizeUploadedMB = (sizeAllUploadedData/(1000*1000)).toFixed(0)

        bar.innerHTML = `${sizeUploadedMB}MB / ${sizeAllMB}MB`
        bar.setAttribute("style", `width:${percent}%`)
    }
}


function finishedUpload(fileName) {
    numberOfUploadedFiles += 1
    document.getElementById('finishedUploadsCount').innerHTML = `${numberOfUploadedFiles}/${numberOfFiles} files were successfully uploaded.`;
    
    let row = document.getElementById(fileName);
    row.innerHTML = `✅ ${row.innerHTML} was successfully uploaded...`;

    if (numberOfUploadedFiles == numberOfFiles) { // When we actually upload all of the files. See function updateProgress for more info.
        let bar = document.getElementById("progress-bar");
        bar.innerHTML = "Finished!"
        bar.setAttribute("aria-valuenow", 100) // For screen readers
        bar.setAttribute("style", "width:100%") // Actual change of the bar
    }
    uploader.uploadNextFile()
}

/**
 * Return byte count of a string. Found on Stack Overflow, don't ask me.
 * @param {string} s 
 */
function byteCount(s) {
    return encodeURI(s).split(/%..|./).length - 1;
}

const dropArea = document.getElementById('drop-area');

dropArea.addEventListener('dragover', (event) => {
  event.stopPropagation();
  event.preventDefault();
  // Style the drag-and-drop as a "copy file" operation.
  event.dataTransfer.dropEffect = 'copy';
  dropArea.style.border = "thick solid red"
});

dropArea.addEventListener('drop', (event) => {
  event.stopPropagation();
  event.preventDefault();
  dropArea.style.border = "thick solid white"
  document.getElementById('id_path').files = event.dataTransfer.files
  uploadFiles(UPLOAD_TYPES.PROGRESS)
});



/**
 * Notifies the web app that we have successfully uploaded a video
 * @param {string} fileName - extended name of the video that was uploaded to the Azure Blob Storage
 */
async function sendConfirmation(fileName) {
    console.log("Finished upload of " + fileName)

    let confirmData = JSON.stringify({"fileName": fileName})

    await fetch(
        window.location.pathname.replace("addVideo","confirmUpload"), // /en/beehives/4/addVideo/ ==> /en/beehives/4/confirmUpload/
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(confirmData),
                "X-CSRFToken": cookie.get("csrftoken")
              },
              body: confirmData
        }
    )
}

/**
 * Requests data neccessary for uploading the file to Azure Blob Storage
 * @param {string} fileName - Name of a file that is being uploaded
 */
async function getUploadData(fileName) {
    const postData = JSON.stringify({
        'file-name': fileName
      })
    let res = await fetch(
        window.location.pathname.replace("addVideo","generateSAS"), // /en/beehives/4/addVideo/ ==> /en/beehives/4/generateSAS/
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(postData),
                "X-CSRFToken": cookie.get("csrftoken")
            },            
            body: postData        
        }
    )
    // body.fileName is different than file.name because body contains the extended filename
    let body = await res.json()
    return body
}

async function uploadFile(file, uploadType) {
    let fileName = file.name.replace(/ /g,"_")

    // Print out files that are going to be uploaded.
    let htmlFilesTable = document.getElementById("uploadFiles");
    let row = document.createElement("tr");
    row.innerHTML = file.name;
    row.id = fileName
    htmlFilesTable.appendChild(row);

    let body = await getUploadData(fileName)

    let blobClient = new BlockBlobClient(
        `${body.host}${body.path}${body.sasToken}`,
        new AnonymousCredential()
    )

    console.log("Uploading " + body.fileName)
    let bin;
    // Upload the file   
    switch(uploadType) {
        case UPLOAD_TYPES.SINGLE:
            console.log("file size = " + file.size)
            await blobClient.upload(file, file.size)
            break;
        case UPLOAD_TYPES.PARALLEL:
            bin = await file.arrayBuffer()
            console.log("file size = " + file.size)
            console.log("array buff= " + bin.byteLength)
            await blobClient.uploadData(
                bin,
                {
                    blockSize: 32 * 1024 * 1024, // 32MB in bytes 
                    onProgress: (e) => {
                        updateProgress(e.loadedBytes)
                    } 
                }
            )  
            break;
        case UPLOAD_TYPES.PROGRESS:
            let sliceStart = 0
            let sliceSize = 1024 * 1024; // 1 MB
            if (file.size > 1024 * 1024 * 128) { // Larger than 128 MB?
                sliceSize = 1024 * 1024 * 64 // 64 MB
            }
            
            var nextSlice = sliceStart + sliceSize + 1;
            
                    

            let stagedBlocks = []
            try {
                let res = await blobClient.getBlockList("all") // "All" is block list type defined in the Azure SDK
    
    
                // Check whether there are some already uploaded, and exclude those from uploading.
                res.uncommittedBlocks.forEach((element) => {
                    stagedBlocks.push(element["name"])
                });                
            } catch (error) {
                // Could be that the blob does not exist, but in that case it means that there are no staged blocks
            }
            

            while (sliceStart < file.size) {
                updateProgress(sliceSize)
                const result = await file.slice(sliceStart, nextSlice).arrayBuffer()
                sliceStart = nextSlice;
                nextSlice = sliceStart + sliceSize + 1;  

                sha256.update(result); // result is the read data i believe
                let hash = sha256.digest()
                hash = hash.toString("base64") // No idea why, but using .digest("base64") created different hashes with different lenght and breaks the azure uploading, because Azure needs the ID lenghts of a blob to be same. I cannot describe my hate towards Microsoft, this fact is not documented in the SDK.
            
                console.log(byteCount(result));
                console.log(result.byteLength);
                console.log(`${hash.length} bytes in the hash.`)
                console.log(hash);
                
                if (stagedBlocks.includes(hash)) {
                    console.log("Skipping because block is already uploaded!")
                    continue;
                }
                
                await blobClient.stageBlock(hash, result, result.byteLength) // Can be sped up, but I lack the knowledge to do it easily ATM.
                stagedBlocks.push(hash)              
            }

            let blocks = await blobClient.getBlockList("all")
            console.log(blocks)
            await blobClient.commitBlockList(stagedBlocks)

    }     
    sendConfirmation(body.fileName)
    finishedUpload(fileName)
}





// I believe this has to be dont in JS because otherwise dont see the functions so when HTML is loaded it crashes.
// Ofc it could be also nice, but this is supposed to be "temporary" / "dev option", so this should be deleted ASAP and not in production
window.addEventListener("DOMContentLoaded", () => {

    // { Commented out for now for stability reasons, dont forget to change the HTML as well
        // var button = document.getElementById("upload-btn-single");
        // button.addEventListener("click", () => uploadFiles(UPLOAD_TYPES.SINGLE));
        // button.disabled = false;
        // button.innerText = 'Upload Single attempts'; 
    
        // var button = document.getElementById("upload-btn-parallel");
        // button.addEventListener("click", () => uploadFiles(UPLOAD_TYPES.PARALLEL));
        // button.disabled = false;
        // button.innerText = 'Upload Parallel';
    // }

    var button = document.getElementById("upload-btn-progress");
    button.addEventListener("click", () => uploadFiles(UPLOAD_TYPES.PROGRESS));
    button.disabled = false;
    button.innerText = 'Upload';    
});



function uploadFiles(uploadType) {
    var files = Array.from(document.getElementById('id_path').files);
    numberOfFiles = files.length
    document.getElementById('finishedUploadsCount').innerHTML = `0/${numberOfFiles} files were successfully uploaded.`;

    files.forEach(f => {
        sizeAllFiles += f.size
    });

    uploader = new Uploader(files, uploadType);
    for (let index = 0; index < parallelUploadsNum; index++) {
        uploader.uploadNextFile()        
    }
    
}



class Uploader {

    constructor(files, uploadType) {
      this.files = files;
      this.currentFileIndex = 0;
      this.uploadType = uploadType;
    }
  
    uploadNextFile() { // oof, I am quite certain there could be some concurrency issues, but oh well, let's see how it works in the first place
        if (this.currentFileIndex < this.files.length) {
            uploadFile(this.files[this.currentFileIndex], this.uploadType)
            this.currentFileIndex += 1
        }
    }
  
  }